// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

class QfdPage {
  visit () {
    cy.visit('/qfd')
  }

  clickBtn (btn) {
    const button = cy.get(`[data-cy=${btn}]`)
    button.click()
    return this
  }

  pushNext () {
    const button = cy.get('[data-cy=btnNext]')
    button.click()
  }

  fill_qfd_name (value) {
    const field = cy.get('[data-cy="qfd_name"]')
    field.clear()
    field.type(value)
    return this
  }

  fill_qfd_objective (value) {
    const field = cy.get('[data-cy="qfd_objective"]')
    field.clear()
    field.type(value)
    return this
  }

  fill_requirement_description (value) {
    const field = cy.get('[data-cy="requirement_description"]')
    field.clear()
    field.type(value)
    return this
  }

  fill_requirement_importance (value) {
    const button = cy.get('.el-input-number__increase').last()
    button.click()
    return this
  }

  fill_functional_requirements_input (target, value) {
    const field = cy.get(`[data-cy=${target}]`)
    field.clear()
    field.type(value)
    return this
  }

  change_functional_requirements_select (target, eq) {
    cy.get(`[data-cy=${target}]`).click()
    cy.get('.el-select-dropdown').find('.el-select-dropdown__item').eq(eq).click()
    return this
  }

  uploadQfdJson (fileName) {
    cy.fixture(fileName, 'base64').then(fileJson => {
      const fileContent = JSON.stringify(fileJson)
      cy.get('[data-cy=uploadQfd]').find('input[type=file]').upload({ fileContent, fileName, mimeType: 'application/json' })
    })
  }

  checkHrefPropLink () {
    cy.get('[data-cy="btnDownloadQfd"]').eq(0)
      .should('have.attr', 'href').and('match', /\/qfds\/\d\?download/)
  }

  checkResponseJson () {
    cy.get('[data-cy="btnDownloadQfd"]').eq(0).should('have.attr', 'href')
      .then((href) => {
        cy.request({
          method: 'GET',
          url: href,
        }).then((response) => {
          expect(response.body.name).to.not.equal('')
          expect(response.body.objective).to.not.equal('')
        })
      })
  }
}
export default QfdPage
