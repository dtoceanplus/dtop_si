// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

class FmeaPage {
  visit () {
    cy.visit('/fmea/fmeas')
  }

  pushNext () {
    cy.wait(500)
    const button = cy.get('[data-cy=btnNext]')
    button.click()
  }

  fill_fmea_name (value) {
    const field = cy.get('[data-cy="fmea_name"]')
    field.clear()
    field.type(value)
    return this
  }

  fill_action_level (value) {
    const field = cy.get('[data-cy="action_level"]').find('.el-input__inner')
    field.clear()
    field.type(value)
    return this
  }

  fill_occurrence_limit (value) {
    const field = cy.get('[data-cy="occurrence_limit"]').find('.el-input__inner')
    field.clear()
    field.type(value)
    return this
  }

  fill_requirement_description (value) {
    const field = cy.get('[data-cy="requirement_description"]')
    field.clear()
    field.type(value)
    return this
  }

  fill_failure_mode_description (value) {
    const field = cy.get('[data-cy="failure_mode_description"]')
    field.clear()
    field.type(value)
    return this
  }

  fill_effect_description (value) {
    const field = cy.get('[data-cy="effect_description"]')
    field.clear()
    field.type(value)
    return this
  }

  fill_cause_description (value) {
    const field = cy.get('[data-cy="cause_description"]')
    field.clear()
    field.type(value)
    return this
  }

  fill_control_description (value) {
    const field = cy.get('[data-cy="control_description"]')
    field.clear()
    field.type(value)
    return this
  }

  select_severity (eq) {
    cy.get('[data-cy="severity"]').click()
    cy.get('.el-select-dropdown').find('.el-select-dropdown__item').eq(eq).click()
    return this
  }

  select_occurrence (eq) {
    cy.get('[data-cy="occurrence"]').click()
    cy.get('.el-select-dropdown').find('.el-select-dropdown__item').eq(eq).click()
    return this
  }

  select_detection (eq) {
    cy.get('[data-cy="detection"]').click()
    cy.get('.el-select-dropdown').find('.el-select-dropdown__item').eq(eq).click()
    return this
  }

  clickBtn (btn) {
    const button = cy.get(`[data-cy=${btn}]`)
    button.click()
    return this
  }

  uploadFmeaJson (fileName) {
    cy.fixture(fileName, 'base64').then(fileJson => {
      const fileContent = JSON.stringify(fileJson)
      cy.get('[data-cy=uploadFmea]').find('input[type=file]').upload({ fileContent, fileName, mimeType: 'application/json' })
    })
  }

  checkHrefPropLink () {
    cy.get('[data-cy="btnDownloadFmea"]').eq(0)
      .should('have.attr', 'href').and('match', /\/fmeas\/\d\?download/)
  }

  checkHrefPropLinkCSV () {
    cy.get('[data-cy="btnDownloadFmeaCSV"]').eq(0)
      .should('have.attr', 'href').and('match', /\/fmeas\/\d\?download_csv/)
  }

  checkResponseJson () {
    cy.get('[data-cy="btnDownloadFmea"]').eq(0).should('have.attr', 'href')
      .then((href) => {
        cy.request({
          method: 'GET',
          url: href,
        }).then((response) => {
          expect(response.body.name).to.not.equal('')
          expect(response.body.objective).to.not.equal('')
        })
      })
  }

  checkResponseCSV () {
    cy.get('[data-cy="btnDownloadFmeaCSV"]').eq(0).should('have.attr', 'href')
      .then((href) => {
        cy.request({
          method: 'GET',
          url: href,
        }).then((response) => {
          expect(response.body.name).to.not.equal('')
          expect(response.body.objective).to.not.equal('')
        })
      })
  }
}
export default FmeaPage
