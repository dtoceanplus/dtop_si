# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

Feature: Fmea

I want to create new Fmea

Scenario: Opening a fmea page
  Given I open Fmea page
  Then I see "fmeas" in the url

Scenario: Fill Fmea Data
  Given I click a button to start create new Fmea
  Then I see "create" in the url
  When I fill Fmea Name "Fmea"
  And I fill Fmea Action Level 75
  And I fill Fmea Occurrence Limit 6
  And I click a button to add new Fmea
  Then I see "requirements" in the url

Scenario: Fill Fmea Requirements
  When I fill Fmea Requirement Description "description of requirement"
  And I click a button to add new Requirement
  And I click a button go to Next step
  Then I see "failure_modes" in the url

Scenario: Fill Fmea Failure Modes
  When I fill Fmea FailureMode Description "failure"
  And I click a button to add new FailureMode
  And I click a button go to Next step
  Then I see "effects" in the url

Scenario: Fill Fmea Effects
  When I fill Fmea Effect Description "effect of failure"
  And I select Severity first point
  And I click a button to add new Effect
  And I click a button go to Next step
  Then I see "causes" in the url

Scenario: Fill Fmea Causes
  When I fill Fmea Cause Description "cause of failure"
  And I select Occurrence first point
  And I click a button to add new Cause
  And I click a button go to Next step
  Then I see "controls" in the url

Scenario: Fill Fmea Controls
  When I fill Fmea Control Description "control of cause"
  And I select Detection first point
  And I click a button to add new Control
  And I click a button go to Next step

Scenario: Donwload CSV file
  When I click a button to download CSV file
  Then corresponding CSV file starts been downloaded on a local disk