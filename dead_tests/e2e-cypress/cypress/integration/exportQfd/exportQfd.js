// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import QfdPage from '../../pages/qfd.page'
import { Given, When, And } from 'cypress-cucumber-preprocessor/steps'
import 'cypress-file-upload'

const qfd = new QfdPage()

Given('that a QFD analysis exists', () => {
  qfd.visit()
  qfd.uploadQfdJson('qfd.json')
})

When('the user initiates “download” procedure for the given QFD title', () => {
  qfd.checkHrefPropLink()
})

Then('corresponding JSON file starts been downloaded on a local disk', () => {
  qfd.checkResponseJson()
})
