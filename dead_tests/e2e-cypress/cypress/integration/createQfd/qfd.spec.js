// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import QfdPage from '../../pages/qfd.page'

describe('Create Fmea', () => {
  const qfd = new QfdPage()
  it('Qfd Begin', () => {
    qfd.visit()
    qfd.clickBtn('btnNewQfd')
      .fill_qfd_name('qfd name')
      .fill_qfd_objective('qfd objective')
      .clickBtn('btnQfdSubmit')
    cy.url().should('include', '/requirements')
  })

  it('Qfd Requirements', () => {
    qfd.fill_requirement_description('Requirement description')
	    			.fill_requirement_importance('1')
      .clickBtn('btnCreateRequirementQfd')
      .pushNext()
    cy.url().should('include', '/functional_requirements')
  })

  it.skip('Qfd Functional Requirements', () => {
    qfd.fill_functional_requirements_input('qfd_description', 'Requirement description')
      .fill_functional_requirements_input('qfd_target', 'Target')
      .fill_functional_requirements_input('qfd_units', 'Units')
      .change_functional_requirements_select('qfd_direction', 0)
      .change_functional_requirements_select('engineering_difficulty', 0)
      .change_functional_requirements_select('delivery_difficulty', 0)
      .pushNext()
    cy.url().should('include', '/Triz-Class')
  })
})
