// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import QfdPage from '../../pages/qfd.page'
import { Given, When, And } from 'cypress-cucumber-preprocessor/steps'

const qfd = new QfdPage()

Given('I open Qfd page', () => {
  qfd.visit()
})

Given('I click a button to start create new Qfd', () => {
  qfd.clickBtn('btnNewQfd')
})

When('I fill Qfd Name {string}', name => {
  qfd.fill_qfd_name(name)
})

And('I fill Qfd Objective {string}', name => {
  qfd.fill_qfd_objective(name)
})

And('I click a button to add new Qfd', () => {
  qfd.clickBtn('btnQfdSubmit')
})

When('I fill Qfd Requirement Description {string}', name => {
  qfd.fill_requirement_description(name)
})

When('I fill Qfd Requirement Importance {int}', value => {
  qfd.fill_requirement_importance(value)
})

And('I click a button to add new Requirements', () => {
  qfd.clickBtn('btnCreateRequirementQfd')
})
