# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

Feature: export FMEA to the local drive

As a user of the design tools
I want to preserve an existing FMEA analysis to the local drive
So that I can reuse corresponding data in the context of a new study later on

Background:
    Given that a FMEA analysis exists
    And the user is visiting FMEA start page

Scenario: Export FMEA to JSON
    When the user initiates “download” procedure for the given FMEA title
    Then corresponding JSON file starts been downloaded on a local disk
