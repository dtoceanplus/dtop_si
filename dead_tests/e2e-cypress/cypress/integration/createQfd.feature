# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

Feature: Qfd

I want to create new Qfd

Scenario: Opening a qfd page
    Given I open Qfd page
    Then I see "qfd" in the url

Scenario: Fill Qfd Data
    Given I click a button to start create new Qfd
    Then I see "create" in the url
    When I fill Qfd Name "Qfd"
    And I fill Qfd Objective "Objective"
    And I click a button to add new Qfd
    Then I see "requirements" in the url

Scenario: Fill Qfd Requirements
    When I fill Qfd Requirement Description "Requirement description"
    And I fill Qfd Requirement Importance 1
    And I click a button to add new Requirements
    And I click a button go to Next step
    Then I see "functional_requirements" in the url
