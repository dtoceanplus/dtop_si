# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

Feature: import new FMEA from local drive

As a user of the design tools
I want to import a FMEA analysis in the form of a local drive file
So that I can reuse corresponding data in the context of a new study

Background:
Given The user is visiting FMEA start page

Scenario: Upload a unique FMEA JSON file
    When the user pushes the “Import FMEA” button and selects JSON "fmea.json" file from the Open file dialog
    Then the application displays an “name” field and “Submit” button
    And the application enables “Submit” button
    When the user presses the button “Submit”
    Then FMEA start page displays the new item in the available analysis list with "This is a test" name

Scenario: Upload a non unique FMEA JSON file
    When the user pushes the “Import FMEA” button and selects JSON "fmea.json" file from the Open file dialog
    Then the application displays an “name” field and “Submit” button
    And the application disables “Submit” button
    And the user sees "Please input a unique FMEA name" error message
    When the user types an unique name "e2e test unique"
    Then the application enables “Submit” button
    When the user presses the button “Submit”
    Then FMEA start page displays the new item in the available analysis list with "e2e test unique" name
