// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import FmeaPage from '../../pages/fmea.page'
import { Given, When, And } from 'cypress-cucumber-preprocessor/steps'
import 'cypress-file-upload'

const fmea = new FmeaPage()

When('the user pushes the “Import FMEA” button and selects JSON {string} file from the Open file dialog', file => {
  fmea.uploadFmeaJson(file)
})

Then('the application displays an “name” field and “Submit” button', () => {
  cy.get('[data-cy="inputNameFmea"]').should('be.visible')
  cy.get('[data-cy="btnUploadFmea"]').should('be.visible')
})

Then('the user presses the button “Submit”', () => {
  cy.get('[data-cy="btnUploadFmea"]').click()
  cy.wait(500)
})

// Upload an unique FMEA JSON file

And('the application enables “Submit” button', () => {
  cy.get('[data-cy="inputNameFmea"]').should('not.be.disabled')
})

Then('FMEA start page displays the new item in the available analysis list with {string} name', name => {
  cy.contains(name).should('be.visible')
})

// Upload a non unique FMEA JSON file

And('the application disables “Submit” button', () => {
  cy.get('[data-cy="btnUploadFmea"]').should('be.disabled')
})

And('the user sees {string} error message', errorMess => {
  cy.contains(errorMess).should('be.visible')
})

When('the user types an unique name {string}', name => {
  cy.get('[data-cy="inputNameFmea"]').clear().type(name)
})

Then('the application enables “Submit” button', () => {
  cy.get('[data-cy="inputNameFmea"]').should('not.be.disabled')
})
