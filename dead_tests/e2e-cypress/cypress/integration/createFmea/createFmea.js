// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import FmeaPage from '../../pages/fmea.page'
import { Given, When, And } from 'cypress-cucumber-preprocessor/steps'

const fmea = new FmeaPage()

Given('I open Fmea page', () => {
  fmea.visit()
})

When('I click a button to start create new Fmea', () => {
  fmea.clickBtn('btnNewFmea')
})

And('I fill Fmea Name {string}', name => {
  fmea.fill_fmea_name(name)
})

And('I fill Fmea Action Level {int}', number => {
  fmea.fill_action_level(number)
})

And('I fill Fmea Occurrence Limit {int}', number => {
  fmea.fill_occurrence_limit(number)
})

When('I click a button to add new Fmea', () => {
  fmea.clickBtn('btnFmeaSubmit')
})

When('I fill Fmea Requirement Description {string}', name => {
  fmea.fill_requirement_description(name)
})

And('I click a button to add new Requirement', () => {
  fmea.clickBtn('btnCreateRequirementQfd')
})

When('I fill Fmea FailureMode Description {string}', name => {
  fmea.fill_failure_mode_description(name)
})

And('I click a button to add new FailureMode', () => {
  fmea.clickBtn('btnAddFailureMode')
})

When('I fill Fmea Effect Description {string}', name => {
  fmea.fill_effect_description(name)
})

And('I select Severity first point', () => {
  fmea.select_severity(0)
})

And('I click a button to add new Effect', () => {
  fmea.clickBtn('btnCreateEffect')
})

When('I fill Fmea Cause Description {string}', name => {
  fmea.fill_cause_description(name)
})

And('I select Occurrence first point', () => {
  fmea.select_occurrence(0)
})

And('I click a button to add new Cause', () => {
  fmea.clickBtn('btnCreateCause')
})

When('I fill Fmea Control Description {string}', name => {
  fmea.fill_control_description(name)
})

And('I select Detection first point', () => {
  fmea.select_detection(0)
})

And('I click a button to add new Control', () => {
  fmea.clickBtn('btnCreateControl')
})

When('I click a button to download CSV file', () => {
  fmea.checkHrefPropLinkCSV()
})

Then('corresponding CSV file starts been downloaded on a local disk', () => {
  fmea.checkResponseCSV()
})
