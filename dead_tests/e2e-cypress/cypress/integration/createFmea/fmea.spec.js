// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import FmeaPage from '../../pages/fmea.page'

describe('Create Fmea', () => {
  const fmea = new FmeaPage()
  it('Fmea Begin', () => {
    fmea.visit()
    fmea.clickBtn('btnNewFmea')
    fmea.fill_fmea_name('Fmea')
    fmea.fill_action_level(75)
    fmea.fill_occurrence_limit(6)
    fmea.clickBtn('btnFmeaSubmit')
    cy.url().should('include', '/requirements')
  })

  it('Fmea Requirement', () => {
    fmea.fill_requirement_description('description of requirement')
    fmea.clickBtn('btnCreateRequirementQfd')
    fmea.pushNext()
    cy.url().should('include', '/failure_modes')
  })

  it('Fmea FailureMode', () => {
    fmea.fill_failure_mode_description('failure')
    fmea.clickBtn('btnAddFailureMode')
    cy.wait(500)
    fmea.pushNext()
    cy.url().should('include', '/effects')
  })

  it('Fmea Effect', () => {
    fmea.fill_effect_description('effect of failure')
    fmea.select_severity(0)
    fmea.clickBtn('btnCreateEffect')
    fmea.pushNext()
    cy.url().should('include', '/causes')
  })

  it('Fmea Cause', () => {
    fmea.fill_cause_description('cause of failure')
    fmea.select_occurrence(0)
    fmea.clickBtn('btnCreateCause')
    fmea.pushNext()
    cy.url().should('include', '/controls')
  })

  it('Fmea Control', () => {
    fmea.fill_control_description('control of failure')
    fmea.select_detection(0)
    fmea.clickBtn('btnCreateControl')
    fmea.pushNext()
    cy.contains('Fmea')
    cy.get('tr').eq(0).should('contain', 'Action Level')
    cy.get('tr').eq(0).should('contain', '75')
    cy.get('tr').eq(1).should('contain', 'Occurrence limit')
    cy.get('tr').eq(1).should('contain', '6')
  })
})
