// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import QfdPage from '../../pages/qfd.page'
import { Given, When, And } from 'cypress-cucumber-preprocessor/steps'
import 'cypress-file-upload'

const qfd = new QfdPage()

When('the user pushes the “Import QFD” button and selects JSON {string} file from the Open file dialog', file => {
  qfd.uploadQfdJson(file)
})

Then('the application displays an “name” field and “Submit” button', () => {
  cy.get('[data-cy="inputNameQfd"]').should('be.visible')
  cy.get('[data-cy="btnUploadQfd"]').should('be.visible')
})

Then('the user presses the button “Submit”', () => {
  cy.get('[data-cy="btnUploadQfd"]').click()
  cy.wait(500)
})

// Upload an unique QFD JSON file

And('the application enables “Submit” button', () => {
  cy.get('[data-cy="inputNameQfd"]').should('not.be.disabled')
})

Then('QFD start page displays the new item in the available analysis list with {string} name', name => {
  cy.contains(name).should('be.visible')
})

When('the user starts exploring the loaded QFD analysis with {string} name', name => {
  cy.contains(name).click()
  cy.wait(500)
  cy.url().should('include', 'Report')
})

// Upload a non unique QFD JSON file

And('the application disables “Submit” button', () => {
  cy.get('[data-cy="btnUploadQfd"]').should('be.disabled')
})

And('the user sees {string} error message', errorMess => {
  cy.contains(errorMess).should('be.visible')
})

When('the user types an unique name {string}', name => {
  cy.get('[data-cy="inputNameQfd"]').clear().type(name)
})

Then('the application enables “Submit” button', () => {
  cy.get('[data-cy="inputNameQfd"]').should('not.be.disabled')
})
