# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import json
from os import path
from copy import deepcopy

import pytest
from deepdiff import DeepDiff
from pydantic import BaseModel, ValidationError

from dtop_structinn.app import pyqfd
from dtop_structinn.pyqfd import (
    _expr,
    _load_inventive_principles,
    _tsv_reader,
    InvalidInput,
    schemas,
)
from dtop_structinn.pyqfd.declarations import (
    Correlation,
    Difficulty,
    Impact,
    Improvement,
)


class BaseModel(BaseModel):
    class Config:
        orm_mode = True


def _try_float(s):
    try:
        return float(s)
    except ValueError:
        return s


def test_qfd(app):
    triz_classes = pyqfd.triz_class.get()
    with pyqfd.analysis.create_contextual(
        {
            "name": "Heat exchanger",
            "objective": "Exchanges heat from one fluid to another",
        }
    ) as analysis:

        qfd_id = analysis["id"]

        tsv = _tsv_reader(
            path.abspath(
                path.join(path.dirname(__file__), "functional_requirements.tsv")
            )
        )
        header = next(tsv)
        functional = (
            {k: _try_float(v) if v else 0 for k, v in zip(header, row)} for row in tsv
        )  # TODO: handle NULLs
        triz_class_map = {
            triz["definition"]: triz["id"] for triz in pyqfd.triz_class.get()
        }

        fr_triz = (
            ["Quantity of substance", "Device complexity"],
            ["Adaptability or versatility"],
            ["Object-affected harmful factors", "Speed"],
            ["Area of stationary object", "Speed"],
            ["Speed", "Loss of Energy"],
            ["Adaptability or versatility", "Difficulty of detecting and measuring"],
            ["Strength", "Stability of the object's composition"],
            ["Reliability", "Reliability"],
            ["Use of energy by moving object"],
        )
        # Can create multiple functional requirements with one call, but need to iterate
        # over different triz definitions, so must be in loop
        fr = []
        for func_req, triz_definitions in zip(functional, fr_triz):
            # Find ids of triz definitions and pass as list
            # May be more consistent to have relationship for qfd_id, but requires manual
            # definition anyway so not a priority
            relationships = {
                "qfd_id": qfd_id,
                "triz": [triz_class_map[triz] for triz in triz_definitions],
                #            "triz": [],
                #            "triz": [triz_class_map[triz_definitions[0]]],
            }
            fr.append(pyqfd.functional_requirement.create(func_req, relationships))

        for func_req, triz_definitions in zip(fr, fr_triz):
            relationships = {
                "triz": [triz_class_map[triz] for triz in triz_definitions]
            }
            pyqfd.functional_requirement.update({"id": func_req["id"]}, relationships)

        functional_requirement_map = {
            func_req["description"]: func_req["id"] for func_req in fr
        }

        header, *tsv = _tsv_reader(
            path.abspath(path.join(path.dirname(__file__), "requirements.tsv"))
        )

        for imp, row in enumerate(reversed(tsv), 1):
            requirement = dict(zip(header[:1], row[:1]))
            requirement["importance"] = imp
            with pyqfd.requirement.create_contextual(
                requirement, {"qfd_id": qfd_id}
            ) as req:
                for k, v in zip(header[1:], row[1:]):
                    pyqfd.impact.create(
                        {
                            "impact": int(v) if v else 0,  # TODO: Handle NULLs
                            "functional_requirement_id": functional_requirement_map[k],
                        },
                        {"requirement_id": req["id"]},
                    )

        tsv = _tsv_reader(
            path.abspath(path.join(path.dirname(__file__), "achievements.tsv"))
        )
        header = next(tsv)

        for row in tsv:
            requirement = dict(zip(header[:1], row[:1]))
            achievements = [
                {
                    "functional_requirement_id": functional_requirement_map[k],
                    "value": float(v),
                }
                for k, v in zip(header[1:], row[1:])
            ]
            with pyqfd.solution.create_contextual(
                requirement, {"qfd_id": qfd_id}
            ) as sol:
                pyqfd.achievement.create(achievements, {"solution_id": sol["id"]})

        tsv = _tsv_reader(
            path.abspath(path.join(path.dirname(__file__), "correlations.tsv"))
        )
        header = next(tsv)

        # TODO: create function based on code below for actions on correlation - need to
        # validate any creates, updates etc.
        for row in tsv:
            for func_req, corr in zip(header[1:], row[1:]):
                if corr and func_req != row[0]:
                    pyqfd.correlation.create(
                        {
                            "functional_requirement_id": functional_requirement_map[
                                func_req
                            ],
                            "functional_id": functional_requirement_map[row[0]],
                            "correlation": schemas.CorrEnum(int(corr)),
                        },
                        {"qfd_id": qfd_id},
                    )

        # TODO: Quick check of update function - need to separate into more rigorous unit
        # test
        for func_req in fr:
            pyqfd.functional_requirement.update(
                {k: v for k, v in func_req.items() if k != "triz"}
            )
    # Test calculations
    results = pyqfd.analysis.get(qfd_id)
    # TODO: verify expected output

    # Expected results include repeated functional requirements. Initial definition of expected_results references functional requirement IDs, and is then modified to contain full object
    expected_results = {
        "functional_requirements": [
            {
                "delivery_difficulty": Difficulty(2),
                "description": "Capital cost",
                "direction": Improvement(-1),
                "engineering_difficulty": Difficulty(4),
                "id": 1,
                "target": 238.33333333333334,
                "triz": [
                    {"definition": "Quantity of substance", "id": 26},
                    {"definition": "Device complexity", "id": 36},
                ],
                "solution_hierarchy_child_uuid": None,
                "units": "£/kW output",
            },
            {
                "delivery_difficulty": Difficulty(3),
                "description": "Controllability",
                "direction": Improvement(1),
                "engineering_difficulty": Difficulty(4),
                "id": 2,
                "target": 90.0,
                "triz": [{"definition": "Adaptability or versatility", "id": 35}],
                "solution_hierarchy_child_uuid": None,
                "units": "% efficiency",
            },
            {
                "delivery_difficulty": Difficulty(3),
                "description": "Degradation",
                "direction": Improvement(-1),
                "engineering_difficulty": Difficulty(2),
                "id": 3,
                "target": 2000.0,
                "triz": [
                    {"definition": "Speed", "id": 9},
                    {"definition": "Object-affected harmful factors", "id": 30},
                ],
                "solution_hierarchy_child_uuid": None,
                "units": "Hours corrosion protection to ASTM B-117",
            },
            {
                "delivery_difficulty": Difficulty(3),
                "description": "Energy Conversion",
                "direction": Improvement(1),
                "engineering_difficulty": Difficulty(3),
                "id": 4,
                "target": 500.0,
                "triz": [
                    {"definition": "Area of stationary object", "id": 6},
                    {"definition": "Speed", "id": 9},
                ],
                "solution_hierarchy_child_uuid": None,
                "units": "kPa Pressure Drop",
            },
            {
                "delivery_difficulty": Difficulty(4),
                "description": "Fluid flow",
                "direction": Improvement(1),
                "engineering_difficulty": Difficulty(3),
                "id": 5,
                "target": 200.0,
                "triz": [
                    {"definition": "Speed", "id": 9},
                    {"definition": "Loss of Energy", "id": 22},
                ],
                "solution_hierarchy_child_uuid": None,
                "units": "Thermal conductivity (W/mK)",
            },
            {
                "delivery_difficulty": Difficulty(2),
                "description": "Human interaction",
                "direction": Improvement(1),
                "engineering_difficulty": Difficulty(1),
                "id": 6,
                "target": 0.0,
                "triz": [
                    {"definition": "Adaptability or versatility", "id": 35},
                    {"definition": "Difficulty of detecting and measuring", "id": 37},
                ],
                "solution_hierarchy_child_uuid": None,
                "units": "Qualitative",
            },
            {
                "delivery_difficulty": Difficulty(2),
                "description": "Material Selection",
                "direction": Improvement(1),
                "engineering_difficulty": Difficulty(2),
                "id": 7,
                "target": 2000.0,
                "triz": [
                    {"definition": "Stability of the object's composition", "id": 13},
                    {"definition": "Strength", "id": 14},
                ],
                "solution_hierarchy_child_uuid": None,
                "units": "£/maintenance interval per year",
            },
            {
                "delivery_difficulty": Difficulty(2),
                "description": "Operational costs",
                "direction": Improvement(-1),
                "engineering_difficulty": Difficulty(4),
                "id": 8,
                "target": 8.0,
                "triz": [{"definition": "Reliability", "id": 27}],
                "solution_hierarchy_child_uuid": None,
                "units": "kW parasitic losses",
            },
            {
                "delivery_difficulty": None,
                "description": "System Integration",
                "direction": Improvement(-1),
                "engineering_difficulty": None,
                "id": 9,
                "target": 0.0,
                "triz": [{"definition": "Use of energy by moving object", "id": 19}],
                "solution_hierarchy_child_uuid": None,
                "units": "Qualitative",
            },
        ],
        "name": "Heat exchanger",
        "objective": "Exchanges heat from one fluid to another",
        "solution_hierarchy_uuid": None,
        "correlations": {
            1: {
                2: Correlation(-4),
                3: Correlation(-1),
                4: Correlation(-4),
                5: Correlation(-9),
                6: Correlation(-1),
                7: Correlation(-4),
                8: Correlation(4),
                9: Correlation(-4),
            },
            2: {
                1: Correlation(-4),
                3: Correlation(4),
                4: Correlation(4),
                5: Correlation(0),
                6: Correlation(0),
                7: Correlation(-4),
                8: Correlation(1),
                9: Correlation(4),
            },
            3: {
                1: Correlation(-1),
                2: Correlation(4),
                4: Correlation(9),
                5: Correlation(0),
                6: Correlation(1),
                7: Correlation(9),
                8: Correlation(-1),
                9: Correlation(1),
            },
            4: {
                1: Correlation(-4),
                2: Correlation(4),
                3: Correlation(9),
                5: Correlation(4),
                6: Correlation(0),
                7: Correlation(4),
                8: Correlation(4),
                9: Correlation(4),
            },
            5: {
                1: Correlation(-9),
                2: Correlation(0),
                3: Correlation(0),
                4: Correlation(4),
                6: Correlation(0),
                7: Correlation(4),
                8: Correlation(-4),
                9: Correlation(0),
            },
            6: {
                1: Correlation(-1),
                2: Correlation(0),
                3: Correlation(1),
                4: Correlation(0),
                5: Correlation(0),
                7: Correlation(0),
                8: Correlation(4),
                9: Correlation(9),
            },
            7: {
                1: Correlation(-4),
                2: Correlation(-4),
                3: Correlation(9),
                4: Correlation(4),
                5: Correlation(4),
                6: Correlation(0),
                8: Correlation(-9),
                9: Correlation(4),
            },
            8: {
                1: Correlation(4),
                2: Correlation(1),
                3: Correlation(-1),
                4: Correlation(4),
                5: Correlation(-4),
                6: Correlation(4),
                7: Correlation(-9),
                9: Correlation(4),
            },
            9: {
                1: Correlation(-4),
                2: Correlation(4),
                3: Correlation(1),
                4: Correlation(4),
                5: Correlation(0),
                6: Correlation(9),
                7: Correlation(4),
                8: Correlation(4),
            },
        },
        "solutions": [
            {
                "achievements": {
                    1: 240.0,
                    2: 96.0,
                    3: 2500.0,
                    4: 500.0,
                    5: 230.0,
                    6: 0.0,
                    7: 1500.0,
                    8: 5.0,
                },
                "description": "Advanced material Plate",
            },
            {
                "achievements": {
                    1: 270.0,
                    2: 94.0,
                    3: 2200.0,
                    4: 500.0,
                    5: 200.0,
                    6: 0.0,
                    7: 2200.0,
                    8: 9.0,
                },
                "description": "Convolution",
            },
            {
                "achievements": {
                    1: 240.0,
                    2: 90.0,
                    3: 2000.0,
                    4: 600.0,
                    5: 200.0,
                    6: 0.0,
                    7: 2000.0,
                    8: 8.0,
                },
                "description": "Plate",
            },
            {
                "achievements": {
                    1: 280.0,
                    2: 87.0,
                    3: 1800.0,
                    4: 800.0,
                    5: 200.0,
                    6: 0.0,
                    7: 1000.0,
                    8: 7.0,
                },
                "description": "Tube",
            },
        ],
        "parent_id": None,
        "requirements": [
            {
                "description": "Highest effectiveness",
                "impacts": {
                    1: Impact.High,
                    2: Impact.Medium,
                    3: Impact.Low,
                    4: Impact.Medium,
                    5: Impact.High,
                    6: Impact.Medium,
                    7: Impact.Medium,
                    8: Impact.Medium,
                },
                "importance": 2,
                "solution_hierarchy_child_uuid": None,
            },
            {
                "description": "Lowest Environmental impact",
                "impacts": {
                    1: Impact.Low,
                    2: Impact.Low,
                    3: Impact.High,
                    4: Impact.Medium,
                    5: Impact["None"],
                    6: Impact.Medium,
                    7: Impact.Medium,
                    8: Impact.Low,
                },
                "importance": 3,
                "solution_hierarchy_child_uuid": None,
            },
            {
                "description": "Lowest Lifetime cost",
                "impacts": {
                    1: Impact.High,
                    2: Impact.High,
                    3: Impact.Medium,
                    4: Impact.High,
                    5: Impact.Medium,
                    6: Impact.Low,
                    7: Impact.Medium,
                    8: Impact.Medium,
                },
                "importance": 1,
                "solution_hierarchy_child_uuid": None,
            },
        ],
    }
    fr_id_map = {fr["id"]: fr for fr in expected_results["functional_requirements"]}
    expected_results["correlations"] = [
        {
            "functional_requirement": fr_id_map[x],
            "functional": fr_id_map[k],
            "correlation": v,
        }
        for x, y in expected_results["correlations"].items()
        for k, v in y.items()
    ]
    for s in expected_results["solutions"]:
        s["achievements"] = [
            {"functional_requirement": fr_id_map[k], "value": v}
            for k, v in s["achievements"].items()
        ]
    for r in expected_results["requirements"]:
        r["impacts"] = [
            {"functional_requirement": fr_id_map[k], "impact": v}
            for k, v in r["impacts"].items()
        ]

    assert not DeepDiff(
        results, expected_results, ignore_order=True, exclude_regex_paths=[r"\['id'\]$"]
    )

    # Check triz classes still exist in database after removal from functional requirement
    fr = pyqfd.functional_requirement.get()[0]
    pyqfd.functional_requirement.update(
        {"id": fr["id"]}, {"triz": [t["id"] for t in fr["triz"][:-1]]}
    )
    assert not DeepDiff(triz_classes, pyqfd.triz_class.get(), ignore_order=True)


def test_contradictions(app):
    expected_inventive_principles = [  # fmt: off
        {
            "worsening": {"id": 3, "definition": "Length of moving object"},
            "improving": {"id": 8, "definition": "Volume of stationary object"},
            "principle": {
                "description": "Periodic action",
                "definition": "Pulsing or vibration spacing or periods",
                "example": "warning light that flash attract more attention, PWM in diesel injectors",
                "marine": "Periodic cleaning of hulls, blade pitch adjustments to give lower turbulence",
                "id": 19,
            },
        },
        {
            "worsening": {"id": 3, "definition": "Length of moving object"},
            "improving": {"id": 8, "definition": "Volume of stationary object"},
            "principle": {
                "description": "Curve increase",
                "definition": "Change lines to arcs, squares to circles, cubes to spheres",
                "example": "domed pressure vessels, turn switches rather than push, ",
                "marine": "blade curvature to offset deflection due to thrust",
                "id": 14,
            },
        },
        {
            "worsening": {
                "id": 15,
                "definition": "Duration of action of moving object",
            },
            "improving": {"id": 3, "definition": "Length of moving object"},
            "principle": {
                "description": "Periodic action",
                "definition": "Pulsing or vibration spacing or periods",
                "example": "warning light that flash attract more attention, PWM in diesel injectors",
                "marine": "Periodic cleaning of hulls, blade pitch adjustments to give lower turbulence",
                "id": 19,
            },
        },
    ]

    inventive_principles = pyqfd.get_principles(
        [{"id": 3}, {"id": 8}], [{"id": 3}, {"id": 15}]
    )
    for p in inventive_principles:
        del p["id"]

    assert expected_inventive_principles == inventive_principles


functional_defaults = {
    "solution_hierarchy_child_uuid": None,
    "direction": None,
    "target": None,
    "units": None,
    "engineering_difficulty": None,
    "delivery_difficulty": None,
    "triz": [],
}

input_correlations = [
    {"correlation": 4, "id": 1, "functional_id": 1, "functional_requirement_id": 2},
    {"correlation": 4, "id": 2, "functional_id": 2, "functional_requirement_id": 1},
    {"correlation": 9, "id": 3, "functional_id": 1, "functional_requirement_id": 3},
    {"correlation": -1, "id": 5, "functional_id": 2, "functional_requirement_id": 3},
    {"correlation": -1, "id": 6, "functional_id": 3, "functional_requirement_id": 2},
]

expected_correlations = [  # fmt: off
    {
        "correlation": Correlation(4),
        "functional": {**functional_defaults, "description": "a"},
        "functional_requirement": {**functional_defaults, "description": "b"},
    },
    {
        "correlation": Correlation(4),
        "functional": {**functional_defaults, "description": "b"},
        "functional_requirement": {**functional_defaults, "description": "a"},
    },
    {
        "correlation": Correlation(9),
        "functional": {**functional_defaults, "description": "a"},
        "functional_requirement": {**functional_defaults, "description": "c"},
    },
    {
        "correlation": Correlation(9),
        "functional": {**functional_defaults, "description": "c"},
        "functional_requirement": {**functional_defaults, "description": "a"},
    },
    {
        "correlation": Correlation(-1),
        "functional": {**functional_defaults, "description": "b"},
        "functional_requirement": {**functional_defaults, "description": "c"},
    },
    {
        "correlation": Correlation(-1),
        "functional": {**functional_defaults, "description": "c"},
        "functional_requirement": {**functional_defaults, "description": "b"},
    },
]


@pytest.fixture
def correlations_qfd(app):
    qfd = pyqfd.analysis.create({"name": "Name", "objective": "Objective"})
    qfd["functional_requirements"] = pyqfd.functional_requirement.create(
        [{"id": i, "description": j} for i, j in enumerate("abc", 1)],
        {"qfd_id": qfd["id"]},
    )
    qfd["correlations"] = pyqfd.correlation.create(
        input_correlations, {"qfd_id": qfd["id"]}
    )
    return qfd


def test_create_correlations(app, correlations_qfd):
    assert not DeepDiff(
        correlations_qfd["correlations"],
        expected_correlations,
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )


def test_update_correlations_by_id(app, correlations_qfd):
    expected_updated_correlations = [
        {**corr, "correlation": Correlation(-corr["correlation"].value)}
        for corr in expected_correlations[:2]
    ]
    # Get correlation result corresponding to first expected correlation
    corr = next(
        c
        for c in correlations_qfd["correlations"]
        if all(
            c[k]["description"] == expected_correlations[0][k]["description"]
            for k in ("functional", "functional_requirement")
        )
    )
    correlations = pyqfd.correlation.update(
        {"id": corr["id"], "correlation": -corr["correlation"].value}
    )

    assert not DeepDiff(
        correlations,
        expected_updated_correlations,
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )

    assert not DeepDiff(
        pyqfd.analysis.get(correlations_qfd["id"])["correlations"],
        expected_updated_correlations + expected_correlations[2:],
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )


def test_update_correlations_by_functional_requirements(app, correlations_qfd):
    expected_updated_correlations = [
        {**corr, "correlation": Correlation(-corr["correlation"].value)}
        for corr in expected_correlations[:2]
    ]
    # Get correlation result corresponding to first expected correlation
    corr = next(
        c
        for c in correlations_qfd["correlations"]
        if all(
            c[k]["description"] == expected_correlations[0][k]["description"]
            for k in ("functional", "functional_requirement")
        )
    )
    correlations = pyqfd.correlation.update(
        {
            "functional_id": corr["functional"]["id"],
            "functional_requirement_id": corr["functional_requirement"]["id"],
            "correlation": -corr["correlation"].value,
        }
    )

    assert not DeepDiff(
        correlations,
        expected_updated_correlations,
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )

    assert not DeepDiff(
        pyqfd.analysis.get(correlations_qfd["id"])["correlations"],
        expected_updated_correlations + expected_correlations[2:],
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )


def test_update_correlations_invalid_functional_requirements(app, correlations_qfd):
    # Get correlation result corresponding to first expected correlation
    corr = correlations_qfd["correlations"][0]
    with pytest.raises(InvalidInput):
        pyqfd.correlation.update(
            {
                "functional_id": corr["functional"]["id"],
                "functional_requirement_id": corr["functional"]["id"],
                "correlation": -corr["correlation"].value,
            }
        )


def test_update_correlations_invalid_id_functional_requirements(app, correlations_qfd):
    # Get correlation result corresponding to first expected correlation
    corr1, corr2, *_ = correlations_qfd["correlations"]
    with pytest.raises(InvalidInput):
        pyqfd.correlation.update(
            {
                "id": corr1["id"],
                "functional_id": corr2["functional"]["id"],
                "functional_requirement_id": corr2["functional"]["id"],
                "correlation": corr2["correlation"].value,
            }
        )


def test_update_correlations_by_id_functional_requirements(app, correlations_qfd):
    expected_updated_correlations = [
        {**corr, "correlation": Correlation(-corr["correlation"].value)}
        for corr in expected_correlations[:2]
    ]
    # Get correlation result corresponding to first expected correlation
    corr = next(
        corr
        for corr in correlations_qfd["correlations"]
        if all(
            corr[k]["description"] == expected_correlations[0][k]["description"]
            for k in ("functional", "functional_requirement")
        )
    )
    correlations = pyqfd.correlation.update(
        {
            "id": corr["id"],
            "functional_id": corr["functional"]["id"],
            "functional_requirement_id": corr["functional_requirement"]["id"],
            "correlation": -corr["correlation"].value,
        }
    )

    assert not DeepDiff(
        correlations,
        expected_updated_correlations,
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )

    assert not DeepDiff(
        pyqfd.analysis.get(correlations_qfd["id"])["correlations"],
        expected_updated_correlations + expected_correlations[2:],
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )


def test_create_requirement_importance(app, correlations_qfd):
    with pytest.raises(ValidationError):
        pyqfd.requirement.create(
            {"description": "Requirement description"},
            {"qfd_id": correlations_qfd["id"]},
        )


expected_requirements_importance = [
    {"description": "A", "importance": 1},
    {"description": "B", "importance": 5},
]


@pytest.fixture
def multiple_requirements(app, correlations_qfd):
    correlations_qfd["requirements"] = pyqfd.requirement.create(
        expected_requirements_importance, {"qfd_id": correlations_qfd["id"]}
    )
    return correlations_qfd


def test_update_requirement(app, multiple_requirements):
    requirement = next(
        r for r in multiple_requirements["requirements"] if r["description"] == "B"
    )
    qfd_id = multiple_requirements["id"]
    expected_requirements = [
        {
            **r,
            "description": r["description"].replace(*"BZ"),
            "impacts": [],
            "solution_hierarchy_child_uuid": None,
        }
        for r in expected_requirements_importance
    ]
    assert not DeepDiff(
        pyqfd.requirement.update({"id": requirement["id"], "description": "Z"}),
        expected_requirements[1],
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )
    assert not DeepDiff(
        pyqfd.analysis.get(qfd_id)["requirements"],
        expected_requirements,
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )


def test_update_requirement_importance(app, multiple_requirements):
    requirement = next(
        r for r in multiple_requirements["requirements"] if r["importance"] == 5
    )
    qfd_id = multiple_requirements["id"]
    expected_requirements = [
        {
            **r,
            "importance": 2 * r["importance"] - 1,
            "impacts": [],
            "solution_hierarchy_child_uuid": None,
        }
        for r in expected_requirements_importance
    ]
    assert not DeepDiff(
        pyqfd.requirement.update({"id": requirement["id"], "importance": 9}),
        expected_requirements[1],
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )
    assert not DeepDiff(
        pyqfd.analysis.get(qfd_id)["requirements"],
        expected_requirements,
        ignore_order=True,
        exclude_regex_paths=[r"\['id'\]$"],
    )


def test_correlation_create_invalid():
    qfd = pyqfd.analysis.create({"name": "A", "objective": "B"})
    frs = [{"id": i, "description": j} for i, j in enumerate("abc", 1)]
    pyqfd.functional_requirement.create(frs, {"qfd_id": qfd["id"]})
    with pytest.raises(InvalidInput):
        pyqfd.correlation.create(
            {
                "correlation": 4,
                "id": 1,
                "functional_id": 1,
                "functional_requirement_id": 1,
            },
            {"qfd_id": qfd["id"]},
        )


def test_source_data():
    # Check inventive principle are uniques
    p, inventive_ids = _load_inventive_principles()
    assert len(p) == len(inventive_ids)

    # Check contradictions are correct
    tsv = _tsv_reader("ContradictionMatrix.tsv")
    columns = next(tsv)[1:]
    feature_map = {j: i for i, j in enumerate(columns, 1)}
    feature_check = set(columns)
    assert len(feature_check) == len(columns)  # Ensure columns are unique
    for row in tsv:
        improving_feature, *inventive = row
        feature_check.remove(improving_feature)
        improving_id = feature_map[improving_feature]
        for worsening_feature, feature_principles in zip(columns, inventive):
            worsening_id = feature_map[worsening_feature]
            assert (
                improving_id != worsening_id or feature_principles == "+"
            )  # Ensure diagonal is marked with '+'
            for principle in _expr.findall(feature_principles):
                assert (
                    int(principle) in inventive_ids
                )  # Ensure inventive principle ID exists
    assert not feature_check  # Ensure rows and columns match


def test_declarations():
    # Test that enumerations correctly cast to integers, returning their input value
    # For simplicity (preventing redefinition of enumerations), just test 1
    for i in (Correlation, Difficulty, Impact, Improvement):
        assert int(i(1)) == 1


def test_qfd_roundtrip(app):
    """Tests the recursive creation and get operations for QFD.

    Given an exported json file, load it in using the recursive create
    function, then fetch it and check that it matches the original.
    """

    example_qfd = path.join(path.dirname(__file__), "megasplosher.json")

    fd = open(example_qfd)
    qfd = json.load(fd)
    fd.close()
    created = pyqfd.create_qfd_recursively(deepcopy(qfd))
    id_ = created["id"]

    original = schemas.QFDAnalysis(**qfd).dict()
    fetched = pyqfd.analysis.get(id_)

    assert not DeepDiff(
        original, fetched, ignore_order=True, exclude_regex_paths="[id]"
    )
