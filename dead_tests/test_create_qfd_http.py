# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from deepdiff import DeepDiff


def test_create_qfd_http(client):
    res = client.post(
        "/qfds/",
        json={"name": "A simple analysis", "objective": "To do some testing"},
    )
    assert res.status_code == 201, res.data.decode()
    qfd = res.json["id"]

    res = client.post(
        f"/qfds/{qfd}/requirements/",
        json={"description": "First requirement", "importance": 2},
    )
    assert res.status_code == 201, res.data.decode()
    r1 = res.json["id"]

    res = client.post(
        f"/qfds/{qfd}/requirements/",
        json={"description": "Second requirement", "importance": 1},
    )
    assert res.status_code == 201, res.data.decode()
    r2 = res.json["id"]

    res = client.post(
        f"/qfds/{qfd}/functional_requirements/",
        json={
            "description": "First functional requirement",
            "direction": 1,
            "target": 23,
            "units": "elephants",
            "engineering_difficulty": 2,
            "delivery_difficulty": 5,
        },
    )
    assert res.status_code == 201, res.data.decode()
    fr1 = res.json["id"]

    res = client.post(
        f"/qfds/{qfd}/functional_requirements/",
        json={
            "description": "Second functional requirement",
            "direction": 1,
            "target": 99,
            "units": "red balloons",
            "engineering_difficulty": 4,
            "delivery_difficulty": 2,
        },
    )
    assert res.status_code == 201, res.data.decode()
    fr2 = res.json["id"]

    res = client.post(
        f"/qfds/{qfd}/functional_requirements/",
        json={
            "description": "Third functional requirement",
            "direction": -1,
            "target": 999,
            "units": "emergencies",
            "engineering_difficulty": 2,
            "delivery_difficulty": 2,
        },
    )
    assert res.status_code == 201, res.data.decode()
    fr3 = res.json["id"]

    res = client.post(
        f"/qfds/{qfd}/functional_requirements/{fr1}/triz_classes/",
        json={"triz": [1, 2, 9, 26]},
    )
    assert res.status_code == 200, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/functional_requirements/{fr2}/triz_classes/",
        json={"triz": [3, 4]},
    )
    assert res.status_code == 200, res.data.decode()

    res = client.post(
        f"/qfds/{qfd}/functional_requirements/correlations/",
        json={"correlation": 4, "functional_id": fr1, "functional_requirement_id": fr2},
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/functional_requirements/correlations/",
        json={"correlation": 9, "functional_id": fr1, "functional_requirement_id": fr3},
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/functional_requirements/correlations/",
        json={
            "correlation": -1,
            "functional_id": fr2,
            "functional_requirement_id": fr3,
        },
    )
    assert res.status_code == 201, res.data.decode()

    res = client.post(
        f"/qfds/{qfd}/requirements/{r1}/impacts/",
        json={"functional_requirement_id": fr1, "impact": 9},
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/requirements/{r1}/impacts/",
        json={"functional_requirement_id": fr2, "impact": 4},
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/requirements/{r2}/impacts/",
        json={"functional_requirement_id": fr1, "impact": 4},
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/requirements/{r2}/impacts/",
        json={"functional_requirement_id": fr2, "impact": 1},
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/requirements/{r1}/impacts/",
        json={"functional_requirement_id": fr3, "impact": 4},
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/requirements/{r2}/impacts/",
        json={"functional_requirement_id": fr3, "impact": 4},
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/solutions/",
        json={
            "description": "The most awesome solution ever",
            "achievements": [
                {"functional_requirement_id": fr1, "value": 102},
                {"functional_requirement_id": fr2, "value": 4},
                {"functional_requirement_id": fr3, "value": 17},
            ],
        },
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        f"/qfds/{qfd}/solutions/",
        json={
            "description": "Potatoes",
            "achievements": [
                {"functional_requirement_id": fr1, "value": 42},
                {"functional_requirement_id": fr2, "value": 42},
                {"functional_requirement_id": fr3, "value": 42},
            ],
        },
    )
    assert res.status_code == 201, res.data.decode()

    res = client.get(f"/qfds/{qfd}")
    assert res.status_code == 200, res.data.decode()
    assert not DeepDiff(
        res.json,
        expected_result,
        ignore_order=True,
        significant_digits=3,
        exclude_regex_paths=[r".*\['id'\]$"],
    )

    res = client.get(f"/qfds/{qfd}?download")
    assert res.status_code == 200, res.data.decode()
    assert res.headers["Content-Disposition"] == f"attachment; filename=qfd-{qfd}.json"
    assert not DeepDiff(
        res.json,
        expected_result,
        ignore_order=True,
        significant_digits=3,
        exclude_regex_paths=[r".*\['id'\]$"],
    )

    # Test multiple analyses
    expected_two_analyses = {
        "solution_hierarchy_uuid": None,
        "parent_id": None,
        "correlations": [],
        "functional_requirements": [],
        "requirements": [],
        "solutions": [],
    }
    expected_two_analyses = [
        {
            **expected_two_analyses,
            "name": "Second analysis",
            "objective": "Testing analyses",
        },
        {
            **expected_two_analyses,
            "name": "Another analysis",
            "objective": "Test multiple",
        },
    ]
    res_multiple = client.post(
        "/qfds/",
        json=[
            {"name": "Second analysis", "objective": "Testing analyses"},
            {"name": "Another analysis", "objective": "Test multiple"},
        ],
    )
    assert res_multiple.status_code == 201
    assert not DeepDiff(
        res_multiple.json,
        expected_two_analyses,
        ignore_order=True,
        exclude_regex_paths=[r".*\['id'\]$"],
    )
    res_multiple = client.get("/qfds/")
    assert res_multiple.status_code == 200
    assert not DeepDiff(
        res_multiple.json,
        [expected_result] + expected_two_analyses,
        ignore_order=True,
        significant_digits=3,
        exclude_regex_paths=[r".*\['id'\]$"],
    )

    # Check impact importance
    impact_importance = client.get(f"/qfds/{qfd}/impact_importance")
    assert impact_importance.status_code == 200
    assert impact_importance.json == [True]

    # Update and check requirement importance
    id_ = min(res.json["requirements"], key=lambda x: x["importance"])["id"]
    new_importance = max(x["importance"] for x in res.json["requirements"]) + 1
    res = client.put(
        f"/qfds/{qfd}/requirements/{id_}",
        json={"id": id_, "importance": new_importance},
    )
    assert res.status_code == 200
    reordered_requirements = [
        {**req, "importance": 4 - req["importance"]}
        for req in expected_result["requirements"]
    ]
    assert not DeepDiff(
        res.json,
        next(x for x in reordered_requirements if x["id"] == id_),
        exclude_regex_paths=[r".*\['id'\]$"],
    )
    impact_importance = client.get(f"/qfds/{qfd}/impact_importance")
    assert impact_importance.status_code == 200
    assert impact_importance.json == [False]

    # Update requirement
    res = client.put(
        f"/qfds/{qfd}/requirements/{id_}",
        json={"id": id_, "description": "Modified second requirement"},
    )
    assert res.status_code == 200
    reordered_requirements[1]["description"] = "Modified second requirement"
    assert not DeepDiff(
        res.json,
        reordered_requirements[1],
        ignore_order=True,
        exclude_regex_paths=[r".*\['id'\]$"],
    )
    res = client.get(f"/qfds/{qfd}")
    assert res.status_code == 200
    assert not DeepDiff(
        res.json,
        {**expected_result, "requirements": reordered_requirements},
        ignore_order=True,
        exclude_regex_paths=[r".*\['id'\]$"],
    )

    # Delete analysis and check it no longer exists
    res = client.delete(f"/qfds/{qfd}")
    assert res.status_code == 204
    res = client.get(f"/qfds/{qfd}")
    assert res.status_code == 500
    # Check impact importance for deleted analysis
    res = client.get(f"/qfds/{qfd}/impact_importance")
    assert res.status_code == 400


def test_qfd_import(client):
    res = client.post("/qfds/import", json=expected_result_import)
    assert res.status_code == 201, res.data.decode()
    assert not DeepDiff(
        res.json,
        expected_result_import,
        ignore_order=True,
        significant_digits=3,
        exclude_regex_paths=[r".*\['id'\]$"],
    )
    qfd = res.json["id"]

    res = client.get(f"/qfds/{qfd}")
    assert res.status_code == 200
    assert not DeepDiff(
        res.json,
        expected_result_import,
        ignore_order=True,
        significant_digits=3,
        exclude_regex_paths=[r".*\['id'\]$"],
    )
    # check importance
    requirement_data_list = (res.json).pop("requirements")
    temp_requirement_data_list = sorted(
        requirement_data_list, key=lambda i: i["importance"]
    )
    req1 = temp_requirement_data_list[0]
    assert req1["description"] == "4-importance0"
    assert req1["importance"] == 1
    req2 = temp_requirement_data_list[1]
    assert req2["description"] == "3-importance1"
    assert req2["importance"] == 2
    req3 = temp_requirement_data_list[2]
    assert req3["description"] == "1-importance2"
    assert req3["importance"] == 3
    req4 = temp_requirement_data_list[3]
    assert req4["description"] == "2-importance3"
    assert req4["importance"] == 6


def test_qfd_duplicate_create(client):
    res = client.post(
        "/qfds/", json={"name": "My analysis", "objective": "First objective"}
    )
    assert res.status_code == 201, res.data.decode()
    res = client.post(
        "/qfds/", json={"name": "My analysis", "objective": "Second objective"}
    )
    assert res.status_code == 400, res.data.decode()
    assert res.json == {"error": "UNIQUE constraint failed: qfd_analysis.name"}


def test_qfd_duplicate_import(client):
    res = client.post("/qfds/import", json=expected_result)
    assert res.status_code == 201, res.data.decode()
    res = client.post("/qfds/import", json=expected_result)
    assert res.status_code == 400, res.data.decode()
    assert res.json == {"error": "UNIQUE constraint failed: qfd_analysis.name"}


expected_result = {
    "solution_hierarchy_uuid": None,
    "parent_id": None,
    "correlations": [
        {
            "correlation": 4,
            "functional": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 5,
                "description": "First functional requirement",
                "direction": 1,
                "engineering_difficulty": 2,
                "id": 1,
                "target": 23.0,
                "triz": [
                    {"definition": "Weight of moving object", "id": 1},
                    {"definition": "Weight of stationary object", "id": 2},
                    {"definition": "Speed", "id": 9},
                    {"definition": "Quantity of substance", "id": 26},
                ],
                "units": "elephants",
            },
            "functional_requirement": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 2,
                "description": "Second functional requirement",
                "direction": 1,
                "engineering_difficulty": 4,
                "id": 2,
                "target": 99.0,
                "triz": [
                    {"definition": "Length of moving object", "id": 3},
                    {"definition": "Length of stationary object", "id": 4},
                ],
                "units": "red balloons",
            },
            "id": 1,
        },
        {
            "correlation": 4,
            "functional": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 2,
                "description": "Second functional requirement",
                "direction": 1,
                "engineering_difficulty": 4,
                "id": 2,
                "target": 99.0,
                "triz": [
                    {"definition": "Length of moving object", "id": 3},
                    {"definition": "Length of stationary object", "id": 4},
                ],
                "units": "red balloons",
            },
            "functional_requirement": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 5,
                "description": "First functional requirement",
                "direction": 1,
                "engineering_difficulty": 2,
                "id": 1,
                "target": 23.0,
                "triz": [
                    {"definition": "Weight of moving object", "id": 1},
                    {"definition": "Weight of stationary object", "id": 2},
                    {"definition": "Speed", "id": 9},
                    {"definition": "Quantity of substance", "id": 26},
                ],
                "units": "elephants",
            },
            "id": 2,
        },
        {
            "correlation": 9,
            "functional": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 5,
                "description": "First functional requirement",
                "direction": 1,
                "engineering_difficulty": 2,
                "id": 1,
                "target": 23.0,
                "triz": [
                    {"definition": "Weight of moving object", "id": 1},
                    {"definition": "Weight of stationary object", "id": 2},
                    {"definition": "Speed", "id": 9},
                    {"definition": "Quantity of substance", "id": 26},
                ],
                "units": "elephants",
            },
            "functional_requirement": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 2,
                "description": "Third functional requirement",
                "direction": -1,
                "engineering_difficulty": 2,
                "id": 3,
                "target": 999.0,
                "triz": [],
                "units": "emergencies",
            },
            "id": 3,
        },
        {
            "correlation": 9,
            "functional": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 2,
                "description": "Third functional requirement",
                "direction": -1,
                "engineering_difficulty": 2,
                "id": 3,
                "target": 999.0,
                "triz": [],
                "units": "emergencies",
            },
            "functional_requirement": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 5,
                "description": "First functional requirement",
                "direction": 1,
                "engineering_difficulty": 2,
                "id": 1,
                "target": 23.0,
                "triz": [
                    {"definition": "Weight of moving object", "id": 1},
                    {"definition": "Weight of stationary object", "id": 2},
                    {"definition": "Speed", "id": 9},
                    {"definition": "Quantity of substance", "id": 26},
                ],
                "units": "elephants",
            },
            "id": 4,
        },
        {
            "correlation": -1,
            "functional": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 2,
                "description": "Second functional requirement",
                "direction": 1,
                "engineering_difficulty": 4,
                "id": 2,
                "target": 99.0,
                "triz": [
                    {"definition": "Length of moving object", "id": 3},
                    {"definition": "Length of stationary object", "id": 4},
                ],
                "units": "red balloons",
            },
            "functional_requirement": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 2,
                "description": "Third functional requirement",
                "direction": -1,
                "engineering_difficulty": 2,
                "id": 3,
                "target": 999.0,
                "triz": [],
                "units": "emergencies",
            },
            "id": 5,
        },
        {
            "correlation": -1,
            "functional": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 2,
                "description": "Third functional requirement",
                "direction": -1,
                "engineering_difficulty": 2,
                "id": 3,
                "target": 999.0,
                "triz": [],
                "units": "emergencies",
            },
            "functional_requirement": {
                "solution_hierarchy_child_uuid": None,
                "delivery_difficulty": 2,
                "description": "Second functional requirement",
                "direction": 1,
                "engineering_difficulty": 4,
                "id": 2,
                "target": 99.0,
                "triz": [
                    {"definition": "Length of moving object", "id": 3},
                    {"definition": "Length of stationary object", "id": 4},
                ],
                "units": "red balloons",
            },
            "id": 6,
        },
    ],
    "functional_requirements": [
        {
            "solution_hierarchy_child_uuid": None,
            "delivery_difficulty": 5,
            "description": "First functional requirement",
            "direction": 1,
            "engineering_difficulty": 2,
            "id": 1,
            "target": 23.0,
            "triz": [
                {"definition": "Weight of moving object", "id": 1},
                {"definition": "Weight of stationary object", "id": 2},
                {"definition": "Speed", "id": 9},
                {"definition": "Quantity of substance", "id": 26},
            ],
            "units": "elephants",
        },
        {
            "solution_hierarchy_child_uuid": None,
            "delivery_difficulty": 2,
            "description": "Second functional requirement",
            "direction": 1,
            "engineering_difficulty": 4,
            "id": 2,
            "target": 99.0,
            "triz": [
                {"definition": "Length of moving object", "id": 3},
                {"definition": "Length of stationary object", "id": 4},
            ],
            "units": "red balloons",
        },
        {
            "solution_hierarchy_child_uuid": None,
            "delivery_difficulty": 2,
            "description": "Third functional requirement",
            "direction": -1,
            "engineering_difficulty": 2,
            "id": 3,
            "target": 999.0,
            "triz": [],
            "units": "emergencies",
        },
    ],
    "id": 1,
    "name": "A simple analysis",
    "objective": "To do some testing",
    "requirements": [
        {
            "solution_hierarchy_child_uuid": None,
            "description": "First requirement",
            "id": 1,
            "impacts": [
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 5,
                        "description": "First functional requirement",
                        "direction": 1,
                        "engineering_difficulty": 2,
                        "id": 1,
                        "target": 23.0,
                        "triz": [
                            {"definition": "Weight of moving object", "id": 1},
                            {"definition": "Weight of stationary object", "id": 2},
                            {"definition": "Speed", "id": 9},
                            {"definition": "Quantity of substance", "id": 26},
                        ],
                        "units": "elephants",
                    },
                    "id": 1,
                    "impact": 9,
                },
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 2,
                        "description": "Second functional requirement",
                        "direction": 1,
                        "engineering_difficulty": 4,
                        "id": 2,
                        "target": 99.0,
                        "triz": [
                            {"definition": "Length of moving object", "id": 3},
                            {"definition": "Length of stationary object", "id": 4},
                        ],
                        "units": "red balloons",
                    },
                    "id": 2,
                    "impact": 4,
                },
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 2,
                        "description": "Third functional requirement",
                        "direction": -1,
                        "engineering_difficulty": 2,
                        "id": 3,
                        "target": 999.0,
                        "triz": [],
                        "units": "emergencies",
                    },
                    "id": 5,
                    "impact": 4,
                },
            ],
            "importance": 2,
        },
        {
            "solution_hierarchy_child_uuid": None,
            "description": "Second requirement",
            "id": 2,
            "impacts": [
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 5,
                        "description": "First functional requirement",
                        "direction": 1,
                        "engineering_difficulty": 2,
                        "id": 1,
                        "target": 23.0,
                        "triz": [
                            {"definition": "Weight of moving object", "id": 1},
                            {"definition": "Weight of stationary object", "id": 2},
                            {"definition": "Speed", "id": 9},
                            {"definition": "Quantity of substance", "id": 26},
                        ],
                        "units": "elephants",
                    },
                    "id": 3,
                    "impact": 4,
                },
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 2,
                        "description": "Second functional requirement",
                        "direction": 1,
                        "engineering_difficulty": 4,
                        "id": 2,
                        "target": 99.0,
                        "triz": [
                            {"definition": "Length of moving object", "id": 3},
                            {"definition": "Length of stationary object", "id": 4},
                        ],
                        "units": "red balloons",
                    },
                    "id": 4,
                    "impact": 1,
                },
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 2,
                        "description": "Third functional requirement",
                        "direction": -1,
                        "engineering_difficulty": 2,
                        "id": 3,
                        "target": 999.0,
                        "triz": [],
                        "units": "emergencies",
                    },
                    "id": 6,
                    "impact": 4,
                },
            ],
            "importance": 1,
        },
    ],
    "solutions": [
        {
            "achievements": [
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 5,
                        "description": "First functional requirement",
                        "direction": 1,
                        "engineering_difficulty": 2,
                        "id": 1,
                        "target": 23.0,
                        "triz": [
                            {"definition": "Weight of moving object", "id": 1},
                            {"definition": "Weight of stationary object", "id": 2},
                            {"definition": "Speed", "id": 9},
                            {"definition": "Quantity of substance", "id": 26},
                        ],
                        "units": "elephants",
                    },
                    "id": 4,
                    "value": 42.0,
                },
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 2,
                        "description": "Second functional requirement",
                        "direction": 1,
                        "engineering_difficulty": 4,
                        "id": 2,
                        "target": 99.0,
                        "triz": [
                            {"definition": "Length of moving object", "id": 3},
                            {"definition": "Length of stationary object", "id": 4},
                        ],
                        "units": "red balloons",
                    },
                    "id": 5,
                    "value": 42.0,
                },
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 2,
                        "description": "Third functional requirement",
                        "direction": -1,
                        "engineering_difficulty": 2,
                        "id": 3,
                        "target": 999.0,
                        "triz": [],
                        "units": "emergencies",
                    },
                    "id": 6,
                    "value": 42.0,
                },
            ],
            "description": "Potatoes",
            "id": 2,
        },
        {
            "achievements": [
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 5,
                        "description": "First functional requirement",
                        "direction": 1,
                        "engineering_difficulty": 2,
                        "id": 1,
                        "target": 23.0,
                        "triz": [
                            {"definition": "Weight of moving object", "id": 1},
                            {"definition": "Weight of stationary object", "id": 2},
                            {"definition": "Speed", "id": 9},
                            {"definition": "Quantity of substance", "id": 26},
                        ],
                        "units": "elephants",
                    },
                    "id": 1,
                    "value": 102.0,
                },
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 2,
                        "description": "Second functional requirement",
                        "direction": 1,
                        "engineering_difficulty": 4,
                        "id": 2,
                        "target": 99.0,
                        "triz": [
                            {"definition": "Length of moving object", "id": 3},
                            {"definition": "Length of stationary object", "id": 4},
                        ],
                        "units": "red balloons",
                    },
                    "id": 2,
                    "value": 4.0,
                },
                {
                    "functional_requirement": {
                        "solution_hierarchy_child_uuid": None,
                        "delivery_difficulty": 2,
                        "description": "Third functional requirement",
                        "direction": -1,
                        "engineering_difficulty": 2,
                        "id": 3,
                        "target": 999.0,
                        "triz": [],
                        "units": "emergencies",
                    },
                    "id": 3,
                    "value": 17.0,
                },
            ],
            "description": "The most awesome solution ever",
            "id": 1,
        },
    ],
}

# to check importance loading independent from order in json file
expected_result_import = {
    "solution_hierarchy_uuid": None,
    "parent_id": None,
    "correlations": [],
    "functional_requirements": [],
    "id": 1,
    "name": "test req importance",
    "objective": "e2e test",
    "requirements": [
        {
            "solution_hierarchy_child_uuid": None,
            "description": "1-importance2",
            "id": 1,
            "impacts": [],
            "importance": 3,
        },
        {
            "solution_hierarchy_child_uuid": None,
            "description": "2-importance3",
            "id": 2,
            "impacts": [],
            "importance": 6,
        },
        {
            "solution_hierarchy_child_uuid": None,
            "description": "3-importance1",
            "id": 3,
            "impacts": [],
            "importance": 2,
        },
        {
            "solution_hierarchy_child_uuid": None,
            "description": "4-importance0",
            "id": 4,
            "impacts": [],
            "importance": 1,
        },
    ],
    "solutions": [],
}
