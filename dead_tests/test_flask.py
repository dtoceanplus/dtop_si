# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
from flask import url_for

from deepdiff import DeepDiff


def test_app(app):
    assert not app.debug, "Ensure the app not in debug mode"


def test_get_all_fmeas(client):
    assert client.get(url_for("fmea.get_all_fmeas")).status_code == 200


def test_post_bad_fmea(client):
    assert (
        client.post(
            url_for("fmea.post_fmea"), json={"name": "This is a test"}
        ).status_code
        == 400
    )


good_fmea_create = {
    "name": "This is a test",
    "action_level": 100,
    "occurrence_limit": 70,
}
good_fmea_retrieve = {
    "id": 1,
    "name": "This is a test",
    "action_level": 100,
    "occurrence_limit": 70,
    "requirements": [],
    "mitigations": [],
}


@pytest.fixture
def fmea_exists(client) -> dict:
    return client.post(url_for("fmea.post_fmea"), json=good_fmea_create).json


def test_post_good_fmea(client):
    assert (
        client.post(url_for("fmea.post_fmea"), json=good_fmea_create).status_code == 201
    )


def test_get_fmea(client, fmea_exists):
    assert client.get(url_for('fmea.get_fmea', _id=1)).status_code == 200
    assert client.get(url_for("fmea.get_fmea", _id=1)).json == good_fmea_retrieve


###############################################################################
def test_add_requirement(client, fmea_exists):
    requirement_in = {"description": "something", "failure_modes": []}
    requirement_out = requirement_in.copy()

    res = client.post(url_for("fmea.post_requirement", fmea_id=1), json=requirement_in)
    assert res.status_code == 201, res.data.decode()

    rid = int(res.json["id"])
    requirement_out["id"] = rid

    assert (
        requirement_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"]
    )

    requirement_update_in = {
        "description": "something new",
        "failure_modes": [],
        "id": rid,
    }
    requirement_update_out = requirement_update_in.copy()
    res = client.put(
        url_for("fmea.update_requirement", fmea_id=1, requirement_id=rid),
        json=requirement_update_in,
    )

    assert res.status_code == 200, res.data.decode()
    assert (
        requirement_update_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"]
    )

    res = client.delete(
        url_for("fmea.delete_requirement", fmea_id=1, requirement_id=rid)
    )
    assert res.status_code == 204
    res = client.get(f"/fmea/1/requirements/{rid}")
    assert res.status_code == 500


########################################################################
good_requirement_create = {
    "description": "This is a requirement",
    "fmea_id": 1,
}
good_requirement_retrieve = {
    "id": 1,
    "description": "This is a requirement",
    "failure_modes": [],
}


@pytest.fixture
def requirement_exists(client, fmea_exists) -> dict:
    return client.post(
        url_for("fmea.post_requirement", fmea_id=1), json=good_requirement_create
    ).json


def test_add_failure_mode(client, requirement_exists):
    failure_mode_in = {"description": "bad thing", "causes": [], "effects": []}
    failure_mode_out = failure_mode_in.copy()

    res = client.post(
        url_for("fmea.post_failure_mode", fmea_id=1, requirement_id=1),
        json=failure_mode_in,
    )
    assert res.status_code == 201, res.data.decode()

    fmid = int(res.json["id"])
    failure_mode_out["id"] = fmid

    assert (
        failure_mode_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"][0][
            "failure_modes"
        ]
    )

    failure_mode_update_in = {
        "description": "updated bad thing",
        "causes": [],
        "effects": [],
        "id": fmid,
    }
    failure_mode_update_out = failure_mode_update_in.copy()
    res = client.put(
        url_for(
            "fmea.update_failure_mode",
            fmea_id=1,
            requirement_id=1,
            failure_mode_id=fmid,
        ),
        json=failure_mode_update_in,
    )

    assert res.status_code == 200, res.data.decode()
    assert (
        failure_mode_update_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"][0][
            "failure_modes"
        ]
    )

    res = client.delete(
        url_for(
            "fmea.delete_failure_mode",
            fmea_id=1,
            requirement_id=1,
            failure_mode_id=fmid,
        )
    )
    assert res.status_code == 204
    res = client.get(f"/fmea/1/requirements/1/failure_modes/{fmid}")
    assert res.status_code == 500


########################################################################
good_failure_mode_create = {
    "description": "This is a failure_mode",
    "failure_mode_id": 1,
}
good_failure_mode_retrieve = {
    "id": 1,
    "description": "This is a failure_mode",
    "effects": [],
    "causes": [],
}


@pytest.fixture
def failure_mode_exists(client, requirement_exists) -> dict:
    return client.post(
        url_for("fmea.post_failure_mode", fmea_id=1, requirement_id=1),
        json=good_failure_mode_create,
    ).json


def test_add_effect(client, failure_mode_exists):
    effect_in = {"description": "effect of bad thing", "severity": 2}
    effect_out = effect_in.copy()
    res = client.post(
        url_for("fmea.post_effect", fmea_id=1, requirement_id=1, failure_mode_id=1),
        json=effect_in,
    )

    assert res.status_code == 201, res.data.decode()

    eid = int(res.json["id"])
    effect_out["id"] = eid
    assert (
        effect_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"][0][
            "failure_modes"
        ][0]["effects"]
    )

    effect_update_in = {
        "description": "updated effect of bad thing",
        "severity": 2,
        "id": eid,
    }
    effect_update_out = effect_update_in.copy()
    res = client.put(
        url_for(
            "fmea.update_effect",
            fmea_id=1,
            requirement_id=1,
            failure_mode_id=1,
            effect_id=eid,
        ),
        json=effect_update_in,
    )

    assert res.status_code == 200, res.data.decode()
    assert (
        effect_update_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"][0][
            "failure_modes"
        ][0]["effects"]
    )

    res = client.delete(
        url_for(
            "fmea.delete_effect",
            fmea_id=1,
            requirement_id=1,
            failure_mode_id=1,
            effect_id=eid,
        )
    )
    assert res.status_code == 204
    res = client.get(f"/fmea/1/requirements/1/failure_modes/1/effects/{eid}")
    assert res.status_code == 500


########################################################################
def test_add_cause(client, failure_mode_exists):
    cause_in = {"description": "cause of bad thing", "occurrence": 2, "controls": []}
    cause_out = cause_in.copy()
    res = client.post(
        url_for("fmea.post_cause", fmea_id=1, requirement_id=1, failure_mode_id=1),
        json=cause_in,
    )

    assert res.status_code == 201, res.data.decode()

    cid = int(res.json["id"])
    cause_out["id"] = cid

    assert (
        cause_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"][0][
            "failure_modes"
        ][0]["causes"]
    )

    cause_update_in = {
        "description": "updated cause of bad thing",
        "occurrence": 1,
        "controls": [],
        "id": cid,
    }
    cause_update_out = cause_update_in.copy()
    res = client.put(
        url_for(
            "fmea.update_cause",
            fmea_id=1,
            requirement_id=1,
            failure_mode_id=1,
            cause_id=cid,
        ),
        json=cause_update_in,
    )

    assert res.status_code == 200, res.data.decode()
    assert (
        cause_update_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"][0][
            "failure_modes"
        ][0]["causes"]
    )

    # lazy
    control_in = {"description": "control of cause of bad thing", "detection": 2}
    control_out = control_in.copy()
    res = client.post(
        url_for(
            "fmea.post_control",
            fmea_id=1,
            requirement_id=1,
            failure_mode_id=1,
            cause_id=1,
        ),
        json=control_in,
    )
    assert res.status_code == 201, res.data.decode()

    coid = int(res.json["id"])
    control_out["id"] = coid

    assert (
        control_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"][0][
            "failure_modes"
        ][0]["causes"][cid - 1]["controls"]
    )
    control_update_in = {
        "description": "updated control of cause of bad thing",
        "detection": 3,
        "id": coid,
    }
    control_update_out = control_update_in.copy()
    res = client.put(
        url_for(
            "fmea.update_control",
            fmea_id=1,
            requirement_id=1,
            failure_mode_id=1,
            cause_id=cid,
            control_id=coid,
        ),
        json=control_update_in,
    )

    assert res.status_code == 200, res.data.decode()
    assert (
        control_update_out
        in client.get(url_for("fmea.get_fmea", _id=1)).json["requirements"][0][
            "failure_modes"
        ][0]["causes"][cid - 1]["controls"]
    )

    res = client.delete(
        url_for(
            "fmea.delete_cause",
            fmea_id=1,
            requirement_id=1,
            failure_mode_id=1,
            cause_id=cid,
        )
    )
    assert res.status_code == 204
    res = client.get(f"/fmea/1/requirements/1/failure_modes/1/causes/{cid}")
    assert res.status_code == 500
    res = client.get(
        f"/fmea/1/requirements/1/failure_modes/1/causes{cid}/controls/{coid}"
    )
    assert res.status_code == 500


expected_result = {
    "action_level": 70,
    "id": 1,
    "name": "fmea_test",
    "occurrence_limit": 10,
    "requirements": [
        {
            "description": "requirement 1",
            "failure_modes": [
                {
                    "causes": [
                        {
                            "controls": [
                                {"description": "control 1", "detection": 2, "id": 1}
                            ],
                            "description": "Anchor Bolt Design - Type / topology",
                            "id": 1,
                            "occurrence": 2,
                        }
                    ],
                    "description": "failure mode 1",
                    "effects": [{"description": "effect 1", "id": 1, "severity": 2}],
                    "id": 1,
                },
                {
                    "causes": [
                        {
                            "controls": [
                                {"description": "control 2", "detection": 4, "id": 2}
                            ],
                            "description": "Anchor bolts loose",
                            "id": 2,
                            "occurrence": 4,
                        }
                    ],
                    "description": "failure mode 3",
                    "effects": [{"description": "effect 2", "id": 2, "severity": 3}],
                    "id": 3,
                },
            ],
            "id": 1,
        },
        {
            "description": "requirement 2",
            "failure_modes": [
                {
                    "causes": [
                        {
                            "controls": [
                                {"description": "control 3", "detection": 5, "id": 3}
                            ],
                            "description": "Environmental Compatibility",
                            "id": 3,
                            "occurrence": 4,
                        }
                    ],
                    "description": "failure mode 2",
                    "effects": [{"description": "effect 3", "id": 3, "severity": 4}],
                    "id": 2,
                }
            ],
            "id": 2,
        },
    ],
    "mitigations": [
        {
            "id": 1,
            "description": "miti1",
            "occurrence": 4,
            "severity": 4,
            "detection": 5,
            "cause_id": 3,
            "effect_id": 1,
            "control_id": 3,
            "fmea_id": 1,
        }
    ],
}
expected_result_csv = "name,action_level,occurrence_limit,requirement,failure_mode,effect,severity,cause,occurrence,control,detection,RPN,mitigation_required,mitigation,mitigation_severity,mitigation_occurrence,mitigation_detection,rRPN\r\n\
fmea_test,70,10,requirement 1,failure mode 1,effect 1,2,Anchor Bolt Design - Type / topology,2,control 1,2,8,No,,,,,\r\n\
fmea_test,70,10,requirement 1,failure mode 3,effect 2,3,Anchor bolts loose,4,control 2,4,48,No,,,,,\r\n\
fmea_test,70,10,requirement 2,failure mode 2,effect 3,4,Environmental Compatibility,4,control 3,5,80,Yes,,,,,\r\n"


def test_fmea_import_export(client):
    res = client.post(url_for("fmea.import_fmea"), json=expected_result)
    assert res.status_code == 201, res.data.decode()
    # removed because in the actual reponse the actual object
    #   (Mitigation) instead of just the ids (MitigationCreate) is
    #   present but export file is equal (no deepdiff) to the import
    #   file

    # assert not DeepDiff(
    #     res.json,
    #     expected_result,
    #     ignore_order=True,
    #     significant_digits=3,
    #     exclude_regex_paths=[r".*\['id'\]$"],
    # )
    assert res.json["id"] == 1

    # res = client.get(url_for("fmea.get_fmea", _id=1))
    # assert res.status_code == 200
    # assert not DeepDiff(
    #     res.json,
    #     expected_result,
    #     ignore_order=True,
    #     significant_digits=3,
    #     exclude_regex_paths=[r".*\['id'\]$"],
    # )
    res = client.get("/fmeas/1?download")
    assert res.status_code == 200, res.data.decode()
    assert res.headers["Content-Disposition"] == "attachment; filename=fmea-1.json"
    assert not DeepDiff(
        res.json,
        expected_result,
        ignore_order=True,
        significant_digits=3,
        exclude_regex_paths=[r".*\['id'\]$"],
    )
    res = client.get("/fmeas/1?download_csv")
    assert res.status_code == 200, res.data.decode()
    assert res.headers["Content-Disposition"] == "attachment; filename=fmea-1.csv"
    assert not DeepDiff(
        str(res.data, "utf-8"),
        expected_result_csv,
        ignore_order=True,
        significant_digits=3,
        exclude_regex_paths=[r".*\['id'\]$"],
    )
