# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from typing import List

import pytest
from pydantic import BaseModel
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

from dtop_structinn import actions
from dtop_structinn.app import db_session
from dtop_structinn.app.db import set_database_uri


Base = declarative_base()


class BaseModel(BaseModel):
    class Config:
        orm_mode = True


class CheeseSchemaCreate(BaseModel):
    name: str
    description: str


class CheeseSchema(CheeseSchemaCreate):
    id: int


class CheeseModel(Base):
    __tablename__ = "cheese"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(100))
    description = Column(String(1024))


@pytest.fixture
def cheese_actions() -> actions.Actions:
    set_database_uri("sqlite:///")
    Base.metadata.create_all(bind=db_session.bind)
    return actions.Actions(CheeseModel, CheeseSchema, CheeseSchemaCreate)


def _find_id(dictionary_to_find, candidates_with_ids):
    # Find id of record based by finding strictly one match from list of candidates.
    # Only checks against items that exist in record being searched for.
    match = None
    for candidate in candidates_with_ids:
        if all(candidate[k] == v for k, v in dictionary_to_find.items()):
            assert match is None
            match = candidate["id"]
    assert match is not None
    return match


data_in = {"name": "cheddar", "description": "crunchy and mature, like a good cowpat"}
data_out = {
    "name": "cheddar",
    "description": "crunchy and mature, like a good cowpat",
    "id": 1,
}
data_in_multiple = [
    {"name": "cheddar", "description": "crunchy and mature, like a good cowpat"},
    {"name": "Double Gloucester", "description": "immature, like a fresh cowpat"},
]


@pytest.fixture
def cheese(cheese_actions) -> dict:
    result = cheese_actions.create(data_in)
    assert result == data_out
    return result


@pytest.fixture
def multiple_cheeses(cheese_actions) -> List[dict]:
    results = cheese_actions.create(data_in_multiple)
    for data_in, data_out in zip(data_in_multiple, results):
        data_out.pop("id")
        assert data_in == data_out
    return results


def test_get_one(cheese_actions, cheese):
    assert cheese_actions.get(1) == data_out


def test_update_one(cheese_actions, cheese):
    new_name = "spuriously strong cheddar"
    update_dict = {"id": 1, "name": new_name}
    cheese["name"] = new_name
    assert cheese_actions.update(update_dict) == cheese


def test_delete_one(cheese_actions, cheese):
    cheese_actions.delete(1)
    with pytest.raises(actions.InvalidInput):
        cheese_actions.get(1)


def test_multiple_get_all(cheese_actions, multiple_cheeses):
    data_out_all = cheese_actions.get()
    for data_in, data_out in zip(data_in_multiple, data_out_all):
        data_out.pop("id")
        assert data_in == data_out


def test_multiple_get_invalid_id(cheese_actions, multiple_cheeses):
    data_out_all = cheese_actions.get()
    data_out_ids = [i["id"] for i in data_out_all]
    data_out_ids.append(max(data_out_ids) + 1)
    with pytest.raises(actions.InvalidInput):
        cheese_actions.get(data_out_ids)


def test_multiple_get(cheese_actions, multiple_cheeses):
    data_out_all = cheese_actions.get()
    data_out_all.sort(key=lambda x: x["id"])
    del data_out_all[2:]
    data_out_multiple = cheese_actions.get([i["id"] for i in data_out_all])
    assert data_out_all == data_out_multiple


def test_multiple_update(cheese_actions, multiple_cheeses):
    data_out_all = cheese_actions.get()
    data_out_all.sort(key=lambda x: x["id"])
    replacement_names = ("spuriously strong cheddar", "Triple Cheltenham")
    for data, name in zip(data_out_all, replacement_names):
        data["name"] = name
    data_update = data_out_all[: len(replacement_names)]
    data_out_multiple = cheese_actions.update(data_update)
    assert data_out_multiple == data_out_all
    data_out_update = cheese_actions.get()
    assert data_out_update == data_out_all


def test_multiple_update_missing_id(cheese_actions, multiple_cheeses):
    data_out_all = cheese_actions.get()
    data_out_all[1].pop("id")
    with pytest.raises(actions.InvalidInput):
        cheese_actions.update(data_out_all)


def test_multiple_update_invalid_id(cheese_actions, multiple_cheeses):
    data_out_all = cheese_actions.get()
    ids = set(i["id"] for i in data_out_all)
    invalid_id = min(i for i in range(min(ids), max(ids) + 2) if i not in ids)
    data_out_all[1]["id"] = invalid_id
    with pytest.raises(actions.InvalidInput):
        cheese_actions.update(data_out_all)


def test_multiple_delete(cheese_actions, multiple_cheeses):
    data_out_all = cheese_actions.get()
    assert (data_out_all) != 0
    cheese_actions.delete([i["id"] for i in data_out_all])
    assert len(cheese_actions.get()) == 0


def test_context_create_exception(cheese_actions):
    data_in1, data_in2 = data_in_multiple
    try:
        with cheese_actions.create_contextual(data_in1) as data_out1:
            data_out1.pop("id")
            assert data_in1 == data_out1
            data_out2 = cheese_actions.create(data_in2)
            data_out2.pop("id")
            assert data_in2 == data_out2
            raise Exception
    except Exception:
        pass
    assert len(cheese_actions.get()) == 0


def test_context_create_success(cheese_actions):
    data_in1, data_in2 = data_in_multiple
    with cheese_actions.create_contextual(data_in1) as data_out1:
        data_out1.pop("id")
        assert data_in1 == data_out1
        data_out2 = cheese_actions.create(data_in2)
        data_out2.pop("id")
        assert data_in2 == data_out2
    data_out = cheese_actions.get()
    for i in data_out:
        i.pop("id")
    assert data_in_multiple == data_out
