# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

def test_targets(client):
    # Create analysis with solutions
    qfd = client.post(
        "/qfds/",
        json={"name": "API", "objective": "Test endpoints"},
    ).json["id"]
    fr = client.post(
        f"/qfds/{qfd}/functional_requirements/",
        json={
            "description": "Kennel capacity",
            "direction": 1,
            "target": 10,
            "units": "dogs",
            "engineering_difficulty": 5,
            "delivery_difficulty": 1,
        },
    ).json["id"]
    client.post(
        f"/qfds/{qfd}/solutions/",
        json={
            "description": "Wood",
            "achievements": [
                {"functional_requirement_id": fr, "value": 20},
            ],
        },
    )

    expected_target = {
        "functional_requirement": "Kennel capacity",
        "units": "dogs",
        "value": 10,
        "direction": "Higher",
    }

    res = client.get(f"/api/{qfd}/targets")
    assert res.status_code == 200
    assert res.json == {"targets": [expected_target]}
    assert client.get("/api/9/targets").status_code == 404

    res = client.get(f"/api/{qfd}/solutions")
    assert res.status_code == 200
    assert res.json == {
        "solutions": [
            {
                "description": "Wood",
                "achievements": [{"target": expected_target, "value": 20}],
            }
        ]
    }
    assert client.get("/api/9/solutions").status_code == 404
