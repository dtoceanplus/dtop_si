# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_structinn.app import create_app
from deepdiff import DeepDiff


expected = [
    {
        "functional_requirement_id": 1,
        "worst_conflicts": [
            {
                "contradictions": [
                    {"improving": 1, "principle": 15, "worsening": 3},
                    {"improving": 1, "principle": 8, "worsening": 3},
                    {"improving": 1, "principle": 29, "worsening": 3},
                    {"improving": 1, "principle": 34, "worsening": 3},
                    {"improving": 9, "principle": 13, "worsening": 3},
                    {"improving": 9, "principle": 14, "worsening": 3},
                    {"improving": 9, "principle": 8, "worsening": 3},
                    {"improving": 26, "principle": 29, "worsening": 3},
                    {"improving": 26, "principle": 14, "worsening": 3},
                    {"improving": 26, "principle": 35, "worsening": 3},
                    {"improving": 26, "principle": 18, "worsening": 3},
                    {"improving": 2, "principle": 10, "worsening": 4},
                    {"improving": 2, "principle": 1, "worsening": 4},
                    {"improving": 2, "principle": 29, "worsening": 4},
                    {"improving": 2, "principle": 35, "worsening": 4},
                ],
                "functional_requirement_id": 2,
            }
        ],
    },
    {
        "functional_requirement_id": 2,
        "worst_conflicts": [
            {
                "contradictions": [
                    {"improving": 3, "principle": 8, "worsening": 1},
                    {"improving": 3, "principle": 15, "worsening": 1},
                    {"improving": 3, "principle": 29, "worsening": 1},
                    {"improving": 3, "principle": 34, "worsening": 1},
                    {"improving": 4, "principle": 35, "worsening": 2},
                    {"improving": 4, "principle": 28, "worsening": 2},
                    {"improving": 4, "principle": 40, "worsening": 2},
                    {"improving": 4, "principle": 29, "worsening": 2},
                    {"improving": 3, "principle": 13, "worsening": 9},
                    {"improving": 3, "principle": 4, "worsening": 9},
                    {"improving": 3, "principle": 8, "worsening": 9},
                    {"improving": 3, "principle": 29, "worsening": 26},
                    {"improving": 3, "principle": 35, "worsening": 26},
                ],
                "functional_requirement_id": 1,
            }
        ],
    },
]


def test_correlation_defaults():

    app = create_app({"TESTING": True, "SQLALCHEMY_DATABASE_URI": "sqlite://"})
    with app.test_client() as client:

        res = client.post(
            "/qfds/",
            json={"name": "A simple analysis", "objective": "To do some testing"},
        )
        assert res.status_code == 201, res.data.decode()
        qfd = res.json["id"]

        res = client.post(
            f"/qfds/{qfd}/requirements/",
            json={"description": "First requirement", "importance": 1},
        )
        assert res.status_code == 201, res.data.decode()
        r1 = res.json["id"]

        res = client.post(
            f"/qfds/{qfd}/requirements/",
            json={"description": "Second requirement", "importance": 2},
        )
        assert res.status_code == 201, res.data.decode()
        r2 = res.json["id"]

        res = client.post(
            f"/qfds/{qfd}/functional_requirements/",
            json={
                "description": "First functional requirement",
                "direction": 1,
                "target": 23,
                "units": "elephants",
                "engineering_difficulty": 2,
                "delivery_difficulty": 5,
            },
        )
        assert res.status_code == 201, res.data.decode()
        fr1 = res.json["id"]

        res = client.post(
            f"/qfds/{qfd}/functional_requirements/",
            json={
                "description": "Second functional requirement",
                "direction": 1,
                "target": 99,
                "units": "red balloons",
                "engineering_difficulty": 4,
                "delivery_difficulty": 2,
            },
        )
        assert res.status_code == 201, res.data.decode()
        fr2 = res.json["id"]

        res = client.post(
            f"/qfds/{qfd}/functional_requirements/",
            json={
                "description": "Third functional requirement",
                "direction": -1,
                "target": 999,
                "units": "emergencies",
                "engineering_difficulty": 2,
                "delivery_difficulty": 2,
            },
        )
        assert res.status_code == 201, res.data.decode()
        fr3 = res.json["id"]

        res = client.post(
            f"/qfds/{qfd}/functional_requirements/{fr1}/triz_classes/",
            json={"triz": [1, 2, 9, 26]},
        )
        assert res.status_code == 200, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/functional_requirements/{fr2}/triz_classes/",
            json={"triz": [3, 4]},
        )
        assert res.status_code == 200, res.data.decode()

        # Correlation between fr2 and fr3 not added to test default value, where behaviour should be the same as {"correlation": 0}, i.e. no 'worst conflicts'
        res = client.post(
            f"/qfds/{qfd}/functional_requirements/correlations/",
            json={
                "correlation": -1,
                "functional_id": fr1,
                "functional_requirement_id": fr2,
            },
        )
        assert res.status_code == 201, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/functional_requirements/correlations/",
            json={
                "correlation": 0,
                "functional_id": fr1,
                "functional_requirement_id": fr3,
            },
        )
        assert res.status_code == 201, res.data.decode()

        res = client.post(
            f"/qfds/{qfd}/requirements/{r1}/impacts/",
            json={"functional_requirement_id": fr1, "impact": 1},
        )
        assert res.status_code == 201, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/requirements/{r1}/impacts/",
            json={"functional_requirement_id": fr2, "impact": 4},
        )
        assert res.status_code == 201, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/requirements/{r2}/impacts/",
            json={"functional_requirement_id": fr1, "impact": 4},
        )
        assert res.status_code == 201, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/requirements/{r2}/impacts/",
            json={"functional_requirement_id": fr2, "impact": 9},
        )
        assert res.status_code == 201, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/requirements/{r1}/impacts/",
            json={"functional_requirement_id": fr3, "impact": 4},
        )
        assert res.status_code == 201, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/requirements/{r2}/impacts/",
            json={"functional_requirement_id": fr3, "impact": 4},
        )
        assert res.status_code == 201, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/solutions/",
            json={
                "description": "The most awesome solution ever",
                "achievements": [
                    {"functional_requirement_id": fr1, "value": 102},
                    {"functional_requirement_id": fr2, "value": 4},
                    {"functional_requirement_id": fr3, "value": 17},
                ],
            },
        )
        assert res.status_code == 201, res.data.decode()
        res = client.post(
            f"/qfds/{qfd}/solutions/",
            json={
                "description": "Potatoes",
                "achievements": [
                    {"functional_requirement_id": fr1, "value": 42},
                    {"functional_requirement_id": fr2, "value": 42},
                    {"functional_requirement_id": fr3, "value": 42},
                ],
            },
        )
        assert res.status_code == 201, res.data.decode()

        res = client.get(f"/qfds/{qfd}/report")
        assert res.status_code == 200, res.data.decode()

        conflicts = res.json["functional_requirements_conflicts"]
        for fr in conflicts:
            for wc in fr["worst_conflicts"]:
                wc["contradictions"] = [
                    {k: v["id"] for k, v in c.items() if k != "id"}
                    for c in wc["contradictions"]
                ]
        assert not DeepDiff(
            conflicts, expected, exclude_regex_paths=[r"\['id'\]$"], ignore_order=True
        )
