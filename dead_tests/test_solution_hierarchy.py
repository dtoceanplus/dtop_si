# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import json

import pytest
from deepdiff import DeepDiff

from dtop_structinn.app import pyqfd
from dtop_structinn.pyqfd.schemas import (
    SolutionHierarchy,
    HierarchyFunctionalRequirement,
)
from dtop_structinn.pyqfd.solution_hierarchy import (
    get_solution_hierarchies,
    get_solution_hierarchy,
    get_suggestions,
    persist_solution_hierarchy,
)


@pytest.fixture
def expected_suggestions():
    with open("tests/expected_suggestions.json") as fd:
        return json.load(fd)


@pytest.fixture
def solution_hierarchy():
    with open("tests/solution_hierarchy.json") as fd:
        return json.load(fd)


@pytest.fixture
def analysis_with_hierarchy():
    with open("tests/qfd_with_solution_hierarchy.json") as fd:
        return pyqfd.create_qfd_recursively(json.load(fd))


def test_solution_hierarchy(app, solution_hierarchy):
    """Tests storage and retrieval of solution hierarchies.

    Given a solution hierarchy, test that it can be added and retrieved.

    Tests the following functions:

        * persist_solution_hierarchy
        * get_solution_hierarchy
        * get_solution_hierarches
    """
    persist_solution_hierarchy(solution_hierarchy)

    uuid = solution_hierarchy["uuid"]

    retrieved = get_solution_hierarchy(uuid)
    retrieved_multiple = get_solution_hierarchies()

    assert not DeepDiff(SolutionHierarchy(**solution_hierarchy).dict(), retrieved)
    assert any(
        not DeepDiff(SolutionHierarchy(**solution_hierarchy).dict(), sh)
        for sh in retrieved_multiple
    )


def test_get_suggestions(expected_suggestions, analysis_with_hierarchy):
    """Tests getting suggestions from the solution hierarchy.

    Given a QFD Analysis which is using the solution hierarchy, test
    that suggestions are correctly given for functional requirements.
    """
    assert not DeepDiff(
        [HierarchyFunctionalRequirement(**x).dict() for x in expected_suggestions],
        get_suggestions(analysis_with_hierarchy["id"]),
        exclude_regex_paths=r"'children'",
        ignore_order=True,
    )
