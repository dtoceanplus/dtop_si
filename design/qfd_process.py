# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import requests
import json


def post(where, data):
    base_url = "http://127.0.0.1:5000/"
    r = requests.post(base_url + where, json=data)
    return r.status_code, json.loads(r.text)


def put(where, data):
    base_url = "http://127.0.0.1:5000/"
    r = requests.post(base_url + where, json=data)
    return r.status_code, json.loads(r.text)


s, r = post("qfds", {"name": "This is a test", "objective": "What I want to achieve"})

analysis_id = r["id"]

_requirements = [
    {"description": "First requirement"},
    {"description": "Second requirement"},
]

requirements = []

for idx, requirement in enumerate(_requirements):
    requirement["order"] = idx
    if hasattr(requirement, "id"):
        s, r = post(
            f"qfds/{analysis_id}/requirements/", requirement
        )  # the analysis id comes from the URL in the flask layer
    else:
        s, r = put(f"qfds/{analysis_id}/requirements/{requirement.id}", requirement)

    requirements.append(r)


_functional_requirements = [
    {
        "description": "FR1",
        "direction": "up",
        "target": 10,
        "units": "kWh",
        "engineering_difficulty": 10,
        "delivery_difficulty": 4,
    },
    {
        "description": "FR2",
        "direction": "up",
        "target": 20,
        "units": "£",
        "engineering_difficulty": 1,
        "delivery_difficulty": 7,
    },
]

functional_requirements = []

# Question: is this being stored linked to requirement or analysis? Want user to think about all requirements
#           but we then only care about the set...
for func_requirement in enumerate(_func_requirements):
    if hasattr(requirement, "id"):
        s, f = post(f"qfds/{analysis_id}/functional_requirements/", func_requirement)
    else:
        s, f = put(
            f"qfds/{analysis_id}/functional_requirements/{func_requirement.id}",
            func_requirement,
        )
    func_requirements.append(f)


# ignoring TRIZ classes for a moment
fr1 = functional_requirements[0]
fr2 = functional_requirements[1]

correlation = {
    "functional_requirement": fr1,
    "correlations": {fr2: -4},
}  # as FR1 goes up FR2 goes down by magnitude 4
post(
    f"qfds/{analysis_id}/functional_requirements/correlations/", correlation
)  # the backend converts this to the conflict value

correlation = {
    "functional_requirement": fr2,
    "correlations": {fr1: -9},
}  # if this happens, the line above is 'overwritten'
post(f"qfds/{analysis_id}/functional_requirements/correlations/", correlation)

a_solution = {"name": "My solution", "achievements": {fr1: 3, fr2: 12}}
post(f"qfds/{analysis_id}/solutions/", a_solution)

b_solution = {"name": "Their solution", "achievements": {fr1: 13, fr2: 2}}
post(f"qfds/{analysis_id}/solutions/", b_solution)


levels_of_impact = [
    {"requirement": requirements[0], "impacts": {fr1: 4, fr2: 0}},
    {"requirement": requirements[1], "impacts": {fr1: 1, fr2: 9}},
]

for level_of_impact in levels_of_impact:
    post(
        f"qfds/{analysis_id}/impacts", level_of_impact
    )  # alternatively: requirement not in the data passed, but in the url?
