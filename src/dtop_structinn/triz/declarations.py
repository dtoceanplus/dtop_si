# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

triz_class_groups = {
    "Geometric": {
        3: "Length of moving object",
        4: "Length of stationary object",
        5: "Area of moving object",
        6: "Area of stationary object",
        7: "Volume of moving object",
        8: "Volume of stationary object",
        12: "Shape",
    },
    "Physical": {
        1: "Weight of moving object",
        2: "Weight of stationary object",
        9: "Speed",
        10: "Force (Intensity)",
        11: "Stress or pressure",
        17: "Temperature",
        18: "Illumination intensity",
        21: "Power",
    },
    "Resource": {
        19: "Use of energy by moving object",
        20: "Use of energy by stationary object",
        22: "Loss of Energy",
        23: "Loss of Substance",
        24: "Loss of Information",
        25: "Loss of Time",
        26: "Quantity of substance",
    },
    "Capacity": {
        13: "Stability of the object's composition",
        14: "Strength",
        15: "Duration of action of moving object",
        16: "Duration of action of stationary object",
        27: "Reliability",
        32: "Ease of manufacture",
        34: "Ease of repair",
        35: "Adaptability or versatility",
        39: "Productivity",
    },
    "Harmful": {
        30: "Object-affected harmful factors",
        31: "Object-generated harmful factors",
    },
    "Operational": {
        28: "Measurement accuracy",
        29: "Manufacturing precision",
        33: "Ease of operation",
        36: "Device complexity",
        37: "Difficulty of detecting and measuring",
        38: "Extent of automation",
    },
}

inventive_principles = {
    1: {
        "description": "Segmentation",
        "definition": "Divide into independent parts, make objects sectional, increase degree of segmentation",
        "example": "Modular furniture",
        "marine": "turbine blades in connectable modules",
    },
    2: {
        "description": "Separation or extraction",
        "definition": "Extract the problem, extract the needed element or function",
        "example": "Outsourcing, containment zones for dangerous substances",
        "marine": "",
    },
    3: {
        "description": "Local quality",
        "definition": "Different properties rather than homogeneous, segmented parts operate under own rules",
        "example": "Surface coatings for part of component rather than the whole",
        "marine": "turbine blade leading edge protection",
    },
    4: {
        "description": "Asymmetry",
        "definition": "Replace symmetry, increase degree of asymmetry",
        "example": "mixer with asymmetry to give turbulence",
        "marine": "WEC floats, blade sections, sub-frame stringers using prime number spacing",
    },
    5: {
        "description": "Merging or combining",
        "definition": "Join similar objects and functions, Bundle multiples, merge dissimilar object to make new object",
        "example": "Radio and Alarm into a single unit",
        "marine": "Bearing cooler acts as a heat sink",
    },
    6: {
        "description": "Multi-functionality",
        "definition": "performs additional functions without hindrance",
        "example": "universal power supply",
        "marine": "generator acts as a brake",
    },
    7: {
        "description": "Nested",
        "definition": "Split structure or function to save space",
        "example": "Telescopic aerials, coaxial cables",
        "marine": "telescopic turbine blade spoilers",
    },
    8: {
        "description": "Counter-weight",
        "definition": "Balance asymmetrical forces and couples",
        "example": "Gas springs for lifting, air-lift bags",
        "marine": "air-lift bags for recovery and installation, balanced blade arrangements",
    },
    9: {
        "description": "Preliminary counter-action",
        "definition": "Counter stress in advance, like pre-loading",
        "example": "pretention moorings, fastener bolts etc",
        "marine": "pre-loading of polymer seals for longer life",
    },
    10: {
        "description": "Preliminary action",
        "definition": "In advance of function, do part or whole action or function",
        "example": "perforation, pre-selection, ",
        "marine": "install blades before turbine installation",
    },
    11: {
        "description": "Preliminary Compensation",
        "definition": "For high occurrence, prepare, provide arrangement to negate problem",
        "example": "Crash airbags, barriers, relief valves, reduce severity",
        "marine": "slipping clutches for torque overloads",
    },
    12: {
        "description": "Equipotential",
        "definition": "Change process for level action, work on flat surfaces, reduce bends and contours",
        "example": "Flat floors over large areas rather than factories on many floors, electrical grounding",
        "marine": "OWC for wave in existing structures",
    },
    13: {
        "description": "Other way around",
        "definition": "Reverse usual action, use opposite features, change part or function, go back to front",
        "example": "Pull, not push; go stiff not soft, turn right not left",
        "marine": "buoyant nacelles, tensegrity foundations",
    },
    14: {
        "description": "Curve increase",
        "definition": "Change lines to arcs, squares to circles, cubes to spheres",
        "example": "domed pressure vessels, turn switches rather than push, ",
        "marine": "blade curvature to offset deflection due to thrust",
    },
    15: {
        "description": "Dynamism",
        "definition": "Add adjustment, movability, ",
        "example": "lift strops that adjust for length, tilting to compensate for cornering",
        "marine": "Adjustable blade sections, flow adjustment devices",
    },
    16: {
        "description": "Partial or excessive action",
        "definition": "Provide more than needed and remove later, provide less than needed and top up later",
        "example": "Coating by immersion and remove by shaking, construction lines, rough balance with large weights, then trim",
        "marine": "rotors machined to size, ",
    },
    17: {
        "description": "Moving to another dimension",
        "definition": "Move to a higher or lower dimension",
        "example": "Car parking along sty (1d), to a square (2d) to a multi-storey car park (3d)",
        "marine": "WEC floats that act in heave and sway",
    },
    18: {
        "description": "Mechanical vibration",
        "definition": "Set structures vibration level or noise",
        "example": "powder dispensing by vibration, TV screen acts as speaker",
        "marine": "use shaker to remove biofouling, WEC floats in resonance",
    },
    19: {
        "description": "Periodic action",
        "definition": "Pulsing or vibration spacing or periods",
        "example": "warning light that flash attract more attention, PWM in diesel injectors",
        "marine": "Periodic cleaning of hulls, blade pitch adjustments to give lower turbulence",
    },
    20: {
        "description": "Continuity of useful action",
        "definition": "Operate at max power, remove idling and transitions",
        "example": "continuous operations at full speed (manufacturing lines?)",
        "marine": "rate for max power, and then load shed anything above that",
    },
    21: {
        "description": "Hurry Through",
        "definition": "Performa harmful or useless operations quickly",
        "example": "UHT pasteurisation, rapid prototypes",
        "marine": "Speed up through resonance",
    },
    22: {
        "description": "Blessing in disguise (harm to benefit)",
        "definition": "Turn harm to good",
        "example": "Make use of delay to do other useful work, post-it notes for weak glue, crack sintered connecting rods",
        "marine": "",
    },
    23: {
        "description": "Feedback",
        "definition": "Introduce feedback, enhance or change existing feedback",
        "example": "Heating systems with learning / timers, monitoring and maintenance",
        "marine": "CBM or in-process testing",
    },
    24: {
        "description": "Intermediary",
        "definition": "Temporary joints or functions ",
        "example": "Assembly aids, files attached to emails",
        "marine": "lift airbags for lowering to sea floor",
    },
    25: {
        "description": "Self-service",
        "definition": "Self-operation by aux functions, self-righting lifeboats",
        "example": "self-winding watches",
        "marine": "Self-repairing systems",
    },
    26: {
        "description": "Copying",
        "definition": "Use cheap copy of original, ",
        "example": "money rather than gold, compressed thumbnails, video rather than visit",
        "marine": "patterns from original prototype",
    },
    27: {
        "description": "Cheap disposable",
        "definition": "Replace robust originals with weaker ",
        "example": "Disposable plates, one-use cameras",
        "marine": "sacrificial anodes",
    },
    28: {
        "description": "Substitution of Mechanical system",
        "definition": "Replace mechanical system with electrical, optical or radiation",
        "example": "Magnetic couplings, Optical sensors",
        "marine": "",
    },
    29: {
        "description": "Pneumatics or hydraulics",
        "definition": "Replace solids with gas or liquids",
        "example": "Air tools, water jets for cutting, inflatable boats",
        "marine": "",
    },
    30: {
        "description": "Flexible films or membranes",
        "definition": "Use flexibility and fine thickness membranes",
        "example": "thin-walled drinks cans held rigid by internal pressure",
        "marine": "Nacelle structures held rigid by auxiliary structures like brake reaction rings",
    },
    31: {
        "description": "Porous materials",
        "definition": "Make the structure porous, or fill holes in porous structures with a dissimilar material",
        "example": "Filters with holes containing charcoal, aerogels with compressive strength, but little mass",
        "marine": "Blade structures with holes filled with resin",
    },
    32: {
        "description": "Optical changes",
        "definition": "Change of colours, brightness or transparency",
        "example": "polarisation, matt or gloss surfaces",
        "marine": "surface coatings on floats to aid visibility",
    },
    33: {
        "description": "Homogeneity",
        "definition": "introduce conformity of key features when there is strong interaction",
        "example": "Hot objects supported by a hot surface to prevent cooling",
        "marine": "Water lubricated bearings need no seals",
    },
    34: {
        "description": "Recycling (rejecting and regenerating)",
        "definition": "Biogradability once used, discarded once used - to be recovered and re-used",
        "example": "pallets for moving heavy stuff, containerisation, assembly jigs",
        "marine": "installation jigs, seals for cooling outlets ",
    },
    35: {
        "description": "Physical or chemical properties",
        "definition": "Change from solid to liquid to gas, freeze rather than heat, use intermediary states like bi-phase or elastic states",
        "example": "transport gas in liquid form, move water as solid ice, change concentrations, eutectic mixtures",
        "marine": "",
    },
    36: {
        "description": "Use phase changes",
        "definition": "use properties that change in phase changes, volume, heat capacity, shape etc. ",
        "example": "Heat pipes, stored energy liquid crystal displays",
        "marine": "heat pipes for cooling purposes",
    },
    37: {
        "description": "Thermal expansion",
        "definition": "Use expansion and contraction properties of materials",
        "example": "Fitting metal tyres to metal wheels with strong interference fit",
        "marine": "",
    },
    38: {
        "description": "Strong oxidants",
        "definition": "Use enriched air, O2 or Ozone O3, or even special gasses to aid the function",
        "example": "bleaching and cleaning using O2, speeding up chemical reactions",
        "marine": "biofouling removal?",
    },
    39: {
        "description": "Inert environment",
        "definition": "Remove O2 and replace with inert gas, work in a vacuum",
        "example": "Use nitrogen to prevent oxidation, inert gases to prevent combustion",
        "marine": "prevent fire in remote structures with nitrogen atmosphere",
    },
    40: {
        "description": "Composite materials",
        "definition": "combine two or more materials to provide improved characteristics",
        "example": "Reinforced concrete and rubber (tyres) note does not have to be in layers",
        "marine": "blades and active structures, dielectric materials",
    },
}
