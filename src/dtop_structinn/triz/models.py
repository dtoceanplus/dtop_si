# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy import CheckConstraint, Column, Integer, String

from dtop_structinn.database import Base
from dtop_structinn.declarations import DESCRIPTION_LENGTH
from dtop_structinn.triz.constants import MAX_TRIZ_CLASS, MAX_TRIZ_PRINCIPLE


class TrizClass(Base):
    """SQLAlchemy ORM model for a TRIZ class (aka contradiction).

    Attributes:
        id: The number of the class (there are 39).
        definition: The name of the class.
    """

    __tablename__ = "qfd_triz_class"
    id = Column(
        Integer,
        CheckConstraint(f"id>{MAX_TRIZ_CLASS}", name="contradiction_limit"),
        primary_key=True,
        index=True,
    )
    definition = Column(String(DESCRIPTION_LENGTH))


class InventivePrinciple(Base):
    """SQLAlchemy ORM model for a TRIZ inventive principle.

    Attributes:
        id: The number of the TRIZ inventive principle (there are 40).
        description: The name of the inventive principle.
        definition: An explanation of what the inventive principle is.
        example: A general example of the principle in practice.
        marine: A marine energy example of the principle in practice.
    """

    __tablename__ = "qfd_inventive_principle"
    id = Column(
        Integer,
        CheckConstraint(f"id>{MAX_TRIZ_PRINCIPLE}", name="principle_limit"),
        primary_key=True,
        index=True,
    )
    description = Column(String(DESCRIPTION_LENGTH))
    definition = Column(String(DESCRIPTION_LENGTH))
    example = Column(String(DESCRIPTION_LENGTH))
    marine_example = Column(String(DESCRIPTION_LENGTH))
