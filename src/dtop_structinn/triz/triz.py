# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from typing import Dict, List, Optional
from pydantic import parse_file_as
from dtop_structinn.triz.schemas import inventive_principle_id, triz_id

triz_dir = Path(__file__).parent

contradiction_matrix = parse_file_as(
    Dict[int, Dict[int, Optional[List[int]]]], triz_dir / "contradiction_matrix.json"
)


def get_principles_from_contradiction_matrix(
    improving: triz_id, worsening: triz_id
) -> List[inventive_principle_id]:
    """Return the inventive principle ids to address a contradiction.

    Args:
        improving: the TRIZ class number of the improving feature.
        worsening: the TRIZ class number of the worsening feature.

    Returns:
        The TRIZ inventive principle numbers that could address the
        contradiction.
    """
    if improving == worsening:
        # Is this the correct behaviour, or do we want to raise an
        # exception?
        return []

    # Replace this with a lookup in the contradiction matrix.
    return contradiction_matrix[improving][worsening]


classes = {
    1: "Weight of moving object",
    2: "Weight of static object",
    3: "Length of moving object",
    4: "Length of static object",
    5: "Area of moving object",
    6: "Area of static object",
    7: "Volume of moving object",
    8: "Volume of static object",
    9: "Speed",
    10: "Force",
    11: "Tension, pressure",
    12: "Shape",
    13: "Stability of object",
    14: "Strength",
    15: "Durability of moving object",
    16: "Durability of static object",
    17: "Temperature",
    18: "Brightness",
    19: "Energy spent by moving object",
    20: "Energy spent by static object",
    21: "Power",
    22: "Energy Losses",
    23: "Material Losses",
    24: "Information losses",
    25: "Time Losses",
    26: "Amount of substance",
    27: "Reliability",
    28: "Accuracy of measurement",
    29: "Accuracy of manufacturing",
    30: "Harmful factors acting on object",
    31: "Harmful side effects",
    32: "Manufacturability",
    33: "Convenience of use",
    34: "Repairability",
    35: "Adaptability",
    36: "Complexity of device",
    37: "Complexity of control",
    38: "Level of automation",
    39: "Productivity",
}

principles = {
    1: "Segmentation",
    2: "Separation or extraction",
    3: "Local quality",
    4: "Asymmetry",
    5: "Merging or combining",
    6: "Universality",
    7: "Nesting dolls",
    8: "Counter-weight",
    9: "Preliminary counter-action",
    10: "Preliminary action",
    11: "Previously placed pillow",
    12: "Equipotential",
    13: "Other way around",
    14: "Spherical shapes",
    15: "Dynamism",
    16: "Partial or excessive action",
    17: "Moving to another dimension",
    18: "Mechanical vibration",
    19: "Periodic action",
    20: "Continuity of useful action",
    21: "Rushing through",
    22: "Blessing in disguise (harm to benefit)",
    23: "Feedback",
    24: "Intermediary",
    25: "Self-service",
    26: "Copying",
    27: "Cheap disposable",
    28: "Replace a mechanical system",
    29: "Pneumatics or hydraulics",
    30: "Flexible films or membranes",
    31: "Porous materials",
    32: "Optical changes",
    33: "Homogeneity",
    34: "Recycling (rejecting and regenerating)",
    35: "Physical or chemical properties",
    36: "Use phase changes",
    37: "Thermal expansion",
    38: "Strong oxidants",
    39: "Inert environment",
    40: "Composite materials",
}
