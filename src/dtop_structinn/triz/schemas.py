# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from typing import TYPE_CHECKING
from pydantic import BaseModel, conint
from dtop_structinn.triz.constants import MAX_TRIZ_CLASS, MAX_TRIZ_PRINCIPLE

if TYPE_CHECKING:
    triz_id = int
    inventive_principle_id = int
else:
    triz_id = conint(gt=0, le=MAX_TRIZ_CLASS)
    inventive_principle_id = conint(gt=0, le=MAX_TRIZ_PRINCIPLE)


class BaseSchema(BaseModel):
    """Base model for other schemas."""

    pass


class TrizClass(BaseSchema):
    id: triz_id
    description: str


class InventivePrinciple(BaseSchema):
    id: inventive_principle_id
    description: str
    definition: str
    example: str
    marine_example: str
