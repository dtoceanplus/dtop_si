# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Contains the route which provides the API user with suggestion.

Sources for suggestions are specified here, along with their label and
unique identifying string.
"""

from typing import List

from flask import Blueprint

from dtop_structinn.suggestions.schemas import (
    SuggestionRequest,
    SuggestionGroup,
)
from dtop_structinn.suggestions import solution_hierarchy, stagegate, qfd, fmea, library

from dtop_structinn.app.decorateroutes import pydantically

from dtop_structinn.pyqfd.models import Qfd
from dtop_structinn.pyfmea.models import Fmea

bp = Blueprint("suggestions", __name__)

sources = [
    {
        "label": "Solution Hierarchy",
        "identifier": "solution_hierarchy",
        "function": solution_hierarchy,
    },
    {"label": "Stage Gate", "identifier": "stagegate", "function": stagegate},
    {"label": "QFD", "identifier": "qfd", "function": qfd},
    {"label": "FMEA", "identifier": "fmea", "function": fmea},
    {"label": "Library", "identifier": "library", "function": library},
]


@bp.route("/", methods=["POST"])
@pydantically(SuggestionRequest, List[SuggestionGroup])
def get_suggestions(data):
    """Return suggestions gathered from provider functions.

    See the dtop_structinn.suggestions documentation for details of
    the suggestion format.

    The data passed to this function should contain:
        * analysis_id: the id of the analysis
        * analysis_type: "qfd" or "fmea"
        * entity: the type of object suggestions are for
        * field: the field the user needs suggestions for
        * is_new: whether this entity is in the database yet **OR**
        * identifier: a dict containing whatever identifies this type of
            entity
        * context: any other information a provider might find useful

    The identifier is a dict rather than a simple type because some
    entities, e.g. achievements, are identified by a combination of
    fields.
    """
    analysis_id = data["analysis_id"]
    analysis_type = data["analysis_type"]
    entity = data["entity"]
    field = data["field"]
    is_new = data["is_new"]
    identifier = data["identifier"]
    context = data["context"]

    id = analysis_id

    if analysis_type == "qfd":
        analysis = Qfd.one(id=id)
    else:
        analysis = Fmea.one(id=id)

    suggestions = []
    for source in sources:  # for all the difference data sources
        res = source["function"](
            analysis_type, analysis, entity, field, identifier, is_new, context
        )
        if res is not None:
            suggestions.append(
                {**source, "source": source["identifier"], "suggestions": res}
            )
    return suggestions
