# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Routes for TRIZ."""
from flask import Blueprint

from dtop_structinn.app.decorateroutes import pydantically
from dtop_structinn.triz import declarations

bp = Blueprint("triz", __name__)


@bp.route("groups", methods=["GET"])
@pydantically(None, dict)
def get_triz_class_groups():
    """Return triz class groups."""
    return declarations.triz_class_groups


@bp.route("principles", methods=["GET"])
@pydantically(None, dict)
def get_suggested_inventive_principles():
    """Return triz class groups."""
    return declarations.inventive_principles
