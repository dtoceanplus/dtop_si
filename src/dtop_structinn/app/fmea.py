# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Routes for FMEA."""

from typing import List
import json

from flask import Blueprint, send_file

from dtop_structinn.app.decorateroutes import pydantically
from dtop_structinn.app.errors import NoSuchEntity

from dtop_structinn.pyfmea import models
from dtop_structinn.pyfmea.file_schemas import AnalysisCurrent, SupportedAnalysisSchemas
from dtop_structinn.pyfmea.schemas import (
    Analysis,
    DesignRequirement,
    Control,
    Cause,
    Effect,
    FailureMode,
    Mitigation,
    RpnReport,
)
from dtop_structinn.notes.schemas import Note
from dtop_structinn.types import Fragment

from dtop_structinn.pyfmea.import_export import import_analysis
from dtop_structinn.pyfmea import validation, report

bp = Blueprint("fmea", __name__)


@bp.route("/<int:id>", methods=["GET"])
@pydantically(None, Analysis)
def get_one_fmea(id):
    """Get a single FMEA by id."""
    if analysis := models.Fmea.one(id=id):
        issues = validation.get_analysis_issues(analysis)
        analysis.issues = issues
        return analysis
    else:
        raise NoSuchEntity


@bp.route("/<int:id>/rpns", methods=["GET"])
@pydantically(None, List[RpnReport])
def get_rpns(id):
    """Get the report for an FMEA analysis."""
    if analysis := models.Fmea.one(id=id):
        return analysis.get_rpns()
    else:
        raise NoSuchEntity


@bp.route("/<int:id>/issues", methods=["GET"])
def get_issues(id):
    """Return issues with a specified analysis."""
    analysis = models.Fmea.one(id=id)
    issues = validation.get_analysis_issues(analysis)
    return json.dumps(issues)


@bp.route("/<int:fmea_id>/notes/", methods=["GET"])
@pydantically(None, List[Note])
def get_all_notes_for_analysis(fmea_id):
    """Get all of the notes for an analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        return analysis.notes
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/notes/", methods=["POST"])
@pydantically(Note, Note, 201)
def add_or_update_note(data, fmea_id):
    """Add a new note, or updates an existing note."""
    if analysis := models.Fmea.one(id=fmea_id):
        note = analysis.add_or_update_note(**data)
        if note is None:
            raise NoSuchEntity
    else:
        raise NoSuchEntity

    return note


@bp.route("/", methods=["POST"])
@pydantically(Analysis, Analysis, 201)
def create_fmea(data):
    """Create a new FMEA analysis."""
    analysis = models.Fmea.create(**data)
    return analysis


@bp.route("/import", methods=["POST"])
@pydantically(SupportedAnalysisSchemas, Analysis, 201, {"si_id": int})
def import_fmea(data, si_id: int = None):
    """Import a FMEA Analysis to the database."""
    return import_analysis(data, si_id)


@bp.route("/<int:id>/export", methods=["GET"])
@pydantically(None, AnalysisCurrent)
def export_fmea(id):
    """Return the FMEA Analysis for a given ID."""
    if analysis := models.Fmea.one(id=id):
        return {"data": analysis, "version": 1}
    else:
        raise NoSuchEntity


@bp.route("/", methods=["GET"])
@pydantically(None, List[Analysis], query_parameters={"si_id": int})
def get_all_fmeas(si_id: int = None):
    """Return all FMEA analyses."""
    return models.Fmea.all(si_id=si_id)


@bp.route("/<int:id>", methods=["PATCH"])
@pydantically(Fragment[Analysis], Analysis)
def update_fmea(data, id):
    """Update fields of an FMEA analysis."""
    if analysis := models.Fmea.one(id=id):
        return analysis.update(**data)
    else:
        raise NoSuchEntity


@bp.route("/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_fmea(id):
    """Delete the analysis with the given id."""
    deleted = models.Fmea.delete_matching(id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/requirements/", methods=["POST"])
@pydantically(DesignRequirement, DesignRequirement, 201)
def create_requirement(data, fmea_id):
    """Add a new requirement to the given analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        requirement = analysis.add_requirement(**data)
        return requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/requirements/<int:id>", methods=["GET"])
@pydantically(None, DesignRequirement)
def get_one_requirement(fmea_id, id):
    """Get a single requirement for an analysis."""
    if requirement := models.DesignRequirement.one(fmea_id=fmea_id, id=id):
        return requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/requirements/<int:id>", methods=["PATCH"])
@pydantically(Fragment[DesignRequirement], DesignRequirement)
def update_requirement(data, fmea_id, id):
    """Given a requirement ID, update fields according to given data."""
    if requirement := models.DesignRequirement.update_by_id(id, data):
        return requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/requirements/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_requirement(fmea_id, id):
    """Delete the requirement specified by fmea_id and id."""
    deleted = models.DesignRequirement.delete_matching(fmea_id=fmea_id, id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/requirements/", methods=["GET"])
@pydantically(None, FailureMode)
def get_all_requirements_for_analysis(fmea_id):
    """Get all requirements for an analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        return analysis.requirements
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/failure_modes/", methods=["GET"])
@pydantically(None, FailureMode)
def get_all_failure_modes_for_analysis(fmea_id):
    """Get all failure modes for an analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        return analysis.failure_modes
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/failure_modes/", methods=["POST"])
@pydantically(FailureMode, FailureMode, 201)
def create_failure_mode(data, fmea_id):
    """Add a new failure_mode to the given analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        failure_mode = analysis.add_failure_mode(**data)
        return failure_mode
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/failure_modes/<int:id>", methods=["GET"])
@pydantically(None, FailureMode)
def get_one_failure_mode(fmea_id, id):
    """Get a single failure mode for an analysis."""
    if failure_mode := models.FailureMode.one(fmea_id=fmea_id, id=id):
        return failure_mode
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/failure_modes/<int:id>", methods=["PATCH"])
@pydantically(Fragment[FailureMode], FailureMode)
def update_failure_mode(data, fmea_id, id):
    """Given failure_mode ID, update fields according to given data."""
    if failure_mode := models.FailureMode.update_by_id(id, data):
        return failure_mode
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/failure_modes/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_failure_mode(fmea_id, id):
    """Delete the failure mode specified by fmea_id and id."""
    deleted = models.FailureMode.delete_matching(fmea_id=fmea_id, id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/effects/", methods=["GET"])
@pydantically(None, Effect)
def get_all_effects_for_analysis(fmea_id):
    """Get all effects for an analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        return analysis.effects
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/effects/", methods=["POST"])
@pydantically(Effect, Effect, 201)
def create_effect(data, fmea_id):
    """Add a new effect to the given analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        effect = analysis.add_effect(**data)
        return effect
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/effects/<int:id>", methods=["GET"])
@pydantically(None, Effect)
def get_one_effect(fmea_id, id):
    """Get a single effect for an anlysis."""
    if effect := models.Effect.one(fmea_id=fmea_id, id=id):
        return effect
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/effects/<int:id>", methods=["PATCH"])
@pydantically(Fragment[Effect], Effect)
def update_effect(data, fmea_id, id):
    """Given a effect ID, update fields according to given data."""
    if effect := models.Effect.update_by_id(id, data):
        return effect
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/effects/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_effect(fmea_id, id):
    """Delete the effect specified by fmea_id and id."""
    deleted = models.Effect.delete_matching(fmea_id=fmea_id, id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/causes/", methods=["GET"])
@pydantically(None, Cause)
def get_all_causes_for_analysis(fmea_id):
    """Get all causes for an analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        return analysis.causes
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/causes/", methods=["POST"])
@pydantically(Cause, Cause, 201)
def create_cause(data, fmea_id):
    """Add a new cause to the given analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        cause = analysis.add_cause(**data)
        return cause
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/causes/<int:id>", methods=["GET"])
@pydantically(None, Cause)
def get_one_cause(fmea_id, id):
    """Get a single cause for an analysis."""
    if cause := models.Cause.one(fmea_id=fmea_id, id=id):
        return cause
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/causes/<int:id>", methods=["PATCH"])
@pydantically(Fragment[Cause], Cause)
def update_cause(data, fmea_id, id):
    """Given a cause ID, update fields according to given data."""
    if cause := models.Cause.update_by_id(id, data):
        return cause
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/causes/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_cause(fmea_id, id):
    """Delete the control specified by fmea_id and id."""
    deleted = models.Cause.delete_matching(fmea_id=fmea_id, id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/controls/", methods=["GET"])
@pydantically(None, Control)
def get_all_controls_for_analysis(fmea_id):
    """Get all controls for an analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        return analysis.controls
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/controls/", methods=["POST"])
@pydantically(Control, Control, 201)
def create_control(data, fmea_id):
    """Add a new control to the given analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        control = analysis.add_control(**data)
        return control
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/controls/<int:id>", methods=["GET"])
@pydantically(None, Control)
def get_one_control(fmea_id, id):
    """Get a single control for an analysis."""
    if control := models.Control.one(fmea_id=fmea_id, id=id):
        return control
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/controls/<int:id>", methods=["PATCH"])
@pydantically(Fragment[Control], Control)
def update_control(data, fmea_id, id):
    """Given a control ID, update fields according to given data."""
    if control := models.Control.update_by_id(id, data):
        return control
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/controls/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_control(fmea_id, id):
    """Delete the control specified by fmea_id and id."""
    deleted = models.Control.delete_matching(fmea_id=fmea_id, id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/mitigations/", methods=["GET"])
@pydantically(None, Mitigation)
def get_all_mitigations_for_analysis(fmea_id):
    """Get all mitigation for an analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        return analysis.mitigations
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/mitigations/", methods=["POST"])
@pydantically(Mitigation, Mitigation, 201)
def create_mitigation(data, fmea_id):
    """Add a new mitigation to the given analysis."""
    if analysis := models.Fmea.one(id=fmea_id):
        mitigation = analysis.add_mitigation(**data)
        return mitigation
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/mitigations/<int:id>", methods=["GET"])
@pydantically(None, Mitigation)
def get_one_mitigation(fmea_id, id):
    """Get a single mitigation for an analysis."""
    if mitigation := models.Mitigation.one(fmea_id=fmea_id, id=id):
        return mitigation
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/mitigations/<int:id>", methods=["PATCH"])
@pydantically(Fragment[Mitigation], Mitigation)
def update_mitigation(data, fmea_id, id):
    """Given a mitigation ID, update fields according to given data."""
    if mitigation := models.Mitigation.update_by_id(id, data):
        return mitigation
    else:
        raise NoSuchEntity


@bp.route("/<int:fmea_id>/report/excel", methods=["GET"])
def get_report_excel(fmea_id):
    """Export the report for the given analysis in Excel format."""
    if analysis := models.Fmea.one(id=fmea_id):
        return send_file(
            report.export_to_excel(analysis),
            attachment_filename="fmea_report.xlsx",
            as_attachment=True,
            cache_timeout=0,
        )
    else:
        raise NoSuchEntity
