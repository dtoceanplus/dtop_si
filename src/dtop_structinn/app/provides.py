# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Routes for services promised to other modules."""
from flask import Blueprint
from enum import IntEnum

from dtop_structinn.pyqfd import models
from dtop_structinn.pyintegration.models import (
    get_si_analysis_by_id,
    delete_si_analysis_by_id,
)
from dtop_structinn.pyintegration.schemas import Analysis
from dtop_structinn.app.decorateroutes import pydantically
from dtop_structinn.app.errors import NoSuchEntity


bp = Blueprint("provides", __name__)


class Direction(IntEnum):
    """Enumeration for direction of improvement."""

    Higher = 1
    Lower = -1


@bp.route("/<int:si_id>", methods=["GET"])
@pydantically(None, Analysis)
def get_si_analysis(si_id):
    "Return selected SI Analysis FMEA and QFD IDs."
    analysis = get_si_analysis_by_id(si_id)
    if not analysis:
        raise NoSuchEntity
    return analysis


@bp.route("/<int:si_id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_si_analysis(si_id):
    """Delete all QFD and FMEA analyses with the given SI ID."""
    deleted = delete_si_analysis_by_id(si_id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/targets", methods=["GET"])
def get_all_targets_for_analysis(qfd_id):
    """Return all targets for a QFD analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        functional_requirements = analysis.functional_requirements
        return {
            "targets": [
                {
                    "functional_requirement": fr.description,
                    "units": fr.units,
                    "value": fr.target,
                    "direction": Direction(fr.direction),
                }
                for fr in functional_requirements
            ]
        }
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/solutions", methods=["GET"])
def get_all_solutions_for_analysis(qfd_id):
    """Return all solutions for a QFD analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        fr_map = {
            fr.id: {
                "functional_requirement": fr.description,
                "units": fr.units,
                "value": fr.target,
                "direction": Direction(fr.direction),
            }
            for fr in analysis.functional_requirements
        }
        solutions = [
            {
                "description": s.description,
                "achievements": [
                    {"value": a.value, "target": fr_map[a.functional_requirement_id]}
                    for a in analysis.achievements
                    if a.solution_id == s.id
                ],
            }
            for s in analysis.solutions
        ]
        return {"solutions": solutions}
    else:
        raise NoSuchEntity
