# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Contains a decorator for validating inputs and outputs of routes."""
import functools
from typing import Any, get_args, get_origin, List, Optional, Type, Union

from flask import jsonify, request
from pydantic import BaseModel, ValidationError, create_model, parse_obj_as


# schemas


class ErrorResponse(BaseModel):
    """A pydantic schema for returning errors."""

    errors: Optional[List[Any]]


model_out_type = Optional[Union[Type[BaseModel], Type[List[Type]]]]
model_in_type = Optional[Type[BaseModel]]

# decorator stuff


@functools.lru_cache(maxsize=None)
def without_readonly(model):
    """Return a copy of a model without any readonly fields."""
    modelcopy = create_model(model.__name__, __base__=model)
    modelcopy.__fields__ = {
        k: v
        for k, v in modelcopy.__fields__.items()
        if not v.field_info.extra.get("readOnly", False)
    }
    return modelcopy


def pydantically(
    model_in: model_in_type,
    model_out: model_out_type,
    success_status: int = 200,
    query_parameters: dict = None,
):
    """Validate request and response json of a route.

    The decorator:
        * uses Pydantic to cast the output of the decorated
          Flask request to a specified type (probably a pydantic model,
          or a list thereof.
        * uses Pydantic to validate that json passed in the request.
        * sets the HTTP status of the response.

    The decorated route should attempt to return something that Pydantic
    can parse into the model_out type (e.g. a dict or list of dicts).

    If the request json is invalid, an error response will be returned
    to the client with status 422 (unable to process request). If the
    client provides json but model_in is None, the provided json is
    ignored.

    If the route returned the wrong type, or any other exception occurs
    in processing, the client will receive a status 500 (server error).

    Otherwise they will receive whatever is defined in success_status.

    Args:
        model_in: type expected for request json
        model_out: type expected for response json
        success_status: HTTP status if everything is valid and request
            succeeds
        query_parameters: Dictionary of function arguments and their
            type to get from query parameters
    """
    if model_in is not None:
        if get_origin(model_in) is Union:
            model_in = Union[tuple(map(without_readonly, get_args(model_in)))]
        else:
            model_in = without_readonly(model_in)

    def decorator_pydantically(func):

        if (success_status == 204) and (model_out is not None):
            raise TypeError(
                "There should be no body to return if the HTTP status is 204."
            )

        @functools.wraps(func)
        def wrapper_pydantically(*args, **kwargs):
            if query_parameters is not None:
                for k, v in query_parameters.items():
                    rarg = request.args.get(k)
                    if rarg is not None:
                        kwargs[k] = parse_obj_as(v, rarg)
            if model_in is not None:
                try:
                    data = parse_obj_as(
                        model_in, request.json or {}
                    )  # HTTP 422 if this is invalid or missing
                except ValidationError as e:
                    return ErrorResponse(errors=e.errors()).dict(), 422

                returned = func(data.dict(), **kwargs)
            else:
                returned = func(**kwargs)

            if (model_out is None) and (returned is None):
                output = returned
            elif (model_out is None) and (returned is not None):
                raise TypeError(
                    "The route unexpectedly returned something."
                )  # HTTP 500
            elif (model_out is not None) and (returned is None):
                raise TypeError("The route unexpectedly returned None.")
            else:
                output = parse_obj_as(
                    model_out, returned
                )  # HTTP 500 if this is invalid

            return jsonify(output), success_status

        return wrapper_pydantically

    return decorator_pydantically
