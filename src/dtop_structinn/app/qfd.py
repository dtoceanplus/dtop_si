# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Routes for QFD."""
from typing import List

from flask import Blueprint, send_file

from dtop_structinn.app.decorateroutes import pydantically
from dtop_structinn.app.errors import NoSuchEntity
from dtop_structinn.pyqfd import models
from dtop_structinn.pyqfd.file_schemas import AnalysisCurrent, SupportedAnalysisSchemas
from dtop_structinn.pyqfd.schemas import (
    Achievement,
    Analysis,
    Correlation,
    FunctionalRequirement,
    Impact,
    Requirement,
    Report,
    Solution,
)
from dtop_structinn.notes.schemas import Note
from dtop_structinn.pyqfd.report import report, export_to_excel
from dtop_structinn.pyqfd.import_export import import_analysis
from dtop_structinn.types import Fragment
from dtop_structinn.pyqfd import validation

bp = Blueprint("qfd", __name__)


@bp.route("/<int:id>/issues")
def get_issues(id):
    """Return issues with a specified analysis."""
    analysis = models.Qfd.one(id=id)
    issues = validation.get_analysis_issues(analysis)
    import json

    return json.dumps(issues)


@bp.route("/", methods=["POST"])
@pydantically(Analysis, Analysis, 201)
def create_qfd(data):
    """Add a new QFD Analysis to the database."""
    analysis = models.Qfd.create(**data)
    return analysis


@bp.route("/import", methods=["POST"])
@pydantically(SupportedAnalysisSchemas, Analysis, 201, {"si_id": int})
def import_qfd(data, si_id: int = None):
    """Import a QFD Analysis to the database."""
    return import_analysis(data, si_id)


@bp.route("/<int:id>/export", methods=["GET"])
@pydantically(None, AnalysisCurrent)
def export_qfd(id):
    """Return the QFD Analysis for a given ID."""
    if analysis := models.Qfd.one(id=id):
        return {"data": analysis, "version": 1}
    else:
        raise NoSuchEntity


@bp.route("/", methods=["GET"])
@pydantically(None, List[Analysis], query_parameters={"si_id": int})
def get_all_qfds(si_id: int = None):
    """Return all QFD Analyses from the database."""
    return models.Qfd.all(si_id=si_id)


@bp.route("/<int:id>", methods=["GET"])
@pydantically(None, Analysis)
def get_one_qfd(id):
    """Return the QFD Analysis for a given ID."""
    if analysis := models.Qfd.one(id=id):
        issues = validation.get_analysis_issues(analysis)
        analysis.issues = issues
        return analysis
    else:
        raise NoSuchEntity


@bp.route("/<int:id>", methods=["PATCH"])
@pydantically(Fragment[Analysis], Analysis)
def update_qfd(data, id):
    """Given an analysis ID, updates fields according to given data."""
    if analysis := models.Qfd.one(id=id):
        return analysis.update(**data)
    else:
        raise NoSuchEntity


@bp.route("/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_qfd(id):
    """Delete the analysis with the given id."""
    deleted = models.Qfd.delete_matching(id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/functional_requirements/", methods=["GET"])
@pydantically(None, List[FunctionalRequirement])
def get_all_functional_requirements_for_analysis(qfd_id):
    """Return all functional requirements for an analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        return analysis.functional_requirements
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/functional_requirements/", methods=["POST"])
@pydantically(FunctionalRequirement, FunctionalRequirement, 201)
def create_functional_requirement(data, qfd_id):
    """Add a new functional requirement to the given analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        functional_requirement = analysis.add_functional_requirement(**data)
        return functional_requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/functional_requirements/<int:id>", methods=["GET"])
@pydantically(None, FunctionalRequirement)
def get_one_functional_requirement(qfd_id, id):
    """Return a single functional requirement for an analysis."""
    if functional_requirement := models.FunctionalRequirement.one(qfd_id=qfd_id, id=id):
        return functional_requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/functional_requirements/<int:id>", methods=["PATCH"])
@pydantically(Fragment[FunctionalRequirement], FunctionalRequirement)
def update_functional_requirement(data, qfd_id, id):
    """Given a functional_requirement ID, updates fields."""
    if functional_requirement := models.FunctionalRequirement.update_by_id(id, data):
        return functional_requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/functional_requirements/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_functional_requirement(qfd_id, id):
    """Delete the functioanl requirement specified by qfd_id and id."""
    deleted = models.FunctionalRequirement.delete_matching(qfd_id=qfd_id, id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/requirements/", methods=["GET"])
@pydantically(None, List[Requirement])
def get_all_requirements_for_analysis(qfd_id):
    """Return all requirements for an analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        return analysis.requirements
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/requirements/", methods=["POST"])
@pydantically(Requirement, Requirement, 201)
def create_requirement(data, qfd_id):
    """Add a new requirement to the given analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        requirement = analysis.add_requirement(**data)
        return requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/requirements/<int:id>", methods=["GET"])
@pydantically(None, Requirement)
def get_one_requirement(qfd_id, id):
    """Return a single requirement for an analysis."""
    if requirement := models.Requirement.one(qfd_id=qfd_id, id=id):
        return requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/requirements/<int:id>", methods=["PATCH"])
@pydantically(Fragment[Requirement], Requirement)
def update_requirement(data, qfd_id, id):
    """Given a requirement ID, update fields according to given data."""
    if requirement := models.Requirement.update_by_id(id, data):
        return requirement
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/requirements/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_requirement(qfd_id, id):
    """Delete the requirement specified by qfd_id and id."""
    deleted = models.Requirement.delete_matching(qfd_id=qfd_id, id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/solutions/", methods=["GET"])
@pydantically(None, List[Solution])
def get_all_solutions_for_analysis(qfd_id):
    """Return all solutions for an analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        return analysis.solutions
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/solutions/", methods=["POST"])
@pydantically(Solution, Solution, 201)
def create_solution(data, qfd_id):
    """Add a new solution to the given analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        solution = analysis.add_solution(**data)
        return solution
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/solutions/<int:id>", methods=["GET"])
@pydantically(None, Solution)
def get_one_solution(qfd_id, id):
    """Return a single solution for an analysis."""
    if solution := models.Solution.one(qfd_id=qfd_id, id=id):
        return solution
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/solutions/<int:id>", methods=["PATCH"])
@pydantically(Fragment[Solution], Solution)
def update_solution(data, qfd_id, id):
    """Given a solution ID, updates fields according to given data."""
    if solution := models.Solution.update_by_id(id, data):
        return solution
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/solutions/<int:id>", methods=["DELETE"])
@pydantically(None, None, 204)
def delete_solution(qfd_id, id):
    """Delete the solution specified by qfd_id and id."""
    deleted = models.Solution.delete_matching(qfd_id=qfd_id, id=id)
    if not deleted:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/achievements/", methods=["GET"])
@pydantically(None, List[Achievement])
def get_all_achievements_for_analysis(qfd_id):
    """Return all achievements for an analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        return analysis.achievements
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/achievements/", methods=["POST"])
@pydantically(Achievement, Achievement, 201)
def create_achievement(data, qfd_id):
    """Add a new achievement to the given analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        achievement = analysis.add_or_update_achievement(**data)
        return achievement
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/correlations/", methods=["GET"])
@pydantically(None, List[Correlation])
def get_all_correlations_for_analysis(qfd_id):
    """Get all the correlations for an analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        return analysis.correlations
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/correlations/", methods=["POST"])
@pydantically(Correlation, Correlation, 201)
def create_correlation(
    data, qfd_id
):  # todo: change pydantically to use a custom validation exception (a wrapper) and throw it if database exception too.
    """Add a new correlation to an analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        correlation = analysis.add_or_update_correlation(**data)
        if correlation is None:
            raise NoSuchEntity
    else:
        raise NoSuchEntity

    return correlation


@bp.route("/<int:qfd_id>/notes/", methods=["GET"])
@pydantically(None, List[Note])
def get_all_notes_for_analysis(qfd_id):
    """Get all of the notes for an analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        return analysis.notes
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/notes/", methods=["POST"])
@pydantically(Note, Note, 201)
def add_or_update_note(data, qfd_id):
    """Add a new note, or updates an existing note."""
    if analysis := models.Qfd.one(id=qfd_id):
        note = analysis.add_or_update_note(**data)
        if note is None:
            raise NoSuchEntity
    else:
        raise NoSuchEntity

    return note


@bp.route("/<int:qfd_id>/impacts/", methods=["GET"])
@pydantically(None, List[Impact])
def get_all_impacts_for_analysis(qfd_id):
    """Return list of all impacts for the analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        return analysis.impacts
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/impacts/", methods=["POST"])
@pydantically(Impact, Impact, 201)
def create_impact(data, qfd_id):
    """Add a new impact to the given analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        impact = analysis.add_or_update_impact(**data)
        if impact is None:
            raise NoSuchEntity
    else:
        raise NoSuchEntity

    return impact


@bp.route("/<int:qfd_id>/report", methods=["GET"])
@pydantically(None, Report)
def get_report(qfd_id):
    """Get the report for the given analysis."""
    if analysis := models.Qfd.one(id=qfd_id):
        return report(analysis)
    else:
        raise NoSuchEntity


@bp.route("/<int:qfd_id>/report/excel", methods=["GET"])
def get_report_excel(qfd_id):
    """Export the given analysis in Excel format."""
    if analysis := models.Qfd.one(id=qfd_id):
        return send_file(
            export_to_excel(analysis),
            attachment_filename="qfd_report.xlsx",
            as_attachment=True,
            cache_timeout=0,
        )
    else:
        raise NoSuchEntity
