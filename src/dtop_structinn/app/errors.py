# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Contains custom exceptions with http status codes."""


class UserError(Exception):
    """Exception when the input is invalid."""

    status_code = 400
    description = "Bad request"


class NoSuchEntity(UserError):
    """Exception when the URL refers to a non-extent entity."""

    status_code = 404
    description = "Nothing was found at that URL"


class InvalidUpload(UserError):
    """Exception when the uploaded format is wrong."""

    status_code = 422
    description = "Invalid format for analysis upload."


class InvalidExcelExport(UserError):
    """Error when validation errors prevent Excel export."""

    status_code = 409
    descripton = "Cannot export the Excel report, due to validation errors."
