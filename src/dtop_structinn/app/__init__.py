# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""A flask application for Structured Innovation.

The blueprints contain the routes, with inputs and responses
validated using pydantic.
"""

from flask import Flask
from flask_cors import CORS

from os import environ
from shutil import copy
from pathlib import Path
from datetime import datetime

from dtop_structinn.app import fmea, qfd, triz, provides, suggestions
from dtop_structinn.app.errors import UserError
from dtop_structinn.database import session, Base
import json
from pydantic.json import pydantic_encoder
import alembic.config
import alembic.command
import alembic.script
from sqlalchemy import create_engine


class JSONEncoder(json.JSONEncoder):
    """Use pydantic for converting to JSON."""

    def default(self, o):
        """Use pydantic for converting to JSON."""
        return pydantic_encoder(o)


def create_app(test_config=None):
    """Instantiate the Flask app."""
    app = Flask(__name__, instance_relative_config=True)
    app.json_encoder = JSONEncoder

    # CORS(resources={r"/api/*": {"origins": "*"}})
    CORS(app)

    configure_database()
    register_blueprints(app)
    register_commands(app)
    register_exceptions(app)

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        session.remove()

    return app


def configure_database():
    """Open the database, creating or updating as necessary.

    Opens the database specified by the environment variable, or
    the default if it is not set.

    If the database does not exist it is created and marked as latest
    using Alembic. If it exists, but is out of date, it is upgraded with
    Alembic. Before the upgrade, a backup is made in the same directory
    as the original.
    """
    db_path = environ.get("DTOP_STRUCTINN_DATABASE_PATH", "./structinn.sqlite")
    db_url = f"sqlite:///{db_path}"
    engine = create_engine(db_url)
    need_create_db = db_path == ":memory:" or not Path(db_path).is_file()
    with engine.begin() as connection:
        context = alembic.migration.MigrationContext.configure(connection)
        config = alembic.config.Config(attributes={"connection": connection})
        config.set_main_option("script_location", "dtop_structinn:migrations")
        config.set_main_option("sqlalchemy.url", db_url)
        scriptdir = alembic.script.ScriptDirectory.from_config(config)

        if need_create_db:
            Base.metadata.create_all(bind=connection)
            alembic.command.stamp(config, "head")
            print("New database created.", flush=True)
        elif set(context.get_current_heads()) != set(scriptdir.get_heads()):
            timestamp = datetime.utcnow().timestamp()
            backup_path = db_path + f".{timestamp}.bak"
            copy(db_path, backup_path)
            try:
                alembic.command.upgrade(config, "head")
            except BaseException as e:
                print("Failed to upgrade the database.", flush=True)
                raise e from e
            print("Upgraded existing database.", flush=True)
        else:
            print("Using current database (already up to date).", flush=True)

    session.configure(bind=engine)


def register_blueprints(app):
    """Register the blueprints."""
    app.register_blueprint(qfd.bp, url_prefix="/qfd")
    app.register_blueprint(triz.bp, url_prefix="/triz")
    app.register_blueprint(fmea.bp, url_prefix="/fmea")
    app.register_blueprint(provides.bp, url_prefix="/api")
    app.register_blueprint(suggestions.bp, url_prefix="/suggestions")


def register_commands(app):
    """Register custom CLI commands."""

    @app.cli.command("init_db")
    def init_db():
        """Delete the database and initialise a new database."""
        db_path = environ["DTOP_STRUCTINN_DATABASE_PATH"]
        if Path(db_path).is_file():
            Path(db_path).unlink()
        engine = create_engine(f"sqlite:///{db_path}")
        Base.metadata.create_all(bind=engine)

    return init_db


def register_exceptions(app):
    """Register error handlers for Flask."""

    @app.errorhandler(UserError)
    def handle_user_error(error):
        return {"error": error.description}, error.status_code
