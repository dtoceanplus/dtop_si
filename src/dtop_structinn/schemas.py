# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from typing import TYPE_CHECKING
from pydantic import BaseModel, constr, confloat, Field
from dtop_structinn.declarations import DESCRIPTION_LENGTH


if TYPE_CHECKING:
    non_empty_str = str
    finite_float = float
else:
    non_empty_str = constr(min_length=1, max_length=DESCRIPTION_LENGTH)
    finite_float = confloat(gt=float("-inf"), lt=float("inf"))


class BaseSchema(BaseModel):
    """Base model for other schemas."""

    class Config:
        """Settings for the BaseModel."""

        orm_mode = True

    created: datetime = Field(readOnly=True)  # todo: database level stuff?
    updated: datetime = Field(readOnly=True)


class BaseReportSchema(BaseModel):
    pass
