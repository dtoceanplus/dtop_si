# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Common database related code for all dtop_structinn modules."""

import uuid

from datetime import datetime
from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy.ext.mutable import MutableSet
from sqlalchemy.types import BINARY, DateTime, TEXT, TypeDecorator
from sqlalchemy.engine import Engine
from sqlalchemy import Column, event, MetaData
from sqlalchemy.orm import scoped_session, sessionmaker

from flask import _app_ctx_stack


session = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=False,
    ),
    scopefunc=_app_ctx_stack.__ident_func__,
)


def update_analysis_timestamp(mapper, conn, target):
    conn.execute(target.analysis.__table__.update().values(updated=datetime.now()))


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(
    dbapi_connection, connection_record
):  # pylint: disable=unused-argument
    """Enable foreign keys for sqlite."""
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


naming_convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}


@as_declarative(metadata=MetaData(naming_convention=naming_convention))
class Base:
    """Base class for all dtop_structinn SQLAlchemy models."""

    created = Column(DateTime, nullable=False, default=datetime.now)
    updated = Column(
        DateTime, nullable=False, default=datetime.now, onupdate=datetime.now
    )

    @classmethod
    def one(cls, **kwargs):
        """Return one that matches the given keyword argument."""
        return session.query(cls).filter_by(**kwargs).one_or_none()

    @classmethod
    def all(cls, **kwargs):
        """Return all that match the keyword arguments given."""
        return session.query(cls).filter_by(**kwargs).all()

    @classmethod
    def update_by_id(cls, id_, data):
        """Update one row that matches the given id, then return it."""
        record = session.query(cls).filter_by(id=id_).one_or_none()
        if record:
            for k, v in data.items():
                setattr(record, k, v)
        session.commit()
        return record

    @classmethod
    def delete_matching(cls, **kwargs):
        """Delete one matching row and return it."""
        record = session.query(cls).filter_by(**kwargs).one_or_none()
        if record:
            session.delete(record)
        session.commit()
        return record

    def delete(self):
        """Delete the object/row and return self."""
        session.delete(self)
        session.commit()
        return self

    def update(self, **kwargs):
        """Update the object/row and return it."""
        for k, v in kwargs.items():
            setattr(self, k, v)
        session.commit()
        return self

    __mapper_args__ = {"confirm_deleted_rows": False}


@MutableSet.as_mutable
class IntSet(TypeDecorator):
    """Set of integers."""

    # todo: tests should include when the set is empty
    # todo: consider replacing use of this with tables and association
    # proxies

    impl = TEXT

    def process_bind_param(self, value, dialect):
        if value is None:
            pass
        elif isinstance(value, set) and all(isinstance(i, int) for i in value):
            value = ",".join(map(str, value))
        else:
            raise TypeError("Expected a set of ints.")

        return value

    def process_result_value(self, value, dialect):
        if value is None:
            return None

        return set() if len(value) == 0 else {int(i) for i in value.split(",")}


class UUID(TypeDecorator):
    """UUID Type for SQLAlchemy storing as bytes."""

    impl = BINARY(16)

    def process_bind_param(self, value, dialect):
        """Process the bound paramaters.

        Converts the value on the python side to the values expected in
        the database.

        In this case, UUID objects or string representations of UUIDs
        are transformed to bytes.

        Args:
            value: the value representing a UUID.
            dialect: the SQL dialect - not used in the current
                     implementation.

        Returns:
            the value as bytes, or None if value was None.
        """
        if value is None:
            pass
        elif isinstance(value, uuid.UUID):
            value = value.bytes
        elif isinstance(value, str):
            value = uuid.UUID(value).bytes
        else:
            raise TypeError("Expected a uuid.UUID")

        return value

    def process_result_value(self, value, dialect) -> uuid.UUID:
        """Process a value from a query result row.

        Converts a value from the database to the expected python type.

        In this case, bytes are converted to uuid.UUID objects.

        Args:
            value: the results row column value.
            dialect: the SQL dialect - not used in the current
                     implementation

        Returns:
            The UUID object, or None if the stored value was NULL.
        """
        if value is not None:
            value = uuid.UUID(bytes=value)

        return value
