# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Provides suggestions for structured innovation entities.

Suggestions are provided by calling a provider function.
The function must take 7 arguments.

analysis_type (str):
    "qfd" or "fmea" as appropriate.
    (although theoretically we could infer this)
analysis:
    the analysis in question.
entity (str):
    the part of the analysis to provide suggestions for.
    E.g. "functional_requirement"
field (str):
    the field of that entity that is active.
identifier (dict):
    a dictionary that uniquely identifies the current entity using
    fields and their values. E.g. {"id": 1}
is_new (bool): whether the entity is new or not
context (dict):
    a dictionary of additional information, required in order
    to define the context of a particular suggestion.

If the provider cannot provide suggestions, it should return None.
For example, a provider may only be able to provide requirements and
the current entity is an achievement.

If the provider can provide suggestions, it should return a list of
dictionaries which conforms to either the Suggestion schema or the
SuggestionGroup schema. These both require an identifier (a string
which uniquely identifies this suggestion among any other this provider
may return), a label (which the end user will use to recognise a
suggestion), source (the unique name of the provider, which should match
throughout).
A suggestion group provides a list of suggestions (which is a homogenous
list of either Suggestion or SuggestionGroup conforming dicts).
A suggestion provides content, which contains the data that will be used
if the user accepts the suggestion.
"""

from dtop_structinn.suggestions.structured_innovation import qfd, fmea, library
from dtop_structinn.suggestions.solution_hierarchy import solution_hierarchy
from dtop_structinn.suggestions.stagegate import stagegate


__all__ = ["qfd", "fmea", "solution_hierarchy", "stagegate", "library"]
