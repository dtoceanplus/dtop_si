# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
import requests


def get_url_and_headers(module):

    dtop_domain = os.environ["DTOP_DOMAIN"]
    dtop_protocol = os.getenv("DTOP_PROTOCOL", "http")

    print()
    print("Headers - auth :")
    headers = {"Authorization": os.getenv("DTOP_AUTH", "")}
    print(headers)

    print()
    print("Host header value :")
    header_host = f"{module}.{dtop_domain}"
    print(header_host)

    module_api_url = f"{dtop_protocol}://{header_host}"

    print()
    print("Check if DTOP is deployed on DNS resolved server or on workstation :")

    try:
        requests.get(module_api_url, headers=headers)
        print("DTOP appears to be deployed on DNS resolved server")
    except requests.ConnectionError:
        module_api_url = "http://172.17.0.1:80"
        headers["Host"] = header_host
        print("DTOP appears to be deployed on workstation")
        print()
        print("Headers - auth and localhost :")
        print(headers)

    print()
    print("Basic Module Open API URL :")
    print(module_api_url)
    print()

    return module_api_url, headers


def project_entities(si_id, module_nickname):
    mm_api_url, mm_headers = get_url_and_headers("mm")
    integration_url = f"{mm_api_url}/api/integration"
    print(mm_api_url)
    response = requests.get(
        f"{integration_url}/modules/si/entities/{si_id}/projects", headers=mm_headers
    )
    print(response)
    pid = response.json()[0]["id"]
    print(pid)
    response = requests.get(
        f"{integration_url}/projects/{pid}/modules/{module_nickname}/entities",
        headers=mm_headers,
    )
    return response.json()
