# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_structinn.pyqfd.models import Qfd
from dtop_structinn.pyfmea.models import Fmea
from importlib import resources
import json

suggestions_library_path = resources.files("dtop_structinn") / "resources/"
with open(suggestions_library_path / "fmea_suggestions_library.json") as fp:
    suggestions_library = json.load(fp)


def fmea(analysis_type, analysis, entity, field, identifier, is_new, context):
    if not is_new:
        return None

    if entity not in ("requirement"):
        return None

    used = [r.description for r in analysis.requirements]
    suggestions = []

    for other_analysis in Fmea.all():
        if analysis_type == "fmea" and other_analysis.id == analysis.id:
            continue

        requirements = [
            {
                "source": "fmea",
                "label": requirement.description,
                "content": {"description": requirement.description},
                "identifier": str((other_analysis.id, "requirement", requirement.id)),
            }
            for requirement in other_analysis.requirements
            if requirement.description not in used
        ]

        if len(requirements) > 0:
            suggestions.append(
                {
                    "source": "fmea",
                    "label": other_analysis.name,
                    "identifier": (other_analysis.id),
                    "suggestions": requirements,
                }
            )

    return suggestions


def library(analysis_type, analysis, entity, field, identifier, is_new, context):
    """return predefined suggestions for stages of an FMEA analyses"""
    if not is_new:
        return None

    if analysis_type != "fmea":
        return None

    entity_map = {
        "failure_mode": {
            "attribute": "failure_modes",
            "parent_attribute": "requirement_id",
        },
        "effect": {"attribute": "effects", "parent_attribute": "failure_mode_id"},
        "cause": {"attribute": "causes", "parent_attribute": "failure_mode_id"},
        "design_control": {
            "attribute": "controls",
            "parent_attribute": "failure_mode_id",
        },
    }

    try:
        used = [
            item.description
            for item in getattr(analysis, entity_map[entity]["attribute"])
            if getattr(item, entity_map[entity]["parent_attribute"])
            == context["parent_id"]
        ]

        return suggestions_from_library(entity, used)
    except KeyError:
        return None


def suggestions_from_library(entity: str, used: list) -> list:
    """return suggestions from the FMEA suggestions library for
    the given entity"""

    suggestion_categories = suggestions_library[entity]

    all_suggestions = []
    for catidx, (category, suggestion_list) in enumerate(suggestion_categories.items()):
        cat_id = "-".join([entity, str(catidx)])
        suggestions = [
            {
                "source": "library",
                "label": suggestion,
                "content": {"description": suggestion},
                "identifier": "-".join([cat_id, str(idx)]),
            }
            for idx, suggestion in enumerate(suggestion_list)
            if suggestion not in used
        ]

        if len(suggestions) > 0:
            all_suggestions.append(
                {
                    "source": "library",
                    "label": category,
                    "identifier": cat_id,
                    "suggestions": suggestions,
                }
            )

    return all_suggestions


def qfd(analysis_type, analysis, entity, field, identifier, is_new, context):
    if not is_new:
        return None

    if entity not in ("requirement", "functional_requirement"):
        return None

    used = [r.description for r in analysis.requirements]
    if analysis_type == "qfd":
        used.extend(fr.description for fr in analysis.functional_requirements)

    suggestions = []

    for other_analysis in Qfd.all():
        if analysis_type == "qfd" and other_analysis.id == analysis.id:
            continue

        analysis_suggestions = []

        requirements = [
            {
                "source": "qfd",
                "label": requirement.description,
                "content": {"description": requirement.description},
                "identifier": str((other_analysis.id, "requirement", requirement.id)),
            }
            for requirement in other_analysis.requirements
            if requirement.description not in used
        ]

        functional_requirements = [
            {
                "source": "qfd",
                "label": functional_requirement.description,
                "content": {
                    "description": functional_requirement.description,
                    "target": functional_requirement.target,
                    "units": functional_requirement.units,
                    "direction": functional_requirement.direction,
                },
                "identifier": str(
                    (
                        other_analysis.id,
                        "functional_requirement",
                        functional_requirement.id,
                    )
                ),
            }
            for functional_requirement in other_analysis.functional_requirements
            if functional_requirement.description not in used
        ]

        if len(requirements) > 0:
            analysis_suggestions.append(
                {
                    "source": "qfd",
                    "label": "Requirements",
                    "identifier": str((other_analysis.id, "requirements")),
                    "suggestions": requirements,
                }
            )

        if len(functional_requirements) > 0:
            analysis_suggestions.append(
                {
                    "source": "qfd",
                    "label": "Functional Requirements",
                    "identifier": str((other_analysis.id, "functional_requirements")),
                    "suggestions": functional_requirements,
                }
            )

        if len(analysis_suggestions) > 0:
            suggestions.append(
                {
                    "source": "qfd",
                    "label": other_analysis.name,
                    "identifier": (other_analysis.id),
                    "suggestions": analysis_suggestions,
                }
            )

    return suggestions
