# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_structinn.solution_hierarchy import hierarchy, get_children, get_all_at_level


def get_top_of_tree(suggestions):
    parents = {}
    try:
        for suggestion in suggestions:
            parent = suggestion["parent"]
            if parent["uuid"] not in parents:
                parents[parent["uuid"]] = hierarchy_entry_to_suggestion(
                    parent, is_group=True
                )
            parents[parent["uuid"]]["suggestions"].append(suggestion)
    except KeyError:  # this might need scope limiting better
        return list(suggestions)
    else:
        return get_top_of_tree(parents.values())


def hierarchy_entry_to_suggestion(entry, is_group=False):
    suggestion = {
        "source": "solution_hierarchy",
        "content": {k: v for k, v in entry.items() if k not in ["parent", "children"]},
        "label": entry["description"],
        "identifier": str(entry["uuid"]),
        "parent": entry["parent"],
    }
    suggestion["content"]["solution_hierarchy_child_uuid"] = suggestion["content"].pop(
        "uuid"
    )
    if is_group:
        suggestion["suggestions"] = []

    return suggestion


def solution_hierarchy(
    analysis_type, analysis, entity, field, identifier, is_new, context
):
    if not is_new:
        return None

    if analysis_type != "qfd":
        return None  # use exception or an empty list instead?

    if entity not in ("requirement", "functional_requirement"):
        return None

    requirements = analysis.requirements
    in_requirements = [
        requirement.solution_hierarchy_child_uuid
        for requirement in requirements
        if requirement.solution_hierarchy_child_uuid is not None
    ]

    if entity == "requirement":
        used_hierarchy_entries = in_requirements
        to_suggest = [
            entry
            for entry in get_all_at_level(hierarchy, analysis.solution_hierarchy_level)
            if entry["uuid"] not in used_hierarchy_entries
        ]

    else:  # entity == "functional_requirement":
        functional_requirements = analysis.functional_requirements

        used_hierarchy_entries = [
            functional_requirement.solution_hierarchy_child_uuid
            for functional_requirement in functional_requirements
            if functional_requirement.solution_hierarchy_child_uuid is not None
        ]

        starting_point = in_requirements

        to_suggest = [
            entry
            for entry in get_children(hierarchy, starting_point)
            if entry["uuid"] not in used_hierarchy_entries
        ]

    suggestions = [
        hierarchy_entry_to_suggestion(suggestion) for suggestion in to_suggest
    ]

    if len(suggestions) < 1:
        suggestions = None
    else:
        return get_top_of_tree(suggestions)
