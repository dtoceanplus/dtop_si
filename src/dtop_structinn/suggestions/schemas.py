# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import Optional, Any, Union
from pydantic import BaseModel, root_validator, Field
from dtop_structinn.schemas import non_empty_str

# from dtop_structinn.types import Fragment


class SuggestionGroup(BaseModel):
    source: non_empty_str
    label: str
    identifier: non_empty_str
    suggestions: Union[list[SuggestionGroup], list[Suggestion]]


class Suggestion(BaseModel):
    source: non_empty_str
    content: dict[str, Any]  # Possibly make Suggestion a generic and then use Fragment?
    identifier: non_empty_str
    label: str


class SuggestionRequest(BaseModel):
    analysis_type: str
    analysis_id: int
    entity: str
    identifier: Optional[dict[str, Any]]
    field: Optional[str]
    is_new: bool = False
    context: dict[str, Any] = Field(default_factory=dict)

    @root_validator
    def check_identifier_present(cls, values):

        got_field = values.get("is_new") is not None
        new = values.get("is_new")
        got_identifier = values.get("is_new") is not None

        if got_field and not new and not got_identifier:
            raise ValueError(
                "An identifier for the entity should be provided (or is_new set to False)."
            )
        return values


SuggestionGroup.update_forward_refs()
