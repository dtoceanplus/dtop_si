# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_structinn.pyqfd.models import Achievement, FunctionalRequirement, Solution
from dtop_structinn.pyqfd.enumerations import ImprovementEnum
from dtop_structinn.suggestions.main_module import get_url_and_headers, project_entities
import requests

threshold_type_direction_map = {
    "upper": ImprovementEnum.lower_is_better,
    "lower": ImprovementEnum.higher_is_better,
}


def functional_requirement(analysis, field, identifier, is_new):
    functional_requirements = FunctionalRequirement.all(qfd_id=analysis.id)
    current = next(
        (fr for fr in functional_requirements if fr.id == identifier.get("id")),
        None,
    )
    functional_requirements = [
        fr.description for fr in functional_requirements if fr is not current
    ]
    entities = project_entities(analysis.si_id, "sg")
    sg_api_url, sg_headers = get_url_and_headers("sg")
    suggestions = []
    for entity in entities:
        sg_id = entity["entityId"]
        entity_suggestions = []
        resp = requests.get(
            f"{sg_api_url}/api/stage-gate-studies/{sg_id}/metric-thresholds",
            headers=sg_headers,
        )
        if resp.status_code == 200:
            for data in resp.json()["metric_thresholds"]:
                name = data["metric"]["name"]
                if name not in functional_requirements:
                    d = {
                        "direction": threshold_type_direction_map[
                            data["metric"]["threshold_type"]
                        ],
                        "description": name,
                        "units": data["metric"]["unit"],
                        "target": data["threshold"],
                    }
                    if is_new:
                        content = d
                    elif all(
                        getattr(current, k) in (None, "", v) for k, v in d.items()
                    ):
                        content = {
                            k: v
                            for k, v in d.items()
                            if getattr(current, k) in (None, "")
                        }
                    else:
                        content = False
                    if content:
                        entity_suggestions.append(
                            {
                                "source": "StageGate",
                                "content": content,
                                "label": d["description"],
                                "identifier": f"{sg_id}metric_threshold{data['metric']['id']}",
                            }
                        )
        if entity_suggestions:
            suggestions.append(
                {
                    "source": "StageGate",
                    "suggestions": entity_suggestions,
                    "label": entity["entityName"],
                    "identifier": f"{sg_id}metric_thresholds",
                }
            )
    return suggestions or None


def solution(analysis, field, identifier, is_new):
    if is_new or field == "description":
        frs = FunctionalRequirement.all(qfd_id=analysis.id)
        solutions = Solution.all(qfd_id=analysis.id)
        current = next((s for s in solutions if s.id == identifier.get("id")), None)
    else:
        frs = filter(None, (FunctionalRequirement.one(qfd_id=analysis.id, id=field),))
        current = Solution.one(qfd_id=analysis.id, id=identifier["id"])
        solutions = (current,)
    solutions = [s.description for s in solutions]
    frs = {(fr.direction, fr.description, fr.units, fr.target): fr.id for fr in frs}
    description = getattr(current, "description", "")
    entities = project_entities(analysis.si_id, "sg")
    sg_api_url, sg_headers = get_url_and_headers("sg")
    suggestions = []
    achievement_fr_ids = None
    for entity in entities:
        solution = entity["entityName"]
        if solution not in solutions or (not is_new and solution == description):
            sg_id = entity["entityId"]
            resp = requests.get(
                f"{sg_api_url}/api/stage-gate-studies/{sg_id}/metric-results",
                headers=sg_headers,
            )
            if resp.status_code == 200:
                achievements = {}
                for data in resp.json()["metric_results"]:
                    fr_id = frs.get(
                        (
                            threshold_type_direction_map[data["threshold_type"]],
                            data["metric"],
                            data["unit"],
                            data["threshold"],
                        )
                    )
                    if fr_id is not None:
                        achievements[fr_id] = data["result"]
                content = {"description": solution}
                if not is_new:
                    if achievement_fr_ids is None:
                        achievement_fr_ids = [
                            a.functional_requirement_id
                            for a in Achievement.all(
                                qfd_id=analysis.id, solution_id=identifier["id"]
                            )
                        ]
                    for fr_id in achievement_fr_ids:
                        achievements.pop(fr_id, None)
                    if description != "":
                        if description != solution:
                            # Don't provide suggestion if non-empty
                            # description doesn't match SG data
                            continue
                        content = {}
                if achievements:
                    suggestions.append(
                        {
                            "source": "StageGate",
                            "content": {**content, "achievements": achievements},
                            "label": solution,
                            "identifier": f"{sg_id}metric_results",
                        }
                    )
    return suggestions or None


def stagegate(analysis_type, analysis, entity, field, identifier, is_new, context):
    if analysis_type == "fmea":  # or analysis.si_id is None:
        return None
    if entity == "functional_requirement":
        return functional_requirement(analysis, field, identifier, is_new)
    if entity == "solution":
        return solution(analysis, field, identifier, is_new)
    return None
