# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""SQLAlchemy models for FMEA."""

from itertools import product

from sqlalchemy import Column, Integer, String, ForeignKey, event

from sqlalchemy.orm import relationship
from dtop_structinn.declarations import DESCRIPTION_LENGTH
from dtop_structinn.database import Base, session, update_analysis_timestamp


class Fmea(Base):
    """SQLAlchemy model for FMEA Analysis."""

    __tablename__ = "fmea_analysis"

    id = Column(Integer, primary_key=True, index=True)
    si_id = Column(Integer, index=True)
    name = Column(String(DESCRIPTION_LENGTH))
    action_level = Column(Integer)
    occurrence_limit = Column(Integer)

    requirements = relationship(
        "DesignRequirement",
        backref="fmea",
        cascade="all, delete-orphan",
    )

    failure_modes = relationship(
        "FailureMode",
        backref="fmea",
        cascade="all, delete-orphan",
    )

    causes = relationship(
        "Cause",
        backref="fmea",
        cascade="all, delete-orphan",
    )

    effects = relationship(
        "Effect",
        backref="fmea",
        cascade="all, delete-orphan",
    )

    controls = relationship(
        "Control",
        backref="fmea",
        cascade="all, delete-orphan",
    )

    mitigations = relationship(
        "Mitigation",
        backref="fmea",
        cascade="all, delete-orphan",
    )
    notes = relationship("FmeaNote", backref="qfd", cascade="all, delete-orphan")

    @classmethod
    def create(
        cls,
        si_id=None,
        name=None,
        action_level=None,
        occurrence_limit=None,
        **extra,
    ):
        """Create a new FMEA analysis.

        Args:
            si_id: When in integrated (main module) mode,
                this is the entity id of the SI module.
            name: The name of the analysis.
            action_level: The RPN from which mitigation is required.
            occurrence_limit:
                The occurrence frequency from which mitigation is
                required.

        Returns:
            An FMEA analysis ORM object.
        """
        analysis = Fmea(
            si_id=si_id,
            name=name,
            action_level=action_level,
            occurrence_limit=occurrence_limit,
        )
        session.add(analysis)
        session.commit()
        return analysis

    def add_requirement(self, **fields):
        """Add a requirement to the analysis.

        Args:
            **fields: All fields to be passed to the
                Requirement init method.

        Returns:
            The requirement ORM object.
        """
        requirement = DesignRequirement(fmea_id=self.id, **fields)
        session.add(requirement)
        session.commit()
        return requirement

    def add_failure_mode(self, **fields):
        """Add a failure mode to the analysis.

        Args:
            **fields: All fields to be passed to the
                Requirement init method.

        Returns:
            The requirement ORM object.
        """
        failure_mode = FailureMode(fmea_id=self.id, **fields)
        session.add(failure_mode)
        session.commit()
        return failure_mode

    def add_cause(self, **fields):
        """Add a cause to the analysis.

        Args:
            **fields: All fields to be passed to the
                Requirement init method.

        Returns:
            The requirement ORM object.
        """
        cause = Cause(fmea_id=self.id, **fields)
        session.add(cause)
        session.commit()
        return cause

    def add_effect(self, **fields):
        """Add a effect to the analysis.

        Args:
            **fields: All fields to be passed to the
                Requirement init method.

        Returns:
            The requirement ORM object.
        """
        effect = Effect(fmea_id=self.id, **fields)
        session.add(effect)
        session.commit()
        return effect

    def add_control(self, **fields):
        """Add a control to the analysis.

        Args:
            **fields: All fields to be passed to the
                Requirement init method.

        Returns:
            The requirement ORM object.
        """
        control = Control(fmea_id=self.id, **fields)
        session.add(control)
        session.commit()
        return control

    def add_mitigation(self, **fields):
        """Add a mitigation to the analysis.

        Args:
            **fields: All fields to be passed to the
                Requirement init method.

        Returns:
            The requirement ORM object.
        """
        mitigation = Mitigation(fmea_id=self.id, **fields)
        session.add(mitigation)
        session.commit()
        return mitigation

    def add_or_update_note(self, **fields):
        """Add or update a note on the analysis.

        Args:
            tag: The note tag being updated/created.
            content: The content of the note.

        Returns:
            The note ORM object.
        """
        tag = fields["tag"]

        if note_obj := FmeaNote.one(fmea_id=self.id, tag=tag):
            note_obj.content = fields["content"]
        else:
            note_obj = FmeaNote(fmea_id=self.id, **fields)
            session.add(note_obj)

        session.commit()

        return note_obj

    def get_rpns(self):
        """Calculate the RPN values for all failure modes.

        If a failure mode does not have at least one cause,
        one effect and one control it will not be returned
        due to the use of product.
        """
        result = []
        for failure_mode in self.failure_modes:
            for effect, cause, control in product(
                (
                    effect
                    for effect in self.effects
                    if effect.failure_mode_id == failure_mode.id
                ),
                (
                    cause
                    for cause in self.causes
                    if cause.failure_mode_id == failure_mode.id
                ),
                (
                    control
                    for control in self.controls
                    if control.failure_mode_id == failure_mode.id
                ),
            ):

                if self.action_level is None or self.occurrence_limit is None:
                    rpn = None
                    requires_mitigation = None
                else:
                    try:
                        rpn = effect.severity * cause.occurrence * control.detection
                    except TypeError:
                        rpn = None

                    if rpn is not None and cause.occurrence is not None:
                        requires_mitigation = (rpn >= self.action_level) | (
                            cause.occurrence >= self.occurrence_limit
                        )
                    else:
                        requires_mitigation = None

                result.append(
                    {
                        "effect_id": effect.id,
                        "cause_id": cause.id,
                        "control_id": control.id,
                        "rpn": rpn,
                        "requires_mitigation": requires_mitigation,
                    }
                )

        return result


class DesignRequirement(Base):
    """SQLAlchemy ORM model for the FMEA Requirements.

    Attributes:
        id: The id assigned by the database
        fmea_id: The FMEA Analysis to which this relates.
        description: The description of the requirement.
    """

    __tablename__ = "fmea_requirement"
    analysis = relationship("Fmea", backref="design_requirement")

    id = Column(Integer, primary_key=True, index=True)
    fmea_id = Column(
        Integer, ForeignKey("fmea_analysis.id", ondelete="CASCADE"), nullable=False
    )
    description = Column(String(DESCRIPTION_LENGTH))


class FailureMode(Base):
    """SQLAlchemy ORM model for a failure mode.

    Attributes:
        id: The id assigned by the database
        fmea_id: The FMEA Analysis to which this relates.
        requirement_id: The requirement to which this relates.
        description: The description of the failure mode.
    """

    __tablename__ = "fmea_failure_mode"
    analysis = relationship("Fmea", backref="failure_mode")

    id = Column(Integer, primary_key=True, index=True)
    fmea_id = Column(
        Integer, ForeignKey("fmea_analysis.id", ondelete="CASCADE"), nullable=False
    )
    requirement_id = Column(
        Integer, ForeignKey("fmea_requirement.id", ondelete="CASCADE"), nullable=False
    )
    description = Column(String(DESCRIPTION_LENGTH))


class Cause(Base):
    """SQLAlchemy ORM model for a cause.

    Attributes:
        id: The id assigned by the database
        fmea_id: The FMEA Analysis to which this relates.
        failure_mode_id: The failure mode to which this relates.
        description: The description of the cause.
        occurrence: A value representing the frequency of
            occurrence due to this cause.
    """

    __tablename__ = "fmea_cause"
    analysis = relationship("Fmea", backref="cause")

    id = Column(Integer, primary_key=True, index=True)
    fmea_id = Column(
        Integer, ForeignKey("fmea_analysis.id", ondelete="CASCADE"), nullable=False
    )
    failure_mode_id = Column(
        Integer, ForeignKey("fmea_failure_mode.id", ondelete="CASCADE"), nullable=False
    )
    description = Column(String(DESCRIPTION_LENGTH))
    occurrence = Column(Integer)


class Effect(Base):
    """SQLAlchemy ORM model for a effect.

    Attributes:
        id: The id assigned by the database
        fmea_id: The FMEA Analysis to which this relates.
        failure_mode_id: The failure mode to which this relates.
        description: The description of the effect.
        severity: A value representing the severity of the effect.
    """

    __tablename__ = "fmea_effect"
    analysis = relationship("Fmea", backref="effect")

    id = Column(Integer, primary_key=True, index=True)
    fmea_id = Column(
        Integer, ForeignKey("fmea_analysis.id", ondelete="CASCADE"), nullable=False
    )
    failure_mode_id = Column(
        Integer, ForeignKey("fmea_failure_mode.id", ondelete="CASCADE"), nullable=False
    )
    description = Column(String(DESCRIPTION_LENGTH))
    severity = Column(Integer)


class Control(Base):
    """SQLAlchemy ORM model for a design control.

    Attributes:
        id: The id assigned by the database
        fmea_id: The FMEA Analysis to which this relates.
        failure_mode_id: The failure mode to which this relates.
        cause_id: The cause to which this relates.
        description: The description of the design control.
        detection: A value representing how hard it is to detect.
    """

    __tablename__ = "fmea_control"
    analysis = relationship("Fmea", backref="control")

    id = Column(Integer, primary_key=True, index=True)
    fmea_id = Column(
        Integer, ForeignKey("fmea_analysis.id", ondelete="CASCADE"), nullable=False
    )
    failure_mode_id = Column(
        Integer, ForeignKey("fmea_failure_mode.id", ondelete="CASCADE"), nullable=False
    )
    cause_id = Column(
        Integer, ForeignKey("fmea_cause.id", ondelete="CASCADE"), nullable=False
    )
    description = Column(String(DESCRIPTION_LENGTH))
    detection = Column(Integer)


class Mitigation(Base):
    """SQLAlchemy ORM model for a mitigation.

    Mitigations refer to a case, an effect, and a design control.

    Attributes:
        id: The id assigned by the database
        fmea_id: The FMEA Analysis to which this relates.
        cause_id: The cause to which this relates.
        effect_id: The effect to which this relates.
        control_id: The design control to which this relates.
        description: The description of the mitigation.
        occurrence: The occurrence after mitigation.
        severity: The severity after mitigation.
        detection: The detection after mitigation.
    """

    __tablename__ = "fmea_mitigation"
    analysis = relationship("Fmea", backref="mitigation")

    fmea_id = Column(
        Integer, ForeignKey("fmea_analysis.id", ondelete="CASCADE"), nullable=False
    )
    id = Column(Integer, primary_key=True, index=True)
    cause_id = Column(
        Integer, ForeignKey("fmea_cause.id", ondelete="CASCADE"), nullable=False
    )
    effect_id = Column(
        Integer, ForeignKey("fmea_effect.id", ondelete="CASCADE"), nullable=False
    )
    control_id = Column(
        Integer, ForeignKey("fmea_control.id", ondelete="CASCADE"), nullable=False
    )
    description = Column(String(DESCRIPTION_LENGTH))
    occurrence = Column(Integer)
    severity = Column(Integer)
    detection = Column(Integer)


class FmeaNote(Base):
    """SQLAlchemy ORM model for FMEA Notes.

    Attributes:
        id: The id assigned by the database.
        qfd_id: The FMEA Analysis to which this note relates.
        content: The textual content of the note.
        tag: Tag indicating which section of the FMEA analysis the
        note relates to.
    """

    __tablename__ = "fmea_note"
    analysis = relationship("Fmea", backref="fmea_note")

    id = Column(Integer, primary_key=True, index=True)
    fmea_id = Column(
        Integer, ForeignKey("fmea_analysis.id", ondelete="CASCADE"), nullable=False
    )
    content = Column(String)
    tag = Column(String(DESCRIPTION_LENGTH))


for child in [
    DesignRequirement,
    FailureMode,
    Cause,
    Effect,
    Control,
    Mitigation,
    FmeaNote,
]:
    event.listen(child, "after_update", update_analysis_timestamp)
