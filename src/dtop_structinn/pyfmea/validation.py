# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Validation of FMEA Analyses.

This module contains functions for validating a FMEA Analysis object.

The `get_analysis_issues` function accumulates the issues from
elsewhere.

An issue has a type ("warning", "error", "question").
    * An error prevents the analysis results from being calculated.
    * A warning means the report will probably not make sense to the
        end user.
    * A question is request that the user double checks something which
        may be correct, but is likely mistake.

An issue also has the following fields:
   obj: The object the field should be on.
   entity: A string pointing towards the type of `obj`.
   id: A unique identifier for obj.
   tags: A tuple giving context to the issue.
   message: A human readable message explaining the issue.
"""
from dtop_structinn.pyqfd.validation import warning_if_missing, error_if_missing


def get_analysis_issues(analysis):
    """Get the current issues for an analysis.

    This function collates the outputs from separate
    validation routines for different aspects of the analysis.

    Args:
        analysis: The analysis to check for issues.

    Returns:
        A list of issues.
    """
    issues = []

    issues += check_definition(analysis)
    issues += check_requirements(analysis)
    issues += check_failure_modes(analysis)
    issues += check_effects(analysis)
    issues += check_causes(analysis)
    issues += check_controls(analysis)
    issues += check_mitigations(analysis)

    issues += check_unactionable_items(analysis)

    return issues


def check_definition(analysis):
    """Check the top level of the analysis.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "analysis"
    tags = ("definition",)
    issues = warning_if_missing(
        ("name", "action_level", "occurrence_limit"),
        analysis,
        entity,
        analysis.id,
        tags,
    )
    return issues


def check_requirements(analysis):
    """Check the requirements.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "requirement"
    tags = ("requirements",)

    issues = []
    for requirement in analysis.requirements:
        issues += warning_if_missing(
            "description", requirement, entity, requirement.id, tags
        )
    return issues


def check_failure_modes(analysis):
    """Check the failure modes.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "failure_mode"
    tags = ("failure_modes", "report")

    issues = []
    for fm in analysis.failure_modes:
        issues += warning_if_missing("description", fm, entity, fm.id, tags)
    return issues


def check_effects(analysis):
    """Check the effects.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "effect"
    tags = ("effects", "report")

    issues = []
    for effect in analysis.effects:
        issues += warning_if_missing("description", effect, entity, effect.id, tags)
        issues += error_if_missing("severity", effect, entity, effect.id, tags)
    return issues


def check_causes(analysis):
    """Check the causes.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "cause"
    tags = ("causes", "report")

    issues = []
    for cause in analysis.causes:
        issues += warning_if_missing("description", cause, entity, cause.id, tags)
        issues += error_if_missing("occurrence", cause, entity, cause.id, tags)
    return issues


def check_controls(analysis):
    """Check the design controls.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "control"
    tags = ("controls", "report")

    issues = []
    for control in analysis.controls:
        issues += warning_if_missing("description", control, entity, control.id, tags)
        issues += error_if_missing("detection", control, entity, control.id, tags)
    return issues


def check_mitigations(analysis):
    """Check the mitigations.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "mitigation"
    tags = ("mitigations",)

    issues = []
    for mitigation in analysis.mitigations:
        issues += warning_if_missing(
            "description", mitigation, entity, mitigation.id, tags
        )
        issues += error_if_missing(
            ("severity", "detection", "occurrence"),
            mitigation,
            entity,
            mitigation.id,
            tags,
        )
    return issues


def check_unactionable_items(analysis):
    """Check for unactionble failure modes.

    An failure mode is unactionable if it does not
    have at least one cause, one effect, and if each cause
    does not have at least one design control.

    Unactionable items do not show in the report or on the mitigations
    page due to the way the data is processed.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    issues = []
    for failure_mode in analysis.failure_modes:
        missing = []
        tags = []
        entity = []
        requirement = next(
            (
                req
                for req in analysis.requirements
                if req.id == failure_mode.requirement_id
            ),
            None,
        )
        if requirement:
            for effect in analysis.effects:
                if effect.failure_mode_id == failure_mode.id:
                    if effect.severity and effect.description:
                        break
            else:
                missing.append("Effect")
                tags.append("effects")
                entity.append("effect")

            for cause in analysis.causes:
                if cause.failure_mode_id == failure_mode.id:
                    if cause.occurrence and cause.description:
                        break
            else:
                missing.append("Cause")
                tags.append("causes")
                entity.append("cause")

            for control in analysis.controls:
                if control.failure_mode_id == failure_mode.id:
                    if control.detection and control.description:
                        break
            else:
                missing.append("Design Control")
                tags.append("controls")
                entity.append("control")

        if len(missing) > 0:

            message = (
                ", ".join(missing[:-1]) + " and " + missing[-1]
                if len(missing) > 1
                else missing[0]
            )

            issues.append(
                {
                    "type": "warning",
                    "entity": entity[0],
                    "id": analysis.id,
                    "field": None,
                    "message": f"Unactionable design requirement '{requirement.description}', with Failure Mode '{failure_mode.description}'. Requires at least one fully defined {message} to be actionable.",
                    "tags": ["requirements", "failure_modes"] + tags,
                }
            )

    return issues
