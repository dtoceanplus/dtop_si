# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Contains functions for generating the Excel export of the report."""

from io import BytesIO
from datetime import datetime
import xlsxwriter

from dtop_structinn.app.errors import InvalidExcelExport
from dtop_structinn.pyqfd.report import write_table
from dtop_structinn.pyfmea import validation


def export_to_excel(fmea: dict) -> BytesIO:
    """Export the FMEA report as an Excel (*.xlsx) file.

    Args:
        fmea (dict): A FMEA analysis

    Returns:
        BytesIO: a stream of binary I/O representing the Excel file.
    """
    if any(
        [issue["type"] == "error" for issue in validation.get_analysis_issues(fmea)]
    ):
        raise InvalidExcelExport

    fp = BytesIO()
    with xlsxwriter.Workbook(fp, {"in_memory": True}) as wb:

        index_sheet(wb, fmea)
        report_sheet(wb, fmea)

    fp.seek(0)
    return fp


def index_sheet(wb: xlsxwriter.Workbook, fmea: dict) -> None:
    """Add an overview sheet."""
    ws = wb.add_worksheet("index")
    ws.write(0, 0, "Summary Report", wb.add_format({"bold": True, "font_size": 16}))

    col_map = {" ": {"attr": None}, "  ": {"attr": None}}

    table = []
    table.append(["Analysis Name", fmea.name])
    table.append(["Action Level", fmea.action_level])
    table.append(["Occurrence Limit", fmea.occurrence_limit])
    table.append(["Date Created", str(fmea.created.strftime("%d/%m/%Y %X"))])
    table.append(["Last Updated", str(fmea.updated.strftime("%d/%m/%Y %X"))])
    table.append(["Date Exported", str(datetime.now().strftime("%d/%m/%Y %X"))])
    write_table(ws, table, col_map, start_row=2)


def report_sheet(wb: xlsxwriter.Workbook, fmea: dict) -> None:
    """Add the main report sheet."""
    ws = wb.add_worksheet("report")
    ws.write(0, 0, "Report", wb.add_format({"bold": True, "font_size": 16}))

    col_map = {
        "Requirement": {"attr": "req"},
        "Failure Mode": {"attr": "fm"},
        "Effect": {"attr": "eff"},
        "Severity": {"attr": "sev"},
        "Cause": {"attr": "cause"},
        "Occurrence": {"attr": "occ"},
        "Design Control": {"attr": "ctrl"},
        "Detection": {"attr": "det"},
        "RPN": {"attr": "rpn"},
        "Mitigation": {"attr": "mit"},
        "mSeverity": {"attr": "msev"},
        "mOccurrence": {"attr": "mocc"},
        "mDetection": {"attr": "mdet"},
        "mRPN": {"attr": "mrpn"},
        "Status": {"attr": "status"},
    }

    requirements = {req.id: req for req in fmea.requirements}
    failure_modes = {fm.id: fm for fm in fmea.failure_modes}
    effects = {effect.id: effect for effect in fmea.effects}
    causes = {cause.id: cause for cause in fmea.causes}
    controls = {control.id: control for control in fmea.controls}
    mitigations = {mit.id: mit for mit in fmea.mitigations}
    rpns = fmea.get_rpns()

    table = []
    for rpn in rpns:
        row = dict()
        row["eff"] = effects[rpn["effect_id"]].description
        row["sev"] = effects[rpn["effect_id"]].severity
        row["cause"] = causes[rpn["cause_id"]].description
        row["occ"] = causes[rpn["cause_id"]].occurrence
        row["ctrl"] = controls[rpn["control_id"]].description
        row["det"] = controls[rpn["control_id"]].detection

        failure_mode_id = effects[rpn["effect_id"]].failure_mode_id
        row["fm"] = failure_modes[failure_mode_id].description

        requirement_id = failure_modes[failure_mode_id].requirement_id
        row["req"] = requirements[requirement_id].description

        row["rpn"] = rpn["rpn"]

        row["mit"] = None
        row["msev"] = None
        row["mocc"] = None
        row["mdet"] = None

        if rpn["requires_mitigation"]:
            for mitigation in mitigations.values():
                if (rpn["effect_id"], rpn["cause_id"], rpn["control_id"]) == (
                    mitigation.effect_id,
                    mitigation.cause_id,
                    mitigation.control_id,
                ):
                    row["mit"] = mitigation.description
                    row["msev"] = mitigation.severity
                    row["mocc"] = mitigation.occurrence
                    row["mdet"] = mitigation.detection

        try:
            row["mrpn"] = row["msev"] * row["mocc"] * row["mdet"]
        except TypeError:
            row["mrpn"] = None

        # Determine the 'status' of each row
        row["status"] = "SUCCESS"
        rpn_status = row["mrpn"] if row["mrpn"] else row["rpn"]
        occ_status = row["mocc"] if row["mocc"] else row["occ"]
        if occ_status >= fmea.occurrence_limit:
            row["status"] = "WARNING"
        if rpn_status >= fmea.action_level:
            row["status"] = "ERROR"

        tablerow = []
        for col in col_map.values():
            tablerow.append(row[col["attr"]])
        table.append(tablerow)

    write_table(ws, table, col_map, start_row=2)
