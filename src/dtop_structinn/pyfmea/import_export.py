# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Handles importing exported FMEA analyses.

Despite the name of the file, export is not handled here.
"""
from dtop_structinn.pyfmea import models
from dtop_structinn.database import session


def import_analysis(data: dict, si_id: int = None):
    """Import an FMEA analysis."""
    data = data["data"]

    data["si_id"] = si_id
    del data["id"]
    del data["issues"]

    # Mappings between imported and newly generated IDs
    req_ids = {req["id"]: None for req in data["requirements"]}
    fm_ids = {fm["id"]: None for fm in data["failure_modes"]}
    eff_ids = {eff["id"]: None for eff in data["effects"]}
    cause_ids = {cause["id"]: None for cause in data["requirements"]}
    control_ids = {control["id"]: None for control in data["controls"]}

    requirements = data.pop("requirements")
    failure_modes = data.pop("failure_modes")
    effects = data.pop("effects")
    causes = data.pop("causes")
    controls = data.pop("controls")
    mitigations = data.pop("mitigations")
    notes = data.pop("notes")

    analysis = models.Fmea(**data)
    session.add(analysis)
    session.flush()
    fmea_id = analysis.id

    for req in requirements:
        id = req.pop("id")
        requirement = models.DesignRequirement(fmea_id=fmea_id, **req)
        session.add(requirement)
        session.flush()
        req_ids[id] = requirement.id

    for mode in failure_modes:
        id = mode.pop("id")
        mode["requirement_id"] = req_ids[mode["requirement_id"]]
        failure_mode = models.FailureMode(fmea_id=fmea_id, **mode)
        session.add(failure_mode)
        session.flush()

        fm_ids[id] = failure_mode.id

    for eff in effects:
        id = eff.pop("id")
        eff["failure_mode_id"] = fm_ids[eff["failure_mode_id"]]
        effect = models.Effect(fmea_id=fmea_id, **eff)
        session.add(effect)
        session.flush()

        eff_ids[id] = effect.id

    for cs in causes:
        id = cs.pop("id")
        cs["failure_mode_id"] = fm_ids[cs["failure_mode_id"]]
        cause = models.Cause(fmea_id=fmea_id, **cs)
        session.add(cause)
        session.flush()

        cause_ids[id] = cause.id

    for ctrl in controls:
        id = ctrl.pop("id")
        ctrl["failure_mode_id"] = fm_ids[ctrl["failure_mode_id"]]
        ctrl["cause_id"] = cause_ids[ctrl["cause_id"]]
        control = models.Control(fmea_id=fmea_id, **ctrl)
        session.add(control)
        session.flush()

        control_ids[id] = control.id

    for mit in mitigations:
        id = mit.pop("id")
        mit["control_id"] = control_ids[mit["control_id"]]
        mit["cause_id"] = cause_ids[mit["cause_id"]]
        mit["effect_id"] = eff_ids[mit["effect_id"]]
        mitigation = models.Mitigation(fmea_id=fmea_id, **mit)
        session.add(mitigation)

    for note in notes:
        del note["id"]
        note = models.FmeaNote(fmea_id=fmea_id, **note)
        session.add(note)

    session.commit()
    return analysis
