# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Pydantic schemas used for import and export."""
from dtop_structinn.pyfmea.schemas import Analysis
from pydantic import BaseModel, Field


class AnalysisCurrent(BaseModel):
    """The current analysis format."""

    data: Analysis
    type: str = Field("fmea", const=True)
    version: int

    class Config:
        """Pydantic configuration."""

        extra = "forbid"


class AnalysisOldStyle(BaseModel):
    """A very old format for export.

    Although this format is no longer used, people who tested the
    software may still have these and want to use them.
    """

    id: int
    name: str
    action_level: int
    occurrence_limit: int
    requirements: list
    failure_modes: list
    effects: list
    causes: list
    controls: list
    mitigations: list


# Unlike QFD, the current FMEA import functionality
# ONLY supports new-style FMEA analyses.
SupportedAnalysisSchemas = AnalysisCurrent
