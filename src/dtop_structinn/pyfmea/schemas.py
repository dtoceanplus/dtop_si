# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Pydantic schemas used in FMEA."""
from __future__ import annotations

from typing import Optional, List
from pydantic import Field
from dtop_structinn.schemas import BaseSchema, BaseReportSchema
from dtop_structinn.notes.schemas import Note

analysis_id = int
requirement_id = int
effect_id = int
cause_id = int
control_id = int
failure_mode_id = int
mitigation_id = int


class Analysis(BaseSchema):
    """Schema for an FMEA Analysis."""

    id: analysis_id = Field(..., readOnly=True)
    si_id: Optional[int]
    name: str = ""
    action_level: Optional[int]
    occurrence_limit: Optional[int]
    notes: List[Note] = Field(default_factory=list, readOnly=True)
    requirements: list[DesignRequirement] = Field(default_factory=list)
    failure_modes: list[FailureMode] = Field(default_factory=list)
    effects: list[Effect] = Field(default_factory=list)
    causes: list[Cause] = Field(default_factory=list)
    controls: list[Control] = Field(default_factory=list)
    mitigations: list[Mitigation] = Field(default_factory=list)
    issues: list = Field(default_factory=list)


class DesignRequirement(BaseSchema):
    """Schema for an FMEA Requirement."""

    id: requirement_id = Field(..., readOnly=True)
    description: str = ""


class FailureMode(BaseSchema):
    """Schema for a failure mode."""

    id: failure_mode_id = Field(..., readOnly=True)
    requirement_id: requirement_id
    description: str = ""


class Effect(BaseSchema):
    """Schema for an effect."""

    id: effect_id = Field(..., readOnly=True)
    failure_mode_id: int
    description: str = ""
    severity: Optional[int]


class Cause(BaseSchema):
    """Schema for a cause."""

    id: cause_id = Field(..., readOnly=True)
    failure_mode_id: int
    description: str = ""
    occurrence: Optional[int]


class Control(BaseSchema):
    """Schema for a control."""

    id: control_id = Field(..., readOnly=True)
    failure_mode_id: int
    cause_id: int
    description: str = ""
    detection: Optional[int]


class Mitigation(BaseSchema):
    """Schema for a mitigation."""

    id: mitigation_id = Field(..., readOnly=True)
    effect_id: effect_id
    cause_id: cause_id
    control_id: control_id
    description: str = ""
    severity: Optional[int]
    occurrence: Optional[int]
    detection: Optional[int]


class RpnReport(BaseReportSchema):
    """Schema for the FMEA report."""

    effect_id: effect_id
    cause_id: cause_id
    control_id: control_id
    rpn: Optional[int]
    requires_mitigation: Optional[bool]


Analysis.update_forward_refs()
