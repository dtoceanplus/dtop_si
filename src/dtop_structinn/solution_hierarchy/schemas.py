# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations
from typing import List
from pydantic import Field, UUID4, BaseModel
from dtop_structinn.schemas import non_empty_str

solution_hierarchy_id = UUID4
solution_hierarchy_child_uuid = UUID4


class SolutionHierarchyEntry(BaseModel):
    """Schema for input and ouptut of a solution hierarchy entry."""

    uuid: UUID4
    description: str
    children: List[SolutionHierarchyEntry] = Field(default_factory=list)
    units: str
    direction: int


class SolutionHierarchy(BaseModel):
    """Schema for the solution hierarchy."""

    uuid: solution_hierarchy_id
    description: non_empty_str
    children: List[SolutionHierarchyEntry] = Field(default_factory=list)


SolutionHierarchyEntry.update_forward_refs()
