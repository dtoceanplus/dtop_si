# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from importlib import resources
from uuid import UUID
from dtop_structinn.solution_hierarchy.schemas import SolutionHierarchy


def flatten_hierarchy(entry, level=1):
    """Flatten a solution hierarchy (generator)."""
    for child in entry["children"]:
        child = {
            "parent_uuid": entry["uuid"],
            "level": level,
            "parent": {k: v for k, v in entry.items() if k != "children"},
            **{k: v for k, v in child.items()},
        }
        yield from flatten_hierarchy(child, level + 1)
        child.pop("children", None)
        yield child


def get_children(hierarchy, uuid):
    """Get items from next level of solution hierarchy."""

    if isinstance(uuid, (str, UUID)):
        uuid = (uuid,)

    uuid = {str(u) for u in uuid}

    for entry in flatten_hierarchy(hierarchy):
        if str(entry["parent_uuid"]) in uuid:
            yield (entry)


def get_all_at_level(hierarchy, level):
    """Get all members of the solution hierarchy at given level."""
    for entry in flatten_hierarchy(hierarchy):
        if entry["level"] == level:
            yield (entry)


hierarchy_path = resources.files("dtop_structinn") / "resources/"
# hierarchies = {}
#
# for path in hierarchy_path.glob("*.json"):
#     try:
#         with path.open() as fd:
#             content = SolutionHierarchy.parse_file(fd)
#     except ValidationError as e:
#         logging.exception(e)
#     else:
#         hierarchies[content["uuid"]] = content

hierarchy = SolutionHierarchy.parse_file(
    hierarchy_path / "solution_hierarchy.json"
).dict()


#
# def get_next_level(current):
#     if isinstance(current, dict):
#         for entry in current["children"]:
#             yield entry
#     else:
#         for entry in current:
#             yield from get_next_level(entry)
