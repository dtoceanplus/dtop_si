# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Functions that support integration with the main module."""

from dtop_structinn.pyfmea.models import Fmea
from dtop_structinn.pyqfd.models import Qfd
from dtop_structinn.database import session


def get_si_analysis_by_id(si_id):
    """Return analysis IDs with given si_id, or None if none exist."""
    fmeas = Fmea.all(("id",), si_id=si_id)
    qfds = Qfd.all(("id",), si_id=si_id)
    if fmeas or qfds:
        return {
            "id": si_id,
            "fmea_ids": [fmea.id for fmea in fmeas],
            "qfd_ids": [qfd.id for qfd in qfds],
        }


def delete_si_analysis_by_id(si_id):
    """Delete matching rows and return them."""
    # session.query(Fmea).filter(Fmea.si_id=si_id).delete()
    # try:
    #     session.query(Qfd).filter(Qfd.si_id=si_id).delete()
    records = session.query(Fmea).filter_by(si_id=si_id).all()
    records += session.query(Qfd).filter_by(si_id=si_id).all()
    for record in records:
        session.delete(record)
    session.commit()
    return records
