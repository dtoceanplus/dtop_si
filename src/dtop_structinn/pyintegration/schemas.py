# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

from pydantic import BaseModel
from typing import List, Optional


class BaseSchema(BaseModel):
    """Base model for other schemas."""

    class Config:
        """Settings for the BaseModel."""

        orm_mode = True


class Analysis(BaseSchema):
    id: Optional[int] = ...
    qfd_ids: Optional[List[int]] = []
    fmea_ids: Optional[List[int]] = []
