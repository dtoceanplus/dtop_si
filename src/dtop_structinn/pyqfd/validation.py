# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Validation of QFD Analyses.

This module contains functions for validating a QFD Analysis object.

The `get_analysis_issues` function accumulates the issues from
elsewhere.

An issue has a type ("warning", "error", "question").
    * An error prevents the analysis results from being calculated.
    * A warning means the report will probably not make sense to the
        end user.
    * A question is request that the user double checks something which
        may be correct, but is likely mistake.

An issue also has the following fields:
   obj: The object the field should be on.
   entity: A string pointing towards the type of `obj`.
   id: A unique identifier for obj.
   tags: A tuple giving context to the issue.
   message: A human readable message explaining the issue.
"""

from typing import Union, Iterable
from itertools import product

missing_field = "Empty field: {}"


def get_analysis_issues(analysis):
    """Get the current issues for an analysis.

    This function collates the outputs from separate
    validation routines for different aspects of the analysis.

    Args:
        analysis: The analysis to check for issues.

    Returns:
        A list of issues.
    """
    issues = []

    issues += check_definition(analysis)
    issues += check_requirements(analysis)
    issues += check_functional_requirements(analysis)
    issues += check_impacts(analysis)
    issues += check_correlations(analysis)
    issues += check_triz(analysis)
    issues += check_solutions(analysis)
    issues += check_achievements(analysis)

    issues += check_priorities(analysis)
    return issues


def warning_if_missing(field: Union[str, Iterable[str]], obj, entity, id, tags):
    """Warns for missing fields.

    Returns a list of issues with type *warning*
    for each field which is not present or is empty.

    This always returns a list, even if field is a single string.

    Args:
        field: The name of the field, or a list of names.
        obj: The object the field should be on.
        entity: A string pointing towards the type of `obj`.
        id: A unique identifier for obj.
        tags: A tuple giving context to the issue.

    Returns:
        A list of issues.
    """
    fields = [field] if isinstance(field, str) else field
    return [
        {
            "type": "warning",
            "entity": entity,
            "id": id,
            "field": field,
            "message": missing_field.format(field),
            "tags": tags,
        }
        for field in fields
        if getattr(obj, field) is None or getattr(obj, field) == ""
    ]


def error_if_missing(field: Union[str, Iterable[str]], obj, entity, id, tags):
    """Errors for missing fields.

    Returns a list of issues with type *error*
    for each field which is not present or is empty.

    This always returns a list, even if field is a single string.

    Args:
        field: The name of the field, or a list of names.
        obj: The object the field should be on.
        entity: A string pointing towards the type of `obj`.
        id: A unique identifier for obj.
        tags: A tuple giving context to the issue.

    Returns:
        A list of issues.
    """
    fields = [field] if isinstance(field, str) else field
    return [
        {
            "type": "error",
            "entity": entity,
            "id": id,
            "field": field,
            "message": missing_field.format(field),
            "tags": tags,
        }
        for field in fields
        if getattr(obj, field) is None or getattr(obj, field) == ""
    ]


def check_definition(analysis):
    """Check the top level of the analysis.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "analysis"
    tags = ("definition",)
    issues = warning_if_missing(
        ("name", "objective"), analysis, entity, analysis.id, tags
    )
    return issues


def check_requirements(analysis):
    """Check the requirements.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "requirement"
    tags_warning = ("requirements",)
    tags_error = ("requirements", "report")

    issues = []
    for requirement in analysis.requirements:
        issues += warning_if_missing(
            "description", requirement, entity, requirement.id, tags_warning
        )
        issues += error_if_missing(
            "importance", requirement, entity, requirement.id, tags_error
        )
    return issues


def check_functional_requirements(analysis):
    """Check the functional requirements.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "functional_requirement"
    tags_warning = ("functional_requirements",)
    tags_error = ("functional_requirements", "report")

    issues = []
    for functional_requirement in analysis.functional_requirements:
        issues += warning_if_missing(
            ("description", "units"),
            functional_requirement,
            entity,
            functional_requirement.id,
            tags_warning,
        )
        issues += error_if_missing(
            ("direction", "target", "engineering_difficulty", "delivery_difficulty"),
            functional_requirement,
            entity,
            functional_requirement.id,
            tags_error,
        )
    return issues


def check_impacts(analysis):
    """Check the impacts.

    Currently this doesn't have anything to check.

    Args:
        analysis: The analysis.

    Returns:
        An empty list.
    """
    return []


def check_correlations(analysis):
    """Check the correlations.

    Currently this doesn't have anything to check.

    Args:
        analysis: The analysis.

    Returns:
        An empty list.
    """
    return []


def check_triz(analysis):
    """Check the TRIZ classes.

    Currently this doesn't have anything to check.

    Args:
        analysis: The analysis.

    Returns:
        An empty list.
    """
    return []


def check_solutions(analysis):
    """Check the solutions.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "solution"
    tags = ("solutions",)

    issues = []
    for solution in analysis.solutions:
        issues += warning_if_missing(
            "description",
            solution,
            entity,
            solution.id,
            tags,
        )
    return issues


def check_achievements(analysis):
    """Check the achievements, and that they are defined.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    entity = "achievement"
    tags = ("solutions", "achievements")

    issues = []

    possible_achievement_ids = list(
        product(
            [fr.id for fr in analysis.functional_requirements],
            [solution.id for solution in analysis.solutions],
        )
    )

    for achievement_id in possible_achievement_ids:
        achievement = [
            ach
            for ach in analysis.achievements
            if ach.functional_requirement_id == achievement_id[0]
            and ach.solution_id == achievement_id[1]
        ]
        if achievement:
            issues += warning_if_missing(
                "value",
                achievement[0],
                entity,
                achievement_id,
                tags,
            )
        else:
            issues.append(
                {
                    "type": "warning",
                    "entity": entity,
                    "id": achievement_id,
                    "field": "value",
                    "message": missing_field.format("value"),
                    "tags": tags,
                }
            )
    return issues


def check_priorities(analysis):
    """Check importance of requirements matches impacts.

    Ranks the requirements based on both the importance and the sum
    of the impacts, and checks the two rankings match.

    Args:
        analysis: The analysis.

    Returns:
        An empty list if check passes, else an issue list with a single
        item.
    """
    issues = []
    requirements = sorted(
        (
            requirement
            for requirement in analysis.requirements
            if requirement.importance is not None
        ),
        key=lambda r: r.importance,
    )
    impacts = [
        sum(i.impact for i in analysis.impacts if i.requirement_id == r.id)
        for r in requirements
    ]
    if not all(a <= b for a, b in zip(impacts, impacts[1:])):
        issues.append(
            {
                "type": "question",
                "entity": "analysis",
                "id": analysis.id,
                "field": None,
                "message": "Importance of customer requirements does not correspond to the impacts. Consider reevaluating.",
                "tags": ("impacts", "requirements"),
            }
        )
    return issues
