# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Pydantic schemas used for import and export."""
from dtop_structinn.pyqfd.schemas import Analysis
from pydantic import BaseModel, Field
from typing import Union


class AnalysisCurrent(BaseModel):
    """The current analysis format."""

    data: Analysis
    type: str = Field("qfd/triz", const=True)
    version: int

    class Config:
        """Pydantic configuration."""

        extra = "forbid"


class AnalysisOldStyle(BaseModel):
    """A very old format for export.

    Although this format is no longer used, people who tested the
    software may still have these and want to use them.
    """

    correlations: list
    functional_requirements: list
    id: int
    name: str
    objective: str
    requirements: list
    solutions: list


SupportedAnalysisSchemas = Union[AnalysisCurrent, AnalysisOldStyle]
