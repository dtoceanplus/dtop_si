# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Pydantic schemas used in QFD."""
from __future__ import annotations

from typing import List, Optional, Set

from pydantic import Field, PositiveInt, root_validator

from dtop_structinn.notes.schemas import Note
from dtop_structinn.pyqfd.enumerations import (
    CorrelationEnum,
    DifficultyEnum,
    ImpactEnum,
    ImprovementEnum,
)
from dtop_structinn.schemas import BaseSchema, finite_float, BaseReportSchema
from dtop_structinn.solution_hierarchy.schemas import (  # noqa : E401
    solution_hierarchy_id,
    solution_hierarchy_child_uuid,
)
from dtop_structinn.triz.schemas import (
    triz_id,
    inventive_principle_id,
)

analysis_id = int
functional_requirement_id = int
requirement_id = int
solution_id = int
achievement_id = int


class Analysis(BaseSchema):
    """Schema for a QfdAnalysis."""

    id: analysis_id = Field(..., readOnly=True)
    si_id: Optional[int]
    name: str = Field(default="")
    objective: str = Field(default="")
    notes: List[Note] = Field(default_factory=list, readOnly=True)
    solution_hierarchy_id: Optional[solution_hierarchy_id]
    solution_hierarchy_level: Optional[int] = Field(default=1)
    child_analyses: List[analysis_id] = Field(default_factory=list)
    parent_analysis_id: Optional[analysis_id]

    requirements: List[Requirement] = Field(default_factory=list, readOnly=True)
    functional_requirements: List[FunctionalRequirement] = Field(
        default_factory=list, readOnly=True
    )
    solutions: List[Solution] = Field(default_factory=list, readOnly=True)
    achievements: List[Achievement] = Field(default_factory=list, readOnly=True)
    impacts: List[Impact] = Field(default_factory=list, readOnly=True)
    correlations: List[Correlation] = Field(default_factory=list, readOnly=True)
    issues: List = Field(default_factory=list)
    # sources: List = Field(default_factory=list)


class FunctionalRequirement(BaseSchema):
    """Schema for a Functional Requirement."""

    id: functional_requirement_id = Field(..., readOnly=True)
    description: str = Field(default="")
    direction: Optional[ImprovementEnum]
    target: Optional[finite_float]
    units: str = Field(default="")
    engineering_difficulty: Optional[DifficultyEnum]
    delivery_difficulty: Optional[DifficultyEnum]
    triz: Set[triz_id] = Field(default_factory=set)
    solution_hierarchy_child_uuid: Optional[solution_hierarchy_child_uuid]


#
#    achievements: Optional[List[Achievement]] = None
#    impacts: Optional[List[Impact]] = None
#    correlations: Optional[List[Correlation]] = None


class Requirement(BaseSchema):
    """Schema for Requirements."""

    id: requirement_id = Field(..., readOnly=True)
    importance: Optional[PositiveInt]
    description: str = Field(default="")
    solution_hierarchy_child_uuid: Optional[solution_hierarchy_child_uuid]


class Solution(BaseSchema):
    """Schema for Solutions."""

    id: solution_id = Field(..., readOnly=True)
    description: str = Field(default="")


class Achievement(BaseSchema):
    """Schema for Achievements."""

    solution_id: solution_id
    functional_requirement_id: functional_requirement_id
    value: Optional[finite_float]


class Impact(BaseSchema):
    """Schema for Impacts."""

    functional_requirement_id: functional_requirement_id
    requirement_id: requirement_id
    impact: ImpactEnum


class Correlation(BaseSchema):
    """Schema for Correlations."""

    functional_requirement_1_id: functional_requirement_id
    functional_requirement_2_id: functional_requirement_id
    correlation: CorrelationEnum

    @root_validator
    def check_not_same_functional_requirement(cls, values):
        """Prevent self-correlation.

        A functional requirement should not be able to correlate to
        itself.
        """
        if (
            "functional_requirement_1_id" in values
            and "functional_requirement_2_id" in values
        ):
            if (
                values["functional_requirement_1_id"]
                == values["functional_requirement_2_id"]
            ):
                raise ValueError(
                    "the ids must be for different functional requirements"
                )
        return values

    def __init__(self, **data):
        super().__init__(**data)
        if self.functional_requirement_1_id > self.functional_requirement_2_id:
            self.functional_requirement_1_id, self.functional_requirement_2_id = (
                self.functional_requirement_2_id,
                self.functional_requirement_1_id,
            )


# reports


class OrganisationalImpact(BaseReportSchema):
    """Schema for organisational impact, for reports."""

    functional_requirement_id: int
    impact: int


class Importance(BaseReportSchema):
    """Schema for importance, for reports."""

    functional_requirement_id: int
    importance: int


class RelativeDeviationFromTarget(BaseReportSchema):
    """Schema for relative deviation from targets."""

    functional_requirement_id: int
    deviation: Optional[float] = Field(...)


class ActualDeviationFromTarget(BaseReportSchema):
    """Schema for actual deviation from targets."""

    functional_requirement_id: int
    deviation: Optional[float] = Field(...)
    units: str


class DeviationFromTargets(BaseReportSchema):
    """Schema for deviation from targets for reports."""

    solution_id: int
    relative_deviation: List[RelativeDeviationFromTarget]
    actual_deviation: List[ActualDeviationFromTarget]


class FunctionalRequirementSummary(BaseReportSchema):
    """Schema for functional requirement summary for reports."""

    functional_requirement_id: int
    importance: int
    organisational_impact: int
    target: float
    units: str
    best_achieved: Optional[float] = Field(
        ...
    )  # the highest acheivement against this functional requirement
    worst_achieved: Optional[float] = Field(
        ...
    )  # the worst achievement against this functional requirement
    achieved_by: int  # the number of solutions that achieve (or exceed) the target
    missed_by: int  # the number of solutions that miss the target


class SolutionIdeality(BaseReportSchema):
    """Schema for the ideality report for a solution."""

    solution_id: int
    targets_achieved: int
    targets_missed: int
    ideality: Optional[float] = Field(
        ...
    )  # fraction of ideal, or None if total ideality is 0


class SuggestedInventivePrinciple(BaseReportSchema):
    """Schema for suggested inventive principles."""

    worsening: triz_id
    improving: triz_id
    principle: inventive_principle_id


class ConflictingFunctionalRequirement(BaseReportSchema):
    """Schema for reporting a single conflict."""

    functional_requirement_id: functional_requirement_id
    contradictions: List[SuggestedInventivePrinciple]


class FunctionalRequirementConflicts(BaseReportSchema):
    """Scehma for reporting conflicts."""

    functional_requirement_id: functional_requirement_id
    worst_conflicts: List[ConflictingFunctionalRequirement]


class Report(BaseReportSchema):
    """Schema for output of a QFD Report."""

    deviation_from_targets: List[DeviationFromTargets]
    functional_requirement_summaries: List[FunctionalRequirementSummary]
    solution_idealities: List[SolutionIdeality]
    functional_requirements_conflicts: List[FunctionalRequirementConflicts]


Analysis.update_forward_refs()
FunctionalRequirement.update_forward_refs()
