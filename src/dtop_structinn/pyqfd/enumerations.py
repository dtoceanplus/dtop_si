# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Enumerations for use in QFD."""
from enum import IntEnum


class DifficultyEnum(IntEnum):
    """Enumeration for difficulty."""

    low = 1
    low_moderate = 2
    moderate = 3
    moderate_high = 4
    high = 5


class ImprovementEnum(IntEnum):
    """Enumeration for improvement direction."""

    lower_is_better = -1
    higher_is_better = 1


class ImpactEnum(IntEnum):
    """Enumeration for impact."""

    none = 0
    low = 1
    medium = 4
    high = 9


class CorrelationEnum(IntEnum):
    """Enumeration for correlation."""

    high_negative = -9
    medium_negative = -4
    low_negative = -1
    none = 0
    low_positive = 1
    medium_positive = 4
    high_positive = 9
