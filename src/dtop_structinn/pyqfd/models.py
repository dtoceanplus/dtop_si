# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""SQLAlchemy models for QFD."""

from sqlalchemy import (
    CheckConstraint,
    Column,
    Float,
    ForeignKey,
    Integer,
    String,
    event,
)
from sqlalchemy.orm import relationship

from dtop_structinn.database import (
    UUID,
    Base,
    IntSet,
    session,
    update_analysis_timestamp,
)
from dtop_structinn.declarations import DESCRIPTION_LENGTH


class Qfd(Base):
    """The SQLAlchemy ORM model for a QFD Analysis.

    A QFD analysis may be top level, in which case it will have no
    parent_id, or it can be the result of "diving deeper" into an
    analysis, usually when the solution hierarchy is being used.

    Attributes:
        id: The id assigned by the database.
        name: The name of the analysis.
        objective: The top level objective of the analysis.
        parent_id: The id of the QFD analysis's parent, if applicable.
        solution_hierarchy_uuid: The uuid of the solution hierarchy if
            in use.
        child_analyses: The direct sub-analyses of this analysis.
        functional_requirements: The functional requirements for this
            analysis.
        requirements: The stakeholder requirements for this analysis.
        correlations: The correlations for this analysis.
        impacts: The impacts for this analysis.
        solutions: The solutions for this analysis.
        achievements: The achievements for this analysis.
        notes: The notes for each section of the analysis.
    """

    __tablename__ = "qfd_analysis"

    id = Column(Integer, primary_key=True, index=True)
    si_id = Column(Integer, index=True)
    name = Column(String(DESCRIPTION_LENGTH))
    objective = Column(String(DESCRIPTION_LENGTH))
    parent_analysis_id = Column(
        Integer, ForeignKey("qfd_analysis.id", ondelete="CASCADE")
    )
    solution_hierarchy_id = Column(UUID)
    solution_hierarchy_level = Column(Integer, default=1, nullable=False)
    # Todo: do we need this? If we do, it should only be the id.
    # child_analyses = relationship(
    #     "Qfd", backref=backref("parent_analysis", remote_side=[id])
    # )

    functional_requirements = relationship(
        "FunctionalRequirement", backref="qfd", cascade="all, delete-orphan"
    )
    requirements = relationship(
        "Requirement", backref="qfd", cascade="all, delete-orphan"
    )
    correlations = relationship(
        "Correlation", backref="qfd", cascade="all, delete-orphan"
    )
    impacts = relationship("Impact", backref="qfd", cascade="all, delete-orphan")
    solutions = relationship("Solution", backref="qfd", cascade="all, delete-orphan")
    achievements = relationship(
        "Achievement", backref="qfd", cascade="all, delete-orphan"
    )
    notes = relationship("Note", backref="qfd", cascade="all, delete-orphan")
    #    sources = relationship(
    #        "Source", backref="qfd", cascade="all, delete-orphan"
    #    )

    @classmethod
    def create(
        cls,
        si_id=None,
        name=None,
        objective=None,
        parent_analysis_id=None,
        solution_hierarchy_id=None,
        **extra,
    ):
        """Create a new QFD analysis.

        Args:
            si_id: When in integrated (main module) mode,
                this is the entity id of the SI module.
            name: The name of the analysis.
            objective:
                The objective of the analysis.
            parent_analysis_id:
                If this is a sub analysis, the id of the parent.
            solution_hierarchy_id:
                If a solution hierarchy is being used, its id.

        Returns:
            The analysis ORM object.
        """
        analysis = Qfd(
            si_id=si_id,
            name=name,
            objective=objective,
            parent_analysis_id=parent_analysis_id,
            solution_hierarchy_id=solution_hierarchy_id,
        )
        session.add(analysis)
        session.commit()
        return analysis

    def add_functional_requirement(self, **fields):
        """Add a functional requirement to the analysis.

        Args:
            **fields: All fields to be passed to the
                FunctionalRequirement init method.

        Returns:
            The functional requirement ORM object.
        """
        functional_requirement = FunctionalRequirement(qfd_id=self.id, **fields)
        session.add(functional_requirement)
        session.commit()
        return functional_requirement

    def add_requirement(self, **fields):
        """Add a requirement to the analysis.

        Args:
            **fields: All fields to be passed to the
                Requirement init method.

        Returns:
            The requirement ORM object.
        """
        requirement = Requirement(qfd_id=self.id, **fields)
        session.add(requirement)
        session.commit()
        return requirement

    def add_solution(self, **fields):
        """Add a solution to the analysis.

        Args:
            **fields: All fields to be passed to the
                Solution init method.

        Returns:
            The solution ORM object.
        """
        solution = Solution(qfd_id=self.id, **fields)
        session.add(solution)
        session.commit()
        return solution

    def add_or_update_impact(self, functional_requirement_id, requirement_id, impact):
        """Add or update an impact.

        If an impact of a FunctionalRequirement on a
        Requirement already exists, it is update. Otherwise
        it is created.

        Args:
            functional_requirement_id:
                The id of the FunctionalRequirement.
            requirement_id: The id of the Requirement.
            impact: The impact.

        Returns:
            The impact ORM object.
        """
        fr_id = functional_requirement_id
        req_id = requirement_id

        if impact_obj := Impact.one(
            qfd_id=self.id,
            functional_requirement_id=fr_id,
            requirement_id=req_id,
        ):
            impact_obj.impact = impact

        else:
            functional_requirement = FunctionalRequirement.one(qfd_id=self.id, id=fr_id)
            if functional_requirement is None:
                raise Exception(
                    "functional requirement does not exist on this analysis."
                )

            requirement = Requirement.one(qfd_id=self.id, id=req_id)
            if requirement is None:
                raise Exception("requirement does not exist on this analysis.")

            impact_obj = Impact(
                qfd_id=self.id,
                functional_requirement_id=fr_id,
                requirement_id=req_id,
                impact=impact,
            )
            session.add(impact_obj)

        session.commit()
        return impact_obj

    def add_or_update_correlation(
        self, functional_requirement_1_id, functional_requirement_2_id, correlation
    ):
        """Add or update a correlation.

        If a correlation between two functional requirements exists,
        it is updated. Otherwise it is created. The functional
        requirement ids must be in ascending order.

        Args:
            functional_requirement_1_id:
                The id of the first FunctionalRequirement.
            functional_requirement_2_id:
                The id of the second FunctionalRequirement.
            correlation: The correlation between fr1_id and fr2_id.

        Returns:
            The correlation ORM object.
        """
        fr1_id = functional_requirement_1_id
        fr2_id = functional_requirement_2_id

        if correlation_obj := Correlation.one(
            qfd_id=self.id,
            functional_requirement_1_id=fr1_id,
            functional_requirement_2_id=fr2_id,
        ):
            correlation_obj.correlation = correlation

        else:
            functional_requirement_1 = FunctionalRequirement.one(
                qfd_id=self.id, id=fr1_id
            )
            if functional_requirement_1 is None:
                raise Exception(
                    "functional requirement does not exist on this analysis."
                )

            functional_requirement_2 = FunctionalRequirement.one(
                qfd_id=self.id, id=fr2_id
            )
            if functional_requirement_2 is None:
                raise Exception(
                    "functional requirement does not exist on this analysis."
                )

            correlation_obj = Correlation(
                qfd_id=self.id,
                functional_requirement_1_id=fr1_id,
                functional_requirement_2_id=fr2_id,
                correlation=correlation,
            )
            session.add(correlation_obj)

        session.commit()
        return correlation_obj

    def add_or_update_note(self, tag, content):
        """Add or update a note on the analysis.

        Args:
            tag: The note tag being updated/created.
            content: The content of the note.

        Returns:
            The note ORM object.
        """
        if note_obj := Note.one(qfd_id=self.id, tag=tag):
            note_obj.content = content
        else:
            note_obj = Note(qfd_id=self.id, tag=tag, content=content)
            session.add(note_obj)

        session.commit()

        return note_obj

    def add_or_update_achievement(self, functional_requirement_id, solution_id, value):
        """Add or update an achievement.

        If an achievement already exists for a
        solution/functional requirement pair, it is updated.
        Otherwise it is created.

        Args:
            functional_requirement_id:
                The id of the FunctionalRequirement.
            solution_id:
                The id of the Solution.
            value:
                The achieved value.

        Returns:
            The achievement ORM object.
        """
        achievement = value

        if achievement_obj := Achievement.one(
            qfd_id=self.id,
            functional_requirement_id=functional_requirement_id,
            solution_id=solution_id,
        ):
            achievement_obj.value = achievement

        else:
            functional_requirement = FunctionalRequirement.one(
                qfd_id=self.id, id=functional_requirement_id
            )
            if functional_requirement is None:
                raise Exception(
                    "functional requirement does not exist on this analysis."
                )

            solution = Solution.one(qfd_id=self.id, id=solution_id)
            if solution is None:
                raise Exception("solution does not exist on this analysis.")

            achievement_obj = Achievement(
                qfd_id=self.id,
                functional_requirement_id=functional_requirement_id,
                solution_id=solution_id,
                value=value,
            )
            session.add(achievement_obj)

        session.commit()
        return achievement_obj


class Impact(Base):
    """The SQLAlchemy ORM model for impacts.

    Impacts express the relationship between a functional requirement
    and a requirement.

    No impact is represented by a value of 0, with the values 1, 4 and 9
    representing low, medium and high impact.

    Attributes:
        functional_requirement_id: The id of the relevant functional
            requirement.
        requirement_id: The id of the relevant stakeholder requirement.
        impact: The amount (0, 1, 4, 9) that the functional requirement
            affects the requirement.
        functional_requirement: The functional requirement.
    """

    __tablename__ = "qfd_impact"
    analysis = relationship("Qfd", backref="impact")

    qfd_id = Column(
        Integer, ForeignKey("qfd_analysis.id", ondelete="CASCADE"), nullable=False
    )  # Todo: is this superfluous?
    functional_requirement_id = Column(
        Integer,
        ForeignKey("qfd_functional_requirement.id", ondelete="CASCADE"),
        primary_key=True,
    )
    requirement_id = Column(
        Integer, ForeignKey("qfd_requirement.id", ondelete="CASCADE"), primary_key=True
    )
    impact = Column(Integer)
    # UniqueConstraint(functional_requirement_id, requirement_id)


class Requirement(Base):
    """SQLAlchemy ORM model for QFD Stakeholder Requirements.

    Attributes:
        id: The id assigned by the database.
        qfd_id: The QFD Analysis to which this requirement relates.
        description: The description of the requirement.
        importance: The relative importance stored as an integer. Lower
            is better.
        solution_hierarchy_child_uuid: The UUID of the solution
            hierarchy entry, if applicable.
        impacts: The collection of impacts on this requirement.
    """

    __tablename__ = "qfd_requirement"
    analysis = relationship("Qfd", backref="requirement")

    id = Column(Integer, primary_key=True, index=True)
    qfd_id = Column(
        Integer, ForeignKey("qfd_analysis.id", ondelete="CASCADE"), nullable=False
    )
    description = Column(String(DESCRIPTION_LENGTH))
    importance = Column(Integer)
    solution_hierarchy_child_uuid = Column(UUID)


class FunctionalRequirement(Base):  # in the spreadsheets this is called solution
    """The SQLAlchemy ORM model for a functional requirement.

    Functional requirement represents the technical/engineering
    requirements. They have units, and a target value. The
    FunctionalRequirement objects also contain information about how
    difficult the target is to achieve.

    Attributes:
        id: The database id of the functional requirement.
        qfd_id: The QFD Analysis to which the functional requirement
            applies.
        description: The description of the functional requirement.
        target: The target value to be achieved.
        units: The units of the target value.
        direction: If it is better to be higher than the target, 1, else
            -1.
        engineering_difficulty: The difficulty to engineer.
        delivery_difficulty: The difficulty to the organisation to
            deliver.
        solution_hierarchy_child_uuid: The UUID of the solution
            hierarchy entry, if applicable.
        triz: The collection of TRIZ classes which apply to this
            functional requirement.
    """

    __tablename__ = "qfd_functional_requirement"
    analysis = relationship("Qfd", backref="functional_requirement")

    id = Column(Integer, primary_key=True, index=True)
    qfd_id = Column(
        Integer, ForeignKey("qfd_analysis.id", ondelete="CASCADE"), nullable=False
    )

    description = Column(String(DESCRIPTION_LENGTH))
    target = Column(Float)
    units = Column(String(DESCRIPTION_LENGTH))
    direction = Column(Integer)
    engineering_difficulty = Column(Integer)
    delivery_difficulty = Column(Integer)
    solution_hierarchy_child_uuid = Column(UUID)
    triz = Column(IntSet, default=set)


class Correlation(Base):
    """The SQL Alchemy ORM model for a correlation.

    A correlation is between two functional requirements.
    If the correlation is positive, then there is positive correlation
    between the functional requirements. If it is negative, the
    correlation is negative.

    The QFD values 9, 4 and 1 are used to represent the strength of the
    correlation. A correlation of 0 indicates that the functional
    requirements are not correlated.

    Attributes:
        id: The id assigned by the database.
        qfd_id: The id of the QFD Analysis in question.
        functional_requirement_1_id: The id of the first functional
            requirement.
        functional_requirement_2_id: The id of the second functional
            requirement.
        correlation: The value of the correlation
            (-9, -4, -1, 0, 1, 4, 9).
    """

    __tablename__ = "qfd_correlation"
    analysis = relationship("Qfd", backref="correlation")

    __table_args__ = (
        CheckConstraint(
            "functional_requirement_2_id > functional_requirement_1_id", name="fr_order"
        ),
    )
    qfd_id = Column(
        Integer, ForeignKey("qfd_analysis.id", ondelete="CASCADE"), nullable=False
    )  # Todo: is this superfluous?
    functional_requirement_1_id = Column(
        Integer,
        ForeignKey("qfd_functional_requirement.id", ondelete="CASCADE"),
        primary_key=True,
    )
    functional_requirement_2_id = Column(
        Integer,
        ForeignKey("qfd_functional_requirement.id", ondelete="CASCADE"),
        primary_key=True,
    )
    correlation = Column(Integer)


class Achievement(Base):
    """The SQL Alchemy ORM model for an achievement.

    An achievement is the value acheived against a given target, defined
    in the functional requirement. It is always associated with a given
    solution.

    Attributes:
        id: The id of the achievement, created by the database.
        functional_requirement_id: The id of the functional requirement
            which sets the target.
        solution_id: The id of the solution which achieves this.
        value: The value achieved against the target.
        fuctional_requirement: The functional requirement the
            acheivement relates to.
    """

    __tablename__ = "qfd_achievement"
    analysis = relationship("Qfd", backref="achievement")

    qfd_id = Column(
        Integer,
        ForeignKey("qfd_analysis.id", ondelete="CASCADE"),
        nullable=False,
    )  # Todo: is this superfluous?

    functional_requirement_id = Column(
        Integer,
        ForeignKey("qfd_functional_requirement.id", ondelete="CASCADE"),
        primary_key=True,
    )
    solution_id = Column(
        Integer, ForeignKey("qfd_solution.id", ondelete="CASCADE"), primary_key=True
    )
    value = Column(Float)


class Solution(Base):
    """The SQL Alchemy ORM model for a QFD solution.

    The solution is the existing or proposed solution to the objective.
    A solution is essentially a collection of achievements, which are
    the value achieved against a given target (Functional Requirement).

    Attributes:
        id: An integer id created by the database,
        qfd_id: The related qfd analysis,
        description: The description or name of the solution.
        achievements: The collection of this solution's achievements.
    """

    __tablename__ = "qfd_solution"
    analysis = relationship("Qfd", backref="solution")

    id = Column(Integer, primary_key=True, index=True)
    qfd_id = Column(
        Integer, ForeignKey("qfd_analysis.id", ondelete="CASCADE"), nullable=False
    )
    description = Column(String(DESCRIPTION_LENGTH))


class Note(Base):
    """SQLAlchemy ORM model for QFD Notes.

    Attributes:
        id: The id assigned by the database.
        qfd_id: The QFD Analysis to which this requirement relates.
        content: The textual content of the note.
        tag: Tag indicating which section of the QFD analysis the
        note relates to.
    """

    __tablename__ = "qfd_note"
    analysis = relationship("Qfd", backref="note")

    id = Column(Integer, primary_key=True, index=True)
    qfd_id = Column(
        Integer, ForeignKey("qfd_analysis.id", ondelete="CASCADE"), nullable=False
    )
    content = Column(String)
    tag = Column(String(DESCRIPTION_LENGTH))


for child in [
    Impact,
    Requirement,
    FunctionalRequirement,
    Correlation,
    Achievement,
    Solution,
    Note,
]:
    event.listen(child, "after_update", update_analysis_timestamp)


# noqa # class Source(Base):
# noqa #     __tablename__ = "qfd_sources"
# noqa #     id = Column(Integer, primary_key=True, index=True)
# noqa #     qfd_id = Column(
# noqa #         Integer, ForeignKey("qfd_analysis.id", ondelete="CASCADE"), nullable=False
# noqa #     )
# noqa #     entity = Column(String)  # e.g. requirement, functional_requirement
# noqa #     field = Column(String)  # field name
# noqa #     source = Column(String)  # e.g. solution_hierarchy
# noqa #     identifier = Column(String)  # something unique, depends on source... uuid for solution_hierarchy
