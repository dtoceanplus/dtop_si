# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Functions to calculate the QFD report.

This module contains functions which calculate the report
for a functional requirement, and also functions to generate
the Excel export of the report.
"""
from typing import Union, Dict
from io import BytesIO
from datetime import datetime
import xlsxwriter
from itertools import product
from dtop_structinn.pyqfd.enumerations import CorrelationEnum, ImprovementEnum
from dtop_structinn.pyqfd.schemas import Report
from dtop_structinn.app.errors import InvalidExcelExport
from dtop_structinn.triz import get_principles_from_contradiction_matrix
from dtop_structinn.triz.declarations import triz_class_groups, inventive_principles
from dtop_structinn.pyqfd.models import FunctionalRequirement
from dtop_structinn.pyqfd import validation, enumerations


def achieves(
    achieved_value: float, functional_requirement: FunctionalRequirement
) -> bool:
    """Decide whether a value would satisfy a functional requirement.

    Args:
        achieved_value (float): the achieved value.
        functional_requirement (dict): the functional_requirement.

    Returns:
        bool: True if the value would satisfy the target, else False.
    """
    if functional_requirement.direction == ImprovementEnum.higher_is_better:
        out = achieved_value >= functional_requirement.target
    else:
        out = achieved_value <= functional_requirement.target

    return out


def importances(qfd: dict) -> dict:
    """Return the importances of the functional requirements.

    Args:
        qfd (dict): A QFD analysis.

    Returns:
        dict: A dictionary of importances, with the key being the
            functional requirement ID.
    """
    the_importances: Dict[int, int] = {}

    for r in qfd.requirements:
        impacts = {
            i.functional_requirement_id: i.impact
            for i in qfd.impacts
            if i.requirement_id == r.id
        }

        for fr in qfd.functional_requirements:
            the_importances[fr.id] = the_importances.get(
                fr.id, 0
            ) + r.importance * impacts.get(fr.id, 0)

    return the_importances


#    result = [
#        {"functional_requirement_id": id_, "importance": importance}
#         for id_, importance in importances.items()
#    ]
#    return result


def organisational_impacts(qfd: dict) -> dict:
    """Return the organisational impact of the functional requirements.

    Args:
        qfd (dict): A QFD analysis

    Returns:
        dict: A dictionary of impacts, with the key being the functional
            requirement ID
    """
    impacts = {}
    for f in qfd.functional_requirements:
        impact = (f.engineering_difficulty or 0) * (f.delivery_difficulty or 0)
        impacts[f.id] = impact

    return impacts


def relative_deviation_from_target(
    target: float, achieved_value: float, lower_is_better: bool = False
) -> Union[float, None]:
    """Return the relative deviation from the target.

    This uses the formula for relative change and makes the assumption
    that target is a positive number. If both target and achieved value
    are 0, the output is also 0 (no deviation, target met). If the
    target is 0 but the achieved value is not, the result is None as it
    cannot be calculated.

    Args:
        target (float): the target value
        achieved_value (float): the achieved value
        lower_is_better (bool): whether being lower than the target is
            desirable

    Returns:
        float: the relative deviation (between 0 and 1), or None
    """
    deviation: Union[float, None]
    if (target == 0) and (achieved_value == 0):
        deviation = 0
    elif target == 0:
        deviation = None
    elif lower_is_better:
        deviation = (target - achieved_value) / target
    else:
        deviation = (achieved_value - target) / target

    return deviation


def actual_deviation_from_target(
    target: float, achieved_value: float, lower_is_better: bool = False
) -> Union[float, None]:
    """Return the actual deviation from the target.

    This simply returns the difference between the achieved value and
    the target, with the sign adjusted to signify whether this
    difference is a good thing or a bad thing. I.e. a positive output
    always signifies overachievement.

    Args:
        target (float): the target value
        achieved_value (float): the achieved value
        lower_is_better (bool): whether being lower than the target is
            desirable

    Returns:
        float: the deviation, in the target units
    """
    if lower_is_better:
        deviation = target - achieved_value
    else:
        deviation = achieved_value - target

    return deviation


def deviation_from_targets(qfd: dict) -> list:
    """Report both the relative and actual deviation from targets.

    The output is a list of dictionaries, one per solution ID, and
    further lists of dictionaries for relative and actual deviation.
    These are one per functional requirement, with the functional
    requirement and the deviation.

    Args:
        qfd (dict): A QFD analysis

    Returns:
        list: a list of dictionaries containg the results
    """
    functional_requirements = {fr.id: fr for fr in qfd.functional_requirements}

    return [
        {
            "solution_id": s.id,
            "relative_deviation": [
                {
                    "functional_requirement_id": a.functional_requirement_id,
                    "deviation": relative_deviation_from_target(
                        functional_requirements[a.functional_requirement_id].target,
                        a.value,
                        functional_requirements[a.functional_requirement_id].direction
                        == ImprovementEnum.lower_is_better,
                    ),
                }
                for a in qfd.achievements
                if a.solution_id == s.id
            ],
            "actual_deviation": [
                {
                    "functional_requirement_id": a.functional_requirement_id,
                    "deviation": actual_deviation_from_target(
                        functional_requirements[a.functional_requirement_id].target,
                        a.value,
                        functional_requirements[a.functional_requirement_id].direction
                        == ImprovementEnum.lower_is_better,
                    ),
                    "units": functional_requirements[a.functional_requirement_id].units,
                }
                for a in qfd.achievements
                if a.solution_id == s.id
            ],
        }
        for s in qfd.solutions
    ]


def summarise_functional_requirements(qfd: dict) -> list:
    """Report information about functional requirements.

    This includes their importance and organisational impact, and
    information about how many solutions are achieving them.

    Args:
        qfd (dict): a QFD analysis

    Returns:
        list: a list of dictionaries, one per functional requirement
    """
    results = []
    fr_importances = importances(qfd)
    fr_organisational_impacts = organisational_impacts(qfd)
    for fr in qfd.functional_requirements:

        achievements = [
            achievement.value
            for solution in qfd.solutions
            for achievement in qfd.achievements
            if achievement.functional_requirement_id == fr.id
            and achievement.solution_id == solution.id
        ]

        achieved_by = sum([achieves(x, fr) for x in achievements])

        if len(achievements) > 0:
            best_achieved = (
                max(achievements)
                if fr.direction == ImprovementEnum.higher_is_better
                else min(achievements)
            )
            worst_achieved = (
                min(achievements)
                if fr.direction == ImprovementEnum.higher_is_better
                else max(achievements)
            )
        else:
            best_achieved = worst_achieved = None

        results.append(
            {
                "functional_requirement_id": fr.id,
                "importance": fr_importances[fr.id],
                "organisational_impact": fr_organisational_impacts[fr.id],
                "target": fr.target,
                "units": fr.units,
                "best_achieved": best_achieved,
                "worst_achieved": worst_achieved,
                "achieved_by": achieved_by,
                "missed_by": len(achievements) - achieved_by,
            }
        )
    return results


def summarise_solution_idealities(qfd: dict) -> list:
    """Report information about solutions.

    This includes how many functional requirements they are achieving,
    and the ideality of the solution (the proportion of targets that
    have been achieved, weighted by the importance of that functional
    requirement.

    Args:
        qfd (dict): A QFD analysis

    Returns:
        list: a list of dictionaries, one per solution
    """
    solutions = qfd.solutions
    functional_requirements = {fr.id: fr for fr in qfd.functional_requirements}
    results = []
    for s in solutions:
        achievements = [a for a in qfd.achievements if a.solution_id == s.id]
        achieved = [
            achieves(a.value, functional_requirements[a.functional_requirement_id])
            for a in achievements
        ]
        fr_importances = importances(qfd)
        importance_total = sum(fr_importances.values())
        if (
            importance_total == 0
        ):  # Nothing has any impact - bad functional requirements
            ideality = None
        else:
            ideality = (
                sum(
                    [
                        fr_importances[achievement.functional_requirement_id]
                        for achievement, is_achieved in zip(achievements, achieved)
                        if is_achieved
                    ]
                )
                / importance_total
            )

        num_achieved = sum(achieved)
        results.append(
            {
                "solution_id": s.id,
                "targets_achieved": num_achieved,
                "targets_missed": len(achieved) - num_achieved,
                "ideality": ideality,
            }
        )

    return results


def get_inventive_principles(qfd: dict) -> list:
    """Get suggested inventive principles for QFD analysis.

    Determine inventive principles to suggest from analysis functional
    requirements and worst conflict.

    Missing correlations assume a default value of zero (Correlation.No)
    and therefore do not result in a worst conflict.

    For each functional requirement, the value of the worst conflict is
    found, and all correlations with that value are returned. For each
    conflict returned, a list of inventive principles is supplied.

    Args:
        qfd (dict): A QFD analysis

    Returns:
        list: a list of dictionaries, one per functional_requirement
    """
    functional_requirements = {fr.id: fr for fr in qfd.functional_requirements}

    # Worst conflicts
    worst_conflict: Dict[int, int] = {}
    conflicting_functional_requirements = {}
    # To intialise new worst conflict dictionary correctly, starting
    # value must be between zero and the highest negative value,
    # therefore set as the midpoint
    initial_worst = max(c.value for c in CorrelationEnum if c.value < 0) * 0.5
    for correlation in qfd.correlations:
        functional_requirement_1 = functional_requirements[
            correlation.functional_requirement_1_id
        ]
        functional_requirement_2 = functional_requirements[
            correlation.functional_requirement_2_id
        ]
        # Calculate conflict for this correlation
        conflict = (
            correlation.correlation
            * functional_requirement_1.direction
            * functional_requirement_2.direction
        )
        for id1, id2, triz1, triz2 in (
            (
                functional_requirement_1.id,
                functional_requirement_2.id,
                functional_requirement_1.triz,
                functional_requirement_2.triz,
            ),
            (
                functional_requirement_2.id,
                functional_requirement_1.id,
                functional_requirement_2.triz,
                functional_requirement_1.triz,
            ),
        ):
            # Get worst conflict so far for thie correlation's
            # functional requirement
            worst = worst_conflict.get(id1, initial_worst)
            # If this correlation's conflict is the worst so far for its
            # functional requirement, create new dictionary of worst
            # conflicts, otherwise add to existing
            if conflict < worst:
                conflicting_functional_requirements[id1] = {
                    id2: (
                        triz1,
                        triz2,
                    )
                }
                # Update value of worst conflict for this correlation's
                # functional requirement
                worst_conflict[id1] = conflict
            elif conflict == worst:
                conflicting_functional_requirements[id1][id2] = (
                    triz1,
                    triz2,
                )

    # Restructure conflicting_functional_requirements
    return [
        {
            "functional_requirement_id": fr1,
            "worst_conflicts": [
                {
                    "functional_requirement_id": fr2,
                    "contradictions": [
                        {
                            "improving": i,
                            "worsening": w,
                            "principle": p,
                        }
                        for (i, w) in product(*tr)
                        for p in get_principles_from_contradiction_matrix(i, w)
                    ],
                }
                for fr2, tr in wc.items()
            ],
        }
        for fr1, wc in conflicting_functional_requirements.items()
    ]


def report(qfd: dict) -> Report:
    """Return the report for QFD.

    This contains the deviations, and the summaries of functional
    requirements and solutions.

    Args:
        qfd (dict): a QFD analysis

    Returns:
        dict: the report contents
    """
    return Report(
        deviation_from_targets=deviation_from_targets(qfd),
        functional_requirement_summaries=summarise_functional_requirements(qfd),
        solution_idealities=summarise_solution_idealities(qfd),
        functional_requirements_conflicts=get_inventive_principles(qfd),
    )


def export_to_excel(qfd: dict) -> BytesIO:
    """Export the QFD report as an Excel (*.xlsx) file.

    Args:
        qfd (dict): A QFD analysis

    Returns:
        BytesIO: a stream of binary I/O representing the Excel file.
    """
    # Check there are no errors on the analysis
    if any([issue["type"] == "error" for issue in validation.get_analysis_issues(qfd)]):
        raise InvalidExcelExport

    solutions = {sol.id: sol for sol in qfd.solutions}
    functional_reqs = {fr.id: fr for fr in qfd.functional_requirements}

    fp = BytesIO()
    with xlsxwriter.Workbook(fp, {"in_memory": True}) as wb:

        index_sheet(wb, qfd)
        requirements_sheet(wb, qfd)
        deviation_sheet(wb, qfd, solutions, functional_reqs)
        disruption_sheet(wb, qfd, functional_reqs)
        ideality_sheet(wb, qfd, solutions)
        triz_conflicts_sheet(wb, qfd, functional_reqs)

    fp.seek(0)
    return fp


def index_sheet(wb: xlsxwriter.Workbook, qfd: dict) -> None:
    """Add an overview sheet."""
    ws = wb.add_worksheet("index")
    ws.write(0, 0, "Summary Report", wb.add_format({"bold": True, "font_size": 16}))

    col_map = {" ": {"attr": None}, "  ": {"attr": None}}

    table = []
    table.append(["Analysis Name", qfd.name])
    table.append(["Date Created", str(qfd.created.strftime("%d/%m/%Y %X"))])
    table.append(["Last Updated", str(qfd.updated.strftime("%d/%m/%Y %X"))])
    table.append(["Date Exported", str(datetime.now().strftime("%d/%m/%Y %X"))])
    write_table(ws, table, col_map, start_row=2)


def requirements_sheet(wb: xlsxwriter.Workbook, qfd: dict) -> None:
    """Add the 'Requirements' summary sheet."""
    ws = wb.add_worksheet("requirements")

    ws.write(
        0, 0, "Customer requirements", wb.add_format({"bold": True, "font_size": 16})
    )

    col_map = {
        "Description": {"attr": "description"},
        "Importance": {"attr": "importance"},
    }

    table = []
    requirements_sorted = sorted(
        qfd.requirements, key=lambda req: req.importance, reverse=True
    )
    for requirement in requirements_sorted:
        row = []
        for col in col_map.values():
            row.append(getattr(requirement, col["attr"]))
        table.append(row)

    cur_row = write_table(ws, table, col_map, start_row=2)

    ws.write(
        cur_row + 2,
        0,
        "Functional requirements to meet the customer requirements",
        wb.add_format({"bold": True, "font_size": 16}),
    )

    col_map = {
        "Description": {"attr": "description"},
        "DoI": {"attr": "direction", "enum": enumerations.ImprovementEnum},
        "Target Value": {"attr": "target"},
        "Units": {"attr": "units"},
        "Engineering Difficulty": {
            "attr": "delivery_difficulty",
            "enum": enumerations.DifficultyEnum,
        },
        "Delivery Difficulty": {
            "attr": "engineering_difficulty",
            "enum": enumerations.DifficultyEnum,
        },
    }

    table = []
    for fr in qfd.functional_requirements:
        row = []
        for k, col in col_map.items():
            value = getattr(fr, col["attr"])
            try:
                value = (
                    col["enum"](value)
                    .name.replace("_", " " if k == "DoI" else "/")
                    .capitalize()
                )
            except KeyError:
                pass
            row.append(value)
        table.append(row)

    start_row = cur_row + 4
    write_table(ws, table, col_map, start_row)


def deviation_sheet(
    wb: xlsxwriter.Workbook, qfd: dict, solutions: dict, functional_reqs: dict
) -> None:
    """Add the 'Deviation from Targets' sheet."""
    ws = wb.add_worksheet("deviation")

    all_deviations = deviation_from_targets(qfd)

    # Sheet heading
    ws.write(
        0, 0, "Deviation from Targets", wb.add_format({"bold": True, "font_size": 16})
    )

    # The list of columns should be the names of the
    # functional requirements
    column_list = []
    units_list = []
    for fr in functional_reqs.values():
        column_list.append(fr.description)
        units_list.append(fr.units)

    ws.write(
        2,
        0,
        "Relative Deviation from Targets",
        wb.add_format({"bold": True, "font_size": 14}),
    )

    relative = {
        solutions[d["solution_id"]].description: d["relative_deviation"]
        for d in all_deviations
    }
    table = []
    for solution, deviations in relative.items():
        row = []
        row.append(solution)
        for fr in functional_reqs:
            try:
                value = next(
                    deviation
                    for deviation in deviations
                    if deviation["functional_requirement_id"] == fr
                )["deviation"]
            except StopIteration:
                value = "-"
            row.append(value)
        table.append(row)

    col_map = {"Solution": {"attr": None}}
    for column_name in column_list:
        col_map[column_name] = {
            "attr": None,
            "format": wb.add_format({"num_format": "+0.00%;-0.00%;0%"}),
        }

    next_row = write_table(ws, table, col_map, start_row=4)

    ws.write(
        next_row + 2,
        0,
        "Actual Deviation from Targets",
        wb.add_format({"bold": True, "font_size": 14}),
    )

    actual = {
        solutions[d["solution_id"]].description: d["actual_deviation"]
        for d in all_deviations
    }
    table = []
    for solution, deviations in actual.items():
        row = []
        row.append(solution)
        for fr in functional_reqs:
            try:
                value = next(
                    deviation
                    for deviation in deviations
                    if deviation["functional_requirement_id"] == fr
                )["deviation"]
            except StopIteration:
                value = "-"
            row.append(value)
        table.append(row)

    col_map = {"Solution": {"attr": None}}
    for column_name, u in zip(column_list, units_list):
        col_map[column_name] = {
            "attr": None,
            "format": wb.add_format({"num_format": f'+0.00 "{u}";-0.00 "{u}";0 "{u}"'}),
        }

    write_table(ws, table, col_map, start_row=next_row + 4)


def disruption_sheet(wb: xlsxwriter.Workbook, qfd: dict, functional_reqs: dict) -> None:
    """Add the 'Potential for Disruption' sheet."""
    ws = wb.add_worksheet("disruption")

    summaries = summarise_functional_requirements(qfd)

    col_map = {
        "Functional Requirement": {"attr": None},
        "Importance": {"attr": "importance"},
        "Organisational Impact": {"attr": "organisational_impact"},
        "Target": {"attr": "target"},
        "Best Achieved": {"attr": "best_achieved"},
        "Worst Achieved": {"attr": "worst_achieved"},
        "Solutions Achieving": {"attr": "achieved_by"},
        "Solutions Missing": {"attr": "missed_by"},
    }

    # Sheet heading
    ws.write(
        0, 0, "Potential for disruption", wb.add_format({"bold": True, "font_size": 16})
    )

    table = []
    for fr in summaries:
        row = []
        for col in col_map.values():
            if (col_attr := col["attr"]) is None:
                value = f"{functional_reqs[fr['functional_requirement_id']].description} ({fr['units']})"
            else:
                value = fr.get(col_attr)
            row.append(value)
        table.append(row)

    write_table(ws, table, col_map, start_row=2)


def ideality_sheet(wb: xlsxwriter.Workbook, qfd: dict, solutions: dict) -> None:
    """Add the 'Ideality of Solutions' sheet."""
    ws = wb.add_worksheet("ideality")

    summaries = summarise_solution_idealities(qfd)

    col_map = {
        "Solution": {"attr": None},
        "Targets Achieved": {"attr": "targets_achieved"},
        "Targets Missed": {"attr": "targets_missed"},
        "Ideality": {
            "attr": "ideality",
            "format": wb.add_format({"num_format": "0%"}),
        },
    }

    ws.write(
        0, 0, "Ideality of solutions", wb.add_format({"bold": True, "font_size": 16})
    )

    table = []
    for sol in summaries:
        row = []
        for col in col_map.values():
            if (col_attr := col["attr"]) is None:
                value = solutions[sol["solution_id"]].description
            else:
                value = sol.get(col_attr)
            row.append(value)
        table.append(row)

    write_table(ws, table, col_map, start_row=2)


def triz_conflicts_sheet(
    wb: xlsxwriter.Workbook, qfd: dict, functional_reqs: dict
) -> None:
    """Add the 'TRIZ conflicts' sheet."""
    # Flatten the TRIZ classes dictionary, to remove the segmentation
    # by groups (not sure what the grouping is even for?)
    triz_classes = {
        id: triz for group in triz_class_groups.values() for id, triz in group.items()
    }
    principles = {k: p["description"] for k, p in inventive_principles.items()}

    col_map = {
        "Conflicting Functional Requirement": {"attr": None},
        "Worsening TRIZ Class": {"attr": "worsening", "name_map": triz_classes},
        "Improving TRIZ Class": {"attr": "improving", "name_map": triz_classes},
        "Suggested Inventive Principle": {"attr": "principle", "name_map": principles},
    }

    ips = get_inventive_principles(qfd)

    def _write_triz_conflict_table(functional_requirement_id: int, start_row: int):
        """Write a TRIZ conflicts table for Functional Requirement."""
        cur_row = start_row
        try:
            fr_conflicts = next(
                conflict
                for conflict in ips
                if conflict["functional_requirement_id"] == functional_requirement_id
            )
            table = []
            for conflict in fr_conflicts["worst_conflicts"]:
                contradictions = conflict["contradictions"]
                fr_description = functional_reqs[
                    conflict["functional_requirement_id"]
                ].description
                for contradiction in contradictions:
                    row = [fr_description]
                    for col in col_map.values():
                        if (col_attr := col["attr"]) is None:
                            continue
                        row.append(col["name_map"].get(contradiction[col_attr]))
                    table.append(row)
                if not contradictions:
                    table.append([fr_description] + ["-"] * (len(col_map) - 1))

            cur_row = write_table(ws, table, col_map, start_row=start_row)

        except StopIteration:
            ws.write(start_row, 0, "No Conflicts")

        return cur_row + 2

    ws = wb.add_worksheet("conflicts")

    ws.write(0, 0, "TRIZ Conflicts", wb.add_format({"bold": True, "font_size": 16}))

    cur_row = 2
    for fr_id, fr in functional_reqs.items():
        ws.write(cur_row, 0, fr.description, wb.add_format({"bold": True}))
        cur_row = _write_triz_conflict_table(fr_id, cur_row + 1)


def write_table(
    ws,
    table: list[list],
    col_map: dict[str, dict],
    start_row: int,
) -> int:
    """Write `table` of data to `ws` starting from `start_row`.

    `col_map.keys()` provides a list of column names, which are added as
    the first row of the table.

    Each column can also have formatting applied to it, as specified in
    each element of col_map, col["format"]

    returns `rowidx` so subsequent calls know where to write from.
    """
    # Add the heading row into the table
    table.insert(0, list(col_map.keys()))

    col_widths = [max([len(str(cell)) for cell in col]) + 2 for col in zip(*table)]
    col_fmts = [col.get("format", None) for col in col_map.values()]

    rowidx = start_row
    for row in table:
        for colidx, val in enumerate(row):
            ws.write(rowidx, colidx, val, col_fmts[colidx])
            ws.set_column(colidx, colidx, col_widths[colidx])
        rowidx += 1

    return rowidx
