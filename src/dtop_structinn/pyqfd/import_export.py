# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Handles importing exported QFD analyses.

Despite the name of the file, export is not handled here.
"""
from copy import deepcopy
from datetime import datetime
from dtop_structinn.database import session
from dtop_structinn.pyqfd import models


def convert_old_style_export(data: dict) -> dict:
    """Import the exported json from an old SI version.

    An importance may no longer be 0, and is no longer a simple
    series (magnitude matters as well as order)
    there was a version with this change and still using the
    old schema, so we check for a 0 in the importances to
    decide what to do. Although naive, we assume that each item
    is twice as important as the last when entered in the old
    style.
    Keep in mind also that in the original version, 0 was the
    most important.
    """
    import_time = datetime.now()

    analysis = deepcopy(data)
    requirements = analysis.pop("requirements")
    functional_requirements = analysis.pop("functional_requirements")
    solutions = analysis.pop("solutions")
    correlations = analysis.pop("correlations")

    if 0 in (r["importance"] for r in requirements):
        for r in requirements:
            r["importance"] = 2 ** (-1 * (r["importance"] - len(requirements) + 1))
    for r in requirements:
        r["created"] = import_time
        r["updated"] = import_time

    for fr in functional_requirements:
        # new style only stores the triz id
        fr["triz"] = {t["id"] for t in fr["triz"]}
        fr["created"] = import_time
        fr["updated"] = import_time

    impacts = []
    for r in requirements:
        ips = r.pop("impacts")
        for i in ips:
            impacts.append(
                {
                    "functional_requirement_id": i["functional_requirement"]["id"],
                    "requirement_id": r["id"],
                    "impact": i["impact"],
                    "created": import_time,
                    "updated": import_time,
                }
            )

    for c in correlations:
        fr1 = c.pop("functional_requirement")
        fr2 = c.pop("functional")
        c["functional_requirement_1_id"] = fr1["id"]
        c["functional_requirement_2_id"] = fr2["id"]
        del c["id"]
        c["created"] = import_time
        c["updated"] = import_time

    correlations = [
        c
        for c in correlations
        if c["functional_requirement_1_id"] < c["functional_requirement_2_id"]
    ]

    achievements = []
    for s in solutions:
        achs = s.pop("achievements")
        for a in achs:
            achievements.append(
                {
                    "solution_id": s["id"],
                    "functional_requirement_id": a["functional_requirement"]["id"],
                    "value": a["value"],
                    "created": import_time,
                    "updated": import_time,
                }
            )
        s["created"] = import_time
        s["updated"] = import_time

    analysis["requirements"] = requirements
    analysis["functional_requirements"] = functional_requirements
    analysis["solutions"] = solutions
    analysis["achievements"] = achievements
    analysis["correlations"] = correlations
    analysis["impacts"] = impacts
    analysis["notes"] = []
    analysis["created"] = import_time
    analysis["updated"] = import_time
    analysis["parent_analysis_id"] = None

    return analysis


def import_analysis(data: dict, si_id: int = None):
    """Import an analysis that was exported.

    Uses the presense of the version field as a heuristic to
    decide whether to convert from the old format or not.

    If the export format needs to change in future, version can
    be incremented and decisions about processing made accordingly.
    """
    if "version" in data:
        data = data["data"]
    else:
        data = convert_old_style_export(data)

    r_ids = {r["id"]: None for r in data["requirements"]}
    fr_ids = {fr["id"]: None for fr in data["functional_requirements"]}
    sol_ids = {sol["id"]: None for sol in data["solutions"]}

    data["si_id"] = si_id
    del data["id"]
    del data["parent_analysis_id"]
    if "issues" in data:
        del data["issues"]
    if "child_analyses" in data:
        del data["child_analyses"]
    requirements = data.pop("requirements")
    functional_requirements = data.pop("functional_requirements")
    solutions = data.pop("solutions")
    achievements = data.pop("achievements")
    correlations = data.pop("correlations")
    impacts = data.pop("impacts")
    notes = data.pop("notes")

    analysis = models.Qfd(**data)
    session.add(analysis)
    session.flush()
    qfd_id = analysis.id

    for r in requirements:
        id = r.pop("id")
        requirement = models.Requirement(qfd_id=qfd_id, **r)
        session.add(requirement)
        session.flush()
        r_ids[id] = requirement.id

    for fr in functional_requirements:
        id = fr.pop("id")
        functional_requirement = models.FunctionalRequirement(qfd_id=qfd_id, **fr)
        session.add(functional_requirement)
        session.flush()
        fr_ids[id] = functional_requirement.id

    for s in solutions:
        id = s.pop("id")
        solution = models.Solution(qfd_id=qfd_id, **s)
        session.add(solution)
        session.flush()
        sol_ids[id] = solution.id

    for c in correlations:
        fr1 = fr_ids[c["functional_requirement_1_id"]]
        fr2 = fr_ids[c["functional_requirement_2_id"]]
        if fr1 > fr2:
            fr1, fr2 = fr2, fr1
        c["functional_requirement_1_id"] = fr1
        c["functional_requirement_2_id"] = fr2
        correlation = models.Correlation(qfd_id=qfd_id, **c)
        session.add(correlation)

    for i in impacts:
        i["functional_requirement_id"] = fr_ids[i["functional_requirement_id"]]
        i["requirement_id"] = r_ids[i["requirement_id"]]
        impact = models.Impact(qfd_id=qfd_id, **i)
        session.add(impact)

    for a in achievements:
        a["solution_id"] = sol_ids[a["solution_id"]]
        a["functional_requirement_id"] = fr_ids[a["functional_requirement_id"]]
        achievement = models.Achievement(qfd_id=qfd_id, **a)
        session.add(achievement)

    for note in notes:
        del note["id"]
        note = models.Note(qfd_id=qfd_id, **note)
        session.add(note)

    session.commit()
    return analysis
