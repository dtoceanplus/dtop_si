# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Functions related to the solution hierarchy.

This module contains functions for dealing with the soluton hierarchy
and its interactions with the database.
"""

import uuid
from itertools import chain
from typing import Dict, List

from dtop_structinn.pyqfd import models
from dtop_structinn.solution_hierarchy import schemas

from ..app.db import db_session


def persist_hierarchy_child(parent, hierarchy_fr) -> list:
    """Store a solution hierarchy entry.

    This function is recursive and it returns a list of the
    sqlalchemy objects that are created.

    Args:
        parent: The parent hierarchy entry.
        hierarchy_fr: The entry to be stored in the database.

    Returns:
        list: The orm objects that were created.
    """
    fr = models.HierarchyFunctionalRequirement(
        uuid=hierarchy_fr["uuid"],
        description=hierarchy_fr["description"],
        units=hierarchy_fr["units"],
        direction=hierarchy_fr["direction"],
        parent_uuid=parent.uuid,
        hierarchy_uuid=parent.hierarchy_uuid,
    )

    orm_objects = [[fr]]
    orm_objects.extend(
        [persist_hierarchy_child(fr, child) for child in hierarchy_fr["children"]]
    )

    return list(chain.from_iterable(orm_objects))


def persist_solution_hierarchy(hierarchy):
    """Add a solution hierarchy and its entries to the database.

    Args:
        hierarchy (dict): The solution hierarchy.

    Returns:
        The created SQLAlchemy ORM object.
    """
    hierarchy["uuid"] = hierarchy["uuid"]

    solution_hierarchy = models.SolutionHierarchy(
        uuid=hierarchy["uuid"],
        description=hierarchy["description"],
    )

    all_orm_objects = []
    for child in hierarchy["children"]:

        fr = models.HierarchyFunctionalRequirement(
            uuid=child["uuid"],
            units=child["units"],
            direction=child["direction"],
            description=child["description"],
            hierarchy_uuid=solution_hierarchy.uuid,
        )

        orm_objects = [[fr]]
        orm_objects.extend(
            [persist_hierarchy_child(fr, child) for child in child["children"]]
        )

        all_orm_objects.extend(list(chain.from_iterable(orm_objects)))

    db_session.add(solution_hierarchy)
    db_session.add_all(all_orm_objects)
    db_session.commit()
    return solution_hierarchy


def get_solution_hierarchy(uuid: uuid.UUID) -> Dict:
    """Get a solution hierarchy from the database by UUID.

    Args:
        uuid (uuid.UUID): The UUID of the hierarchy to be retrieved

    Returns:
        The solution hierarchy.
    """
    result = db_session.query(models.SolutionHierarchy).get(uuid)
    return schemas.SolutionHierarchy.from_orm(result).dict()


def get_solution_hierarchies() -> List[Dict]:
    """Get all solution hierarchies from the database.

    Args:

    Returns:
        A list containing all of the solution hierarchies
        from the database.
    """
    all_solhi = db_session.query(models.SolutionHierarchy).all()
    return [schemas.SolutionHierarchy.from_orm(x).dict() for x in all_solhi]


def get_suggestions(qfd_id: int) -> List:
    """Suggest functional requirements for a QFD.

    The suggested items from the hierarchy are e direct children of
    the requirements that are solution hierarchy entries.

    If there are no suggestions, the returned list is empty.

    Args:
        qfd_id: The ID of the analysis in question.

    Returns:
        The suggested functional requirements.
    """
    suggestions = (
        db_session.query(models.HierarchyFunctionalRequirement)
        .join(
            models.Requirement,
            models.HierarchyFunctionalRequirement.parent_uuid
            == models.Requirement.solution_hierarchy_child_uuid,
        )
        .filter(models.Requirement.qfd_id == qfd_id)
        .all()
    )

    suggestions = [
        schemas.HierarchyFunctionalRequirement.from_orm(x).dict() for x in suggestions
    ]

    return suggestions
