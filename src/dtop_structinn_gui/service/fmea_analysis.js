// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { baseUrl } from '@/service/http.js'
import { sendUpdate } from '@/service/analysis.js'

const fmeaUrlComponent = '/fmea/'

export const getAllAnalyses = async (siId) => {
  const url = baseUrl + fmeaUrlComponent + (siId === undefined ? '' : '?' + new URLSearchParams({ si_id: siId }))
  const response = await fetch(url, { method: 'GET', headers: { 'Content-Type': 'application/json' } })
  const json = await response.json()
  return json
}

export class Analysis {
  constructor (
    data = {}
  ) {
    this.id = data.id || null
    this.parent_analysis_id = data.parent_analysis_id || null
    this.child_analyses = data.child_analyses || []
    this.created = data.created || null
    this.updated = data.updated || null
    this.name = data.name || null
    this.objective = data.objective || null
    this.causes = data.causes || []
    this.action_level = data.action_level || null
    this.occurrence_limit = data.occurrence_limit || null
    this.controls = data.controls || []
    this.effects = data.effects || []
    this.failure_modes = data.failure_modes || []
    this.mitigations = data.mitigations || []
    this.requirements = data.requirements || []
    this.issues = data.issues || []
    this.notes = data.notes || []

    this._type = 'fmea'
    this._queue = Promise.resolve()
    this._queueLength = 0
    this._tempIds = { requirement: {}, failure_mode: {}, effect: {}, cause: {}, control: {}, mitigation: {} }
    this._tempIdCounter = 0
  }

  static async getById (id) {
    let analysis = null
    const response = await fetch(baseUrl + fmeaUrlComponent + id, { method: 'GET', headers: { 'Content-Type': 'application/json' } })
    if (response.status === 200) {
      const data = await response.json()
      analysis = new Analysis(data)
    }
    return analysis
  }

  static async create (siId) {
    console.log(siId)
    const response = await fetch(baseUrl + fmeaUrlComponent, { method: 'POST', body: JSON.stringify({ si_id: siId === undefined ? null : siId }), headers: { 'Content-Type': 'application/json' } })
    if (response.status === 201) {
      const data = await response.json()
      console.log(data)
      return new Analysis(data)
    } else {
      throw Error('Some unknown error... not retrying')
    }
  }

  static async upload (data, siId) {
    const url = baseUrl + fmeaUrlComponent + 'import' + (siId === undefined ? '' : '?' + new URLSearchParams({ si_id: siId }))
    const response = await fetch(url, { method: 'POST', body: data, headers: { 'Content-Type': 'application/json' } })
    if (response.status !== 201) {
      throw Error('Some unknown error... not retrying')
    }
  }

  static async downloadById (id) {
    const response = await fetch(baseUrl + fmeaUrlComponent + id + '/export', { method: 'GET' })
    if (response.status === 200) {
      const data = await response.blob()
      return data
    } else {
      throw Error('Some unknown error... not retrying')
    }
  }

  static async exportReportToExcel (id) {
    const response = await fetch(baseUrl + fmeaUrlComponent + id + '/report/excel', { method: 'GET' })
    if (response.status === 200) {
      const data = await response.blob()
      return data
    } else {
      throw Error('Some unknown error... not retrying')
    }
  }

  static async deleteById (id) {
    const response = await fetch(baseUrl + fmeaUrlComponent + id, { method: 'DELETE' })
    if (response.status === 204) {
      return true
    } else {
      throw Error('Some unknown error... not retrying')
    }
  }

  enqueue (action) {
    this._queueLength++
    this._queue = this._queue.then(
      async () => {
        await action()
        // as a test, sleep 5s after every queue item
        // await sleep(5000)
      }
    ).finally(() => {
      this._queueLength--
      this.updated = Date.now()
    })
  }

  resolveId (key, id) {
    return id < 0 ? this._tempIds[key][id] : id
  }

  reverseResolveId (key, id) {
    const tempId = Object.entries(this._tempIds[key] || {}).find((item) => item[1] === id)
    return tempId !== undefined ? tempId[0] : id
  }

  async getRpns () {
    if (!this.id) {
      console.log('Analysis has no id')
    } else {
      let data = null
      const response = await fetch(baseUrl + fmeaUrlComponent + this.id + '/rpns', { method: 'GET', headers: { 'Content-Type': 'application/json' } })
      if (response.status === 200) {
        data = await response.json()
      }
      return data.map(d => ({ ...d, effect_id: this.reverseResolveId('effect', d.effect_id), cause_id: this.reverseResolveId('cause', d.cause_id), control_id: this.reverseResolveId('control', d.control_id) }))
    }
  }

  async getReport () {
    if (!this.id) {
      console.log('Analysis has no id')
    } else {
      let data = null
      const response = await fetch(baseUrl + fmeaUrlComponent + this.id + '/report', { method: 'GET', headers: { 'Content-Type': 'application/json' } })
      if (response.status === 200) {
        data = await response.json()
      }
      return data
    }
  }

  async updateRequirementById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/requirements/' + this.resolveId('requirement', id),
        'PATCH',
        200,
        data
      )
    )
    const idx = this.requirements.findIndex((r) => r.id === id)
    const r = this.requirements[idx]
    this.requirements.splice(idx, 1, { ...r, ...data })
    return this.requirements[idx]
  }

  async deleteRequirementById (id) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/requirements/' + this.resolveId('requirement', id),
        'DELETE',
        204
      )
    )
    const idx = this.requirements.findIndex((r) => r.id === id)
    if (idx !== -1) { // TODO: what do we do if the requirement is already missing for some reason?
      this.requirements.splice(idx, 1)
    }
  }

  async addRequirement (data) {
    const tempId = --this._tempIdCounter
    this.enqueue(() => {
      return sendUpdate(baseUrl + fmeaUrlComponent + this.id + '/requirements/', 'POST', 201, data)
        .then((response) => {
          this._tempIds.requirement[tempId] = response.id
        }
        )
    })
    const requirement = { id: tempId, ...data }
    this.requirements.push(requirement)
    return requirement
  }

  async updateFailureModeById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/failure_modes/' + this.resolveId('failure_mode', id),
        'PATCH',
        200,
        { ...data, requirement_id: this.resolveId('requirement', data.requirement_id) }
      )
    )
    const idx = this.failure_modes.findIndex((fm) => fm.id === id)
    const fm = this.failure_modes[idx]
    this.failure_modes.splice(idx, 1, { ...fm, ...data })
    return this.failure_modes[idx]
  }

  async deleteFailureModeById (id) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/failure_modes/' + this.resolveId('failure_mode', id),
        'DELETE',
        204
      )
    )
    const idx = this.failure_modes.findIndex((r) => r.id === id)
    if (idx !== -1) {
      this.failure_modes.splice(idx, 1)
    }
  }

  async addFailureMode (data) {
    const tempId = --this._tempIdCounter
    this.enqueue(() => {
      return sendUpdate(baseUrl + fmeaUrlComponent + this.id + '/failure_modes/',
        'POST',
        201,
        { ...data, requirement_id: this.resolveId('requirement', data.requirement_id) }
      ).then((response) => {
        this._tempIds.failure_mode[tempId] = response.id
      }
      )
    })
    const failureMode = { id: tempId, ...data }
    this.failure_modes.push(failureMode)
    return failureMode
  }

  async updateEffectById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/effects/' + this.resolveId('effect', id),
        'PATCH',
        200,
        data
      )
    )
    const idx = this.effects.findIndex((eff) => eff.id === id)
    const eff = this.effects[idx]
    this.effects.splice(idx, 1, { ...eff, ...data })
    return this.effects[idx]
  }

  async deleteEffectById (id) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/effects/' + this.resolveId('effect', id),
        'DELETE',
        204
      )
    )
    const idx = this.effects.findIndex((r) => r.id === id)
    if (idx !== -1) {
      this.effects.splice(idx, 1)
    }
  }

  async addEffect (data) {
    const tempId = --this._tempIdCounter
    this.enqueue(() => {
      return sendUpdate(baseUrl + fmeaUrlComponent + this.id + '/effects/',
        'POST',
        201,
        { ...data, failure_mode_id: this.resolveId('failure_mode', data.failure_mode_id) }
      ).then((response) => {
        this._tempIds.effect[tempId] = response.id
      }
      )
    })
    const effect = { id: tempId, ...data }
    this.effects.push(effect)
    return effect
  }

  async updateCauseById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/causes/' + this.resolveId('cause', id),
        'PATCH',
        200,
        data
      )
    )
    const idx = this.causes.findIndex((cause) => cause.id === id)
    const cause = this.causes[idx]
    this.causes.splice(idx, 1, { ...cause, ...data })
    return this.causes[idx]
  }

  async deleteCauseById (id) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/causes/' + this.resolveId('cause', id),
        'DELETE',
        204
      )
    )
    const idx = this.causes.findIndex((cause) => cause.id === id)
    if (idx !== -1) {
      this.causes.splice(idx, 1)
    }
  }

  async addCause (data) {
    const tempId = --this._tempIdCounter
    this.enqueue(() => {
      return sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/causes/',
        'POST',
        201,
        { ...data, failure_mode_id: this.resolveId('failure_mode', data.failure_mode_id) }
      ).then((response) => {
        this._tempIds.cause[tempId] = response.id
      }
      )
    })
    const cause = { id: tempId, ...data }
    this.causes.push(cause)
    return cause
  }

  async updateDesignControlById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/controls/' + this.resolveId('control', id),
        'PATCH',
        200,
        data
      )
    )
    const idx = this.controls.findIndex((dc) => dc.id === id)
    const dc = this.controls[idx]
    this.controls.splice(idx, 1, { ...dc, ...data })
    return this.controls[idx]
  }

  async deleteDesignControlById (id) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/controls/' + this.resolveId('control', id),
        'DELETE',
        204
      )
    )
    const idx = this.controls.findIndex((dc) => dc.id === id)
    if (idx !== -1) {
      this.controls.splice(idx, 1)
    }
  }

  async addDesignControl (data) {
    const tempId = --this._tempIdCounter

    this.enqueue(() => {
      return sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/controls/',
        'POST',
        201,
        { ...data, cause_id: this.resolveId('cause', data.cause_id), failure_mode_id: this.resolveId('failure_mode', data.failure_mode_id) }
      ).then((response) => {
        this._tempIds.control[tempId] = response.id
      }
      )
    })
    const dc = { id: tempId, ...data }
    this.controls.push(dc)
    return dc
  }

  async updateMitigationById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/mitigations/' + this.resolveId('mitigation', id),
        'PATCH',
        200,
        data
      )
    )
    const idx = this.mitigations.findIndex((mit) => mit.id === id)
    const mit = this.mitigations[idx]
    this.mitigations.splice(idx, 1, { ...mit, ...data })
    return this.mitigations[idx]
  }

  async addMitigation (data) {
    const tempId = --this._tempIdCounter
    this.enqueue(() => {
      return sendUpdate(baseUrl + fmeaUrlComponent + this.id + '/mitigations/',
        'POST',
        201,
        { ...data, effect_id: this.resolveId('effect', data.effect_id), cause_id: this.resolveId('cause', data.cause_id), control_id: this.resolveId('control', data.control_id) }
      ).then((response) => {
        this._tempIds.mitigation[tempId] = response.id
      }
      )
    })
    const mit = { id: tempId, ...data }
    this.mitigations.push(mit)
    return mit
  }

  async updateAnalysis (data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id,
        'PATCH',
        200,
        data
      )
    )
    const attr = Object.keys(data)[0]
    this[attr] = data[attr]
  }

  async getIssues () {
    const response = await fetch(baseUrl + fmeaUrlComponent + this.id + '/issues', { method: 'GET', headers: { 'Content-Type': 'application/json' } })
    if (response.status === 200) {
      const data = await response.json()
      this.issues = data
      console.log('updated issues')
    }
  }

  async addOrUpdateNotes (data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + fmeaUrlComponent + this.id + '/notes/',
        'POST',
        201,
        data
      )
    )

    const idx = this.notes.findIndex(note => note.tag === data.tag)
    if (idx === -1) { this.notes.push(data) } else { this.notes[idx].content = data.content }
    return data
  }
}
