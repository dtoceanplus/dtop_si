// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { baseUrl } from '@/service/http.js'

const sleep = (ms) => { return new Promise(resolve => setTimeout(resolve, ms)) } // only works in an async function using await

export const sendUpdate = async (url, method, expected, data) => {
  while (true) {
    var response
    try {
      response = await fetch(
        url,
        {
          method: method,
          body: data ? JSON.stringify(data) : undefined,
          headers: { 'Content-Type': 'application/json' },
        }
      )
      let json
      switch (response.status) {
        case expected:
          if (expected !== 204) {
            return response.json()
          } else {
            return null
          }
        case 422:
          json = await response.json()
          console.log(json)
          return json
        case 404:
          console.log('Does not exist, assuming deleted, should improve how this is handled.')
          break
        default:
          throw Error('Some unknown error... will catch and try again')
      }
    } catch (error) {
      console.log(error)
      await sleep(5000) // sleep 5s before trying again
      // Ignore the error
    }
  }
}

const qfdUrlComponent = '/qfd/'

export const getAllAnalyses = async (siId) => {
  const url = baseUrl + qfdUrlComponent + (siId === undefined ? '' : '?' + new URLSearchParams({ si_id: siId }))
  const response = await fetch(url, { method: 'GET', headers: { 'Content-Type': 'application/json' } })
  const json = await response.json()
  return json
}

export class Analysis {
  constructor (
    data = {}
  ) {
    this.id = data.id || null
    this.parent_analysis_id = data.parent_analysis_id || null
    this.child_analysis = data.child_analysis || null
    this.created = data.created || null
    this.name = data.name || null
    this.objective = data.objective || null
    this.solution_hierarchy_id = data.solution_hierarchy_id || null
    this.solution_hierarchy_level = data.solution_hierarchy_level || null
    this.updated = data.updated || null
    this.achievements = data.achievements || []
    this.correlations = data.correlations || []
    this.functional_requirements = data.functional_requirements || []
    this.impacts = data.impacts || []
    this.notes = data.notes || []
    this.requirements = data.requirements || []
    this.solutions = data.solutions || []
    this.issues = data.issues || []

    this._type = 'qfd'
    this._queue = Promise.resolve()
    this._queueLength = 0
    this._tempIds = { requirement: {}, functional_requirement: {}, solution: {} }
    this._tempIdCounter = 0
  }

  static async getById (id) {
    let analysis = null
    const response = await fetch(baseUrl + qfdUrlComponent + id, { method: 'GET', headers: { 'Content-Type': 'application/json' } })
    if (response.status === 200) {
      const data = await response.json()
      analysis = new Analysis(data)
    }
    return analysis
  }

  static async create (siId, parentId) {
    const response = await fetch(baseUrl + qfdUrlComponent, { method: 'POST', body: JSON.stringify({ si_id: siId === undefined ? null : siId, parent_analysis_id: parentId }), headers: { 'Content-Type': 'application/json' } })
    if (response.status === 201) {
      const data = await response.json()
      return new Analysis(data)
    } else {
      throw Error('Some unknown error... not retrying')
    }
  }

  static async upload (data, siId) {
    const url = baseUrl + qfdUrlComponent + 'import' + (siId === undefined ? '' : '?' + new URLSearchParams({ si_id: siId }))
    const response = await fetch(url, { method: 'POST', body: data, headers: { 'Content-Type': 'application/json' } })
    if (response.status !== 201) {
      throw Error('Some unknown error... not retrying')
    }
  }

  static async downloadById (id) {
    const response = await fetch(baseUrl + qfdUrlComponent + id + '/export', { method: 'GET' })
    if (response.status === 200) {
      const data = await response.blob()
      return data
    } else {
      throw Error('Some unknown error... not retrying')
    }
  }

  static async exportReportToExcel (id) {
    const response = await fetch(baseUrl + qfdUrlComponent + id + '/report/excel', { method: 'GET' })
    if (response.status === 200) {
      const data = await response.blob()
      return data
    } else {
      throw Error('Some unknown error... not retrying')
    }
  }

  static async deleteById (id) {
    const response = await fetch(baseUrl + qfdUrlComponent + id, { method: 'DELETE' })
    if (response.status === 204) {
      return true
    } else {
      throw Error('Some unknown error... not retrying')
    }
  }

  enqueue (action) {
    this._queueLength++
    this._queue = this._queue.then(
      async () => {
        await action()
        // as a test, sleep 5s after every queue item
        // await sleep(5000)
      }
    ).finally(() => {
      this._queueLength--
      this.updated = Date.now()
    }
    )
  }

  resolveId (key, id) {
    return id < 0 ? this._tempIds[key][id] : id
  }

  reverseResolveId (key, id) {
    const tempId = Object.entries(this._tempIds[key] || {}).find((item) => item[1] === id)
    return tempId !== undefined ? tempId[0] : id
  }

  async getReport () {
    if (!this.id) {
      console.log('Analysis has no id')
    } else {
      let data = null
      const response = await fetch(baseUrl + qfdUrlComponent + this.id + '/report', { method: 'GET', headers: { 'Content-Type': 'application/json' } })
      if (response.status === 200) {
        data = await response.json()
      }
      return data
    }
  }

  async addOrUpdateImpact (data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/impacts/',
        'POST',
        201,
        { ...data, requirement_id: this.resolveId('requirement', data.requirement_id), functional_requirement_id: this.resolveId('functional_requirement', data.functional_requirement_id) }
      )
    )
    const idx = this.impacts.findIndex((impact) => (impact.requirement_id === data.requirement_id && impact.functional_requirement_id === data.functional_requirement_id))
    if (idx === -1) { this.impacts.push(data) } else { this.impacts.splice(idx, 1, data) }
    return data
  }

  async addOrUpdateCorrelation (data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/correlations/',
        'POST',
        201, // TODO: This probably should be [200, 201] so that the backend can return appropriately whether it existed before or not.
        { ...data, functional_requirement_1_id: this.resolveId('functional_requirement', data.functional_requirement_1_id), functional_requirement_2_id: this.resolveId('functional_requirement', data.functional_requirement_2_id) }
      )
    )
    const idx = this.correlations.findIndex((corr) => (corr.functional_requirement_1_id === data.functional_requirement_1_id && corr.functional_requirement_2_id === data.functional_requirement_2_id))
    if (idx === -1) { this.correlations.push(data) } else { this.correlations.splice(idx, 1, data) }
    return data
  }

  async updateRequirementById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/requirements/' + this.resolveId('requirement', id),
        'PATCH',
        200,
        data
      )
    )
    const idx = this.requirements.findIndex((r) => r.id === id)
    const r = this.requirements[idx]
    this.requirements.splice(idx, 1, { ...r, ...data })
    return this.requirements[idx]
  }

  async deleteRequirementById (id) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/requirements/' + this.resolveId('requirement', id),
        'DELETE',
        204
      )
    )
    const idx = this.requirements.findIndex((r) => r.id === id)
    if (idx !== -1) { // TODO: what do we do if the requirement is already missing for some reason?
      this.requirements.splice(idx, 1)
    }
  }

  async addRequirement (data) {
    const tempId = --this._tempIdCounter
    this.enqueue(() => {
      return sendUpdate(baseUrl + qfdUrlComponent + this.id + '/requirements/', 'POST', 201, data)
        .then((response) => {
          this._tempIds.requirement[tempId] = response.id
        }
        )
    })
    const requirement = { id: tempId, ...data }
    this.requirements.push(requirement)
    return requirement
  }

  async updateFunctionalRequirementById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/functional_requirements/' + this.resolveId('functional_requirement', id),
        'PATCH',
        200,
        data
      )
    )
    const idx = this.functional_requirements.findIndex((fr) => fr.id === id)
    const fr = this.functional_requirements[idx]
    this.functional_requirements.splice(idx, 1, { ...fr, ...data })
    return this.functional_requirements[idx]
  }

  async deleteFunctionalRequirementById (id) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/functional_requirements/' + this.resolveId('functional_requirement', id),
        'DELETE',
        204
      )
    )
    const idx = this.functional_requirements.findIndex((r) => r.id === id)
    if (idx !== -1) { // TODO: what do we do if the functional_requirement is already missing for some reason?
      this.functional_requirements.splice(idx, 1)
    }
  }

  async addFunctionalRequirement (data) {
    const tempId = --this._tempIdCounter
    this.enqueue(() => {
      return sendUpdate(baseUrl + qfdUrlComponent + this.id + '/functional_requirements/', 'POST', 201, data)
        .then((response) => {
          this._tempIds.functional_requirement[tempId] = response.id
        }
        )
    })
    const functionalRequirement = { id: tempId, ...data }
    this.functional_requirements.push(functionalRequirement)
    return functionalRequirement
  }

  async updateSolutionById (id, data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/solutions/' + this.resolveId('solution', id),
        'PATCH',
        200,
        data
      )
    )
    const idx = this.solutions.findIndex((s) => s.id === id)
    const s = this.solutions[idx]
    this.solutions.splice(idx, 1, { ...s, ...data })
    return this.solutions[idx]
  }

  async deleteSolutionById (id) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/solutions/' + this.resolveId('solution', id),
        'DELETE',
        204
      )
    )
    const idx = this.solutions.findIndex((s) => s.id === id)
    if (idx !== -1) { // TODO: what do we do if the solution is already missing for some reason?
      this.solutions.splice(idx, 1)
    }
  }

  async addSolution (data) {
    const tempId = --this._tempIdCounter
    this.enqueue(() => {
      return sendUpdate(baseUrl + qfdUrlComponent + this.id + '/solutions/', 'POST', 201, data)
        .then((response) => {
          this._tempIds.solution[tempId] = response.id
        }
        )
    })
    const solution = { id: tempId, ...data }
    this.solutions.push(solution)
    return solution
  }

  async addOrUpdateAchievement (data) {
    if (data.solution_id === undefined) {
      const solution = await this.addSolution({})
      data.solution_id = solution.id
    }
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/achievements/',
        'POST',
        201, // TODO: This probably should be [200, 201] so that the backend can return appropriately whether it existed before or not.
        { ...data, solution_id: this.resolveId('solution', data.solution_id), functional_requirement_id: this.resolveId('functional_requirement', data.functional_requirement_id) }
      )
    )
    const idx = this.achievements.findIndex((ach) => (ach.solution_id === data.solution_id && ach.functional_requirement_id === data.functional_requirement_id))
    if (idx === -1) { this.achievements.push(data) } else { this.achievements.splice(idx, 1, data) }
    return data
  }

  async updateAnalysis (data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id,
        'PATCH',
        200,
        data
      )
    )
    const attr = Object.keys(data)[0]
    this[attr] = data[attr]
  }

  async getIssues () {
    const response = await fetch(baseUrl + qfdUrlComponent + this.id + '/issues', { method: 'GET', headers: { 'Content-Type': 'application/json' } })
    if (response.status === 200) {
      const data = await response.json()
      this.issues = data // todo: once wrapped in pydantically at backend, this should be data.data
      console.log('updated issues')
    }
  }

  async addOrUpdateNotes (data) {
    this.enqueue(
      () => sendUpdate(
        baseUrl + qfdUrlComponent + this.id + '/notes/',
        'POST',
        201,
        data
      )
    )

    const idx = this.notes.findIndex(note => note.tag === data.tag)
    if (idx === -1) { this.notes.push(data) } else { this.notes[idx].content = data.content }
    return data
  }
}
