// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { baseUrl } from '@/service/http.js'

const requestOptions = { method: 'GET', headers: { 'Content-type': 'application/json' } }

function getValidated (url) {
  return fetch(`${baseUrl}/${url}`, requestOptions).then(response => {
    if (response.status === 200) {
      return response.json()
    } else {
      throw new Error('Not the expected http status')
    }
  })
}

export function getTrizClassGroups () {
  return getValidated('triz/groups')
}

export function getSuggestedInventivePrinciples () {
  return getValidated('triz/principles')
}
