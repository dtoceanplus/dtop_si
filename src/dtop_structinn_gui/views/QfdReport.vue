<!-- DTOcean+ Structured Innovation Module
     Copyright (C) 2021 Energy Systems Catapult

     This program is free software: you can redistribute it and/or modify it
     under the terms of the GNU Affero General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
     License for more details.

     You should have received a copy of the GNU Affero General Public License
     along with this program. If not, see <https://www.gnu.org/licenses/>. -->

<template>
  <div>
    <el-alert
      type="info"
      show-icon
      :closable="false"
    >
      On this page, you can view the results of this analysis: How far solutions deviate from functional requirement targets, the potential for innovation to disrupt,
      the ideality of each solution, and suggested inventive principles to consider where functional requirements conflict.
    </el-alert>

    <div
      class="report-header"
    >
      <h2> Report </h2>
      <el-button
        v-if="analysisErrors === 0"
        type="secondary"
        size="small"
        @click="download"
      >
        Export to Excel
        <i
          class="el-icon-download"
        />
      </el-button>
    </div>
    <el-tabs
      v-if="analysisErrors === 0"
      v-model="currentTab"
    >
      <el-tab-pane
        label="Requirements"
        name="requirements"
        min-width="350"
      >
        <h3>Customer requirements</h3>
        <el-table
          :data="analysis.requirements"
        >
          <el-table-column
            label="Description"
            prop="description"
          />
          <el-table-column
            label="Importance"
            prop="importance"
          />
        </el-table>
        <h3>Functional requirements to meet the customer requirements</h3>
        <el-table
          :data="analysis.functional_requirements"
        >
          <el-table-column
            label="Description"
            prop="description"
          />
          <el-table-column
            label="DoI"
            prop="direction"
            min-width="13"
          >
            <template v-slot:default="{ row }">
              <i
                :class="row.direction === -1 ? 'el-icon-bottom-right' : 'el-icon-top-right'"
                style="font-weight:bold"
              />
            </template>
          </el-table-column>
          <el-table-column
            label="Target Value"
            prop="target"
            min-width="31"
          />
          <el-table-column
            label="Units"
            prop="units"
            min-width="31"
          />
          <el-table-column
            label="Engineering Difficulty"
            prop="engineering_difficulty"
            :formatter="r => difficultyLabel(r.engineering_difficulty)"
            min-width="20"
          />
          <el-table-column
            label="Delivery Difficulty"
            prop="delivery_difficulty"
            :formatter="r => difficultyLabel(r.delivery_difficulty)"
            min-width="20"
          />
        </el-table>
      </el-tab-pane>
      <el-tab-pane
        label="Deviation from targets"
        name="deviation"
      >
        <h3>Relative deviation from targets</h3>

        <QfdRelativeDeviationTable
          :functional-requirements="analysis.functional_requirements"
          :solutions="analysis.solutions"
          :deviation-from-targets="deviation_from_targets"
        />

        <h3>Actual deviation from targets</h3>

        <QfdActualDeviationTable
          :functional-requirements="analysis.functional_requirements"
          :solutions="analysis.solutions"
          :deviation-from-targets="deviation_from_targets"
        />
      </el-tab-pane>

      <el-tab-pane
        label="Potential for disruption"
        name="disruption"
      >
        <h3>Potential for disruption</h3>

        <QfdDisruptionPotentialTable
          :functional-requirements="analysis.functional_requirements"
          :functional-requirement-summaries="report.functional_requirement_summaries"
        />
      </el-tab-pane>

      <el-tab-pane
        label="Ideality of solutions"
        name="ideality"
      >
        <h3>Ideality of solutions</h3>

        <QfdSolutionIdealityTable
          :solutions="analysis.solutions"
          :solution-idealities="report.solution_idealities"
        />
      </el-tab-pane>
      <el-tab-pane
        label="TRIZ conflicts"
        name="conflicts"
      >
        <el-switch
          v-model="show_inventive_principle_details"
          active-text="Show inventive principle definitions and examples"
        />
        <el-collapse
          v-model="activeFR"
          accordion
        >
          <el-collapse-item
            v-for="fr in analysis.functional_requirements"
            :key="fr.id"
            :title="fr.description"
          >
            <qfd-suggested-inventive-principles-table
              v-bind="report.functional_requirements_conflicts.find(x => x.functional_requirement_id === fr.id)"
              :functional-requirements="analysis.functional_requirements"
              :triz-classes="triz_classes"
              :show-details="show_inventive_principle_details"
            />
          </el-collapse-item>
        </el-collapse>
      </el-tab-pane>
      <el-tab-pane
        label="Next Steps..."
        name="nextsteps"
      >
        <h2>Definition and analysis of the innovation problem using QFD</h2>
        <p>
          QFD is a structured process used to identify the voice of the customer (needs, expectations, etc.),
          prioritise them, and translate them into applicable technical requirements for each stage of product development
          and production.
          The process enables structured thinking, facilitating the development of innovative concept designs.
        </p>
        <p>When fully completed, the process captures</p>
        <ul>
          <li>customer requirements (and importance) to define the innovation design space;</li>
          <li>functional requirements (and importance), with measurable target values, to meet or exceed the customer requirements; </li>
          <li>interactions between requirements; </li>
          <li>the organisational effort to engineer and deliver such requirements at or beyond the target set; and</li>
          <li>
            benchmarking of competing state-of-the-art designs (leading-edge technology or design data,
            including the newest ideas or concepts across the sector) to understand the extent to which each
            of the proposed functional requirement targets has been met elsewhere.
          </li>
        </ul>
        <p>
          Concept designs can therefore be created with confidence that all key requirements have been fully considered.
          Innovators can also understand whether it is worth investing in developing novel solutions to meeting particular
          requirements and can prioritise important innovation areas based on:
        </p>
        <ol type="a">
          <li>the importance of the customer requirements;</li>
          <li>the functional requirements that would be most likely to disrupt the market; and</li>
          <li>
            the organisational effort to engineer and deliver these requirements (i.e. to secure the skills,
            resources, supply, finance, etc., required to deliver).
          </li>
        </ol>
        <h2>Resolving conflicting requirements and innovating using TRIZ</h2>
        <p>
          TRIZ is a systematic inventive problem-solving methodology that enables users to generate potential innovative solutions to any conflicting
          requirements raised during the QFD process.
          When fully completed, each conflict between requirements should be eliminated by applying generic inventive principles to determine the most appropriate alternative solutions.
        </p>
        <h2>Potential Next Steps</h2>
        <p>
          The critical functional requirements are those with the highest <b>overall impact</b> and the <b>least organisational effort</b> to implement.
          Reviewing these rankings allows the user to understand the interrelationship between the functional requirements,
          so one lower-ranked requirement might be key to more impactful requirements, and this should be considered in those cases.
          The user should also consider value-added <b>areas beyond the state-of-the-art</b> that contribute to the intended targets and customer needs.
        </p>
        <p>Once the critical functional requirements are defined, the user can choose to:</p>
        <ul>
          <li>Repeat the TRIZ process to consider and compare two or more potential innovative approaches to meeting the defined requirements; </li>
          <li>Assess the technical risks associated with the selected functional requirements using FMEA; </li>
          <li>
            Refine the functional requirements into more detailed and specific requirements by “Diving Deeper” into the QFD/TRIZ analysis,
            either by selecting a subsystem, assembly or component or defining requirements for the following system design and production phases.
            More information can be found in the documentation; and/or
          </li>
          <li>
            Use the detailed Design and Assessment tools in other modules of the DTOcean+ suite to develop detailed designs for the concepts created
            in this Structured Innovation tool, and assess their potential deployment to specific sites.
          </li>
        </ul>
      </el-tab-pane>
      <el-tab-pane
        label="Dive Deeper"
        name="divedeeper"
      >
        <el-table
          :data="analysis.functional_requirements"
          @selection-change="handleSelectionChange"
        >
          <el-table-column type="selection" />
          <el-table-column
            label="Functional Requirement"
            property="description"
          />
        </el-table>
        <div style="margin-top: 20px">
          <el-button
            type="primary"
            @click="onDiveDeeper"
          >
            Dive Deeper
          </el-button>
        </div>
      </el-tab-pane>
    </el-tabs>
    <div
      v-else
      id="error-message"
    >
      <p>
        <i
          class="el-icon-error"
          style="padding-right:10px"
        />Please fix all errors before viewing the QFD Report.
      </p>
      <p>
        View the errors in the Issues Toolpanel, by clicking the <i class="el-icon-warning-outline" /> icon.
      </p>
    </div>
  </div>
</template>

<script>

import { Analysis } from '@/service/analysis.js'

import declarations from '@/declarations.js'
import QfdRelativeDeviationTable from '@/components/QfdRelativeDeviationTable.vue'
import QfdActualDeviationTable from '@/components/QfdActualDeviationTable.vue'
import QfdDisruptionPotentialTable from '@/components/QfdDisruptionPotentialTable.vue'
import QfdSolutionIdealityTable from '@/components/QfdSolutionIdealityTable.vue'
import QfdSuggestedInventivePrinciplesTable from '@/components/QfdSuggestedInventivePrinciplesTable.vue'

export default {
  name: 'QfdReport',
  components: {
    QfdRelativeDeviationTable,
    QfdActualDeviationTable,
    QfdDisruptionPotentialTable,
    QfdSolutionIdealityTable,
    QfdSuggestedInventivePrinciplesTable,
  },
  props: {
    analysis: { type: Object, required: true },
    siId: { type: String, required: false, default: undefined },
  },
  data () {
    return {
      currentTab: 'requirements',
      show_inventive_principle_details: false,
      activeFR: null,
      report: { deviation_from_targets: [], functional_requirement_summaries: [], functional_requirements_conflicts: [], solution_idealities: [] },
      difficultyOptions: declarations.difficulty,
    }
  },
  computed: {
    deviation_from_targets () {
      return this.report.deviation_from_targets
    },
    triz_classes () {
      return Object.assign({}, ...Object.values(this.$trizClassGroups))
    },
    analysisErrors () {
      return this.analysis.issues.filter(issue => issue.type === 'error').length
    },
  },
  watch: {
    analysis () {
      this.getReport()
      this.selected = []
    },
  },
  created () {
    if (this.analysis.id) {
      this.getReport()
      this.selected = []
    }
  },
  methods: {
    handleSelectionChange (val) {
      this.selected = val
    },
    async getReport () {
      this.report = await this.analysis.getReport()
    },
    async onDiveDeeper () {
      const newAnalysis = await Analysis.create(this.siId, this.analysis.id)
      newAnalysis.updateAnalysis({ solution_hierarchy_level: this.analysis.solution_hierarchy_level + 1 })
      this.selected.forEach((fr) => newAnalysis.addRequirement(fr))
      await newAnalysis._queue
      this.$router.push({ name: 'QfdDefine', params: { id: newAnalysis.id } }) // todo: changes route correctly but old analysis is shown until refresh
    },
    download () {
      var dl = document.createElement('a')
      Analysis.exportReportToExcel(this.analysis.id).then(data => {
        dl.href = window.URL.createObjectURL(data)
        dl.download = `qfd-${this.analysis.id}-report.xlsx`
        dl.click()
      }).catch(() => {
        this.$message({
          dangerouslyUseHTMLString: true,
          message: '<p>Error exporting the QFD report to Excel. Please fix all errors before exporting the QFD report.</p><br><p>View the errors in the Issues Toolpanel, by clicking the <i class="el-icon-warning-outline" /> icon.</p>',
          type: 'error',
        })
      }).finally(() => {
        dl.remove()
      })
    },
    difficultyLabel (difficultyValue) {
      return this.difficultyOptions.find(x => x.value === difficultyValue).label
    },
  },
}
</script>

<style lang="scss" >
@use '@/variables.scss';
#error-message i {
  color: variables.$error;
  font-size: 1.5em;
}

.report-header {
  display: inline-block;
  text-align: left;
}

.report-header h2{
  display: inline-block;
  padding-right: 20px;

}
</style>
