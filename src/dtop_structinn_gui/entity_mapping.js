// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

// Mapping between entity keywords and component names
export const entityPageMap = {
  qfd: {
    requirement: 'QfdRequirements',
    functional_requirement: 'QfdFunctionalRequirements',
    analysis: 'QfdDefine',
    triz: 'QfdTriz',
    solution: 'QfdSolutions',
    achievement: 'QfdSolutions',
  },
  fmea: {
    requirement: 'FmeaRequirements',
    failure_mode: 'FmeaFailureModes',
    analysis: 'FmeaDefine',
    effect: 'FmeaEffects',
    cause: 'FmeaCauses',
    control: 'FmeaDesignControls',
    mitigation: 'FmeaMitigations',
  },
}

// Mapping between entity keywords and display labels
export const entityLabelMap = {
  qfd: {
    requirement: 'Customer Requirements',
    functional_requirement: 'Functional Requirements',
    analysis: 'Analysis',
    triz: 'TRIZ',
    solution: 'Solutions',
    achievement: 'Solutions',
  },
  fmea: {
    requirement: 'Design Requirements',
    failure_mode: 'Failure Modes',
    analysis: 'Analysis',
    effect: 'Effects',
    cause: 'Causes',
    control: 'Design Controls',
    mitigation: 'Mitigations',
  },
}

// Mapping between component names and tags used by issues and notes toolpane items.
export const tagPageMap = {
  qfd: {
    QfdRequirements: 'requirements',
    QfdFunctionalRequirements: 'functional_requirements',
    QfdSolutions: 'solutions',
    QfdImpacts: 'impacts',
    QfdDefine: 'definition',
    QfdTriz: 'triz_classes',
    QfdCorrelations: 'correlations',
    QfdReport: 'report',
  },
  fmea: {
    FmeaRequirements: 'requirements',
    FmeaFailureModes: 'failure_modes',
    FmeaDefine: 'definition',
    FmeaEffects: 'effects',
    FmeaCauses: 'causes',
    FmeaDesignControls: 'controls',
    FmeaMitigations: 'mitigations',
    FmeaReport: 'report',
  },
}
