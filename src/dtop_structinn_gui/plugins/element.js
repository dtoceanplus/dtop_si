// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import Element from 'element-ui'
import { getCell, getColumnByCell } from 'element-ui/packages/table/src/util'
import '../element-variables.scss'
import locale from 'element-ui/lib/locale/lang/en'

/* Adds an event to ElTable for when the focussed cell changes, whether it
  * is a click or a keyboard tab, or something else.
  */
const origMounted = Element.Table.mounted
Element.Table.mounted = function () {
  this.$refs.bodyWrapper.addEventListener('focusin', (e) => {
    const cell = getCell(e)
    const column = getColumnByCell(
      this, cell
    )
    const row = this.tableData[cell.parentNode.rowIndex]
    if (cell !== this.lastCell) {
      this.lastCell = cell
      this.$emit('cell-focus', row, column, cell, e)
    }
  })
  origMounted.call(this)
}

Vue.use(Element, { locale })
