// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import { getTrizClassGroups, getSuggestedInventivePrinciples } from './service/declarations.js'

// Import global styling
import './assets/css/global.css'

Vue.config.productionTip = false

getTrizClassGroups().then(r => {
  Vue.prototype.$trizClassGroups = r
})

getSuggestedInventivePrinciples().then(r => {
  Vue.prototype.$suggestedInventivePrinciples = r
})

Vue.prototype.$state = Vue.observable({ showToolPane: false })

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
