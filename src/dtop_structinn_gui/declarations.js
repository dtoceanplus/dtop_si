// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

export default {
  difficulty: [
    { value: 1, label: 'Low' },
    { value: 2, label: 'Low/moderate' },
    { value: 3, label: 'Moderate' },
    { value: 4, label: 'Moderate/high' },
    { value: 5, label: 'High' },
  ],

  correlation: // Order of elements determines slider order (it matters!)
    [
      { value: -9, label: '- High' },
      { value: -4, label: '- Medium' },
      { value: -1, label: '- Low' },
      { value: 0, label: 'None' },
      { value: 1, label: '+ Low' },
      { value: 4, label: '+ Medium' },
      { value: 9, label: '+ High' },
    ],

  impact: [
    { value: 0, label: 'None' },
    { value: 1, label: 'Low' },
    { value: 4, label: 'Medium' },
    { value: 9, label: 'High' },
  ],

  improvement: [
    { value: -1, label: 'Lower is better' },
    // { value: 0, label: "Other" },
    { value: 1, label: 'Higher is better' },
  ],

  occurrence_limit: [
    { value: 1, label: 'Failure unlikley (1 in 1,000,000)' },
    { value: 2, label: 'Relative few failures (1 in 250,000)' },
    { value: 3, label: 'Relative few failures (1 in 200,000)' },
    { value: 4, label: 'Occasional failures (1 in 1000)' },
    { value: 5, label: 'Occasional failures (1 in 400)' },
    { value: 6, label: 'Occasional failures (1 in 80)' },
    { value: 7, label: 'Repeated failures (1 in 20)' },
    { value: 8, label: 'Repeated failures (1 in 8)' },
    { value: 9, label: 'Failure inevitable (1 in 4)' },
    { value: 10, label: 'Failure inevitable > 1 in 2' },
  ],

  severity: [
    { value: 1, label: 'No effect' },
    { value: 2, label: 'Minor performance loss, defect noticed only by close inspection' },
    { value: 3, label: 'Minor performance loss, defect noticed only by most observers' },
    { value: 4, label: 'Minor performance loss, defect apparent' },
    { value: 5, label: 'Device operable, but minor performance loss, end-user experience some dissatisfaction' },
    { value: 6, label: 'Device operable, but auxiliary performance lost' },
    { value: 7, label: 'Device operable, but with reduced performance level, end-user dissatisfied' },
    { value: 8, label: 'Device inoperable, primary function lost' },
    { value: 9, label: 'Device inoperable, safe function lost with warning' },
    { value: 10, label: 'Device inoperable, safe function lost without warning' },
  ],

  detection: [
    { value: 1, label: 'Design controls will detect a potential cause / mechanism and failure mode' },
    { value: 2, label: 'Very high chance that DC can find the failure mode and cause' },
    { value: 3, label: 'high chance that DC can find the potential failure mode and cause' },
    { value: 4, label: 'Moderately high chance' },
    { value: 5, label: 'Moderate chance' },
    { value: 6, label: 'low chance' },
    { value: 7, label: 'Very low chance' },
    { value: 8, label: 'Remote chance' },
    { value: 9, label: 'Design control is unlikely to predict failure mode or cause' },
    { value: 10, label: 'Design control cannot detect the potential cause and failure mode, or there is no design control' },
  ],

}
