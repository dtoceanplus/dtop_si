// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import VueRouter from 'vue-router'
import FmeaEdit from '../views/FmeaEdit.vue'
import FmeaList from '../views/FmeaList.vue'
import FmeaDefine from '../views/FmeaDefine.vue'
import FmeaRequirements from '../views/FmeaRequirements.vue'
import FmeaFailureModes from '../views/FmeaFailureModes.vue'
import FmeaEffects from '../views/FmeaEffects.vue'
import FmeaCauses from '../views/FmeaCauses.vue'
import FmeaDesignControls from '../views/FmeaDesignControls.vue'
import FmeaMitigations from '../views/FmeaMitigations.vue'
import FmeaReport from '../views/FmeaReport.vue'
import QfdEdit from '../views/QfdEdit.vue'
import QfdList from '../views/QfdList.vue'
import QfdDefine from '../views/QfdDefine.vue'
import QfdRequirements from '../views/QfdRequirements.vue'
import QfdFunctionalRequirements from '../views/QfdFunctionalRequirements.vue'
import QfdImpacts from '../views/QfdImpacts.vue'
import QfdCorrelations from '../views/QfdCorrelations.vue'
import QfdTriz from '../views/QfdTriz.vue'
import QfdSolutions from '../views/QfdSolutions.vue'
import QfdReport from '../views/QfdReport.vue'
import Home from '../views/Home.vue'
import SiAnalysis from '../views/SiAnalysis.vue'

// Patch router push to allow navigation guard to redirect without uncaught error

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) {
    return originalPush.call(this, location, onResolve, onReject)
  }
  return originalPush.call(this, location).catch((err) => {
    if (!VueRouter.isNavigationFailure(err, VueRouter.NavigationFailureType.redirected)) {
      // rethrow error if not due to redirect
      return Promise.reject(err)
    } else {
      console.log('redirect')
    }
  })
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/:siId(\\d+)?',
    component: SiAnalysis,
    props: true,
    children: [
      {
        path: '',
        name: 'Home',
        component: Home,
      },
      {
        path: 'about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
      },
      {
        path: 'fmea',
        name: 'FmeaList',
        component: FmeaList,
      },
      {
        path: 'fmea/:id',
        name: 'FmeaEdit',
        component: FmeaEdit,
        props: route => ({ id: Number(route.params.id) }),
        children: [ // Each child must have a unique name
          {
            path: 'define',
            name: 'FmeaDefine',
            component: FmeaDefine,
            title: 'Definition',
          },
          {
            path: 'requirements',
            name: 'FmeaRequirements',
            component: FmeaRequirements,
            title: 'Design Requirements',
          },
          {
            path: 'failure-modes',
            name: 'FmeaFailureModes',
            component: FmeaFailureModes,
            title: 'Failure Modes',
          },
          {
            path: 'effects',
            name: 'FmeaEffects',
            component: FmeaEffects,
            title: 'Effects',
          },
          {
            path: 'causes',
            name: 'FmeaCauses',
            component: FmeaCauses,
            title: 'Causes',
          },
          {
            path: 'design-controls',
            name: 'FmeaDesignControls',
            component: FmeaDesignControls,
            title: 'Design Controls',
          },
          {
            path: 'mitigations',
            name: 'FmeaMitigations',
            component: FmeaMitigations,
            title: 'Mitigations',
          },
          {
            path: 'report',
            name: 'FmeaReport',
            component: FmeaReport,
            title: 'Report',
          },
        ],
      },
      {
        path: 'qfd',
        name: 'QfdList',
        component: QfdList,
      },
      {
        path: 'qfd/:id',
        name: 'QfdEdit',
        component: QfdEdit,
        props: route => ({ id: Number(route.params.id) }),
        children: [ // Each child must have a unique name
          {
            path: 'define',
            name: 'QfdDefine',
            component: QfdDefine,
            title: 'Definition',
          },
          {
            path: 'requirements',
            name: 'QfdRequirements',
            component: QfdRequirements,
            title: 'Customer Requirements',
          },
          {
            path: 'functional_requirements',
            name: 'QfdFunctionalRequirements',
            component: QfdFunctionalRequirements,
            title: 'Functional Requirements',
          },
          {
            path: 'impacts',
            name: 'QfdImpacts',
            component: QfdImpacts,
            title: 'Impacts',
          },
          {
            path: 'correlations',
            name: 'QfdCorrelations',
            component: QfdCorrelations,
            title: 'Correlations',
          },
          {
            path: 'triz',
            name: 'QfdTriz',
            component: QfdTriz,
            title: 'TRIZ',
          },
          {
            path: 'solutions',
            name: 'QfdSolutions',
            component: QfdSolutions,
            title: 'Solutions',
          },
          {
            path: 'report',
            name: 'QfdReport',
            component: QfdReport,
            title: 'Report',
          },
        ],
      },
    ],
  // todo: Should Home use the same code as About? Or a normal import?
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, from, next) => {
  if (to.params.siId || !from.params.siId) {
    next()
  } else {
    to.params.siId = from.params.siId
    next(to)
  }
})

export default router
