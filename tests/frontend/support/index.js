// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import ElementUI from 'element-ui'
import { mount, createLocalVue } from '@vue/test-utils'
import TableWrapper from './TableWrapper.vue'

const createTestVue = () => {
  const localVue = createLocalVue()
  localVue.use(ElementUI)
  return localVue
}

export const mountComponent = (component, options) => {
  const localVue = createTestVue()
  return mount(component, {
    localVue,
    ...options,
  })
}

export const initTableWrapper = (data, slotComponent, slotComponentProps) => {
  /* The TableWrapper component provides a minimal shell in which TableColumn* components can
  be tested. This is required as ElementUI will not allow internal table components, such
  as <el-table-column> to be rendered independently of a parent <el-table>.

    data: data passed to TableWrapper, which will be passed through to the component under test (Array)
    slotComponent: name of the component under test (Component)
    slotComponentProps: Props to be passed to the slot component (Object)
  */

  // Render the slotComponent, with the required props
  const slotWrapper = {
    render (h) {
      return h(slotComponent, {
        props: slotComponentProps,
      })
    },
  }

  // Now we can mount TableWrapper, passing in our required component-under-test
  // to the default slot.
  const wrapper = mountComponent(TableWrapper, {
    propsData: {
      tdata: data,
    },
    slots: {
      default: slotWrapper,
    },
  })

  return wrapper
}
