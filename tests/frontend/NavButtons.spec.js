// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import NavButtons from '@/components/NavButtons.vue'
import { mountComponent } from './support/index.js'

describe('NavButtons unit tests', () => {
  const defaultProps = {
    steps: [{ name: 'First' },
      { name: 'Second' },
      { name: 'Third' },
      { name: 'Last' }],
  }

  const defaultRoute = { $route: defaultProps.steps[0] }
  let wrapper

  function nextBtn (wrapper) {
    return wrapper.get('#next-btn')
  }
  function prevBtn (wrapper) {
    return wrapper.get('#prev-btn')
  }

  afterEach(() => {
    if (wrapper) { wrapper.destroy() }
  })

  describe('NavButtons display correctly', () => {
    beforeEach(() => {
      wrapper = mountComponent(NavButtons, {
        propsData: defaultProps,
        mocks: defaultRoute,
      })
    })
    test('Both NavButtons are rendered', () => {
      expect(nextBtn(wrapper).exists()).toBe(true)
      expect(prevBtn(wrapper).exists()).toBe(true)
    })
    test('Previous button contains correct text and styling', () => {
      expect(prevBtn(wrapper).text()).toEqual('Previous')
      expect(prevBtn(wrapper).element).toHaveClass('el-button--secondary')
    })
    test('Next button contains correct text and styling', () => {
      expect(nextBtn(wrapper).text()).toEqual('Next')
      expect(nextBtn(wrapper).element).toHaveClass('el-button--primary')
    })
  })
  describe('NavButtons are conditionally hidden based on the current route', () => {
    test('For first step, Previous button is hidden.', () => {
      wrapper = mountComponent(NavButtons, {
        propsData: defaultProps,
        mocks: {
          $route: { name: 'First' },
        },
      })
      expect(wrapper.vm.$route.name).toEqual('First')
      expect(prevBtn(wrapper).isVisible()).toBe(false)
      expect(nextBtn(wrapper).isVisible()).toBe(true)
    })
    test('For middle steps, both buttons are shown.', () => {
      wrapper = mountComponent(NavButtons, {
        propsData: defaultProps,
        mocks: {
          $route: { name: 'Second' },
        },
      })
      expect(wrapper.vm.$route.name).toEqual('Second')
      expect(prevBtn(wrapper).isVisible()).toBe(true)
      expect(nextBtn(wrapper).isVisible()).toBe(true)
    })
    test('For final step, Next button is hidden.', () => {
      wrapper = mountComponent(NavButtons, {
        propsData: defaultProps,
        mocks: {
          $route: { name: 'Last' },
        },
      })
      expect(wrapper.vm.$route.name).toEqual('Last')
      expect(prevBtn(wrapper).isVisible()).toBe(true)
      expect(nextBtn(wrapper).isVisible()).toBe(false)
    })
  })
  describe('NavButtons route correctly when clicked', () => {
    let $router

    beforeEach(() => {
      // mock the $router
      $router = { push: jest.fn() }
    })
    test('Next button routes to the next page', async () => {
      const stepNum = 0

      wrapper = mountComponent(NavButtons, {
        propsData: defaultProps,
        mocks: {
          $route: defaultProps.steps[stepNum],
          $router,
        },
      })

      await nextBtn(wrapper).trigger('click')
      expect(wrapper.vm.$router.push).toHaveBeenCalledWith(defaultProps.steps[stepNum + 1])
    })
    test('Previous button routes to the previous page', async () => {
      const stepNum = 3

      wrapper = mountComponent(NavButtons, {
        propsData: defaultProps,
        mocks: {
          $route: defaultProps.steps[stepNum],
          $router,
        },
      })
      await prevBtn(wrapper).trigger('click')
      expect(wrapper.vm.$router.push).toHaveBeenCalledWith(defaultProps.steps[stepNum - 1])
    })
  })
})
