// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import TableListAnalyses from '@/components/TableListAnalyses.vue'
import { mountComponent } from './support/index.js'

describe('Unit tests for TableListAnalyses component', () => {
  const defaultProps = { analyses: [], definePage: '' }
  let wrapper

  beforeEach(() => {
    wrapper = mountComponent(TableListAnalyses, {
      propsData: defaultProps,
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  describe('Table with no content', () => {
    test('Table has correct set of default columns.', () => {
      const tableHeaders = wrapper.findAll('th .cell')
      expect(tableHeaders.length).toEqual(3)
      expect(tableHeaders.filter(hdr => hdr.text() === 'Name').exists()).toBe(true)
      expect(tableHeaders.filter(hdr => hdr.text() === 'Updated').exists()).toBe(true)
      expect(tableHeaders.filter(hdr => hdr.text() === '').exists()).toBe(true)
    })
    test('Table has no body content.', () => {
      expect(wrapper.findAll('tbody tr').exists()).toBe(false)
    })
  })
  describe('Test table with actual content', () => {
    const dummyAnalyses = [
      { name: 'Analysis A', updated: '2021-01-01', id: 100 },
      { name: 'Analysis B', updated: '2020-01-01', id: 200 },
      { name: 'Analysis C', updated: '2019-01-01', id: 300 },
    ]

    beforeEach(async () => {
      await wrapper.setProps({ analyses: dummyAnalyses })
    })
    test('Table displays analyses sorted in the correct initial order', () => {
      // Default sort is most recently updated at the top
      wrapper.findAll('tbody tr').wrappers.forEach((row, idx) => {
        const analysis = dummyAnalyses[idx]
        const cells = row.findAll('td')

        expect(cells.filter(cell => cell.text() === analysis.name).exists()).toBe(true)
      })
    })

    test('Table displays correct fields for all analyses', () => {
      wrapper.findAll('tbody tr').wrappers.forEach((row, idx) => {
        const analysis = dummyAnalyses[idx]
        const cells = row.findAll('td')

        expect(cells.filter(cell => cell.text() === analysis.name).exists()).toBe(true)
        const formattedDate = wrapper.vm.localiseDate(null, null, dummyAnalyses[idx].updated)
        expect(cells.filter(cell => cell.text() === formattedDate).exists()).toBe(true)
      })
    })
    test('Table renders the delete and download icons for each row, and hides them', () => {
      wrapper.findAll('tbody tr').wrappers.forEach((row, idx) => {
        expect(row.find('.el-icon-delete').element).not.toBeVisible()
        expect(row.find('.el-icon-download').element).not.toBeVisible()
      })
    })
    test('Table makes delete and download icons visible on mouseover', async () => {
      await wrapper.findAll('tbody tr').wrappers.forEach((row, idx) => {
        const delIcon = row.find('.el-icon-delete')
        const downloadIcon = row.find('.el-icon-download')
        delIcon.trigger('mouseover')
        expect(delIcon.isVisible()).toBe(true)
        downloadIcon.trigger('mouseover')
        expect(downloadIcon.isVisible()).toBe(true)
      })
    })
    test('Table updates when analysis is added', async () => {
      const updatedAnalyses = [...dummyAnalyses, { name: 'AnalysisD', updated: '2021-01-02', id: 400 }]
      expect(wrapper.findAll('tbody tr').length).toBe(dummyAnalyses.length)
      await wrapper.setProps({ analyses: updatedAnalyses })
      expect(wrapper.findAll('tbody tr').length).toBe(updatedAnalyses.length)
    })
    test('Table updates when analysis is removed', async () => {
      const updatedAnalyses = dummyAnalyses.slice(0, 1)
      await wrapper.setProps({ analyses: updatedAnalyses })
      expect(wrapper.findAll('tbody tr').length).toBe(updatedAnalyses.length)
    })
    test('Table updates when an analysis property is changed', async () => {
      // Modify the attributes in an analysis
      expect(wrapper.findAll('tbody tr').at(0).find('td').text()).toBe(dummyAnalyses[0].name)
      const updatedAnalyses = [...dummyAnalyses]
      updatedAnalyses[0].name = 'Analysis X'
      await wrapper.setProps({ analyses: updatedAnalyses })
      expect(wrapper.findAll('tbody tr').at(0).find('td').text()).toBe(updatedAnalyses[0].name)
    })
  })
  describe('Test table with additional columns, passed as slots', () => {
    let wrapper
    const dummyAnalyses = [
      { name: 'Analysis A', updated: '2021-01-01', id: 100, attrA: 'SomeAttrA', attrB: 'SomeAttrB' },
    ]

    beforeEach(() => {
      wrapper = mountComponent(TableListAnalyses, {
        propsData: {
          analyses: dummyAnalyses,
          definePage: 'qfd',
        },
        slots: {
          cols: ['<el-table-column label="ExtraCol1" prop="attrA" />',
            '<el-table-column label="ExtraCol2" prop="attrB" />'],
        },
      })
    })
    test('Table renders the correct number of columns, in the correct order.', () => {
      // 3 default columns, plus those passed via slots.
      expect(wrapper.findAll('th').length).toEqual(5)
      // First column is 'Name'. Next N columns are slots, then 'Updated'
      expect(wrapper.findAll('th').at(0).text()).toBe('Name')
      expect(wrapper.findAll('th').at(1).text()).toBe('ExtraCol1')
      expect(wrapper.findAll('th').at(2).text()).toBe('ExtraCol2')
      expect(wrapper.findAll('th').at(3).text()).toBe('Updated')
    })
    test('Table renders the relevant content in the correct columns.', () => {
      // Check the data from the slots is rendered in the correct columns
      expect(wrapper.findAll('tbody td').at(0).text()).toBe(dummyAnalyses[0].name) // the name
      expect(wrapper.findAll('tbody td').at(1).text()).toBe(dummyAnalyses[0].attrA) // first slot content
      expect(wrapper.findAll('tbody td').at(2).text()).toBe(dummyAnalyses[0].attrB) // second slot content
      expect(wrapper.findAll('tbody td').at(3).text()).toBe(wrapper.vm.localiseDate(null, null, dummyAnalyses[0].updated))
    })
  })
  describe('Table handles events', () => {
    const dummyAnalyses = [
      { name: 'Analysis A', updated: '2021-01-01', id: 100 },
      { name: 'Analysis B', updated: '2020-01-01', id: 200 },
    ]

    beforeEach(async () => {
      wrapper = mountComponent(TableListAnalyses, {
        propsData: { analyses: dummyAnalyses, definePage: 'qfd' },
        mocks: {
          $router: {
            push: jest.fn(),
          },
        },
      })
    })
    test('Table routes to the correct page when a row is clicked', async () => {
      const routerPush = jest.spyOn(wrapper.vm.$router, 'push')
      const url = wrapper.props('definePage')

      // Check each row
      dummyAnalyses.forEach((analysis, idx) => {
        // const expectedRoute = '/' + url + '/' + analysis.id + '/define'
        const expected = { name: url, params: { id: analysis.id } }
        wrapper.findAll('tbody tr').at(idx).find('td').trigger('click')
        expect(routerPush).toHaveBeenCalledWith(expected)
      })

      // Modify the definePage prop
      await wrapper.setProps({ definePage: 'fmea' })

      // Check this updates properly, for the first row
      const expected = { name: url, params: { id: dummyAnalyses[0].id } }
      wrapper.findAll('tbody tr').at(0).find('td').trigger('click')
      expect(routerPush).toHaveBeenCalledWith(expected)
    })
    test('Table correctly emits a delete event', async () => {
      const delIcon = wrapper.find('.el-icon-delete')
      await delIcon.trigger('click')
      const yesBtn = wrapper.findAll('button').filter(btn => btn.text() === 'Yes')
      await yesBtn.trigger('click')
      expect(wrapper.emitted('del')).toBeTruthy()
      expect(wrapper.emitted('del')[0]).toEqual([dummyAnalyses[0].id])
    })
    test('Table correctly emits a download event', async () => {
      const downloadIcon = wrapper.find('.el-icon-download')
      await downloadIcon.trigger('click')
      expect(wrapper.emitted('download')).toBeTruthy()
      expect(wrapper.emitted('download')[0]).toEqual([dummyAnalyses[0].id])
    })
  })
})
