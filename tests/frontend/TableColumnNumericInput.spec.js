// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import TableColumnNumericInput from '@/components/TableColumnNumericInput.vue'
import { initTableWrapper } from './support/index.js'

const dummyData = [
  { value: '100', id: 1 }, { value: '200', id: 2 }, { value: '300', id: 3 },
]
const colProps = {
  label: 'ColName',
  modelAttr: 'value',
  minWidth: '50',
  className: 'numeric-col',
}

describe('Unit tests for TableColumnNumericInput component', () => {
  describe('Default rendering and props', () => {
    let wrapper
    beforeEach(() => {
      wrapper = initTableWrapper(dummyData, TableColumnNumericInput, colProps)
    })

    test('Check column is rendered correctly', () => {
      expect(wrapper.findAll('th').length).toBe(1)
      expect(wrapper.find('th div.cell').text()).toBe(colProps.label)
      expect(wrapper.findAll('tbody input').at(0).element).toHaveAttribute('type', 'text')
      expect(wrapper.findAll('tbody input').at(0).element).toHaveAttribute('id', colProps.modelAttr + '-' + dummyData[0].id)
    })
    test('Check props are applied correctly', () => {
      // Check the column label is correct
      expect(wrapper.find('th div.cell').text()).toBe(colProps.label)

      // Values should be correctly set for the inputs.
      // Each input should also have the id="[modelAttr]-[id]"

      wrapper.findAll('input').wrappers.forEach((input, idx) => {
        expect(input.element.value).toBe(dummyData[idx][colProps.modelAttr])
        expect(input.element).toHaveAttribute('id', colProps.modelAttr + '-' + dummyData[idx].id)
      })

      // All <col> elements should have the 'width' attribute set
      wrapper.findAll('col').wrappers.forEach((col) => {
        expect(col.element).toHaveAttribute('width', colProps.minWidth)
      })

      // All <td> elements should have the class properly set
      wrapper.findAll('td').wrappers.forEach((td) => {
        expect(td.element).toHaveClass(colProps.className)
      })
    })
  })
  describe('Modifiying props', () => {
    let wrapper
    beforeEach(() => {
      // idGenerator returns an ID of 'fake-id-<id>' for each row
      // readonlyFunc returns 'true' for the row with id 1, false otherwise
      const colPropsMod = { ...colProps, idGenerator: (row) => 'fake-id-' + row.id, readonlyFunc: (row) => row.id === 1 || false }
      wrapper = initTableWrapper(dummyData, TableColumnNumericInput, colPropsMod)
    })

    test('Custom ID can be set', () => {
      wrapper.findAll('input').wrappers.forEach((input, idx) => {
        expect(input.element).toHaveAttribute('id', 'fake-id-' + dummyData[idx].id)
      })
    })
  })
})
describe('Handling events', () => {
  let wrapper

  beforeEach(() => {
    wrapper = initTableWrapper(dummyData, TableColumnNumericInput, colProps)
  })
  test('Correct event emitted from @input from child component', () => {
    // Mock the $emit method of our component under test
    const tcni = wrapper.findComponent(TableColumnNumericInput)
    tcni.vm.$emit = jest.fn()

    // Get the first numeric input and fake an $emit event
    const rownum = 0
    const numeric = wrapper.findAllComponents({ name: 'NumericInput' }).at(rownum)
    const event = 'input'
    const updated = 20
    numeric.vm.$emit(event, updated)

    // Check our TableColumnNumericInput $emit was called
    // (remember wrapper is actually our TableWrapper test component!)
    // expected payload: event, row (updated), modelAttr, newValue
    expect(tcni.vm.$emit).toHaveBeenCalledWith(event, { ...dummyData[rownum], value: updated }, 'value', updated)
  })
  test('Correct event emitted from @focus from child component', () => {
    // Mock the $emit method of our component under test
    const tcni = wrapper.findComponent(TableColumnNumericInput)
    tcni.vm.$emit = jest.fn()

    // Simulate a focus event
    const rownum = 0
    const numeric = wrapper.findAllComponents({ name: 'NumericInput' }).at(rownum)
    const event = 'focus'
    numeric.vm.$emit(event)

    // expected payload: event, row
    expect(tcni.vm.$emit).toHaveBeenCalledWith('selected', dummyData[rownum])
  })
})
