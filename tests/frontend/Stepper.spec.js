// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Stepper from '@/components/Stepper.vue'
import Vue from 'vue'
import { mountComponent } from './support/index.js'

describe('Stepper unit tests', () => {
  const defaultSteps = [
    { name: 'First', path: '/first', title: 'First Step', errors: 0 },
    { name: 'Second', path: '/second', title: 'Second Step', errors: 0 },
    { name: 'Third', path: '/third', title: 'Third Step', errors: 0 },
    { name: 'Last', path: '/last', title: 'Last Step', errors: 0 },
  ]

  let wrapper

  afterEach(() => {
    if (wrapper) { wrapper.destroy() }
  })

  describe('Stepper displays correctly', () => {
    beforeEach(() => {
      // Create a default mount
      wrapper = mountComponent(Stepper, {
        propsData: {
          steps: defaultSteps,
        },
        mocks: {
          $route: {
            name: 'First',
          },
        },
      })
    })
    test('Stepper is rendered correctly', () => {
      expect(wrapper.find('#stepper').exists()).toBe(true)
      for (let i = 0; i < defaultSteps.length; i++) {
        expect(wrapper.find('#step-' + i).exists()).toBe(true)
      }
    })
    test('Stepper displays correct state based on current route', () => {
      // First step should have the 'is-process' class
      // others should have 'is-wait' class
      expect(wrapper.find('#step-0 > .is-process').exists()).toBe(true)
      for (let i = 1; i < defaultSteps.length; i++) {
        expect(wrapper.find('#step-' + i + '> .is-wait').exists()).toBe(true)
      }
    })
    test('The status of the steps change as the route changes', async () => {
      // The different step statuses, based on progress along the stepper
      const current = '.is-process'
      const done = '.is-finish'
      const todo = '.is-wait'

      for (const [stepNum, stepInfo] of defaultSteps.entries()) {
        wrapper.vm.$route.name = stepInfo.name
        await Vue.nextTick()
        for (let i = 0; i < defaultSteps.length; i++) {
          if (i < stepNum) {
            expect(wrapper.find('#step-' + i + '> ' + done).exists()).toBe(true)
          } else if (i === stepNum) {
            expect(wrapper.find('#step-' + i + '> ' + current).exists()).toBe(true)
          } else {
            expect(wrapper.find('#step-' + i + '> ' + todo).exists()).toBe(true)
          }
        }
      }
    })
    test('Steps with errors display an error badge', async () => {
      // Assign errors to steps 1 and 3
      const stepsWithErrors = [...defaultSteps]
      stepsWithErrors[0].errors = 1
      stepsWithErrors[2].errors = 3
      await wrapper.setProps({ steps: stepsWithErrors })

      // Steps 1 and 3 display the error badge
      expect(wrapper.find('#step-0 .is-dot').isVisible()).toBe(true)
      expect(wrapper.find('#step-2 .is-dot').isVisible()).toBe(true)

      // Steps 2 and 4 DO NOT display the error badge
      expect(wrapper.find('#step-1 sup.is-dot').element).not.toBeVisible()
      expect(wrapper.find('#step-3 sup.is-dot').element).not.toBeVisible()
    })
    test('Updating steps updates the error badge', async () => {
      const stepsWithErrors = [...defaultSteps]
      stepsWithErrors[0].errors = 1
      await wrapper.setProps({ steps: stepsWithErrors })

      // Step 1 displays an error badge
      expect(wrapper.find('#step-0 .is-dot').isVisible()).toBe(true)

      // Update the props data. Error is resolved.
      const stepsWithResolvedErrors = [...stepsWithErrors]
      stepsWithResolvedErrors[0].errors = 0

      await wrapper.setProps({ steps: stepsWithResolvedErrors })

      // Error no longer displayed.
      expect(wrapper.find('#step-0 .is-dot').element).not.toBeVisible()
    })
  })
  describe('Stepper routes correctly', () => {
    let wrapper
    beforeEach(() => {
      wrapper = mountComponent(Stepper, {
        propsData: { steps: defaultSteps },
        mocks: {
          $router: {
            push: jest.fn(),
          },
          $route: {
            name: 'fake',
          },
        },
      })
    })
    test('Clicking on the step routes to the correct place', async () => {
      // Click each step and assert the router.push behaviour is correct

      for (const [idx, stepInfo] of defaultSteps.entries()) {
        const step = wrapper.find('#step-' + idx)
        await step.trigger('click')
        expect(wrapper.vm.$router.push).toHaveBeenCalledWith(stepInfo)
      }

      expect(wrapper.vm.$router.push).toHaveBeenCalledTimes(defaultSteps.length)
    })
    test('Clicking on a step while on that page does not invoke a router.push', async () => {
      // Set the current $route to be 'First'
      wrapper.vm.$route.name = 'First'

      // $router.push should NOT be called, because we are already at that route.
      const step = wrapper.find('#step-0')
      await step.trigger('click')
      expect(wrapper.vm.$router.push).not.toHaveBeenCalled()
    })
  })
})
