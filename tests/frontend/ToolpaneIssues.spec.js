// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import ToolpaneIssues from '@/components/ToolpaneIssues.vue'
import { mountComponent } from './support/index.js'
import { tagPageMap, entityLabelMap } from '@/entity_mapping.js'

const dummyAnalysis = {
  issues: [],
  _type: 'qfd',
  _queueLength: 0,
  getIssues: jest.fn(),
}

const issueA = {
  entity: 'requirement',
  field: 'description',
  message: 'This is issue A',
  tags: ['requirements'],
  type: 'warning',
}

const issueB = {
  entity: 'impact',
  field: 'description',
  message: 'This is issue B',
  tags: ['impacts'],
  type: 'warning',
}

const issueD = {
  entity: 'solution',
  field: 'description',
  message: 'This is issue D',
  tags: ['solutions'],
  type: 'error',
}

describe('Unit tests for ToolpaneIssues', () => {
  test('Issues toolpane renders', () => {
    const wrapper = mountComponent(ToolpaneIssues, {
      propsData: {
        analysis: dummyAnalysis,
      },
      mocks: {
        $route: {
          name: 'mock',
        },
      },
    })
    expect(wrapper.find('h3').text()).toBe('Issues')
    expect(wrapper.find('.el-switch').exists()).toBe(true)
    expect(wrapper.find('#issues-table').exists()).toBe(true)
    // there should be no content in the table
    expect(wrapper.findAll('#issues-table tbody tr').length).toBe(0)
  })
  test('Displays only issues for the current page, by default', async () => {
    const analysis = { ...dummyAnalysis, issues: [issueA, issueB] }

    const routeName = Object.keys(tagPageMap[analysis._type])[0]

    const wrapper = mountComponent(ToolpaneIssues, {
      propsData: {
        analysis: analysis,
      },
      mocks: {
        $route: {
          name: routeName,
        },
      },
    })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.$route.name).toBe(routeName)
    expect(wrapper.vm.$data.showAll).toBe(false)

    // Because showAll = false, we expect issueA to be shown, but NOT issueB
    expect(wrapper.findAll('#issues-table tbody tr').length).toBe(1)

    const expectedState = issueA.type
    const expectedLabel = entityLabelMap[analysis._type][issueA.entity]
    const expectedMessage = issueA.message

    expect(wrapper.findAll('#issues-table td').at(0).find('i').classes()).toContain('el-icon-' + expectedState)
    expect(wrapper.findAll('#issues-table td').at(1).find('span').text()).toBe(expectedLabel)
    expect(wrapper.findAll('#issues-table td').at(1).find('.cell').text()).toContain(expectedMessage)
  })
  test('Displays issues for all pages, when switch is set', async () => {
    const analysis = { ...dummyAnalysis, issues: [issueA, issueB] }

    const routeName = Object.keys(tagPageMap[analysis._type])[0]

    const wrapper = mountComponent(ToolpaneIssues, {
      propsData: {
        analysis: analysis,
      },
      data () {
        return {
          showAll: true,
        }
      },
      mocks: {
        $route: {
          name: routeName,
        },
      },
    })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.$route.name).toBe(routeName)
    expect(wrapper.vm.$data.showAll).toBe(true)

    // Because showAll = true, we expect to see both issues displayed
    expect(wrapper.findAll('#issues-table tbody tr').length).toBe(2)
  })
  test('Issues panel updates when the switch is toggled', async () => {
    const analysis = { ...dummyAnalysis, issues: [issueD, issueA] }

    const routeName = Object.keys(tagPageMap[analysis._type])[2] // QfdSolutions (issueD)

    const wrapper = mountComponent(ToolpaneIssues, {
      propsData: {
        analysis: analysis,
      },
      mocks: {
        $route: {
          name: routeName,
        },
      },
    })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.$route.name).toBe(routeName)
    expect(wrapper.vm.$data.showAll).toBe(false)

    // We would expect one issue to be shown which corresponds to this route (issueD)
    expect(wrapper.findAll('#issues-table tbody tr').length).toBe(1)
    expect(wrapper.findAll('#issues-table td').at(1).find('.cell').text()).toContain(issueD.message)

    // toggle the switch
    await wrapper.find('.el-switch').trigger('click')
    expect(wrapper.vm.$data.showAll).toBe(true)

    // We are now 'showing all' so would expect both rows to be displayed
    expect(wrapper.findAll('#issues-table tbody tr').length).toBe(2)
    expect(wrapper.findAll('#issues-table td').at(3).find('.cell').text()).toContain(issueA.message)
  })
  test('Issues panel updates when the page is changed', async () => {
    // Test as above, but this time we change page rather than toggle the 'showAll' switch

    const analysis = { ...dummyAnalysis, issues: [issueD, issueA] }

    const routeName = Object.keys(tagPageMap[analysis._type])[2] // QfdSolutions (issueD)

    const wrapper = mountComponent(ToolpaneIssues, {
      propsData: {
        analysis: analysis,
      },
      mocks: {
        $route: {
          name: routeName,
        },
      },
    })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.$route.name).toBe(routeName)
    expect(wrapper.vm.$data.showAll).toBe(false)

    expect(wrapper.findAll('#issues-table tbody tr').length).toBe(1)
    expect(wrapper.findAll('#issues-table td').at(1).find('.cell').text()).toContain(issueD.message)

    // Change page
    wrapper.vm.$route.name = Object.keys(tagPageMap[analysis._type])[0] // QfdRequirements (issueA)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.showAll).toBe(false) // this is still false

    // We have now switched page, but 'showAll' is still false, so we expect to see only 1 issue (issueA)
    expect(wrapper.findAll('#issues-table tbody tr').length).toBe(1)
    expect(wrapper.findAll('#issues-table td').at(1).find('.cell').text()).toContain(issueA.message)
  })
  test('Issues are refreshed when there are changes to the analysis', async () => {
    // Testing that when analysis._queueLength changes, the getIssues() method is called

    // We can mock out getIssues() and check it is called
    const getIssuesMock = jest.fn()

    // This lets Jest mock the timeouts
    jest.useFakeTimers()

    const analysis = { ...dummyAnalysis, getIssues: getIssuesMock, _queueLength: 3 }

    const routeName = Object.keys(tagPageMap[analysis._type])[0]

    const wrapper = mountComponent(ToolpaneIssues, {
      propsData: {
        analysis: analysis,
      },
      data () {
        return {
          showAll: true,
        }
      },
      mocks: {
        $route: {
          name: routeName,
        },
      },
    })
    await wrapper.vm.$nextTick()

    // Starting with no issues
    expect(wrapper.vm.$route.name).toBe(routeName)
    expect(wrapper.vm.$data.showAll).toBe(true)
    expect(wrapper.findAll('#issues-table tbody tr').length).toBe(0)

    // Simulate the queue being flushed. It is now back to zero.
    await wrapper.setProps({ analysis: { ...analysis, _queueLength: 0 } })
    jest.runAllTimers()
    expect(getIssuesMock).toBeCalled()
  })
  test.skip('Double-clicking an issue will navigate to and take focus, on same page', () => {
    // todo
  })
  test.skip('Double-clicking an issue will navigate to, and take focus, on a different page', () => {
    // todo
  })
})
