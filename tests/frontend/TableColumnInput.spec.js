// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import TableColumnInput from '@/components/TableColumnInput.vue'
import { initTableWrapper } from './support/index.js'

const dummyData = [
  { name: 'A', id: 1 }, { name: 'B', id: 2 }, { name: 'C', id: 3 },
]
const colProps = {
  label: 'ColName',
  modelAttr: 'name',
  minWidth: '100',
  className: 'test-col',
}

describe('Unit tests for TableColumnInput component', () => {
  describe('Default rendering and props', () => {
    let wrapper
    beforeEach(() => {
      wrapper = initTableWrapper(dummyData, TableColumnInput, colProps)
    })

    test('Check column is rendered correctly', () => {
      expect(wrapper.findAll('th').length).toBe(1)
      expect(wrapper.find('th div.cell').text()).toBe(colProps.label)
      expect(wrapper.findAll('tbody input').at(0).element).toHaveAttribute('type', 'text')
      expect(wrapper.findAll('tbody input').at(0).element).toHaveAttribute('id', colProps.modelAttr + '-' + dummyData[0].id)
    })
    test('Check props are applied correctly', () => {
      // Check the column label is correct
      expect(wrapper.find('th div.cell').text()).toBe(colProps.label)

      // Values should be correctly set for the inputs.
      // Each input should also have the id="[modelAttr]-[id]"
      // Each input should be WRITEABLE by default(e.g. readonly = false)
      wrapper.findAll('input').wrappers.forEach((input, idx) => {
        expect(input.element.value).toBe(dummyData[idx][colProps.modelAttr])
        expect(input.element).toHaveAttribute('id', colProps.modelAttr + '-' + dummyData[idx].id)
        expect(input.element).not.toHaveAttribute('readonly')
      })

      // All <col> elements should have the 'width' attribute set
      wrapper.findAll('col').wrappers.forEach((col) => {
        expect(col.element).toHaveAttribute('width', colProps.minWidth)
      })

      // All <td> elements should have the class properly set
      wrapper.findAll('td').wrappers.forEach((td) => {
        expect(td.element).toHaveClass(colProps.className)
      })
    })
  })
  describe('Modifiying props', () => {
    let wrapper
    beforeEach(() => {
      // idGenerator returns an ID of 'fake-id-<id>' for each row
      // readonlyFunc returns 'true' for the row with id 1, false otherwise
      const colPropsMod = { ...colProps, idGenerator: (row) => 'fake-id-' + row.id, readonlyFunc: (row) => row.id === 1 || false }
      wrapper = initTableWrapper(dummyData, TableColumnInput, colPropsMod)
    })

    test('Custom ID and readonly status can be set', () => {
      wrapper.findAll('input').wrappers.forEach((input, idx) => {
        expect(input.element).toHaveAttribute('id', 'fake-id-' + dummyData[idx].id)
        // Assert the ReadOnly status of the various input elements
        if (dummyData[idx].id === 1) {
          expect(input.element).toHaveAttribute('readonly')
        } else {
          expect(input.element).not.toHaveAttribute('readonly')
        }
      })
    })
  })
  describe('Handling events', () => {
    let wrapper

    beforeEach(() => {
      wrapper = initTableWrapper(dummyData, TableColumnInput, colProps)
    })
    test('Changing the input emits correct event', async () => {
      // Mock the method which emits the event
      const tci = wrapper.getComponent(TableColumnInput)
      tci.vm.$emit = jest.fn()

      // Find the input, and modify the value
      const input = wrapper.get('#name-1')
      await input.setValue('ChangedValue')

      // expected payload is: event, row, modelAttr, newValue
      const event = 'input'

      expect(tci.vm.$emit).toHaveBeenCalledWith(event, dummyData[0], 'name', 'ChangedValue')
    })
    test('Deleting the input emits correct event', async () => {
    // Mock the method which emits the event
      const tci = wrapper.getComponent(TableColumnInput)
      tci.vm.$emit = jest.fn()

      // Find the SECOND input, and modify the value
      const input = wrapper.get('#name-2')
      await input.setValue('')

      // expected payload is: event, row, modelAttr, newValue
      const event = 'input'

      expect(tci.vm.$emit).toHaveBeenCalledWith(event, dummyData[1], 'name', '')
    })
  })
})
