// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import TableAddDelete from '@/components/TableAddDelete.vue'
import { mountComponent } from './support/index.js'

describe('Unit tests for TableListAnalyses component', () => {
  const defaultProps = {
    data: [
      { name: 'A', id: 10, size: 'big' },
      { name: 'B', id: 20, size: 'medium' },
      { name: 'C', id: 30, size: 'small' },
    ],
    deleteText: 'Delete me',
    emptyText: 'No Data',
  }
  let wrapper

  beforeEach(() => {
    wrapper = mountComponent(TableAddDelete, {
      propsData: defaultProps,
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  describe('Table displays correctly', () => {
    test('Displays one row per data item, plus an empty new row', () => {
      expect(wrapper.findAll('.table-add-del-row').length).toBe(defaultProps.data.length + 1)
    })
    test('All existing rows have a delete icon', () => {
      defaultProps.data.forEach((row) => {
        expect(wrapper.find('#delete-' + row.id).exists()).toBe(true)
      })
    })
    test('Delete icon is hidden by default', () => {
      defaultProps.data.forEach((row) => {
        expect(wrapper.find('#delete-' + row.id).element).not.toBeVisible()
      })
    })
    test('Delete icon displays on mouseover', async () => {
      for (const row of defaultProps.data) {
        const delIcon = wrapper.find('#delete-' + row.id)
        expect(delIcon.element).not.toBeVisible()
        await delIcon.trigger('mouseover')
        expect(delIcon.isVisible()).toBe(true)
      }
    })
    test('The new row has a delete icon, but it is always hidden', async () => {
      // The new row will have an id of 'delete-undefined' by default
      const delIcon = wrapper.find('#delete-undefined')
      expect(delIcon.exists()).toBe(true)
      expect(delIcon.attributes().style).toBe('display: none;')
      expect(delIcon.element).not.toBeVisible()

      await delIcon.trigger('mouseover')
      // Even on mouseover, it should be hidden
      expect(delIcon.element).not.toBeVisible()
    })
  })
  describe('Table handles events correctly', () => {
    test('Table correctly emits a delete event', async () => {
      // Click on the first row (Id of the first row in our dummy dataset is 10)
      const delIcon = wrapper.find('#delete-10')
      await delIcon.trigger('click')
      expect(wrapper.emitted('delete')).toBeTruthy()
      // Payload is the data itself, plus the 'is_new' flag
      const expectedPayload = { ...defaultProps.data[0], is_new: false }
      expect(wrapper.emitted('delete')[0]).toEqual([expectedPayload])
    })
  })
})
