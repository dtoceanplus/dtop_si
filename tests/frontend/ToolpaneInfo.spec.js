// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import ToolpaneInfo from '@/components/ToolpaneInfo.vue'
import { mountComponent } from './support/index.js'

describe('ToolpaneInfo unit tests', () => {
  test('Info panel renders', async () => {
    const wrapper = mountComponent(ToolpaneInfo, {
      propsData: {
        analysis: { created: '2021-03-29', updated: '2021-04-25' },
      },
    })
    await wrapper.vm.$nextTick() // *** required to allow the table to properly render ***

    expect(wrapper.find('h3').exists()).toBe(true)
    expect(wrapper.find('.el-table').exists()).toBe(true)
    // there should be two rows in the table
    expect(wrapper.findAll('.el-table tbody tr').length).toBe(2)

    // No name or objective property passed in the analysis prop
    expect(wrapper.find('h3').text()).toBe('Untitled Analysis')
    expect(wrapper.find('p').exists()).toBe(false)
  })
  test('Analysis name and objective are rendered, if present ', async () => {
    const wrapper = mountComponent(ToolpaneInfo, {
      propsData: {
        analysis: {
          created: '2021-03-29',
          updated: '2021-04-25',
          name: 'Analysis Name',
          objective: 'analysis objective',
        },
      },
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.find('h3').text()).toBe('Analysis Name')
    expect(wrapper.find('p').exists()).toBe(true)
    expect(wrapper.find('p').text()).toBe('analysis objective')
  })
  test('Created and Updated fields are formatted correctly.', async () => {
    const createdDate = '2021-03-29'
    const updatedDate = '2021-04-25'

    const wrapper = mountComponent(ToolpaneInfo, {
      propsData: {
        analysis: {
          created: createdDate,
          updated: updatedDate,
        },
      },
    })
    await wrapper.vm.$nextTick()

    // Use the formatData() method from the component to produce the expected output
    const expectedCreatedDate = wrapper.vm.formatDate(createdDate)
    const expectedUpdatedDate = wrapper.vm.formatDate(updatedDate)

    // Get the created date information (row1)
    const row1 = wrapper.findAll('tbody tr').at(0)
    expect(row1.findAll('td .cell').at(0).text()).toBe('Created')
    expect(row1.findAll('td .cell').at(1).text()).toBe(expectedCreatedDate)

    // Get the updated date information (row2)
    const row2 = wrapper.findAll('tbody tr').at(1)
    expect(row2.findAll('td .cell').at(0).text()).toBe('Last Updated')
    expect(row2.findAll('td .cell').at(1).text()).toBe(expectedUpdatedDate)
  })
  test('formatDate method actually formats dates as expected', () => {
    const wrapper = mountComponent(ToolpaneInfo, { propsData: { analysis: {} } })

    // format here is YYYY-MM-DD (ISO standard date)
    const date1 = '2021-03-10'
    const expected1 = '10/03/2021, 00:00:00'
    expect(wrapper.vm.formatDate(date1)).toBe(expected1)

    const date2 = '2021-03-10T16:02:00'
    const expected2 = '10/03/2021, 16:02:00'
    expect(wrapper.vm.formatDate(date2)).toBe(expected2)

    const date3 = 'blahahah'
    const expected3 = '-'
    expect(wrapper.vm.formatDate(date3)).toBe(expected3)
  })
  test('Created and Updated fields are handled, if not present.', async () => {
    const wrapper = mountComponent(ToolpaneInfo, {
      propsData: {
        analysis: { randomProp: 'misc' },
      },
    })
    await wrapper.vm.$nextTick()

    // Undefined date fields should yield '-'
    const row1 = wrapper.findAll('tbody tr').at(0)
    expect(row1.findAll('td .cell').at(0).text()).toBe('Created')
    expect(row1.findAll('td .cell').at(1).text()).toBe('-')
    const row2 = wrapper.findAll('tbody tr').at(1)
    expect(row2.findAll('td .cell').at(0).text()).toBe('Last Updated')
    expect(row2.findAll('td .cell').at(1).text()).toBe('-')
  })
  test('Additional display data is rendered correctly', async () => {
    const wrapper = mountComponent(ToolpaneInfo, {
      propsData: {
        analysis: { created: '2021-02-02T00:00:00', updated: '2021-04-25' },
        displayInfo: [
          { field: 'HitPoints', value: '20' },
          { field: 'Power', value: 'Flight' },
          { field: 'Weakness', value: 'Cryptonite' },
        ],
      },
    })
    await wrapper.vm.$nextTick()

    // The rows should render in the correct order, so we can iterate through the table to check
    const cells = wrapper.findAll('td .cell')
    expect(cells.length).toBe(10)
    // Cells 0-3 are the Created/Updated rows cells

    expect(cells.at(4).text()).toBe('HitPoints')
    expect(cells.at(5).text()).toBe('20')
    expect(cells.at(6).text()).toBe('Power')
    expect(cells.at(7).text()).toBe('Flight')
    expect(cells.at(8).text()).toBe('Weakness')
    expect(cells.at(9).text()).toBe('Cryptonite')
  })
})
