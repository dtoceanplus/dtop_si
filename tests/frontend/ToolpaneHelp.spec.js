// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import ToolpaneHelp from '@/components/ToolpaneHelp.vue'
import { mountComponent } from './support/index.js'

describe('ToolpaneHelp unit tests', () => {
  test('Help panel renders', () => {
    const wrapper = mountComponent(ToolpaneHelp, {
      propsData: {
        currentPage: 'DummyPage',
      },
    })
    expect(wrapper.find('h3').text()).toBe('Help')
  })
  test.skip('Correct help content is display for current page', () => {
    // todo: The Help panel needs to be updated first, before we can implement this test.
  })
})
