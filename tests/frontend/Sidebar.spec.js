// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Sidebar from '@/components/Sidebar.vue'
import { mountComponent } from './support/index.js'

describe('Sidebar unit tests', () => {
  let wrapper
  let menu

  beforeEach(() => {
    wrapper = mountComponent(Sidebar, {
      mocks: {
        $router: {
          push: jest.fn(),
        },
        $route: {
          path: '',
        },
      },
    })

    // Get the main menu element
    menu = wrapper.find('.sidebar__menu')
  })

  test('Sidebar renders with single item, by default.', () => {
    expect(menu.findAll('li').length).toBe(1)
  })
  test('Sidebar renders with correct default data.', () => {
    expect(wrapper.vm.$data.isCollapsed).toBe(true)
    expect(wrapper.vm.$data.collapseIcon).toBe('right')
  })
  test('Sidebar is collapsed, by default.', () => {
    expect(menu.element).toHaveClass('el-menu--collapse')
  })
  test('Sidebar toggle button displays correct logo, by default.', () => {
    expect(menu.find('li i').element).toHaveClass('el-icon-arrow-right')
  })
  test('Sidebar collapsed state can be toggled', async () => {
    let toggleBtn = menu.findComponent({ name: 'ElMenuItem' })
    expect(menu.element).toHaveClass('el-menu--collapse')
    await toggleBtn.trigger('click')
    // Menu should expand and icon should update
    expect(menu.element).not.toHaveClass('el-menu--collapse')
    expect(menu.find('i').element).toHaveClass('el-icon-arrow-left')

    // Refresh our reference to the toggle button
    toggleBtn = menu.findComponent({ name: 'ElMenuItem' })

    // Close the menu again
    await toggleBtn.trigger('click')
    expect(menu.element).toHaveClass('el-menu--collapse')
    expect(menu.find('i').element).toHaveClass('el-icon-arrow-right')
  })
  test('Pass a single item, via slot.', () => {
    wrapper = mountComponent(Sidebar, {
      slots: {
        default: '<el-menu-item>ItemA</el-menu-item>',
      },
      mocks: {
        $route: {
          path: '',
        },
      },
    })

    // Get the main menu element
    menu = wrapper.find('.sidebar__menu')

    // We should render an additional item
    const menuItems = menu.findAllComponents({ name: 'ElMenuItem' })
    expect(menuItems.length).toBe(2)
    expect(menuItems.at(1).text()).toBe('ItemA')
  })
  test('Pass multiple items, via the slot.', () => {
    wrapper = mountComponent(Sidebar, {
      slots: {
        default: ['<el-menu-item>ItemA</el-menu-item>',
          '<el-menu-item>ItemB</el-menu-item>',
          '<el-menu-item>ItemC</el-menu-item>',
        ],
      },
      mocks: {
        $route: {
          path: '',
        },
      },
    })

    // Get the main menu element
    menu = wrapper.find('.sidebar__menu')

    // We should render THREE additional items
    const menuItems = menu.findAllComponents({ name: 'ElMenuItem' })
    expect(menuItems.length).toBe(4)
    expect(menuItems.at(1).text()).toBe('ItemA')
    expect(menuItems.at(2).text()).toBe('ItemB')
    expect(menuItems.at(3).text()).toBe('ItemC')
  })
  test('Sidebar items route on click.', async () => {
    const $router = { push: jest.fn() }

    wrapper = mountComponent(Sidebar, {
      slots: {
        default: '<el-menu-item index="/some-url">ItemA</el-menu-item>',
      },
      mocks: {
        $router,
        $route: {
          path: '',
        },
      },
    })
    // Get the main menu element
    menu = wrapper.find('.sidebar__menu')

    const itemA = menu.findAllComponents({ name: 'ElMenuItem' }).filter(item => item.text() === 'ItemA')
    await itemA.trigger('click')

    expect(wrapper.vm.$router.push).toHaveBeenCalledTimes(1)
    // For the first call, check the first argument should match the expected URL
    expect(wrapper.vm.$router.push.mock.calls[0][0]).toBe('/some-url')
  })
})
