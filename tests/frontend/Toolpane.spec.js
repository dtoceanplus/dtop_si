// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Toolpane from '@/components/Toolpane.vue'
import { mountComponent } from './support/index.js'

// stub out the Toolpane* child components
const toStub = ['ToolpaneInfo', 'ToolpaneNotes', 'ToolDataSources', 'ToolpaneHelp', 'ToolpaneIssues']

// minimal analysis object, with only the properties required by the Toolpane component
const dummyAnalysis = {
  issues: [],
  _type: 'qfd',
  _queueLength: 0,
  notes: [],
  addOrUpdateNotes: jest.fn(),
}

// Map the menu item buttons, to their respective toolpanels
const menuItemMap = [
  { btn: '#toolpane-menu-info', toolpane: 'ToolpaneInfo' },
  { btn: '#toolpane-menu-issues', toolpane: 'ToolpaneIssues' },
  { btn: '#toolpane-menu-notes', toolpane: 'ToolpaneNotes' },
  { btn: '#toolpane-menu-help', toolpane: 'ToolpaneHelp' }]

describe('Toolpane unit tests', () => {
  test('Toolpane renders correctly', () => {
    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    // Check the relevant menu items and child components have been rendered
    expect(wrapper.get('.toolpane__menu').exists()).toBe(true)
    expect(wrapper.get('.toolpane__content').exists()).toBe(true)
    expect(wrapper.get('#toolpane-menu-info').exists()).toBe(true)
    expect(wrapper.get('#toolpane-menu-issues').exists()).toBe(true)
    expect(wrapper.get('#toolpane-menu-notes').exists()).toBe(true)
    expect(wrapper.get('#toolpane-menu-help').exists()).toBe(true)
    expect(wrapper.getComponent({ name: 'ToolpaneInfo' }).exists()).toBe(true)
    expect(wrapper.getComponent({ name: 'ToolpaneIssues' }).exists()).toBe(true)
    expect(wrapper.getComponent({ name: 'ToolpaneNotes' }).exists()).toBe(true)
    expect(wrapper.getComponent({ name: 'ToolpaneHelp' }).exists()).toBe(true)
  })
  test('Toolpane defaults to open', () => {
    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    // Get the content panel
    const toolpanePanel = wrapper.get('.toolpane__content')
    expect(toolpanePanel.isVisible()).toBe(true)
  })

  test('Toolpane default page is "Info"', () => {
    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    // Info panel is visible by default
    const infoPanel = wrapper.getComponent({ name: 'ToolpaneInfo' })
    expect(infoPanel.isVisible()).toBe(true)

    // Assert the others are NOT visible
    expect(wrapper.getComponent({ name: 'ToolpaneIssues' }).isVisible()).toBe(false)
    expect(wrapper.getComponent({ name: 'ToolpaneNotes' }).isVisible()).toBe(false)
    expect(wrapper.getComponent({ name: 'ToolpaneHelp' }).isVisible()).toBe(false)
  })
  test('Menu item switches to the correct toolpanel', async () => {
    const wrapper = mountComponent(Toolpane, {
      data () {
        return { activeMenu: 'dummy' }
      },
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    // We have overridden the default starting page, so currently NO pages should be visible
    for (const menuItem of menuItemMap) {
      // Click the menuItem
      await wrapper.get(menuItem.btn).trigger('click')
      expect(wrapper.getComponent({ name: menuItem.toolpane }).isVisible()).toBe(true)
    }
  })
  test('Menu item closes the toolpanel, if already on that page.', async () => {
    const wrapper = mountComponent(Toolpane, {
      data () {
        return { activeMenu: 'dummy' }
      },
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })
    expect(wrapper.get('.toolpane__content').isVisible()).toBe(true)

    // As above, but this time we click twice to check the panel is hidden
    for (const menuItem of menuItemMap) {
      // Click the menuItem
      await wrapper.get(menuItem.btn).trigger('click')
      expect(wrapper.getComponent({ name: menuItem.toolpane }).isVisible()).toBe(true)
      await wrapper.get(menuItem.btn).trigger('click')
      // The toolpanel should have minimised
      expect(wrapper.get('.toolpane__content').element).not.toBeVisible()
    }
  })
  test('Info icon shows saved status when there are no items on the queue.', () => {
    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    const infoIcon = wrapper.get('#toolpane-menu-info i')
    expect(infoIcon.classes()).not.toContain('unsaved')
  })
  test('Info icon shows unsaved status when there are items on the queue.', () => {
    const unsavedAnalysis = { ...dummyAnalysis, _queueLength: 3 }

    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: unsavedAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    const infoIcon = wrapper.get('#toolpane-menu-info i')
    expect(infoIcon.classes()).toContain('unsaved')
  })
  test('Info icon updates saved status when number of items on the queue changes.', async () => {
    const unsavedAnalysis = { ...dummyAnalysis, _queueLength: 3 }

    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    const infoIcon = wrapper.get('#toolpane-menu-info i')
    expect(infoIcon.classes()).not.toContain('unsaved')
    await wrapper.setProps({ analysis: unsavedAnalysis })
    expect(infoIcon.classes()).toContain('unsaved')
  })
  test('Issues icon hides issues badge when no issues are present.', () => {
    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    const issuesItem = wrapper.get('#toolpane-menu-issues')
    expect(issuesItem.classes()).not.toContain('issues')
    const issuesBadge = issuesItem.get('.el-badge')
    expect(issuesBadge.element).not.toBeVisible()
  })
  test('Issues icon shows issues badge when issues are present', () => {
    const analysisIssues = { ...dummyAnalysis, issues: ['an issue', 'another issue', 'a third issue'] }

    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: analysisIssues, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    const issuesItem = wrapper.get('#toolpane-menu-issues')
    expect(issuesItem.classes()).toContain('issues')
    const issuesBadge = issuesItem.get('.el-badge')
    expect(issuesBadge.isVisible()).toBe(true)
    // Check the <el-badge> component, that it's value has been set correctly
    expect(issuesBadge.vm.$props.value).toBe(analysisIssues.issues.length)
  })
  test('Issues icon updates when number of issues changes.', async () => {
    const analysisIssues = { ...dummyAnalysis, issues: ['an issue', 'another issue', 'a third issue'] }
    const analysisIssues2 = { ...dummyAnalysis, issues: ['an issue', 'another issue'] }

    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: analysisIssues, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    // There are three issues at first
    const issuesItem = wrapper.get('#toolpane-menu-issues')
    expect(issuesItem.classes()).toContain('issues')
    const issuesBadge = issuesItem.get('.el-badge')
    expect(issuesBadge.isVisible()).toBe(true)
    expect(issuesBadge.vm.$props.value).toBe(analysisIssues.issues.length)

    // We remove one issue.
    await wrapper.setProps({ analysis: analysisIssues2 })
    expect(issuesItem.classes()).toContain('issues')
    expect(issuesBadge.isVisible()).toBe(true)
    expect(issuesBadge.vm.$props.value).toBe(analysisIssues2.issues.length)

    // We remove both remaining issues
    await wrapper.setProps({ analysis: dummyAnalysis })
    expect(issuesItem.classes()).not.toContain('issues')
    expect(issuesBadge.element).not.toBeVisible()
    expect(issuesBadge.vm.$props.value).toBe(dummyAnalysis.issues.length)
  })
  test('When notes are updated correct method on analysis object is called', () => {
    const wrapper = mountComponent(Toolpane, {
      propsData: { analysis: dummyAnalysis, selection: {} },
      mocks: { $route: { name: 'dummyRoute' }, $state: { showToolPane: false } },
      stubs: toStub,
    })

    // Get the toolpane notes component
    const toolpaneNotes = wrapper.getComponent({ name: 'ToolpaneNotes' })
    // Fake an emit event from the component
    const fakeEvent = { tag: 'notetag', content: 'This is the note' }
    toolpaneNotes.vm.$emit('update', fakeEvent)

    // Check that the method is called to update the notes at the backend
    expect(dummyAnalysis.addOrUpdateNotes).toHaveBeenCalledWith(fakeEvent)
  })
})
