// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import TableColumnSelect from '@/components/TableColumnSelect.vue'
import { initTableWrapper } from './support/index.js'

describe('Unit tests for TableColumnSelect component', () => {
  test('Basic render', async () => {
    const colProps = {
      modelAttr: 'value',
      options: [
        { value: 1, label: 'A' },
        { value: 2, label: 'B' },
        { value: 3, label: 'C' },
      ],
      idGenerator: () => { return 'some-custom-id' },
    }

    const data = [
      { value: '', id: 1 }, { value: '', id: 2 }, { value: '', id: 3 },
    ]

    const wrapper = initTableWrapper(data, TableColumnSelect, colProps)
    await wrapper.vm.$nextTick()

    // We would expect three rows, each with a dropdown with the appropriate
    // options available for selection
    const rows = wrapper.findAll('tbody tr')
    expect(rows.length).toBe(3)
    rows.wrappers.forEach((row) => {
      // the idGenerator function passed via a prop
      expect(row.find('input').attributes('id')).toBe('some-custom-id')
      const dropdown = row.find('.el-select-dropdown')
      // dropdown should be hidden
      expect(dropdown.isVisible()).toBe(false)
      const options = dropdown.findAll('li').wrappers.map((op) => {
        return op.text()
      })
      // The rendered options should match the options labels passed as props
      expect(options).toEqual(colProps.options.map(row => row.label))
    })
  })
  test('Changing the selection emits an event', async () => {
    const colProps = {
      modelAttr: 'value',
      options: [
        { value: 20, label: 'Option1' },
        { value: 50, label: 'Option2' },
        { value: 100, label: 'Option3' },
      ],
    }

    const data = [
      { value: '', id: 1 },
    ]

    const wrapper = initTableWrapper(data, TableColumnSelect, colProps)
    await wrapper.vm.$nextTick()

    // Mock the method which emits the event
    const tcs = wrapper.getComponent(TableColumnSelect)
    tcs.vm.$emit = jest.fn()

    // Find the dropdown and click it
    await wrapper.find('input').trigger('click')
    // Find 'Option1' and click it
    const op1 = wrapper.findAll('li').filter(item => item.text() === 'Option1')
    await op1.trigger('click')

    // expect $emit to be called with
    // 'change' event, the row object (with value updated), modelAttr and newValue
    const newVal = 20
    const emitPayload = ['change', { ...data[0], value: newVal }, 'value', newVal]

    expect(tcs.vm.$emit).toBeCalledWith(emitPayload[0], emitPayload[1], emitPayload[2], emitPayload[3])
  })
})
