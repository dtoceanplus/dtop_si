// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import NumericInput from '@/components/NumericInput.vue'
import { mountComponent } from './support/index.js'

describe('Unit tests for the NumericInput component', () => {
  // Function for mounting the component with different props for each test
  function mount (props) {
    return mountComponent(NumericInput, {
      propsData: props,
    })
  }
  function btnInc (wrapper) {
    return wrapper.get('#plus-button')
  }
  function btnDec (wrapper) {
    return wrapper.get('#minus-button')
  }
  function getInput (wrapper) {
    return wrapper.find('input')
  }

  describe('Component basic rendering', () => {
    test('Component renders as expected, with default props', () => {
      const wrapper = mount()

      // Check the input element
      const input = getInput(wrapper)
      expect(input.attributes('type')).toBe('text')
      expect(input.attributes('id')).toBe('')
      expect(input.element.value).toBe('')

      // Check the 'control' elements - the increment/decrement buttons
      const buttons = wrapper.findAllComponents({ name: 'ElButton' })
      expect(buttons.length).toBe(2)
      expect(btnDec(wrapper).find('i').classes()).toContain('el-icon-minus')
      expect(btnInc(wrapper).find('i').classes()).toContain('el-icon-plus')
    })
    test('Component renders without control buttons', () => {
      const wrapper = mount({ controls: false })

      // Check the input element is present, as in previous test
      const input = getInput(wrapper)
      expect(input.attributes('type')).toBe('text')
      expect(input.attributes('id')).toBe('')
      expect(input.element.value).toBe('')

      // There should be no control buttons rendered
      expect(wrapper.findAllComponents({ name: 'ElButton' }).exists()).toBe(false)
    })
    test('Component applies ID passed via prop', () => {
      const id = 'testing-input-id'
      const wrapper = mount({ id: id })

      const input = wrapper.get('input')
      expect(input.attributes('id')).toBe(id)
    })
    test('Component sets default value from prop, as number', () => {
      const val = 123
      const expected = String(val)
      const wrapper = mount({ value: val })

      // returned as a string
      expect(getInput(wrapper).element.value).toBe(expected)
    })
    test('Component sets default value from prop, as string', () => {
      const val = '123'
      const wrapper = mount({ value: val })

      // returned as a string
      expect(getInput(wrapper).element.value).toBe(val)
    })
  })
  describe('Test control buttons', () => {
    // Note: Clicking the button will $emit the updated value, but does not actually change
    // it on the component. That is handled with the v-model directive. To simulate this
    // effect, and to allow us to change the value multiple times in the same test, we
    // can manually update the value using setProps

    test('Increment button increments value correctly', async () => {
      const wrapper = mount({ value: -2 })
      const btn = btnInc(wrapper)

      // Increment 4 times
      for (let click = 0; click < 4; click++) {
        await btn.trigger('click')
        await wrapper.setProps({ value: wrapper.vm.$data.valueBuffer += 1 })
      }

      expect(wrapper.emitted().input.length).toBe(4)
      expect(wrapper.emitted().input[0]).toEqual([-1])
      expect(wrapper.emitted().input[1]).toEqual([0])
      expect(wrapper.emitted().input[2]).toEqual([1])
      expect(wrapper.emitted().input[3]).toEqual([2])
    })
    test('Decrement button decrements value correctly', async () => {
      const wrapper = mount({ value: 2 })
      const btn = btnDec(wrapper)

      // Decrement 4 times
      for (let click = 0; click < 4; click++) {
        await btn.trigger('click')
        await wrapper.setProps({ value: wrapper.vm.$data.valueBuffer -= 1 })
      }

      expect(wrapper.emitted().input.length).toBe(4)
      expect(wrapper.emitted().input[0]).toEqual([1])
      expect(wrapper.emitted().input[1]).toEqual([0])
      expect(wrapper.emitted().input[2]).toEqual([-1])
      expect(wrapper.emitted().input[3]).toEqual([-2])
    })
    test('Increment button can increment from NULL', async () => {
      const wrapper = mount({ value: '' })
      const btn = btnInc(wrapper)
      await btn.trigger('click')

      expect(wrapper.emitted().input[0]).toEqual([1])
    })
    test('Decrement button can decrement from NULL', async () => {
      const wrapper = mount({ value: '' })
      const btn = btnDec(wrapper)
      await btn.trigger('click')

      expect(wrapper.emitted().input[0]).toEqual([-1])
    })
    test('Increment button uses the step value set by props', async () => {
      const props = { value: 5, step: 10 }
      const wrapper = mount(props)
      const btn = btnInc(wrapper)
      await btn.trigger('click')

      expect(wrapper.emitted().input[0]).toEqual([props.value + props.step])
    })
    test('Decrement button uses the step value set by props', async () => {
      const props = { value: 5, step: 20 }
      const wrapper = mount(props)
      const btn = btnDec(wrapper)
      await btn.trigger('click')

      expect(wrapper.emitted().input[0]).toEqual([props.value - props.step])
    })
  })
  describe('Test input valididation', () => {
    async function enter (wrapper, val) {
      const input = wrapper.find('input')
      await input.setValue(val)
    }

    test('Can enter integers', () => {
      const wrapper = mount()
      enter(wrapper, 1)
      expect(wrapper.emitted().input[0]).toEqual([1])

      enter(wrapper, -10)
      expect(wrapper.emitted().input[1]).toEqual([-10])

      enter(wrapper, 637)
      expect(wrapper.emitted().input[2]).toEqual([637])

      enter(wrapper, 0)
      expect(wrapper.emitted().input[3]).toEqual([0])
    })
    test('Can enter floats ', () => {
      const wrapper = mount()
      enter(wrapper, 2.67)
      expect(wrapper.emitted().input[0]).toEqual([2.67])

      enter(wrapper, -42.85)
      expect(wrapper.emitted().input[1]).toEqual([-42.85])

      enter(wrapper, 1465.212)
      expect(wrapper.emitted().input[2]).toEqual([1465.212])
    })
    test('Deleting the value will emit 0', () => {
      const wrapper = mount({ value: 5 })
      enter(wrapper, '')
      expect(wrapper.emitted().input[0]).toEqual([0])
    })
    test('Cannot enter a non-numeric value', () => {
      const wrapper = mount()
      enter(wrapper, 'one-two-three')
      expect(wrapper.emitted('input')).toBeFalsy()

      enter(wrapper, '1---')
      expect(wrapper.emitted('input')).toBeFalsy()

      enter(wrapper, '100%!')
      expect(wrapper.emitted('input')).toBeFalsy()

      enter(wrapper, '12^3')
      expect(wrapper.emitted('input')).toBeFalsy()
    })
    test('Cannot enter <0 when nonNegative prop is true', () => {
      const wrapper = mount({ nonNegative: true })
      enter(wrapper, -3)
      expect(wrapper.emitted('input')).toBeFalsy()

      // zero is OK here.
      enter(wrapper, 0)
      expect(wrapper.emitted('input')).toBeTruthy()
    })
    test('Cannot enter <= 0 when strictPositive prop is true', () => {
      const wrapper = mount({ strictPositive: true })
      enter(wrapper, -3)
      expect(wrapper.emitted('input')).toBeFalsy()

      // strict positive - so zero is invalid.
      enter(wrapper, 0)
      expect(wrapper.emitted('input')).toBeFalsy()
    })
    test('Cannot enter a float when integer prop is true', () => {
      const wrapper = mount({ integer: true })
      enter(wrapper, 123.456)
      expect(wrapper.emitted('input')).toBeFalsy()
    })
    test('Invalid entry leaves value unchanged and highlights input element.', () => {
      const wrapper = mount({ value: 5 })
      enter(wrapper, 'invalid')

      // The input should be highlighted as invalid, the value remains unchanged.
      expect(wrapper.vm.$data.invalid).toBe(true)
      expect(wrapper.vm.$data.valueBuffer).toBe(5)
    })
  })
})
