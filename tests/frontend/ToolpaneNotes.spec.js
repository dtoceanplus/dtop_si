// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import ToolpaneNotes from '@/components/ToolpaneNotes.vue'
import Vue from 'vue'
import { mountComponent } from './support/index.js'
import { tagPageMap } from '@/entity_mapping.js'

describe('ToolpaneNotes unit tests', () => {
  test('Notes panel renders', () => {
    const analysisType = 'qfd'
    const defaultRoute = Object.keys(tagPageMap[analysisType])[0]

    const wrapper = mountComponent(ToolpaneNotes, {
      propsData: {
        notes: [],
        analysisType: analysisType,
      },
      mocks: {
        $route: {
          name: defaultRoute,
        },
      },
    })
    expect(wrapper.find('h3').text()).toBe('Notes')
    expect(wrapper.find('textarea').exists()).toBe(true)
  })
  test('Displays notes for the current route.', () => {
    const analysisType = 'qfd'
    const currentRoute = Object.keys(tagPageMap[analysisType])[2]

    const notes = [{
      tag: tagPageMap[analysisType][currentRoute],
      content: 'This is the content of the note.',
    }]

    const wrapper = mountComponent(ToolpaneNotes, {
      propsData: {
        notes: notes,
        analysisType: analysisType,
      },
      mocks: {
        $route: {
          name: currentRoute,
        },
      },
    })
    const textarea = wrapper.get('textarea')
    expect(textarea.element.value).toBe(notes[0].content)
  })
  test('Displays no content, if no notes are available for the current route.', () => {
    const analysisType = 'fmea'
    // currentRoute and noteRoute are different
    const currentRoute = Object.keys(tagPageMap[analysisType])[4]
    const noteRoute = Object.keys(tagPageMap[analysisType])[2]

    const notes = [{
      tag: tagPageMap[analysisType][noteRoute],
      content: 'This content will not be seen, as we are not on the correct page.',
    }]

    const wrapper = mountComponent(ToolpaneNotes, {
      propsData: {
        notes: notes,
        analysisType: analysisType,
      },
      mocks: {
        $route: {
          name: currentRoute,
        },
      },
    })
    const textarea = wrapper.get('textarea')
    expect(textarea.element.value).toBe('')
  })
  test('Displays notes for the current route, when the route changes', async () => {
    const analysisType = 'qfd'
    const firstRoute = Object.keys(tagPageMap[analysisType])[1]
    const secondRoute = Object.keys(tagPageMap[analysisType])[3]

    const notes = [{
      tag: tagPageMap[analysisType][secondRoute],
      content: 'This content will be visible when we navigate to the correct page.',
    }]

    const wrapper = mountComponent(ToolpaneNotes, {
      propsData: {
        notes: notes,
        analysisType: analysisType,
      },
      mocks: {
        $route: {
          name: firstRoute,
        },
      },
    })
    const textarea = wrapper.get('textarea')
    expect(textarea.element.value).toBe('')
    // now lets move to the correct page
    wrapper.vm.$route.name = secondRoute
    await Vue.nextTick()
    expect(textarea.element.value).toBe(notes[0].content)
  })
  test('Emits event when existing note content changes.', () => {
    const analysisType = 'qfd'
    const noteRoute = Object.keys(tagPageMap[analysisType])[1]

    const notes = [{
      tag: tagPageMap[analysisType][noteRoute],
      content: 'Original note content',
    }]

    const wrapper = mountComponent(ToolpaneNotes, {
      propsData: {
        notes: notes,
        analysisType: analysisType,
      },
      mocks: {
        $route: {
          name: noteRoute,
        },
      },
    })
    const textarea = wrapper.get('textarea')
    expect(textarea.element.value).toBe(notes[0].content)
    textarea.setValue('Original note content, updated.')
    expect(wrapper.emitted('update')).toBeTruthy()

    const eventPayload = {
      content: 'Original note content, updated.',
      tag: tagPageMap[analysisType][noteRoute],
    }

    expect(wrapper.emitted('update')[0]).toEqual([eventPayload])
  })
  test('Emits event when a new note content is added.', () => {
    const analysisType = 'fmea'
    const existingNoteRoute = Object.keys(tagPageMap[analysisType])[1]
    const newNoteRoute = Object.keys(tagPageMap[analysisType])[3]

    const notes = [{
      tag: tagPageMap[analysisType][existingNoteRoute],
      content: 'An existing note.',
    }]

    const wrapper = mountComponent(ToolpaneNotes, {
      propsData: {
        notes: notes,
        analysisType: analysisType,
      },
      mocks: {
        $route: {
          name: newNoteRoute,
        },
      },
    })
    const textarea = wrapper.get('textarea')
    expect(textarea.element.value).toBe('')
    textarea.setValue('A new note is added!')
    expect(wrapper.emitted('update')).toBeTruthy()

    const eventPayload = {
      content: 'A new note is added!',
      tag: tagPageMap[analysisType][newNoteRoute],
    }

    expect(wrapper.emitted('update')[0]).toEqual([eventPayload])
  })
  test('Emits event when existing note content is deleted.', () => {
    const analysisType = 'fmea'
    const noteRoute = Object.keys(tagPageMap[analysisType])[1]

    const notes = [{
      tag: tagPageMap[analysisType][noteRoute],
      content: 'This note will be deleted :( ',
    }]

    const wrapper = mountComponent(ToolpaneNotes, {
      propsData: {
        notes: notes,
        analysisType: analysisType,
      },
      mocks: {
        $route: {
          name: noteRoute,
        },
      },
    })
    const textarea = wrapper.get('textarea')
    expect(textarea.element.value).toBe(notes[0].content)
    textarea.setValue('')
    expect(wrapper.emitted('update')).toBeTruthy()

    const eventPayload = {
      content: '',
      tag: tagPageMap[analysisType][noteRoute],
    }

    expect(wrapper.emitted('update')[0]).toEqual([eventPayload])
  })
})
