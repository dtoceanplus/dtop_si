// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

// Note: Using custom command selectOption() which is defined in support/commands.js

describe('Step through the FMEA process', { requestTimeout: 10000 }, () => {
  beforeEach(() => {
    // cy.intercept('GET', /fmea\/([0-9]+)\/issues$/).as('getIssues')
    cy.intercept('GET', 'fmea/*/issues').as('getIssues')
  })
  describe('Create FMEA', () => {
    it('Can create a new FMEA', () => {
      cy.visit('/fmea')
      cy.contains('New').click()
      cy.url().should('include', '/define')
    })
  })
  describe('FMEA Define page', () => {
    it('Checks the toolpane is closed by default.', () => {
      cy.get('.toolpane__content').should('not.have.class', 'visible')
    })
    it('Can add an analysis name, and assert missing parameter raises an issue', () => {
      cy.contains('tr', 'Name').find('input').type('AutoTestFMEA')

      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('have.class', 'issues')
    })
    it('Checks the correct name is displayed on the Info panel', () => {
      cy.get('#toolpane-info-panel h3').contains('AutoTestFMEA')
    })
    it('Checks the Issues panel, for missing define parameters.', () => {
      // Select the Issues option on the toolpane menu
      cy.get('#toolpane-menu-issues').click()

      cy.get('#toolpane-issues-panel').should('be.visible').within(() => {
        cy.get('tr').should('have.length', 2).and('contain', 'Empty field: action_level').and('contain', 'Empty field: occurrence_limit')
      })
    })
    it('Can add the required analysis properties, and ensure the issues are cleared.', () => {
      cy.contains('tr', 'Action Level').find('input').type(10)
      cy.contains('tr', 'Occurrence Limit').find('input').selectOption(7) // Repeated Failures (1 in 20)

      cy.wait('@getIssues')
      cy.get('#toolpane-menu-issues').should('not.have.class', 'issues')
    })
    it('Can close the toolpane', () => {
      cy.get('#toolpane-menu-issues').click()
      cy.get('#toolpane-issues-panel').should('not.be.visible')
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/requirements')
    })
  })
  describe('FMEA Design Requirements Page', () => {
    it('Check the requirements table is empty', () => {
      // only the default empty row is present
      cy.get('#fmea-design-requirements-table tbody tr').should('have.length', 1)
    })
    it('Can add multiple Design Requirements', () => {
      cy.get('#fmea-design-requirements-table tbody').within(() => {
        cy.get('tr').eq(0).find('input').type('Should be big')
        cy.get('tr').eq(1).find('input').type('Should be loud')
        cy.get('tr').eq(2).find('input').type('Should be yellow')
        cy.get('tr').eq(3).find('input').type('Should be fantastic!')

        cy.get('tr').should('have.length', 5)
      })
    })
    it('Can remove the description of a Design Requirement and check issue is raised', () => {
      cy.get('#fmea-design-requirements-table tbody').within(() => {
        cy.get('tr').eq(3).find('input').clear()
      })

      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('have.class', 'issues')

      // Open the toolpane
      cy.get('#toolpane-menu-issues').click()
      cy.get('#toolpane-issues-panel').should('be.visible').within(() => {
        cy.get('tr').should('have.length', 1)
        cy.get('.el-icon-warning').should('have.length', 1)
      })
    })
    it('Can remove the invalid Design Requirement and check the issue is removed', () => {
      cy.get('#fmea-design-requirements-table tbody').within(() => {
        cy.get('tr').eq(3).within(() => {
          cy.get('[id^=delete-]').first().invoke('show').click({ force: true })
        })
      })

      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('not.have.class', 'issues')

      // close the toolpane
      cy.get('#toolpane-menu-issues').click()
      cy.get('.toolpane__content').should('not.have.class', 'visible')
    })
    it('Checks the number of Design Requirements is correct', () => {
      cy.get('#fmea-design-requirements-table tbody tr').should('have.length', 4)
    })
    it('Can Navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/failure-modes')
    })
  })
  describe('FMEA Failure Modes Page', () => {
    function getTableBody () {
      return cy.get('#fmea-failure-modes-table .el-table__body-wrapper tbody')
    }

    it('Checks the Design Requirements have populated the table correctly', () => {
      // Each row contains one design requirement
      getTableBody().should('contain', 'Should be big')
        .should('contain', 'Should be loud')
        .should('contain', 'Should be yellow')
    })
    it('Can add some failure modes', () => {
      // We need to be careful here, as the table uses expanding rows...
      getTableBody().within(() => {
        cy.get('tr').eq(0).find('input').type('It is too small')
        cy.get('tr').eq(1).find('input').type('It is actually too large!')
        // tr(2) is the blank row
        cy.get('tr').eq(3).find('input').type('It is too quiet')
        // tr(4) is the blank row
        cy.get('tr').eq(5).find('input').type('It is not yellow')
        cy.get('tr').eq(6).find('input').type('It is actually blue')
        cy.get('tr').eq(7).find('input').type('It is invisible')
        // tr(8) is the blank row

        cy.get('tr').should('have.length', 9)
      })
    })
    it('Can remove a failure mode', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(6).within(() => {
          // delete row6
          cy.get('[id^=delete-]').first().invoke('show').click({ force: true })
        })

        cy.get('tr').should('have.length', 8)
      })
    })
    it('Can Navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/effects')
    })
  })
  describe('FMEA Effects Page', () => {
    function getTableBody () {
      return cy.get('#fmea-effects-table .el-table__body-wrapper tbody')
    }
    it('Checks the Effects table is populated correctly', () => {
      getTableBody().within(() => {
        // First two rows are Requirement 1 (split rows)
        cy.get('tr').eq(0).should('contain', 'Should be big').and('contain', 'It is too small')
        cy.get('tr').eq(1).should('contain', 'It is actually too large!')

        // Third row is Requirement 2 (only one Failure mode)
        cy.get('tr').eq(2).should('contain', 'Should be loud').and('contain', 'It is too quiet')

        // Fourth/fifth rows are Requirement 3 (split rows)
        cy.get('tr').eq(3).should('contain', 'Should be yellow').and('contain', 'It is not yellow')
        cy.get('tr').eq(4).should('contain', 'It is invisible')
      })
    })
    it('Can add a single effect and severity for each Failure Mode for Requirement 1', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(0).within(() => {
          cy.get('input').eq(0).type('Easily lost')
          cy.get('input').eq(1).selectOption(8)
        })
        cy.get('tr').eq(2).within(() => {
          cy.get('input').eq(0).type('Won\'t fit into my house')
          cy.get('input').eq(1).selectOption(7)
        })
      })
    })
    it('Can add multiple effects and severity for the Failure Mode for Requirement 2', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(4).within(() => {
          cy.get('input').eq(0).type('Hard to hear')
          cy.get('input').eq(1).selectOption(3)
        })
        cy.get('tr').eq(5).within(() => {
          cy.get('input').eq(0).type('Cannot locate in the dark')
          cy.get('input').eq(1).selectOption(2)
        })
      })
    })
    it('Can add a single effect for Requirement 3, and check an Error is recorded.', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(7).within(() => {
          cy.get('input').eq(0).type('Not very good camo')
        })
      })

      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('have.class', 'issues')

      cy.get('#toolpane-menu-issues').click()

      cy.get('#toolpane-issues-panel').should('be.visible').within(() => {
        cy.get('tr').should('contain', 'Empty field: severity')
        // It should be an ERROR, not a warning
        cy.get('.el-icon-error').should('have.length', 1)
      })
    })
    it('Checks the indicator on the Stepper bar is displayed', () => {
      // Get the relevant step on the stepper bar (4 = Effects)
      cy.get('.el-step__icon').contains('4').within(() => {
        cy.get('div.el-badge').should('be.visible')
      })
    })
    it('Can resolve the issue and ensure that all errors are cleared', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(7).within(() => {
          cy.get('input').eq(1).selectOption(2)
        })
      })

      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      // There are still warnings, relating to the unactionable items.

      cy.get('.el-step__icon').contains('4').within(() => {
        cy.get('div.el-badge').should('not.be.visible')
      })
    })
    it('Can add some notes to an FMEA page', () => {
      cy.get('#toolpane-menu-notes').click()
      cy.get('#toolpane-notes-panel').should('be.visible').within(() => {
        cy.get('textarea').should('have.value', '').type('Some notes for FMEA Effects page')
      })

      // Close the toolpane
      cy.get('#toolpane-menu-notes').click()
      cy.get('.toolpane__content').should('not.have.class', 'visible')
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/causes')
    })
  })
  describe('FMEA Causes Page', () => {
    function getTableBody () {
      return cy.get('#fmea-causes-table .el-table__body-wrapper tbody')
    }
    it('Can check that notes are correct for each page', () => {
      // Check that the notes for this page are empty, but when navigating back to the previous page they
      // are still present

      // Open the toolpane
      cy.get('#toolpane-menu-notes').click()
      cy.get('.toolpane__content').should('have.class', 'visible')

      // Check this page should be empty
      cy.get('#toolpane-notes-panel').should('be.visible').within(() => {
        cy.get('textarea').should('be.empty')
      })

      // Move back to the 'Effects' page
      cy.contains('Previous').click()
      cy.url().should('include', '/effects')

      // Check the notes are as added in the previous test
      cy.get('#toolpane-notes-panel').should('be.visible').within(() => {
        cy.get('textarea').should('have.value', 'Some notes for FMEA Effects page')
      })

      // Close the toolpane and go back to 'Causes' page
      cy.get('#toolpane-menu-notes').click()
      cy.contains('Next').click()
      cy.url().should('include', '/causes')
    })
    it('Can add a cause/occurrence limit for Requirement 1, Failure mode 1', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(0).within(() => {
          cy.get('input').eq(0).type('Wrong scale on the plans!')
          cy.get('input').eq(1).selectOption(1)
        })
      })
    })
    it('Can add (and remove) a cause/occurrence limit for Requirement 1, Failure Mode 2', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(2).within(() => {
          // Add
          cy.get('input').eq(0).type('Shrink ray!')
          cy.get('input').eq(1).selectOption(4)

          // Check we can remove
          cy.get('[id^=delete-]').invoke('show').click({ force: true })

          // Re-add the same Cause
          cy.get('input').eq(0).type('Shrink ray!')
          cy.get('input').eq(1).selectOption(4)
        })
      })
    })
    it('Add an occurence limit ONLY for Requirement 2, Failure Mode 1. Missing cause is a Warning.', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(4).within(() => {
          cy.get('input').eq(1).selectOption(5)
        })
      })
      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('have.class', 'issues')

      cy.get('#toolpane-menu-issues').click()

      cy.get('#toolpane-issues-panel').should('be.visible').within(() => {
        cy.get('tr').should('contain', 'Empty field: description')
        cy.get('tr').should('contain', 'Unactionable design requirement')
      })
    })
    it('Add a cause/occurrence limit for Requirement 3, Failure Mode 2', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(7).within(() => {
          cy.get('input').eq(0).type('Invisibility cloak')
          cy.get('input').eq(1).selectOption(8)
        })
      })
    })
    it('Can resolve issue and check the issue is cleared.', () => {
      cy.get('#toolpane-issues-panel tr').should('contain', 'Empty field: description')
      getTableBody().within(() => {
        cy.get('tr').eq(4).within(() => {
          cy.get('input').eq(0).type('Amplifier is broken')
        })
      })
      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-issues-panel tr').should('not.contain', 'Empty field: description')
    })
    it('Check that ommitting BOTH a cause and occurrence limit for a Requirement/Failure Mode is acceptable', () => {
      // We have left one of the rows totally blank - this should be OK.
      getTableBody().within(() => {
        cy.get('tr').eq(6).within(() => {
          cy.get('input').eq(0).should('be.empty')
          cy.get('input').eq(1).should('be.empty')
        })
      })
      // This should not raise an issue (no need to wait on the issues here because we haven't changed anything so no GET to /issues URL will
      // be made)
      cy.get('#toolpane-issues-panel tr').should('not.contain', 'Empty field: description')
      cy.get('#toolpane-issues-panel tr').should('not.contain', 'Empty field: occurrence')
      // Close toolpane
      cy.get('#toolpane-menu-issues').click()
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/design-controls')
    })
  })
  describe('FMEA Design Controls Page', () => {
    function getTableBody () {
      return cy.get('#fmea-design-controls-table .el-table__body-wrapper tbody')
    }
    it('Check the Design Controls table is populated correctly', () => {
      // We would expect one ROW per complete REQUIREMENT, FAILURE MODE and CAUSE combination.
      getTableBody().within(() => {
        // First row is Requirement 1 (Failure Mode 1 + Cause)
        cy.get('tr').eq(0).should('contain', 'Should be big').and('contain', 'It is too small').and('contain', 'Wrong scale on the plans')

        // Second row is Requirement 1 (but it's not on this row, as the row is split, Failure Mode 2 + Cause)
        cy.get('tr').eq(1).should('contain', 'It is actually too large!').and('contain', 'Shrink ray!')

        // Third row is Requirement 2 (Failure Mode and Cause)
        cy.get('tr').eq(2).should('contain', 'Should be loud').and('contain', 'It is too quiet').and('contain', 'Amplifier is broken')

        // Fourth row is Requirement 3 (because we DIDN'T populate a cause for one of the Failure modes it is not populated here)
        cy.get('tr').eq(3).should('contain', 'Should be yellow').and('contain', 'It is invisible').and('contain', 'Invisibility cloak')
      })
    })
    it('Can add a design control and Detection for Requirement 1, Failure Mode 1', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(0).within(() => {
          cy.get('input').eq(0).type('Check the plans')
          cy.get('input').eq(1).selectOption(3)
        })
      })
    })
    it('Can add design controls and Detection limits for all other Requirements', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(3).within(() => {
          cy.get('input').eq(0).type('Maintain the amp better')
          cy.get('input').eq(1).selectOption(4)
        })
        cy.get('tr').eq(5).within(() => {
          cy.get('input').eq(0).type('Throw the cloak away')
          cy.get('input').eq(1).selectOption(1)
        })
      })
    })
    it('Can remove a design control', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(5).within(() => {
          cy.get('[id^=delete-]').invoke('show').click({ force: true })
        })
      })
    })
    it('Check the correct amount of Design Controls are left', () => {
      getTableBody().within(() => {
        cy.get('tr').should('have.length', 6)
      })
    })
  })
  describe('FMEA Mitigiations Page', () => {
    beforeEach(() => {
      cy.intercept('GET', 'fmea/*/rpns').as('getRpns')
    })
    function getTableBody () {
      return cy.get('#fmea-mitigations-table .el-table__body-wrapper tbody')
    }
    it('Check the mitigations table has been populated correctly', () => {
      /*
      We expect to see a row in the mitigation table if the following conditions are met:
        - A Failure Mode, Effect, Cause and Design Control ALL defined.
        - Either the generated RPN (Risk Priority Number) is >= the Analysis action level
        OR the Cause occurence limit >= Analysis occurence limit

      The RPN = effect.severity * cause.occurence * design_control.detection

      The previous tests have only defined THREE fully defined items:

          FM                Eff(Sev)                        Cause(Occ)                          DC(Det)                        RPN
      1.  It is too small   Easily lost (8)                 Wrong scale on the plans (1)        Check the plans (3)            24
      2.  It is too quiet   Hard to hear (3)                Amplifier is broken (5)             Maintain the amp better(4)     60
      3.  It is too quiet   Cannot locate in the dark (2)   Amplifier is broken (5)             Maintain the amp better(4)     40

      The intial settings for Action Level (10) and Occurrence limit (7) mean all three rows should be displayed, because their RPNs are > 10

      */

      // Note: We are still on the /design_controls page at this point. We want to defer moving to /mitigations until now
      // so we can intercept the GET /rpns call and wait on it.
      cy.url().should('include', '/design-controls')
      cy.contains('Next').click()
      cy.url().should('include', '/mitigations')

      cy.wait('@getRpns').its('response.statusCode').should('eq', 200)

      cy.get('#fmea-mitigations-table .el-table__body-wrapper tbody tr').should('have.length', 3)
    })
    it('Can define mitigation parameters', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(0).within(() => {
          // this mitigation lowers the RPN to 5 (< threshold)
          cy.get('input').eq(0).type('mitigation A')
          cy.get('input').eq(1).selectOption(5)
          cy.get('input').eq(2).selectOption(1)
          cy.get('input').eq(3).selectOption(1)
        })
        cy.get('tr').eq(1).within(() => {
          // this mitigiation lowers the RPN to 8, which is below the threshold.
          // BUT the mitigated Occurrence is now ABOVE the threshold
          cy.get('input').eq(0).type('mitigation B')
          cy.get('input').eq(1).selectOption(1)
          cy.get('input').eq(2).selectOption(8)
          cy.get('input').eq(3).selectOption(1)
        })

        // the third row has no mitigation applied
      })
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/report')
    })
  })
  describe('FMEA Report Page', () => {
    function getTableBody () {
      return cy.get('#fmea-report-table .el-table__body-wrapper tbody')
    }
    it('FMEA Report shows correct rows with correct status', () => {
      getTableBody().within(() => {
        // Row1: Mitigation lowered the RPN to 5, which is < threshold,
        // mitigated occurrence is 1 < threshold also, so SUCCESS status
        cy.get('tr').eq(0).should('contain', 'mitigation A')
        cy.get('tr').eq(0).find('i').should('have.class', 'el-icon-success')

        // Row2: Mitigation lowered the RPN to 8, which is < threshold, but mitigated Occurrence is now
        // 8 > threshold, so WARNING status
        cy.get('tr').eq(1).should('contain', 'mitigation B')
        cy.get('tr').eq(1).find('i').should('have.class', 'el-icon-warning')

        // Row3: No mitigation applied. Status should still be ERROR
        cy.get('tr').eq(2).find('i').should('have.class', 'el-icon-error')
      })
    })
  })
  describe('Check the server status', () => {
    it('Can check that the sever is not hanging', () => {
      // Give the server a few seconds to catch up, if it needs it...
      cy.wait(3000)
      cy.get('#toolpane-menu-info').should('not.have.class', 'unsaved')
    })
  })
  describe('Delete FMEA', () => {
    it('Can delete the FMEA Analysis', () => {
      cy.visit('/fmea')
      cy.get('[id^=delete-]').first().invoke('show').click({ force: true })
      cy.contains('Yes').click({ force: true })
    })
  })
})
