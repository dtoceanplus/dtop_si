// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

// Note: Using custom command selectOption() which is defined in support/commands.js

describe('Step through the QFD process', { requestTimeout: 10000 }, () => {
  beforeEach(() => {
    cy.intercept('GET', 'qfd/*/issues').as('getIssues')
  })
  describe('Create QFD', () => {
    it('Can create a new QFD', () => {
      cy.visit('/qfd')
      cy.contains('New').click()
      cy.url().should('include', '/define')
    })
  })
  describe('QFD Define page', () => {
    it('Checks toolpane is closed by default.', () => {
      cy.get('.toolpane__content').should('not.have.class', 'visible')
    })
    it('Can add an analysis name, and assert issue is raised', () => {
      cy.contains('tr', 'Name').find('input').type('AutoTestQFD')
      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('have.class', 'issues')
    })
    it('Checks the correct name is displayed on the Info panel', () => {
      cy.get('#toolpane-info-panel h3').contains('AutoTestQFD')
    })
    it('Checks the Issues panel, for missing objective.', () => {
      // Select the Issues option on the toolpane menu
      cy.get('#toolpane-menu-issues').click()

      cy.get('#toolpane-issues-panel').should('be.visible').within(() => {
        cy.get('tr').should('have.length', 1).contains('Empty field: objective')
      })
    })
    it('Can add an analysis objective, and check issue is cleared.', () => {
      cy.contains('tr', 'Objective').find('input').type('QFD analysis for the purposes of automated testing')
      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('not.have.class', 'issues')
    })
    it('Can close the toolpane', () => {
      cy.get('#toolpane-menu-issues').click()
      cy.get('#toolpane-info-panel').should('not.be.visible')
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/requirements')
    })
  })
  describe('QFD Requirements Page', () => {
    it('Checks there are no requirements present for a new analysis', () => {
      cy.get('#qfd-requirements-table tbody tr').should('have.length', 1)
    })
    it('Can add a first customer requirement and importance', () => {
      cy.get('#qfd-requirements-table tbody').within(() => {
        cy.get('tr').eq(0).within(() => {
          cy.get('input').eq(0).type('Can float')
          cy.get('button').eq(1).click().click().click() // Importance of 3
        })
      })
    })
    it('Can add several more customer requirement and importance values', () => {
      cy.get('#qfd-requirements-table tbody').within(() => {
        cy.get('tr').eq(1).within(() => {
          cy.get('input').eq(0).type('Looks cool')
          cy.get('input').eq(1).type(25) // Importance of 25
        })
        cy.get('tr').eq(2).within(() => {
          cy.get('input').eq(0).type('Has lots of holes in it')
          cy.get('button').eq(1).click() // Importance of 1
        })
      })
    })
    it('Can add a final customer requirement, and check issues panel for missing Importance', () => {
      cy.get('#qfd-requirements-table tbody').within(() => {
        cy.get('tr').eq(3).within(() => {
          cy.get('input').eq(0).type('Generates some power')
        })
      })
      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('have.class', 'issues')
    })
    it('Can navigate to the previous page, and use the toolpane to navigate back to the current Issue', () => {
      cy.contains('Previous').click()
      cy.url().should('include', '/define')

      // Open up the toolpane
      cy.get('#toolpane-menu-issues').click()
      cy.get('#toolpane-issues-panel').should('be.visible').within(() => {
        // By default only show issues from the current page
        cy.get('tr').should('have.length', 0)

        // Click the toggle to 'show all'
        cy.get('[role=switch]').should('not.have.class', 'is-checked').click().should('have.class', 'is-checked')

        // Now it should display the issue from the Requirements page
        cy.get('tr').should('have.length', 1).contains('Empty field: importance').dblclick()
      })

      // Now we should be back to Customer Requirements page, with the corresponding importance box in focus
      cy.url().should('include', '/requirements')
      cy.focused().should('be.empty')
    })
    it('Can add a value for importance for the third customer requirement', () => {
      // Importance box should have the focus from the dbl-click
      cy.focused().type(10).should('have.value', '10')

      // Issue should be resolved
      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('not.have.class', 'issues')

      // Close the toolpane
      cy.get('#toolpane-menu-issues').click()
      cy.get('.toolpane__content').should('not.have.class', 'visible')
    })
    it('Can remove one customer requirement', () => {
      // Lets remove the third requirement we entered
      cy.get('#qfd-requirements-table tbody').within(() => {
        cy.get('tr').eq(2).within(() => {
          cy.get('[id^=delete-]').first().invoke('show').click({ force: true })
        })
      })
    })
    it('Checks there are three customer requirements present, plus the default empty row', () => {
      cy.get('#qfd-requirements-table tbody').within(() => {
        cy.get('tr').should('have.length', 4)
      })
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/functional_requirements')
    })
  })
  describe('QFD Functional Requirements Page', () => {
    it('Checks there are no Functional Requirements for this empty analysis.', () => {
      cy.get('#qfd-functional-requirements-table tbody tr').should('have.length', 1)
    })
    it('Can add three complete functional requirements.', () => {
      // Functional Requirement consists of
      //  Description (text input)
      //  Direction of Improvement (button)
      //  Target Value (numeric input)
      //  Units (text input)
      //  Engineering Difficulty (select)
      //  Delivery Difficulty (select)

      cy.get('#qfd-functional-requirements-table tbody').within(() => {
        // FR1
        cy.get('tr').eq(0).within(() => {
          cy.get('input').eq(0).type('should be big-ish')
          cy.get('button').eq(0).click() // DoI is upwards
          cy.get('button').eq(2).click().click().click().click() // Increment Target Value using the button
          cy.get('input').eq(2).type('m^2')
          cy.get('input').eq(3).selectOption(1) // Using a custom command to select option 1 ('Low')
          cy.get('input').eq(4).selectOption(2) // 'Low/Moderate'
        })

        // FR2
        cy.get('tr').eq(1).within(() => {
          cy.get('input').eq(0).type('should be shiny')
          cy.get('button').eq(0).click().click() // DoI is downwards
          cy.get('input').eq(1).type('100') // Change the target value by typing
          cy.get('input').eq(2).type('shineyness')
          cy.get('input').eq(3).selectOption(3) // 'Moderate'
          cy.get('input').eq(4).selectOption(4) // 'Moderate/high'
        })

        // FR3
        cy.get('tr').eq(2).within(() => {
          cy.get('input').eq(0).type('should be hot')
          cy.get('button').eq(0).click().click() // DoI is downwards
          cy.get('input').eq(1).type('15') // Change the target value by typing
          cy.get('input').eq(2).type('K')
          cy.get('input').eq(3).selectOption(5) // 'High'
          cy.get('input').eq(4).selectOption(1) // 'Low'
        })
      })

      // Check no issues present
      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('not.have.class', 'issues')
    })
    it('Can remove a parameter from an existing Functional Requirement', () => {
      cy.get('#qfd-functional-requirements-table tbody').within(() => {
        // FR2
        cy.get('tr').eq(1).within(() => {
          cy.get('input').eq(0).clear()
        })
      })
    })
    it('Can add an incomplete fourth Functional Requirement', () => {
      cy.get('#qfd-functional-requirements-table tbody').within(() => {
      // FR4
        cy.get('tr').eq(3).within(() => {
          cy.get('input').eq(0).type('should be fast')
          cy.get('button').eq(0).click() // DoI is upwards
          cy.get('button').eq(2).click().click().click().click() // Increment Target Value using the button

        // Units, Engineering Difficulty and Delivery difficulty are incomplete
        })
      })

      // Check the incomplete FR creates an issue
      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('have.class', 'issues')
    })
    it('Checks that the correct issues are displayed in the toolpane', () => {
      // Open up the toolpane
      cy.get('#toolpane-menu-issues').click()
      cy.get('#toolpane-issues-panel').should('be.visible').within(() => {
      // We expect to see 4 issues
        cy.get('tr').should('have.length', 4)

        // Missing description or units are WARNINGS (2). The missing Engineering/Delivery Difficulty
        // fields are ERRORS (2)
        cy.get('.el-icon-warning').should('have.length', 2)
        cy.get('.el-icon-error').should('have.length', 2)
      })
    })
    it('Can resolve all issues and check Issues toolpane is updated correctly', () => {
      cy.get('#qfd-functional-requirements-table tbody').within(() => {
      // FR1 - fix the description error
        cy.get('tr').eq(1).within(() => {
          cy.get('input').eq(0).type('should be shiny')
        })
        // FR4 - fix the units, Engineering Difficulty and Delivery difficulty errors
        cy.get('tr').eq(3).within(() => {
          cy.get('input').eq(2).type('km/s')
          cy.get('input').eq(3).selectOption(5) // 'High'
          cy.get('input').eq(4).selectOption(3) // 'Moderate'
        })
      })

      cy.wait('@getIssues').its('response.statusCode').should('eq', 200)
      cy.get('#toolpane-menu-issues').should('not.have.class', 'issues')
    })
    it('Can add some Functional Requirements notes', () => {
    // Select the notes toolpane item
      cy.get('#toolpane-menu-notes').click()
      cy.get('#toolpane-notes-panel').should('be.visible').within(() => {
        cy.get('textarea').should('have.value', '').type('Some notes for QFD Functional Requirements')
      })

      // Close notes
      cy.get('#toolpane-menu-notes').click()
    })
    it('Checks the correct number of Functional Requirements are displayed, plus the empty row', () => {
      cy.get('#qfd-functional-requirements-table tbody').within(() => {
        cy.get('tr').should('have.length', 5)
      })
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/impacts')
    })
  })
  describe('QFD Impacts Page', () => {
    function getTableBody () {
      // The impacts table uses a 'fixed' element. ElementUI appears to generate TWO tables, and then hide elements of each
      // to get the fixed behaviour. This means we need to specifically select the .el-table__body-wrapper
      return cy.get('#qfd-impacts-table .el-table__body-wrapper tbody')
    }
    function getTableHeader () {
      return cy.get('#qfd-impacts-table .el-table__header-wrapper thead')
    }

    function getTableFixedCol () {
      return cy.get('#qfd-impacts-table .el-table__fixed-body-wrapper tbody')
    }

    it('Checks that Requirements and Functional Requirements have populated the impacts table correctly', () => {
      getTableBody().within(() => {
        // Each requirement should be a ROW of the table
        cy.get('tr').should('have.length', 3)

        // Each functional requirement should be a COLUMN
        // Each table cell contains an input (select box) so we can check the total is as expected
        cy.get('input').should('have.length', 12) // 3 CRs and 4 FRs
      })

      getTableFixedCol().within(() => {
        cy.contains('Can float')
        cy.contains('Looks cool')
        cy.contains('Generates some power')
      })

      getTableHeader().within(() => {
        cy.contains('should be big-ish')
        cy.contains('should be shiny')
        cy.contains('should be hot')
        cy.contains('should be fast')
      })
    })
    it('Can assign Impact values to each Customer Requirement/Functional Requirement combination', () => {
      getTableBody().within(() => {
        // Each requirement should be a ROW of the table
        cy.get('tr').eq(0).within(() => {
          cy.get('input').eq(0).selectOption(4)
          cy.get('input').eq(1).selectOption(1)
          cy.get('input').eq(2).selectOption(1)
          cy.get('input').eq(3).selectOption(2)
        })
        cy.get('tr').eq(1).within(() => {
          cy.get('input').eq(0).selectOption(3)
          cy.get('input').eq(1).selectOption(4)
          cy.get('input').eq(2).selectOption(1)
          cy.get('input').eq(3).selectOption(4)
        })
        cy.get('tr').eq(2).within(() => {
          cy.get('input').eq(0).selectOption(4)
          cy.get('input').eq(1).selectOption(1)
          cy.get('input').eq(2).selectOption(4)
          cy.get('input').eq(3).selectOption(1)
        })
      })
    })
    it('Can add some notes for impacts', () => {
      cy.get('#toolpane-menu-notes').click()
      cy.get('#toolpane-notes-panel').should('be.visible').within(() => {
        cy.get('textarea').should('be.empty').type('Some notes for QFD Impacts')
      })
    })
    it('Checks notes persist between page transitions', () => {
      // A basic check to ensure that notes are only displayed for the relevant page
      cy.contains('Previous').click().contains('Previous').click()
      cy.url().should('include', '/requirements')

      // We didn't enter any notes here, so should be empty
      cy.get('#toolpane-notes-panel').should('be.visible').within(() => {
        cy.get('textarea').should('be.empty')
      })
    })
    it('Can use the stepper to navigate back to the correct page', () => {
      cy.get('.el-step__title').contains('Impacts').click()
      cy.url().should('include', '/impacts')

      cy.get('#toolpane-notes-panel').should('be.visible').within(() => {
        cy.get('textarea').should('have.value', 'Some notes for QFD Impacts')
      })

      // Close the toolpane
      cy.get('#toolpane-menu-notes').click()
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/correlations')
    })
  })
  describe('QFD Correlations Page', () => {
    function getTableBody () {
      return cy.get('#qfd-correlations-table .el-table__body-wrapper tbody')
    }
    function getTableHeader () {
      return cy.get('#qfd-correlations-table .el-table__header-wrapper thead')
    }

    function getTableFixedCol () {
      return cy.get('#qfd-correlations-table .el-table__fixed-body-wrapper tbody')
    }

    it('Checks the Functional Requirements have populated the Correlations table correclty', () => {
      getTableBody().within(() => {
        // Each functional requirement is a row of the table
        cy.get('tr').should('have.length', 4)

        // Each functional requirement should be a row and a column.
        // The total number of inputs should be numFR^2
        cy.get('input').should('have.length', 4 * 4)
      })

      getTableFixedCol().within(() => {
        cy.contains('should be big-ish')
        cy.contains('should be shiny')
        cy.contains('should be hot')
        cy.contains('should be fast')
      })

      getTableHeader().within(() => {
        cy.contains('should be big-ish')
        cy.contains('should be shiny')
        cy.contains('should be hot')
        cy.contains('should be fast')
      })
    })
    it('Checks that cross-correlations are disabled', () => {
      // Functional requirements cannot cross correlate with themselves.
      // For each row, n, the corresponding column n should be disabled
      const numFr = 4

      getTableBody().within(() => {
        for (let i = 0; i < numFr; i++) {
          cy.get('tr').eq(i).within(() => {
            cy.get('input').eq(i).should('be.disabled')
          })
        }
      })
    })
    it('Can assign Correlation values between each pair of Functional Requirements', () => {
      getTableBody().within(() => {
        // Each requirement should be a ROW of the table
        // The corresponding correlation should be automatically set and 'mirrored' by the table itself.
        // e.g setting (0,3) ALSO sets (3,0) to the SAME VALUE

        cy.get('tr').eq(0).within(() => {
          cy.get('input').eq(1).selectOption(6) // '+ Medium'
          cy.get('input').eq(2).selectOption(4) // 'None'
          cy.get('input').eq(3).selectOption(2) // '- Medium'
        })
        cy.get('tr').eq(1).within(() => {
          cy.get('input').eq(0).should('have.value', '+ Medium') // Mirrored from row0
          cy.get('input').eq(2).selectOption(5) // '+ Low'
          cy.get('input').eq(3).selectOption(1) // '- High'
        })
        cy.get('tr').eq(2).within(() => {
          cy.get('input').eq(0).should('have.value', 'None') // Mirrored from row0
          cy.get('input').eq(1).should('have.value', '+ Low') // Mirrored from row1
          cy.get('input').eq(3).selectOption(4) // 'None'
        })
        cy.get('tr').eq(3).within(() => {
          cy.get('input').eq(0).should('have.value', '- Medium') // Mirrored from row0
          cy.get('input').eq(1).should('have.value', '- High') // Mirrored from row1
          cy.get('input').eq(2).should('have.value', 'None') // Mirrored from row2
        })
      })
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/triz')
    })
  })
  describe('QFD TRIZ page', () => {
    function getTableBody () {
      return cy.get('#qfd-triz-table .el-table__body-wrapper tbody')
    }
    function selectTrizClass (el, option) {
      // Ensure we lose focus after each selection by using the {esc} key
      el.selectOption(option).type('{esc}', { force: true })
    }

    it('Checks the TRIZ table has been populated correctly', () => {
      // there should be one row, per Functional Requirement
      getTableBody().within(() => {
        // Each functional requirement is a row of the table
        cy.get('tr').should('have.length', 4)

        // The first row-text should be the Functional Requirements
        cy.contains('should be big-ish')
        cy.contains('should be shiny')
        cy.contains('should be hot')
        cy.contains('should be fast')
      })
    })
    it('Can select TRIZ classes for each Functional Requirement', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(0).within(() => {
          selectTrizClass(cy.get('input').eq(0), 15) // Volume of stationary object
          selectTrizClass(cy.get('input').eq(0), 16) // Shape
          selectTrizClass(cy.get('input').eq(0), 26) // Weight of stationary object
        })
        cy.get('tr').eq(1).within(() => {
          selectTrizClass(cy.get('input').eq(0), 30) // Temperature
          selectTrizClass(cy.get('input').eq(0), 31) // Illumination intensity
        })
        cy.get('tr').eq(2).within(() => {
          selectTrizClass(cy.get('input').eq(0), 30) // Temperature
          selectTrizClass(cy.get('input').eq(0), 32) // Power
          selectTrizClass(cy.get('input').eq(0), 29) // Stress or pressure
          selectTrizClass(cy.get('input').eq(0), 28) // Force (Intensity)
        })
        cy.get('tr').eq(3).within(() => {
          selectTrizClass(cy.get('input').eq(0), 27) // Speed
        })
      })
    })
    it('Can navigate to the next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/solutions')
    })
  })
  describe('QFD Solutions Page', () => {
    function getTableBody () {
      return cy.get('#qfd-solutions-table .el-table__body-wrapper tbody')
    }
    function getTableHeader () {
      return cy.get('#qfd-solutions-table .el-table__header-wrapper thead')
    }

    it('Checks the Solutions table has been populated correctly', () => {
      // The Functional requirements and their units should form the column headers as
      // 'functional_requirement_name [unit]'
      getTableHeader().within(() => {
        cy.contains('Description') // static text
        cy.contains('should be big-ish [m^2]')
        cy.contains('should be shiny [shineyness]')
        cy.contains('should be hot [K]')
        cy.contains('should be fast [km/s]')
      })

      getTableBody().within(() => {
        cy.get('tr').should('have.length', 1)
      })
    })
    it('Can add three complete solutions', () => {
      getTableBody().within(() => {
        cy.get('tr').eq(0).within(() => {
          cy.get('input').eq(0).type('Solution A')
          cy.get('input').eq(1).type(1)
          cy.get('input').eq(2).type(2)
          cy.get('input').eq(3).type(3)
          cy.get('input').eq(4).type(4)
        })
        cy.get('tr').eq(1).within(() => {
          cy.get('input').eq(0).type('Solution X (not a great one)')
          cy.get('input').eq(1).type(10)
          cy.get('input').eq(2).type(20)
          cy.get('input').eq(3).type(30)
          cy.get('input').eq(4).type(40)
        })
        cy.get('tr').eq(2).within(() => {
          cy.get('input').eq(0).type('Solution B')
          cy.get('input').eq(1).type(25)
          cy.get('input').eq(2).type(50)
          cy.get('input').eq(3).type(75)
          cy.get('input').eq(4).type(100)
        })
      })
    })
    it('Can remove a solution', () => {
      cy.get('tr').eq(1).within(() => {
        cy.get('[id^=delete-]').first().invoke('show').click({ force: true })
      })
    })
    it('Checks the correct number of solutions are present', () => {
      getTableBody().within(() => {
        cy.get('tr').should('have.length', 3)
      })
    })
    it('Checks the information in the toolpane Info panel', () => {
      // Open the toolpane info panel
      cy.get('#toolpane-menu-info').click()
      cy.get('.toolpane__content').should('have.class', 'visible')
      cy.get('#toolpane-info-panel').should('be.visible').within(() => {
        cy.contains('tr', 'Customer Requirements').find('td').eq(1).contains('3')
        cy.contains('tr', 'Functional Requirements').find('td').eq(1).contains('4')
        cy.contains('tr', 'Solutions').find('td').eq(1).contains('2')
      })

      // close the toolpane
      cy.get('#toolpane-menu-info').click()
    })
    it('Can navigate to next page', () => {
      cy.contains('Next').click()
      cy.url().should('include', '/report')
    })
  })
  describe.skip('QFD Report Page', () => {
    it('Placeholder testing TBD', () => {
      // Implementation of the Report page is still partially TBD
    })
  })
  describe('Check the server status', () => {
    it('Can check that the sever is not hanging', () => {
      // Give the server a few seconds to catch up, if it needs it...
      cy.wait(3000)
      cy.get('#toolpane-menu-info').should('not.have.class', 'unsaved')
    })
  })
  describe('Delete QFD', () => {
    it('Can delete the QFD Analysis', () => {
      cy.visit('/qfd')
      cy.get('[id^=delete-]').first().invoke('show').click({ force: true })
      cy.contains('Yes').click({ force: true })
    })
  })
})
