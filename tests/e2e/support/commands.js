// DTOcean+ Structured Innovation Module
// Copyright (C) 2021 Energy Systems Catapult
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

Cypress.Commands.add('mockWithFixture', ({ route, fixture, method = '*', modifier = {}, alias }) => {
  // Requests matching `route` will return the data from `fixture`, potentially modified by `modifier`.
  cy.fixture(fixture).then((fixture) => {
    fixture = { ...fixture, ...modifier }
    cy.intercept(method, route, fixture).as(alias || route + method)
  })
})

Cypress.Commands.add('mockWithStaticResponse', ({ route, response = {}, method = '*', alias }) => {
  // Requests matching `route` will return `response`.
  cy.intercept(method, route, (req) => {
    if (alias) {
      req.alias = alias
    }
    req.reply(response)
  }).as(alias)
})

Cypress.Commands.add('selectOption', { prevSubject: true }, (subject, optionNumber) => {
  // This command is to allow us to work with Element UI dropdowns, which have proved overly difficult.
  // Because they are not native <select> elements, the Cypress select() method cannot be used.
  // They use a readonly <input> element, and then a hidden <div> which is displayed by some JavaScript.
  // This causes Cypress to consider the <div> non-visible, and so does not let the user click it.
  // Using { force: true } can force the click, but then something weird happens when there are multiple
  // dropdowns on a page, the selected option is bound to to wrong input box.
  // This method does atleast let us get something going for the time being.

  // optionNumber is 1 based (i.e. the first option is 1 not 0)

  // `optionNumber` {downarrow}'s to pick the option we want, and then {enter} to confirm
  const clickPattern = '{downarrow}'.repeat(optionNumber) + '{enter}'

  // Using {force : true } is still required!
  cy.wrap(subject).type(clickPattern, { force: true })
})
