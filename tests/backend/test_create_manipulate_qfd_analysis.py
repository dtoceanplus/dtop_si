# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
from dtop_structinn.pyqfd.models import Qfd


@pytest.fixture
def analysis():
    """A simple analysis as a starting point for most tests."""
    name = "An Analysis"
    objective = "Test the feature"

    return Qfd.create(name=name, objective=objective)


def test_create_analysis():
    """Test that a QFD analysis can be created."""
    # Given
    name = "An Analysis"
    objective = "Test the feature"

    # When
    analysis = Qfd.create(name=name, objective=objective)

    # Then
    assert analysis.name == name
    assert analysis.objective == objective
    assert analysis.requirements == []
    assert analysis.functional_requirements == []
    assert analysis.solutions == []
    assert analysis.achievements == []


def test_update_analysis(analysis):
    """Test updating an analysis."""
    # Given
    new_name = "An awesome analysis"

    # When
    analysis.update(name=new_name)

    # Then
    assert analysis.name == new_name


def test_add_functional_requirement_to_analysis(analysis):
    """Test that a functional requirement can be added."""
    description = "My Funky Functional Requirement"
    functional_requirement = analysis.add_functional_requirement(
        description=description
    )
    assert functional_requirement.description == description
    assert functional_requirement in analysis.functional_requirements


def test_add_requirement_to_analysis(analysis):
    """Test that a requirement can be added."""
    description = "A Requirement"
    importance = 10
    requirement = analysis.add_requirement(
        description=description, importance=importance
    )
    assert requirement.description == description
    assert requirement.importance == 10
    assert requirement in analysis.requirements


def test_add_solution_to_analysis(analysis):
    """Test that a solution can be added."""
    description = "My awesome solution."
    solution = analysis.add_solution(description=description)
    assert solution in analysis.solutions


def test_add_achievement_to_analysis(analysis):
    """Test adding an achievement to the analysis."""
    # Given
    target = 100
    units = "MWh"
    functional_requirement_id = analysis.add_functional_requirement(
        target=target, units=units
    ).id
    solution_id = analysis.add_solution().id
    value = 60

    # When
    achievement = analysis.add_or_update_achievement(
        functional_requirement_id=functional_requirement_id,
        solution_id=solution_id,
        value=value,
    )

    # Then

    assert achievement.value == 60
    assert achievement in analysis.achievements


def test_add_achievement_with_bad_fr_or_solution_id(analysis):
    """Test adding an achievement with a bad func req or solution id."""
    functional_requirement_id = analysis.add_functional_requirement().id
    solution_id = analysis.add_solution().id

    with pytest.raises(Exception):  # todo: use better exception
        analysis.add_or_update_achievement(
            functional_requirement_id=999, solution_id=solution_id, value=3
        )

    with pytest.raises(Exception):  # todo: use better exception
        analysis.add_or_update_achievement(
            functional_requirement_id=functional_requirement_id,
            solution_id=53434,
            value=3,
        )


def test_update_achivement(analysis):
    """Test updating an achievement."""
    # Given
    target = 100
    units = "MWh"
    functional_requirement_id = analysis.add_functional_requirement(
        target=target, units=units
    ).id
    solution_id = analysis.add_solution().id
    value = 60

    achievement = analysis.add_or_update_achievement(
        functional_requirement_id=functional_requirement_id,
        solution_id=solution_id,
        value=value,
    )

    # When
    new_value = 90
    returned_achievement = analysis.add_or_update_achievement(
        functional_requirement_id=functional_requirement_id,
        solution_id=solution_id,
        value=new_value,
    )

    # Then
    assert returned_achievement == achievement  # same achievement returned
    assert len(analysis.achievements) == 1  # no extra added
    assert achievement.value == new_value


def test_add_impact_to_analysis(analysis):
    """Test adding impact to an analysis."""
    # Given
    requirement = analysis.add_requirement()
    functional_requirement = analysis.add_functional_requirement()
    value = 4

    # When
    impact = analysis.add_or_update_impact(
        requirement_id=requirement.id,
        functional_requirement_id=functional_requirement.id,
        impact=value,
    )

    # Then
    assert impact.impact == value
    assert impact in analysis.impacts


def test_add_impact_with_wrong_functional_requirement_id(analysis):
    requirement_id = analysis.add_requirement().id
    functional_requirement_id = 1545
    value = 4

    with pytest.raises(Exception):  # todo: use a better Exception
        analysis.add_or_update_impact(
            requirement_id=requirement_id,
            functional_requirement_id=functional_requirement_id,
            impact=value,
        )


def test_add_impact_with_wrong_requirement_id(analysis):
    requirement_id = 4000
    functional_requirement_id = analysis.add_functional_requirement().id
    value = 4

    with pytest.raises(Exception):  # todo: use a better Exception
        analysis.add_or_update_impact(
            requirement_id=requirement_id,
            functional_requirement_id=functional_requirement_id,
            impact=value,
        )


def test_update_impact(analysis):
    """Test updating an impact."""
    # Given
    requirement = analysis.add_requirement()
    functional_requirement = analysis.add_functional_requirement()
    value = 4

    impact = analysis.add_or_update_impact(
        requirement_id=requirement.id,
        functional_requirement_id=functional_requirement.id,
        impact=value,
    )

    # When
    new_value = 9
    returned_impact = analysis.add_or_update_impact(
        requirement_id=requirement.id,
        functional_requirement_id=functional_requirement.id,
        impact=new_value,
    )

    # Then
    assert returned_impact == impact  # same returned
    assert len(analysis.impacts) == 1  # no extra added
    assert impact.impact == new_value


def test_add_correlation_to_analysis(analysis):
    """Test adding correlation to an analysis."""
    # Given
    id1 = analysis.add_functional_requirement().id
    id2 = analysis.add_functional_requirement().id
    value = 9

    # When
    correlation = analysis.add_or_update_correlation(
        functional_requirement_1_id=id1,
        functional_requirement_2_id=id2,
        correlation=value,
    )

    # Then
    assert correlation.correlation == value
    assert correlation in analysis.correlations


def test_update_correlation(analysis):
    """Test adding correlation to an analysis."""
    # Given
    id1 = analysis.add_functional_requirement().id
    id2 = analysis.add_functional_requirement().id
    value = 9

    correlation = analysis.add_or_update_correlation(
        functional_requirement_1_id=id1,
        functional_requirement_2_id=id2,
        correlation=value,
    )

    # When
    new_value = 4
    returned_correlation = analysis.add_or_update_correlation(
        functional_requirement_1_id=id1,
        functional_requirement_2_id=id2,
        correlation=new_value,
    )

    # Then
    assert returned_correlation == correlation  # same object returned
    assert len(analysis.correlations) == 1  # no extra added
    assert correlation.correlation == new_value


def test_add_correlation_with_wrong_functional_requirement_id(analysis):
    id1 = analysis.add_functional_requirement().id
    id2 = 12345
    value = 9

    with pytest.raises(Exception):  # todo: use a better Exception
        analysis.add_or_update_correlation(
            functional_requirement_1_id=id1,
            functional_requirement_2_id=id2,
            correlation=value,
        )

    with pytest.raises(Exception):  # todo: use a better Exception
        analysis.add_or_update_correlation(
            functional_requirement_1_id=4444,
            functional_requirement_2_id=id2,
            correlation=value,
        )
