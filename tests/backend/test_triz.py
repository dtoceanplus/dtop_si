# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_structinn.triz import get_principles_from_contradiction_matrix


def test_get_principles_from_contradiction_matrix():
    """Test getting principles that do exist."""
    assert get_principles_from_contradiction_matrix(1, 7) == [29, 2, 40, 28]


def test_get_empty_principles_from_contradiction_matrix():
    """Test that an empty list is returned when there
    are no inventive principles for a contradiction."""
    assert get_principles_from_contradiction_matrix(1, 2) == []


def test_empty_list_of_principles_returned_for_matching_ids():
    """Test that if the same number is passed in for both
    the improving and worsening feature, an empty list is returned."""
    assert get_principles_from_contradiction_matrix(1, 1) == []
