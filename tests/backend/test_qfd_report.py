# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from os import path
import json

# import pandas as pd
import pytest
from deepdiff import DeepDiff

# from pandas.testing import assert_frame_equal

from dtop_structinn.pyqfd.models import FunctionalRequirement
from dtop_structinn.pyqfd.import_export import import_analysis
from dtop_structinn.pyqfd.enumerations import ImprovementEnum
from dtop_structinn.pyqfd.report import (
    achieves,
    actual_deviation_from_target,
    deviation_from_targets,
    importances,
    organisational_impacts,
    relative_deviation_from_target,
    summarise_functional_requirements,
    #    summarise_solution_idealities,
    get_inventive_principles,
)

with open(path.join(path.dirname(__file__), "megasplosher.json")) as fd:
    megasplosher = json.load(fd)


@pytest.fixture
def qfd():
    """A qfd as it would be from within the pyqfd module"""
    analysis = import_analysis(megasplosher)
    return analysis


def test_calculate_importances(qfd):
    """Check the calculation of importance when given a qfd analysis"""

    expected_result = {
        4: 25,
        2: 43,
        6: 16,
        1: 58,
        8: 35,
        7: 24,
        5: 61,
        3: 24,
    }

    assert importances(qfd) == expected_result


def test_calculate_organisational_impacts(qfd):
    """Check the calculation of organisational impact, given a qfd."""

    expected_result = {
        1: 20,
        2: 15,
        3: 25,
        4: 15,
        5: 20,
        6: 3,
        7: 2,
        8: 25,
    }

    assert organisational_impacts(qfd) == expected_result


@pytest.mark.parametrize(
    "target, achievement, lower_is_better, expected",
    [
        (0, 0, False, 0),
        (100, 50, False, -0.5),
        (20, 20, False, 0),
        (0, 0.4, False, None),
        (100, 50, True, 0.5),
        (20, 20, True, 0),
        (0, 0.4, True, None),
    ],
)
def test_calculate_relative_deviation(target, achievement, lower_is_better, expected):
    """Check the calculation of relative deviation"""

    # if https://github.com/pytest-dev/pytest/pull/7710 is merged
    # could simplify this test
    if expected is None:
        assert (
            relative_deviation_from_target(target, achievement, lower_is_better)
            is expected
        )
    else:
        assert relative_deviation_from_target(
            target, achievement, lower_is_better
        ) == pytest.approx(expected)


@pytest.mark.parametrize(
    "target, achievement, lower_is_better, expected",
    [
        (0, 0, False, 0),
        (100, 50, False, -50),
        (20, 20, False, 0),
        (0, 0.4, False, 0.4),
        (100, 50, True, 50),
        (20, 20, True, 0),
        (0, 0.4, True, -0.4),
    ],
)
def test_calculate_actual_deviation(target, achievement, lower_is_better, expected):
    """Check the calculation of actual deviation"""

    assert actual_deviation_from_target(
        target, achievement, lower_is_better
    ) == pytest.approx(expected)


def test_deviation_from_targets(qfd):
    """Returns both types of deviation from targets of each solution"""

    expected_result = [
        {
            "solution_id": 1,
            "relative_deviation": [
                {"functional_requirement_id": 4, "deviation": -0.9999999999999912},
                {"functional_requirement_id": 2, "deviation": 0.0},
                {"functional_requirement_id": 6, "deviation": 0.18518518518518517},
                {"functional_requirement_id": 1, "deviation": 0.06382978723404255},
                {"functional_requirement_id": 8, "deviation": 0.0},
                {"functional_requirement_id": 7, "deviation": 0.011363636363636364},
                {"functional_requirement_id": 5, "deviation": 0.011235955056179775},
                {"functional_requirement_id": 3, "deviation": -0.08928571428571429},
            ],
            "actual_deviation": [
                {
                    "functional_requirement_id": 4,
                    "deviation": -799999999999993.0,
                    "units": "wonderflops",
                },
                {"functional_requirement_id": 2, "deviation": 0.0, "units": "m^3"},
                {
                    "functional_requirement_id": 6,
                    "deviation": 0.5,
                    "units": "jellies/m^2",
                },
                {
                    "functional_requirement_id": 1,
                    "deviation": 3.0,
                    "units": "bananas/min",
                },
                {"functional_requirement_id": 8, "deviation": 0.0, "units": "litres"},
                {"functional_requirement_id": 7, "deviation": 1.0, "units": "mph"},
                {"functional_requirement_id": 5, "deviation": 1.0, "units": "%"},
                {"functional_requirement_id": 3, "deviation": -5.0, "units": "hz"},
            ],
        },
        {
            "solution_id": 2,
            "relative_deviation": [
                {"functional_requirement_id": 4, "deviation": -0.9999999999999974},
                {"functional_requirement_id": 2, "deviation": -1.173913043478261},
                {"functional_requirement_id": 6, "deviation": -1.5925925925925923},
                {"functional_requirement_id": 1, "deviation": 0.425531914893617},
                {"functional_requirement_id": 8, "deviation": -0.075},
                {"functional_requirement_id": 7, "deviation": -0.011363636363636364},
                {"functional_requirement_id": 5, "deviation": -0.2696629213483146},
                {"functional_requirement_id": 3, "deviation": -0.19642857142857142},
            ],
            "actual_deviation": [
                {
                    "functional_requirement_id": 4,
                    "deviation": -799999999999998.0,
                    "units": "wonderflops",
                },
                {"functional_requirement_id": 2, "deviation": -2.7, "units": "m^3"},
                {
                    "functional_requirement_id": 6,
                    "deviation": -4.3,
                    "units": "jellies/m^2",
                },
                {
                    "functional_requirement_id": 1,
                    "deviation": 20.0,
                    "units": "bananas/min",
                },
                {
                    "functional_requirement_id": 8,
                    "deviation": -300.0,
                    "units": "litres",
                },
                {"functional_requirement_id": 7, "deviation": -1.0, "units": "mph"},
                {"functional_requirement_id": 5, "deviation": -24.0, "units": "%"},
                {"functional_requirement_id": 3, "deviation": -11.0, "units": "hz"},
            ],
        },
    ]

    assert not DeepDiff(
        deviation_from_targets(qfd), expected_result, significant_digits=4
    )


@pytest.mark.parametrize(
    "value, functional_requirement, expected",
    [
        (100, {"target": 100, "direction": ImprovementEnum.lower_is_better}, True),
        (100, {"target": 10, "direction": ImprovementEnum.lower_is_better}, False),
        (10, {"target": 100, "direction": ImprovementEnum.lower_is_better}, True),
        (110, {"target": 100, "direction": ImprovementEnum.higher_is_better}, True),
        (90, {"target": 100, "direction": ImprovementEnum.higher_is_better}, False),
    ],
)
def test_check_achieves(value, functional_requirement, expected):
    assert achieves(value, FunctionalRequirement(**functional_requirement)) == expected


def test_summarise_functional_requirements(qfd):
    expected_result = [
        {
            "functional_requirement_id": 1,
            "importance": 58,
            "organisational_impact": 20,
            "target": 47.0,
            "units": "bananas/min",
            "best_achieved": 67.0,
            "worst_achieved": 50.0,
            "achieved_by": 2,
            "missed_by": 0,
        },
        {
            "functional_requirement_id": 2,
            "importance": 43,
            "organisational_impact": 15,
            "target": 2.3,
            "units": "m^3",
            "best_achieved": 2.3,
            "worst_achieved": 5.0,
            "achieved_by": 1,
            "missed_by": 1,
        },
        {
            "functional_requirement_id": 3,
            "importance": 24,
            "organisational_impact": 25,
            "target": 56.0,
            "units": "hz",
            "best_achieved": 51.0,
            "worst_achieved": 45.0,
            "achieved_by": 0,
            "missed_by": 2,
        },
        {
            "functional_requirement_id": 4,
            "importance": 25,
            "organisational_impact": 15,
            "target": 800000000000000.0,
            "units": "wonderflops",
            "best_achieved": 7.0,
            "worst_achieved": 2.0,
            "achieved_by": 0,
            "missed_by": 2,
        },
        {
            "functional_requirement_id": 5,
            "importance": 61,
            "organisational_impact": 20,
            "target": 89.0,
            "units": "%",
            "best_achieved": 90.0,
            "worst_achieved": 65.0,
            "achieved_by": 1,
            "missed_by": 1,
        },
        {
            "functional_requirement_id": 6,
            "importance": 16,
            "organisational_impact": 3,
            "target": 2.7,
            "units": "jellies/m^2",
            "best_achieved": 2.2,
            "worst_achieved": 7.0,
            "achieved_by": 1,
            "missed_by": 1,
        },
        {
            "functional_requirement_id": 7,
            "importance": 24,
            "organisational_impact": 2,
            "target": 88.0,
            "units": "mph",
            "best_achieved": 89.0,
            "worst_achieved": 87.0,
            "achieved_by": 1,
            "missed_by": 1,
        },
        {
            "functional_requirement_id": 8,
            "importance": 35,
            "organisational_impact": 25,
            "target": 4000.0,
            "units": "litres",
            "best_achieved": 4000.0,
            "worst_achieved": 4300.0,
            "achieved_by": 1,
            "missed_by": 1,
        },
    ]

    assert summarise_functional_requirements(qfd) == expected_result


def test_summarise_solution_idealities(qfd):
    expected_result = [
        {
            "functional_requirement_id": 1,
            "worst_conflicts": [{"contradictions": [], "functional_requirement_id": 3}],
        },
        {
            "functional_requirement_id": 2,
            "worst_conflicts": [
                {
                    "functional_requirement_id": 3,
                    "contradictions": [
                        {
                            "improving": 2,
                            "worsening": 18,
                            "principle": 19,
                        },
                        {
                            "improving": 2,
                            "worsening": 18,
                            "principle": 32,
                        },
                        {
                            "improving": 2,
                            "worsening": 18,
                            "principle": 35,
                        },
                        {
                            "improving": 3,
                            "worsening": 18,
                            "principle": 32,
                        },
                        {
                            "improving": 12,
                            "worsening": 18,
                            "principle": 13,
                        },
                        {
                            "improving": 12,
                            "worsening": 18,
                            "principle": 15,
                        },
                        {
                            "improving": 12,
                            "worsening": 18,
                            "principle": 32,
                        },
                    ],
                },
            ],
        },
        {
            "functional_requirement_id": 3,
            "worst_conflicts": [
                {"contradictions": [], "functional_requirement_id": 1},
                {
                    "functional_requirement_id": 8,
                    "contradictions": [
                        {
                            "improving": 18,
                            "worsening": 2,
                            "principle": 2,
                        },
                        {
                            "improving": 18,
                            "worsening": 2,
                            "principle": 35,
                        },
                        {
                            "improving": 18,
                            "worsening": 2,
                            "principle": 32,
                        },
                    ],
                },
            ],
        },
        {
            "functional_requirement_id": 4,
            "worst_conflicts": [
                {
                    "functional_requirement_id": 8,
                    "contradictions": [
                        {
                            "improving": 22,
                            "worsening": 2,
                            "principle": 19,
                        },
                        {
                            "improving": 22,
                            "worsening": 2,
                            "principle": 6,
                        },
                        {
                            "improving": 22,
                            "worsening": 2,
                            "principle": 18,
                        },
                        {
                            "improving": 22,
                            "worsening": 2,
                            "principle": 9,
                        },
                        {
                            "improving": 23,
                            "worsening": 2,
                            "principle": 35,
                        },
                        {
                            "improving": 23,
                            "worsening": 2,
                            "principle": 6,
                        },
                        {
                            "improving": 23,
                            "worsening": 2,
                            "principle": 22,
                        },
                        {
                            "improving": 23,
                            "worsening": 2,
                            "principle": 32,
                        },
                    ],
                },
            ],
        },
        {
            "functional_requirement_id": 5,
            "worst_conflicts": [
                {
                    "functional_requirement_id": 8,
                    "contradictions": [
                        {
                            "improving": 12,
                            "worsening": 2,
                            "principle": 15,
                        },
                        {
                            "improving": 12,
                            "worsening": 2,
                            "principle": 10,
                        },
                        {
                            "improving": 12,
                            "worsening": 2,
                            "principle": 26,
                        },
                        {
                            "improving": 12,
                            "worsening": 2,
                            "principle": 3,
                        },
                    ],
                },
            ],
        },
        {
            "functional_requirement_id": 6,
            "worst_conflicts": [
                {
                    "functional_requirement_id": 7,
                    "contradictions": [
                        {
                            "improving": 3,
                            "worsening": 9,
                            "principle": 13,
                        },
                        {
                            "improving": 3,
                            "worsening": 9,
                            "principle": 4,
                        },
                        {
                            "improving": 3,
                            "worsening": 9,
                            "principle": 8,
                        },
                    ],
                },
            ],
        },
        {
            "functional_requirement_id": 7,
            "worst_conflicts": [
                {
                    "functional_requirement_id": 6,
                    "contradictions": [
                        {
                            "improving": 9,
                            "worsening": 3,
                            "principle": 13,
                        },
                        {
                            "improving": 9,
                            "worsening": 3,
                            "principle": 14,
                        },
                        {
                            "improving": 9,
                            "worsening": 3,
                            "principle": 8,
                        },
                    ],
                },
            ],
        },
        {
            "functional_requirement_id": 8,
            "worst_conflicts": [
                {
                    "functional_requirement_id": 3,
                    "contradictions": [
                        {
                            "improving": 2,
                            "worsening": 18,
                            "principle": 19,
                        },
                        {
                            "improving": 2,
                            "worsening": 18,
                            "principle": 32,
                        },
                        {
                            "improving": 2,
                            "worsening": 18,
                            "principle": 35,
                        },
                    ],
                },
                {
                    "functional_requirement_id": 4,
                    "contradictions": [
                        {
                            "improving": 2,
                            "worsening": 22,
                            "principle": 18,
                        },
                        {
                            "improving": 2,
                            "worsening": 22,
                            "principle": 19,
                        },
                        {
                            "improving": 2,
                            "worsening": 22,
                            "principle": 28,
                        },
                        {
                            "improving": 2,
                            "worsening": 22,
                            "principle": 15,
                        },
                        {
                            "improving": 2,
                            "worsening": 23,
                            "principle": 5,
                        },
                        {
                            "improving": 2,
                            "worsening": 23,
                            "principle": 8,
                        },
                        {
                            "improving": 2,
                            "worsening": 23,
                            "principle": 13,
                        },
                        {
                            "improving": 2,
                            "worsening": 23,
                            "principle": 30,
                        },
                    ],
                },
            ],
        },
    ]

    conflicts = get_inventive_principles(qfd)
    assert not DeepDiff(conflicts, expected_result, ignore_order=True)


# def test_excel_workbooks_match(qfd, client):
#     """Test that an exported report matches the reference workbook.
#
#     The reference workbook and exported report are excel workbooks,
#     which are then compared using pandas. The order of the rows is not
#     important with the exception of the header row.
#     """
#
#     reference_workbook = pd.read_excel("tests/qfd_results.xlsx", None)
#     exported_report = pd.read_excel(
#         client.get(url_for("qfd.get_qfd_report_xlsx", _id=qfd["id"])).data,  # noqa: W505
#         None,
#     )
#
#     assert reference_workbook.keys() == exported_report.keys()
#
#     for sheet in reference_workbook:
#         columns = list(reference_workbook[sheet].columns)
#         reference_worksheet = (
#             reference_workbook[sheet].sort_values(by=columns).reset_index(drop=True)
#         )
#         report_worksheet = (
#             exported_report[sheet].sort_values(by=columns).reset_index(drop=True)
#         )
#
#         assert_frame_equal(reference_worksheet, report_worksheet)
