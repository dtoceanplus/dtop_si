# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
from flask import Flask
from flask.testing import FlaskClient

from dtop_structinn.app import create_app
from dtop_structinn.database import session, Base
from sqlalchemy import create_engine


@pytest.fixture
def app() -> Flask:
    # create a new in memory database for the test
    engine = create_engine("sqlite://")

    # create the tables
    Base.metadata.create_all(bind=engine)

    # configure the session
    session.configure(bind=engine)

    return create_app(
        {
            "TESTING": True,
            "SQLALCHEMY_DATABASE_URI": "sqlite://",
        }
    )


@pytest.fixture
def client(app) -> FlaskClient:
    test_client = app.test_client()
    ctx = app.app_context()
    ctx.push()
    rctx = app.test_request_context()
    rctx.push()
    yield test_client
    ctx.pop()
    rctx.pop()


@pytest.fixture(autouse=True)
def setup_engine():
    # create a new in memory database for the test
    engine = create_engine("sqlite://")

    # create the tables
    Base.metadata.create_all(bind=engine)

    # configure the session
    session.configure(bind=engine)

    yield  # <- do the test

    # remove the session
    # (necessary because we're not in a Flask request context)
    # and unconfigure ready for the next test
    session.remove()
    session.configure(bind=None)
