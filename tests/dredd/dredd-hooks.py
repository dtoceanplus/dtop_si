# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import dredd_hooks as hooks
import requests


targets_200 = (
    "/api/{qfdId}/targets > List targets for a QFD analysis > 200 > application/json"
)
solutions_200 = "/api/{qfdId}/solutions > List solutions for a QFD analysis > 200 > application/json"


@hooks.before(targets_200)
@hooks.before(solutions_200)
def insert_minimal_qfd(transaction):
    """Insert just enough of a QFD for endpoints to return a 200."""
    base_url = f"{transaction['protocol']}//{transaction['host']}:{transaction['port']}"
    r = requests.post(
        f"{base_url}/qfd/", json={"name": "dredd test", "objective": "dredd testing"}
    ).json()
    qfdId = r["id"]
    requests.post(
        f"{base_url}/qfd/{qfdId}/functional_requirements/",
        json={
            "description": "capital cost",
            "units": "GBP",
            "target": 1000000,
            "direction": -1,
        },
    )
    requests.post(
        f"{base_url}/qfd/{qfdId}/functional_requirements/",
        json={
            "description": "rated power",
            "units": "MW",
            "target": "15",
            "direction": 1,
        },
    )


@hooks.after(targets_200)
@hooks.after(solutions_200)
def delete_qfd(transaction):
    """Delete the qfd created for the tests."""
    base_url = f"{transaction['protocol']}//{transaction['host']}:{transaction['port']}"
    requests.delete(f"{base_url}/qfd/1")
