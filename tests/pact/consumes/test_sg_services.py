# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests

from pact import Consumer, Like, Provider, Term


METRIC_THRESHOLDS = {
    "metric_thresholds": [
        {
            "threshold": 0,
            "metric": {
                "id": 1,
                "name": "LCOE",
                "threshold_type": "upper",
                "unit": "Euro/kWh",
            },
        },
    ],
}

METRIC_RESULTS = {
    "metric_results": [
        {
            "metric": "LCOE",
            "result": None,
            "threshold": 150,
            "threshold_type": "upper",
            "unit": "Euro/kWh",
        },
    ],
}

pact = Consumer("si").has_pact_with(Provider("sg"), port=1241)
pact.start_service()
atexit.register(pact.stop_service)


def test_sg_metric_thresholds():

    pact.given("sg 1 exists and has metric thresholds").upon_receiving(
        "a request for metric thresholds"
    ).with_request(
        "GET",
        Term(
            r"/api/stage-gate-studies/\d+/metric-thresholds",
            "/api/stage-gate-studies/1/metric-thresholds",
        ),
    ).will_respond_with(
        200, body=Like(METRIC_THRESHOLDS)
    )

    with pact:
        metric_thresholds = requests.get(
            f"{pact.uri}/api/stage-gate-studies/1/metric-thresholds"
        )
        assert metric_thresholds.status_code == 200
        assert metric_thresholds.json() == METRIC_THRESHOLDS


def test_sg_metric_results():

    pact.given("sg 1 exists and has metric results").upon_receiving(
        "a request for metric results"
    ).with_request(
        "GET",
        Term(
            r"/api/stage-gate-studies/\d+/metric-results",
            "/api/stage-gate-studies/1/metric-results",
        ),
    ).will_respond_with(
        200, body=Like(METRIC_RESULTS)
    )

    with pact:
        metric_results = requests.get(
            f"{pact.uri}/api/stage-gate-studies/1/metric-results"
        )
        assert metric_results.status_code == 200
        assert metric_results.json() == METRIC_RESULTS
