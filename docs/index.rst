.. _si-home:

STRUCTURED INNOVATION
=====================

Introduction
------------

The Structured Innovation (SI) module comprises innovation methodologies
that can enhance concept creation and selection in ocean energy systems
(including subsystems, energy capture devices and arrays), enabling a
structured approach to address complex ocean energy engineering
challenges where design options are numerous. Thus it can facilitate
efficient evolution from concept to commercialisation.

For a sector such as ocean energy, where the number of design options is
still very high, the SI module can help users understand the complexity
and interdependencies of the engineering challenge – resulting in a more
efficient evolution from concept to commercialisation. To achieve these
benefits, the SI module is provided as two components:

1. The Quality Function Deployment (QFD) methodology defines the
   innovation problem and identifies trade-offs in the system. This is
   combined with the Theory of inventive problem solving (TRIZ), a
   systematic inventive problem-solving methodology, to generate
   potential solutions to the often-contradictory requirements raised
   from the QFD. The output from the integrated QFD/TRIZ component
   comprises design requirements along with target engineering metrics.

2. The Failure Modes and Effects Analysis (FMEA) assesses the technical
   risks associated with the proposed design concepts. The FMEA
   component’s output comprises a ranked set of failure modes per design
   requirement and identifies where mitigation actions are required to
   reduce the assessed risk.

The SI module can be used either as a standalone tool or within the
DTOceanPlus design modules framework. It offers two main purposes for
new concepts – to estimate costs and performance at an early stage in
the concept creation/design process – and design improvements – to allow
for a more detailed assessment of innovation within an existing
device/project development path.

Structure
---------

This documentation is divided into four main sections:

-  :ref:`Tutorials <si-tutorials>` to give step-by-step instructions on using the SI module for new users.  

-  :ref:`How-to Guides <si-how-to>` that show how to achieve specific outcomes using the SI module.  

-  A section on :ref:`background, theory, and calculation methods <si-explanation>` describe how the SI module works.  

-  The :ref:`API reference <si-reference>` section documents the code of modules, classes, API, and GUI.  

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------

The Structured Innovation module has six major functionalities:

1. **Defining objectives of the study:** This stage captures the project
   objectives and the list of the stakeholder needs (WHATs) broadly
   defined. In the context of developing a new product, this is a list
   of customer requirements. These requirements – often general, vague,
   and difficult to implement directly – are prioritised in order of
   importance.

2. **Scanning the design space:** The SI module’s QFD/TRIZ component is
   used for two purposes. Firstly, to scan the design space by mapping
   options for each of the key parameters which make up ocean energy
   concepts or projects, then ranking the attractiveness of these
   options through high level physical and economic assessments.
   Secondly, to define the innovation problem space representing the
   customer’s voice and make an immediate objective assessment of the
   best solutions that fit the users’ requirements.

   a. **Definition of functional requirements:** This is when the
      customer needs are translated into measurable functional
      requirements (HOWs) that can satisfy the needs.

   b. **Definition of Impacts:** In this stage, the relationships
      between the stakeholder needs (WHATs) and the functional
      requirements (HOWs) are determined using a predefined scale. Many
      of the HOWs identified affect more than one WHAT.

   c. **Requirement interactions:** This establishes the
      interdependencies between functional requirements (HOWs). The
      purpose is to identify areas where trade-off decisions, conflicts,
      and innovation may be required.

3. **Identifying attractive areas of innovation:** The SI module is
   developed to include fundamental relationships between key parameters
   in ocean energy concepts, evidence from the first ocean energy
   arrays, and a standard library of problem solution
   inter-relationships. QFD uses a set of requirements (WHATs) and
   answers them with a set of functional requirements (HOWs). There will
   be various solutions to solve each requirement, with each solution
   aims to produce the best requirement improvement. These solutions may
   contradict each other, and the QFD/TRIZ methodology allows these
   contradictions to be identified and their impact assessed.

   The possible concepts will be ranked in order of importance and
   achievability, highlighting options that would attract attractive
   investment opportunities. Evaluation of these options will be based
   on high-level metrics.

4. **Assessing contradictions:** The TRIZ component of the SI module is
   used to produce solutions to the QFD requirements where an
   improvement is needed, or if there is no existing solution, or if the
   key performance indicators are not satisfactorily met. The TRIZ
   methodology can ensure completeness in the key parameters that define
   the design space using the Effects Database and in the series of
   provocative prompts to provide the well-known forty inventive
   principles and other modules to solve contradictions contained within
   the QFD. The QFD and TRIZ components are integrated into a single
   component within the SI module to visualise opportunity and risk
   areas.

5. **Assessing technical risk:** Technical risks are framed using the
   ‘concept’ or ‘design’ FMEA component. The component provides ratings
   for each defect or failure in terms of severity, occurrence, and
   detection. The FMEA uses a database of validated defect parameters to
   improve understanding of technical risk during the design assessment
   process and offer both risk mitigation and cost reduction
   opportunities. The structured innovation process will conclude with a
   visualisation method to represent the process and results obtained
   and deviation from the SI module’s key performance metrics. The
   results will be expressed in terms of a ranking of attractive options
   and a presentation of the QFD requirements. The overall result will
   be an acceptability rating that allows objective assessments of the
   design.

6. **Reporting outputs:** This generates a summary page of all the
   outputs, including a list of proposed innovative functions, metrics,
   conflicts and interrelationships, and impact. This can be in report
   format or as a set of data files for further analysis and future
   updates.
