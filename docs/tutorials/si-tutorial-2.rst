.. _si-tutorial-2:

***************************************
2. Accessing QFD/TRIZ and FMEA studies
***************************************

To perform a Structured Innovation study, the user must first create a
new QFD/TRIZ study or FMEA study using the SI module’s applicable
component. Studies are the entities used to record and save the input
and output data associated with each analysis. Each study should be
named appropriately to help identify its purpose and objective at a
later date.

Once created, it is possible to load an existing study for editing,
import/export studies to/from the module, and delete studies no longer
required.

The tutorial contains two parts focusing on how to:

.. contents::
   :local:

Access QFD/TRIZ studies
=======================

Create a new QFD/TRIZ study 
---------------------------

7. To start a new QFD/TRIZ study from the Structured Innovation home
   page, select *Start QFD & TRIZ*; alternatively, select *Concept
   Creation and Improvement* from the menu bar.

8. From the List of QFD/TRIZ Analyses page, select the *New* button.

Access an existing QFD/TRIZ study 
---------------------------------

1. To access an existing QFD/TRIZ study from the Structured Innovation
   home page, select *Start QFD & TRIZ*; alternatively, select *Concept
   Creation and Improvement* from the menu bar.

2. To load an existing QFD/TRIZ study, select the relevant filename.

3. To download an existing QFD/TRIZ study, select the ‘down arrow’ next
   to the filename.

4. To import an existing QFD/TRIZ study, select the *Import* button.

5. To delete an existing QFD/TRIZ study, select the ‘bin’ icon next to
   the relevant filename.

.. warning::

   Deleted studies cannot be recovered. Ensure studies are
   downloaded and backed up to prevent accidental deletion.

Access FMEA studies
===================

Create a new FMEA study 
-----------------------

1. To start a new FMEA study from the Structured Innovation home page,
   select *Start FMEA*; alternatively, select *Concept Risk Analysis*
   from the menu bar.

2. From the List of FMEA Analyses page, select the *New* button.

Access an existing FMEA study 
-----------------------------

1. To access an existing FMEA study from the Structured Innovation home
   page, select *Start FMEA*; alternately, select *Concept Risk
   Analysis* from the menu bar.

2. To load an existing FMEA study, select the relevant filename.

3. To download an existing FMEA study, select the ‘down arrow’ next to
   the filename.

4. To import an existing FMEA study, select the *Import* button.

5. To delete an existing FMEA study, select the ‘bin’ icon next to the
   relevant filename.

.. warning::

   Deleted studies cannot be recovered. Ensure studies are
   downloaded and backed up to prevent accidental deletion.