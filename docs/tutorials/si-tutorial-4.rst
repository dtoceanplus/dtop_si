.. _si-tutorial-4:

*****************************
4. Scanning the Design Space
*****************************

This functionality enables the user to define the measurable functional
requirements (the HOWs) to satisfy the customer requirements (the WHATs)
and how much each functional requirement impacts each customer
requirement. In addition, the user can establish the interdependencies
between functional requirements to identify areas where trade-off
decisions, conflicts, and innovation may be required.

The tutorial contains three parts focusing on how to:

.. contents::
   :local:

Define the Functional Requirements 
==================================

1. From the Functional Requirements page, enter the description, target
   value, and units for each identified functional requirement. Then,
   use the toggle button to select the Direction of Improvement (DoI)
   (lower/higher) and use the drop-down lists to select the engineering
   difficulty and delivery difficulty.

   .. note::
   
      DoI must be selected. To confirm the direction, select the toggle
      switch until the applicable arrow is shown and the icon changes colour
      to blue

   .. note::
      
      Target Value must be a number. The ‘+’ and ‘–‘ buttons allow a
      value to be entered or increased/decreased in integer steps. To enter a
      fraction, manually type the required value.

2. To add a requirement using data from another part of the DTOceanPlus
   suite of tools, click on an empty description field and select the
   Data Sources icon from the Additional Information bar. Then select
   the required entry from the drop-down list(s) under each heading.

   .. note::

      On selection, applicable data will be added to a new functional
      requirement. This will reflect content already added in another study
      but can be amended if required. Missing data will need to be added as
      per previous instructions.

3. To edit any functional requirement, change the relevant field. To
   delete, move the cursor over the relevant functional requirement and
   select the ‘bin’ icon.

4. Select *Next* to proceed to the next step or *Previous* to return to
   the last step.

Define the level of impact between Customer and Functional Requirements 
=======================================================================

1. From the Impacts page, enter the impact of each functional
   requirement on each customer requirement using the drop-down list.

   .. note::

      Impact rankings are predefined as high, medium, low, and none. A
      functional requirement with a high ranking has a larger impact on
      achieving the customer requirement than one ranked as medium.

2. Select *Next* to proceed to the next step or *Previous* to return to
   the last step.

Define the correlations between the Functional Requirements 
===========================================================

1. From the Correlations page, enter the correlation between each
   functional requirement by using the drop-down list.

   .. note::

      It is not necessary or possible to enter a correlation between the
      same functional requirement. The field is greyed out if this is
      attempted.
   
   .. note::
   
      Correlation rankings are predefined as high negative, medium
      negative, low negative, none, low positive, medium positive, and high
      positive. Positive correlation implies that increasing one functional
      requirement will greatly affect increasing the other, and vice versa.
      Likewise, a negative correlation implies that increasing one
      functionality will hinder decreasing the other, and vice versa. The
      purpose is to identify areas where trade-off decisions, conflicts, and
      innovation may be required.
   
   .. note::
   
      As correlation is reciprocal, entries against one Functional
      Requirement are automatically added for the corresponding Functional
      Requirement. For example, suppose the Correlation between Functional
      Requirement A and Functional Requirement B is entered as low negative.
      In that case, the tool ensures that the Correlation between Functional
      Requirement B and Functional Requirement A is also low negative.

2. Select *Next* to proceed to the next step or *Previous* to return to
   the last step.
