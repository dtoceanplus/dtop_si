.. _si-tutorial-5:

***************************************************************************
5. Identifying Attractive Areas of Innovation and Assessing Contradictions
***************************************************************************

This functionality provides inventive inspiration for the user using the
TRIZ contradictions matrix – encouraging the user to look for existing
solutions to similar problems at different scales and times. This allows
the user to adopt principles that might offer idealised solutions from
other industries, countries, and times in history. In addition, the TRIZ
methodology can ensure completeness in the key parameters that define
the design space using provocative prompts to provide the well-known
forty inventive principles and other tools to solve contradictions
within the QFD.

The tutorial contains three parts focusing on how to:

.. contents::
   :local:

Enter TRIZ classes 
==================

1. When entering the TRIZ page for the first time, select *Next* to
   generalise the contradictive functional requirements into relevant
   TRIZ general problems (known in the tool as TRIZ classes) using the
   drop-down list.

2. For each functional requirement, enter all the relevant TRIZ classes,
   selecting each in turn from the drop-down list.

   .. note::
   
      The drop-down list contains the predefined TRIZ classes from the
      39×39 contradiction matrix. Multiple classes can be selected for each
      Functional Requirement.

3. To delete a TRIZ class, select the ‘x’ next to any entry.

4. Select *Next* to proceed to the next step or *Previous* to return to
   the last step.

Document existing solutions
===========================

1. From the Solutions page, Enter the description of each solution that
   exists and the solution values for each Functional Requirement.

   .. note::

      Use each entry to capture the state-of-the-art or competitors’
      technologies so they can be compared to the target values proposed.

2. To edit any entry, change the relevant field. To delete, move the
   cursor over the relevant entry and select the ‘bin’ icon.

3. Select *Next* to proceed to the next step or Previous to return to
   the last step.

Collate and interpret results
=============================

1. From the Report page, select the *deviation from targets* tab to
   review each potential solution’s results against the target values
   entered for each Functional Requirement. Both relative and actual
   deviations are provided.

   .. note::

      A value of 0 means that the target is met. A positive value means
      that the target is exceeded, and vice versa. Calculating this value
      makes some assumptions about targets, such as that they are on a scale
      beginning at 0. If this value is missing, it is either because
      achievement has not yet been added for this functional
      requirement/solution combination, or the target is 0. If the target is
      0, the relative deviation cannot meaningfully be calculated in this way,
      and you should refer to the actual deviation table instead.

2. Select the *Potential for disruption* tab to assess the importance
   and impact of each functional requirement.

   .. note::

      The Functional Requirement’s importance considers how much it
      impacts the Customer Requirements and its importance. The organisational
      impact combines the technical difficulty of achieving a target with the
      difficulty to deliver it. Therefore, a high value in the importance
      column and a low value in the organisational impact column could
      indicate low hanging fruit.

   This table also shows the best value and worst values being achieved by
   the solutions against this target and how many are achieving it. For
   example, a high value in the importance column, with a low value in the
   Solutions Achieving column, would suggest a high potential for
   disruption.

3. Select the *Ideality of solutions* tab to understand how ideal each
   solution compares against the requirements.

   .. note::

      For each solution, Targets Achieved shows how many targets have
      been met or exceeded, whereas Targets Missed shows how many have not yet
      been met. Ideality is a measure of how far the solution is from ideal,
      with 100% meaning all targets have been met or exceeded and 0% meaning
      none have. This score is weighted by the importance of the functional
      requirement achieved.

4. Select the *TRIZ conflicts* tab to view the proposed TRIZ general
   solutions to the contradictive functional requirements. First, review
   the list of contradictions between Functional Requirements by
   selecting each requirement in turn. Then, use inventive principles to
   identify potential solutions.

  .. note::

     Use the toggle switch to add a description of the inventive
     principle, alongside a generic example and an example from the marine
     energy sector where available. This depends on TRIZ classes having
     been appropriately assigned during the TRIZ step.

5. Select the *Next Steps* tab to consider Functional Requirements for
   further innovation. Then, select the applicable requirements and
   start a new analysis by selecting the blue *Dive Deeper* button.

   .. note::

      Selecting the tick box next to the Functional Requirement
      heading will select all functional requirements

6. Alternatively, select *Previous* to return to the last step or select
   *Structured Innovation Home* on the menu bar to return to the tool
   home page. This report can be reloaded from the List of QFD/TRIZ
   Analyses page by selecting the relevant filename from the list.
