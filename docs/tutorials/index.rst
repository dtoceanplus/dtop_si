.. _si-tutorials:

*********
Tutorials
*********

The SI module produces metrics and assessments, conflicts and impact
reports, and a design report. The metrics and assessments include both
ideality (a measure of what might be theoretically possible to achieve)
and development values (how difficult it would be to implement the
selected solution), relevant to the benchmark assessments of ideal
innovative concepts for wave and tidal renewable energy projects at
different stages of development. The design report then includes
requirements, specifications and gap analyses.


Below is a set of tutorials that guide the user through each of the
Structured Innovation module’s main functionalities. They are intended
for those who are new to the Structured Innovation module. However, it
is recommended to follow the tutorials in the suggested order listed
below, as certain tutorials are dependent on others. For instance, you
cannot complete *Scanning the Design Space* without first completing
*Defining Objectives of a Study*. Moreover, all the features require a
new study to be created (or an existing study to be accessed).

   1. :ref:`Navigating Around the Tool <si-tutorial-1>`

   2. :ref:`Accessing QFD/TRIZ and FMEA Studies <si-tutorial-2>`

   3. :ref:`QFD/TRIZ: Defining Objectives of a Study <si-tutorial-3>`

   4. :ref:`QFD/TRIZ: Scanning the Design Space <si-tutorial-4>`

   5. :ref:`QFD/TRIZ: Identifying Attractive Areas of Innovation and Assessing
      Contradictions <si-tutorial-5>`

   6. :ref:`FMEA: Assessing Technical Risk <si-tutorial-6>`

.. toctree::
   :maxdepth: 1
   :hidden:

   si-tutorial-1
   si-tutorial-2
   si-tutorial-3
   si-tutorial-4
   si-tutorial-5
   si-tutorial-6
 



