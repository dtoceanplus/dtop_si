.. _si-tutorial-3:

*********************************************
3. QFD/TRIZ: dDefining Objectives of a Study
*********************************************

Introduction
============

This functionality enables the user to define the project’s top-level
objective that will be the basis of the QFD/TRIZ study. This is also
where the user defines the list of the customer needs (the WHATs)
broadly. In the context of developing a new product, this is a list of
customer requirements. These requirements – often general, vague, and
difficult to implement directly – are prioritised in order of
importance.

The tutorial contains two parts focusing on how to:

.. contents::
   :local:

Define the project’s objective
==============================

1. After creating a new QFD/TRIZ study or loading an existing one, enter
   or edit the project name and objective in the relevant text field.

2. Select the solution hierarchy level using the + or – buttons.

   .. note::

      For a new study, the solution hierarchy will be predefined at level 1. 
      Once a study has been completed and ‘dive deeper’ is selected for one
      or more functional requirements (see :ref:`si-tutorial-5`), solution 
      hierarchy will be predefined at a level higher than the previous study, 
      e.g. if a level 1 study was completed, dive deeper results in a level 2 
      study. Studies completed in this way are shown hierarchically on the 
      List of QFD/TRIZ Analyses page.

3. Select *Next* to proceed to the next step.

Define and prioritise the customer requirements
===============================================

1. From the Customer Requirements page, enter the description and
   importance for each identified customer requirement.

   .. note::

      The importance of the requirements should be set relative to one
      another. For example, a requirement with an importance of 4 is twice as
      important as a requirement with an importance of 2. The lowest value
      possible is 1.

2. To add a requirement using data from another part of the DTOceanPlus
   suite of tools, click on an empty description field and select the
   Data Sources icon from the Additional Information bar. Then select
   the required entry from the drop-down list(s) under each heading.

   .. note::
    
      On selection, applicable data will be added to a new customer
      requirement. This will reflect content already added in another study
      but can be amended if required. Missing data will need to be added as
      per previous instructions.

3. To edit any customer requirement, change the relevant field. To
   delete, move the cursor over the relevant customer requirement and
   select the ‘bin’ icon.

4. Select *Next* to proceed to the next step or *Previous* to return to
   the last step.