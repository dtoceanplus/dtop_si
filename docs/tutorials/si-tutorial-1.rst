.. _si-tutorial-1:

******************************
1. Navigating around the tool
******************************

The Structured Innovation tool user interface is provided in four parts,
as shown in :numref:`image2`; the main window, which provides access to the
QFD/TRIZ and FMEA components; the menu bar, which provides access to the
home pages and user documentation; the navigation bar, which provides
quick access to each page of an existing study; and the Additional
Information bar, which provides notification of issues, guidance notes,
and help.

.. _image2:

.. figure:: ../media/image2.png

   Structured Innovation Tool Layout

The tutorial contains two parts focusing on how to:

.. contents::
   :local:

Guidance on using the main window is covered in later tutorials.

Use the menu and navigation bars
=================================

1. To quickly navigate to the tool home pages, select the applicable
   icon from the Menu Bar. Selecting the > arrow extends the menu bar to
   show the icon labels.

   .. note::

      Moving the cursor over each icon also shows the icon labels.

2. To navigate directly to the QFD/TRIZ or FMEA components, select the
   *Concept Creation & Improvement* icon (QFD/TRIZ) or *Concept Risk
   Analysis* icon (FMEA).

3. To navigate referenced documentation, including these tutorials and
   how-to guides, select the *Documentation & References* icon.

4. When a QFD/TRIZ or FMEA study is open, select the relevant numbered
   page, e.g. *(8) Report* from the Navigation Bar, to move directly to
   that page.

Access additional information
==============================

1. When a QFD/TRIZ or FMEA study is open, select the applicable icon
   from the Additional Information Bar to browse provided content.

   .. note::

      Moving the cursor over each icon shows the icon labels.

2. Selecting the *i* icon provides information about the study itself,
   including its name, the date it was last created/updated, and key
   metrics from the analysis.

3. Selecting the (!) icon show identified issues with the study,
   including missing entries or tool analysis hints and tips.

   .. note::

      Issues can be cleared by browsing the relevant page and updating
      or deleting the highlighted issue. Pages containing issues are
      highlighted with a red dot next to the number on the Navigation Bar.

4. Selecting the Data Sources icon provides a list of relevant data
   originating from various sources within the DTOceanPlus suite of
   tools, such as the solution hierarchy, Stage Gate tool improvement
   areas, and existing QFD/TRIZ or FMEA studies.

   .. note::
   
      The user must click within the description field (e.g.
      requirement or functional requirement cell) to view the list of
      available data for the specific step. For example, the description
      suggested by the Solution hierarchy relates to the selected level of
      detail of the hierarchy, the direction of improvement, and the target
      unit. Likewise, the Stage Gate description relates to the improvement
      area metrics, the direction of improvement, target value and unit.
      Similarly, customer or functional requirements from existing FMEA or
      QFD/TRIZ studies can be used.

5. Selecting the Notes icons allows notes to be captured for the current
   page.

   .. note::
   
      Typical notes may include references or rationale for data
      entries. Notes are automatically saved and can be deleted, edited, or
      changed as and when required.

6. Selecting the Help icon brings up relevant guidance for the study
   page.
   