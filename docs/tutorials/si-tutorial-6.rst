.. _si-tutorial-6:

****************************
6. Assessing Technical Risk
****************************

Technical risks are framed using the ‘concept’ or ‘design’ FMEA
component. The component provides ratings for each defect or failure in
terms of severity, occurrence, and detection. The FMEA uses a database
of validated defect parameters to improve understanding of technical
risk during the design assessment process and offer both risk mitigation
and cost reduction opportunities. In the SI module, the structured
innovation process will conclude with a visualisation method to
represent the process and results obtained and deviation from the key
performance metrics. The results will be expressed in terms of a ranking
of attractive options and a presentation of the QFD requirements. The
overall result will be an acceptability rating that allows objective
assessments of the design.

The tutorial contains seven parts focusing on how to:

.. contents::
   :local:



Define the Action Level And Occurrence Limit
============================================

1. After creating a new FMEA study or loading an existing one, enter or
   edit the action level and occurrence limit by entering the numbers or
   using the ‘+’ and ‘–’ buttons.

   .. note::

      Action level is the agreed threshold Risks Priority Number (RPN)
      for action on causes. Occurrence Limit is the agreed threshold ranking
      associated with the likelihood that failure mode and its associated
      cause need to be investigated.

2. Select *Next* to proceed to the next step.

Define the design requirements
==============================

1. From the Design Requirements page, enter the description for each
   identified design requirement.

2. To add a requirement using data from another part of the DTOceanPlus
   suite of tools, click on an empty description field and select the
   Data Sources icon from the Additional Information bar. Then select
   the required entry from the drop-down list(s) under each heading.

3. To edit any entry, change the relevant field. To delete, move the
   cursor over the relevant entry and select the ‘bin’ icon.

4. Select *Next* to proceed to the next step or *Previous* to return to
   the last step.

Define the Failure Modes for each Design Requirement
====================================================

1. From the Failure Modes page, enter the failure mode description for
   each design requirement.

   .. note:: At least one failure mode must be added to each design requirement

2. To edit any entry, change the relevant field. To delete, move the
   cursor over the relevant entry and select the ‘bin’ icon.

3. Select *Next* to proceed to the next step or *Previous* to return to
   the last step.

Define ehe failure mode effects
===============================

1. From the Effects page, enter the effect of each failure mode and use
   the drop-down list to select the severity.

   .. note:: At least one effect must be added to each failure mode

2. To edit any entry, change the relevant field. To delete, move the
   cursor over the relevant entry and select the ‘bin’ icon.

3. Select Next to proceed to the next step or Previous to return to the
   last step.

Define the causes and likelihood of each failure mode
=====================================================

1. From the Causes page, enter the cause of each failure mode and use
   the drop-down list to select the occurrence limit.

   .. note:: At least one cause must be added to each failure mode

2. To edit any entry, change the relevant field. To delete, move the
   cursor over the relevant entry and select the ‘bin’ icon.

3. Select Next to proceed to the next step or Previous to return to the
   last step.

Define the design controls
==========================

1. From the Design Controls page, enter the design controls that are in
   place for each failure mode. Then, use the drop-down list to select
   the detection ranking.

   .. note:: At least one design control must be added to each failure mode

2. To edit any entry, change the relevant field. To delete, move the
   cursor over the relevant entry and select the ‘bin’ icon.

3. Select Next to proceed to the next step or Previous to return to the
   last step.

Define additional mitigations
=============================

1. From the Mitigations page, Review the FMEA generated for each design
   requirement.

   .. note::

      Red (!) indicator - indicates that the final RPN value is higher
      than the action level
   
      Green (✓) indicator- indicates that the final RPN value is lower 
      than the action level.
   
      Orange (!) indicator- indicates that the RPN is below the action
      level, but the occurrence is higher than the occurrence limit.

2. Where required to reduce the RPN to below the action level, enter the
   name of the mitigation and any revisions to the severity, probability
   of occurrence, and detection likelihood.

3. Select Next to proceed to the next step or Previous to return to the
   last step.

4. From the Report page, review the updated FMEA to identify if any
   remaining mitigations are required or if proposed mitigations are
   sufficient.

5. Alternatively, select *Previous* to return to the last step or select
   *Structured Innovation Home* on the menu bar to return to the tool
   home page. This report can be reloaded from the List of FMEA Analyses
   page by selecting the relevant filename from the list.
