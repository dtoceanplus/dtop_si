.. _si-libraries:

*****************************
SI Tool Suggestion Libraries
*****************************

.. _si-solhi:

Solution Hierarchy
==================

The solution hierarchy is a multi-level list of potential solutions for
ocean energy systems that start with the energy trilemma as
requirements: delivering secure, affordable, and environmentally
sustainable energy; and lists potential solutions for each requirement.
The intention is to offer this hierarchy a structured set of prompts and
help the user consider multiple solutions to different QFD levels – the
user can then understand the potential for ideality and innovation and
thoroughness. An example of a solution hierarchy is shown in :numref:`solhi1`.

.. _solhi1:
.. figure:: ../media/solhi1.png

   Example of application of Solution Hierarchy

This example shows several design selection decisions for the first
level requirement – minimal lifetime cost. The example is associated
with reducing the operational costs and the links between the Levels of
hierarchy. The user has design flexibility over the levels- 3 to 5, with
the top two levels being influenced by external stakeholders and
legislation. The full list of the solution hierarchy is presented in the figure below.

.. image:: ../media/solhi2.svg

As part of the GUI refinement, the solution hierarchy can now be
accessed via the right-hand pane information bar under the Data source
tab. A user-friendly map will be developed to complement the
context-sensitive drop-down lists.


FMEA library in SI tool
=======================

The generic FMEA library implemented in the SI tool is presented in
:numref:`generic_failure_modes` to :numref:`generic_controls`.

Note that this is a generic library that contains suggested failure
modes, control and failure causes that might not be applicable to every
FMEA system. The objective is to prompt the user to consider all
possible failure modes, associated consequences and controls.

.. _generic_failure_modes:
.. csv-table:: Generic Failure Modes in the SI tool :download:`Download CSV <generic_failure_modes.csv>`
   :header-rows: 1
   :stub-columns: 1
   :file: generic_failure_modes.csv

.. _generic_failure_effects:
.. csv-table:: Generic Effects of Failure in the SI tool :download:`Download CSV <generic_failure_effects.csv>`
   :header-rows: 1
   :stub-columns: 1
   :file: generic_failure_effects.csv

.. _generic_failure_causes:
.. csv-table:: Generic Causes of failure in SI the tool :download:`Download CSV <generic_failure_causes.csv>`
   :header-rows: 1
   :stub-columns: 1
   :file: generic_failure_causes.csv

.. _generic_controls:
.. csv-table:: Generic Design and Process controls in the SI tool :download:`Download CSV <generic_controls.csv>`
   :header-rows: 1
   :stub-columns: 1
   :file: generic_controls.csv

