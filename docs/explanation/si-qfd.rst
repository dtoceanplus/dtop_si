.. _si-qfd:

**********************************
Quality Function Deployment (QFD)
**********************************

Developed in the late sixties in Japan, QFD is a structured methodology
used to identify customers’ requirements, prioritise them, and translate
them into suitable technical requirements for each stage of product
development and production [Mazur97]_. This is achieved using the House of
Quality (HoQ), a matrix used to describe the most crucial product or
service attributes or qualities. The HoQ matrix is used to translate the
customer needs into design characteristics using a relationship matrix
and demonstrates the strength of the relationship between the customer
needs (WHATs) and the design parameters (HOWs). This approach allows
collaboration between the various teams and capturing and visualising
information in one place. In addition, the HoQ matrix is data-intensive
and allows the team to capture a large amount of information in one
place [Tunga19a]_ [Mazur97]_.

Building a HoQ matrix
=====================

The HoQ matrix, as shown in :numref:`image5`, is built in consecutive
steps by determining each of the following sections:

**WHATs** captures what the customer needs – the customer requirements.
The identification of who the customers are is crucial to the deployment
of the tool. This should include customers/stakeholders directly or
indirectly involved or affected by the product/service. The requirements
of the multitude of stakeholders could originate from market research,
interviews, laws and regulations, contracts, operational concepts, site
conditions, external interfaces and utilities, industrial codes and
standards, operator needs, the public interest and other sources [Tunga19a]_.
The WHATs are listed down the left-hand side of the HoQ matrix.

**Importance** – also known as a priority, evaluates each customer’s
importance relative to the others by generating a ranking scale.

**HOWs** – describes how to meet the customer’ needs using design
requirements, also known as technical solutions. These design
requirements are translated from refining the customer requirements into
parameters that can be achieved, measured, and with measurable target
values. This shows how customer needs can derive from various sources
(interview, regulations, and references) and are then refined to design
requirements that provide parameters and information meaningful to and
measurable by a system [Tunga19b]_. The HOWs are listed across the top of the
HoQ matrix.

**The Roof** of the HoQ (**HOW vs HOW**) – indicates how the design
requirements interact with each other in synergy or conflicting with
each other. Different studies use various rating scales to indicate the
positive impacts and negative impacts of the correlations of one design
requirement with another (e.g. strong positive interaction (++), strong
negative interaction (-‍-), no relationship (0 or -)).

**Main Body (WHAT vs HOW)** – provides a relationship and correlation
matrix carried out to find out the relative importance of each parameter
(HOW) against the customer requirement (WHAT) rankings.

**HOW-MUCH** – also known as target values or metrics, describes each
design requirement’s ideal values. These can be used to trigger or
provoke innovation. The competing solutions/products (whether in-house
design alternatives or commercial competitors) can also be assessed
against these target values to determine whether and how closely each
solution meets the target criteria and requirements.

.. _image5:
.. figure:: ../media/image5.png
   :alt: Image result for roof on the House of quality

   Quality Function Deployment (House of Quality) [ODI09]_

.. _si-house-of-quality:

Phases of the House of Quality
==============================

The deployment of the QFD is a 4-phase process that uses the HoQ matrix
to translate the stakeholder requirements into technical requirements.
The technical requirements of the system of interest are then refined
into specific requirements for the subsystems and components. The 4
phases, as illustrated in :numref:`image6`, are:

-  Phase-1, Design requirement phase: defines the stakeholder needs and
   translates them into high-level design requirements to meet the
   needs. This level is also known as Top-Level HoQ. It includes a
   competitive analysis against the state-of-the-art technologies or
   processes and ideal target values against which to compare.

-  Phase-2, Product or Part requirement phase: uses the technical
   requirements defined in the top-level HoQ as requirements that need
   to be met. These technical requirements are refined into product or
   part characteristics of the subsystems.

-  Phase-3, Process or Manufacturing phase: defines manufacturing or
   assembly requirements for the product or part characteristics defined
   in the HoQ phase-2.

-  Phase-4, the Production or Quality Control phase, identifies the
   critical elements of the subsystems or components and the specific
   requirements for production or deployment. For example, this step
   could include inspection for quality assurance or condition testing.

.. _image6:
.. figure:: ../media/image6.png

   QFD 4-phases process [Tunga19a]_

QFD process within the SI tool
==============================

As highlighted in :numref:`image6`, the QFD is a multi-level analysis that
enables an experienced user to follow this process in each phase to
develop design requirements, gain insight into conflicts, and propose
innovative solutions, all of which inform the next phase.

As the user refines their design or functional requirements into more
detailed and specific requirements, the same QFD process is implemented
as described in Section 2.2.1.1.

Within the Structured Innovation tool implemented in the DTOceanPlus
suite of tools, the user carries out Phase-1 of the HoQ. Once the
phase-1 analysis is complete, the user may choose to take this to
Phase-2 by starting a new study and transferring only the relevant,
impactful functional requirements from the Phase-1 analysis into the
Phase-2 study. The same process may be implemented for all phases to
Phase-4 with the relevant details.
