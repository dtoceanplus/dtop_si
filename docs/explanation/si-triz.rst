.. _si-triz:

*******************************************
Theory of Inventive Problem Solving (TRIZ)
*******************************************

TRIZ, a Russian acronym for ‘‘\ *Teoriya Resheniya Izobretatelskikh
Zadatch*\ ’’, translates to the Theory of Inventive Problem Solving. It
is a systematic innovation problem-solving tool that goes beyond
intuition to used logic, data and research derived from the study of
patterns of invention in the global patent literature [Roberts11]_ [Gadd11]_.
Invented by Genrich Altshuller, TRIZ was developed from several designs
and patent inputs conducted by Vladimir Petrov, who studied its
evolution. TRIZ has been used for over 50 years as a problem-solving
technique and tool to supplement or improve product designs. Through
problem-solving, to increase Ideality, TRIZ out the benefits of
achieving an output against the costs and harms of achieving it [Roberts11]_
[Gadd11]_.

The process provides inventive inspiration for the designer –
encouraging the user to look for existing solutions to similar problems
at different scales and times. This allows the user to adopt principles
that might offer idealised solutions from other industries, countries,
times in history. “Someone somewhere has already solved your problem”
[TTJ19]_. The methodology makes use of past inventions, problems and
solutions, evolution trends and patents in areas of science and
technology and across different industries to define a knowledge-based
database of 40 inventive principles, a contradiction matrix, 76 standard
solutions, and trends of evolution that can be used as brainstorming
tools to solve any contradiction and/or incremental problems.

As a systematic, innovative tool, TRIZ attempts to eliminate the
compromises and trade-offs usually accepted in a system or process at
the early stages of the design. Instead, the specific problems or
innovation needs are simplified to TRIZ conceptual problems using the
TRIZ knowledge database (the inventive principles and standard
solutions), and the solution is then evaluated for the specific problem.

The TRIZ process is executed in steps: the first step analyses the
specific problem and formulates generic problems, from which generic
solutions are then identified using the TRIZ library, which is then
translated to solutions relevant to the initial problem. This is
illustrated in :numref:`image7`. The TRIZ database consists of
various tools and information such as the 40 Inventive Principles, the
technical contradiction matrix, the separation principles, the 76
standard solutions, evolution patterns, and substance-field analysis.

.. _image7:
.. figure:: ../media/image7.png

   Solving technical problems using TRIZ [Gadd11]_

TRIZ Contradiction Matrix
=========================

The contradiction matrix, also known as the 39 Engineering Parameters,
consists of specific parameters identified by Altshuller that can
improve or worsen the design of a system. The 39×39 contradiction matrix
presents these parameters based on their ability to either improve or
worsen each of the other parameters and thus the design or operational
conditions [Tunga19a]_ [Gadd11]_.

The contradiction matrix, presented in :numref:`image9` and :numref:`image10`,
represents the improving features in the first column and potentially
worsening features in the top row. Each of the corresponding squares in
the body of the matrix contains a list of inventive principles that can
resolve the contradiction by improving one parameter without worsening
the other.

Various software tools have implemented this matrix into algorithms
following a step-by-step process to solve the contradictions (e.g.
TechOptimizer, Goldfire Innovator, CreaTRIZ, TriSolver, TRIZ Explorer,
TRIZContrasolve, Guided Brainstorming [CarayannisElias13]_) – although none has combined
this with QFD and FMEA, as the DTOceanPlus Structured Innovation tool
has done.

.. _image9:
.. figure:: ../media/image9.PNG

   TRIZ 39x39 contradiction matrix in SI tool [Gadd11]_

:numref:`image10` represents an instance of using the contradiction matrix: The
user wants to increase the area of their device (improved feature),
resulting in increasing the weight of the device (worsening feature).
The 39x39 contradiction matrix guides the user to consider specific
inventive principles for this particular contradiction – in this case,
principles 2, 29, 17 or 4, discussed further in Section 2.2.2.2. The
complete 39x39 contradiction matrix fully implemented in the SI tool is
presented in :numref:`image9`.

.. _image10:
.. figure:: ../media/image10.PNG

   Example of the TRIZ 39×39 contradiction matrix

These contradictions can be grouped into physical contradictions and
technical contradictions.

-  Physical contradictions refer to functions of a system that are
   subject to contradictory, opposing requirements. For example, a car
   should be user-friendly and simple enough to drive, but at the same
   time, it could have the most advanced and complex features.

-  On the other hand, technical contradictions are typical trade-offs in
   the system, where an ideal state cannot be reached due to another
   feature in the system preventing it; e.g. direct-drive generator
   machines are more reliable than geared machines but heavier.

There are numerous descriptions of the contradiction parameters
available online and elsewhere. The following table, for example, was
reproduced from TRIZ Oxford [Haines-Gadd16]_.

.. csv-table:: TRIZ Contradiction matrix descriptions [Haines-Gadd16]_ :download:`Download CSV <TRIZ_contradictions.csv>`
   :header-rows: 1
   :file: TRIZ_contradictions.csv


TRIZ Inventive Principles
=========================

It is normal that, when contradictions arise during the design of
products or processes, a trade-off between design parameters occurs. The
standard or traditional approach involves a brainstorming and/or
trial-and-error process, resulting potentially in the inability to
resolve contradictions beyond existing knowledge and experience.
Altshuller reviewed hundreds of thousands of patents and inventions and
distinguished 40 Inventive Principles based on breakthrough inventions.
These inventive principles are solutions to overcome the contradiction
patterns described in the 39X39 contradiction matrix [Tunga19a]_ [Gadd11]_. The
contradictions and inventive principles are generic enough to apply to
various sectors.

Each matrix cell points to inventive principles, as shown in :numref:`image10`,
that have previously been used to resolve the contradictions. The user
should evaluate these principles (:numref:`triz-inventive-principles`) to determine the most
relevant system.

.. _triz-inventive-principles:
.. csv-table:: TRIZ 40 inventive principles [Haines-Gadd16]_ :download:`Download CSV <TRIZ_inventive_principles.csv>`
   :header-rows: 1
   :file: TRIZ_inventive_principles.csv


TRIZ Separation Principles
==========================

In some cases, there may be contradictions within a parameter. For
instance, a wave energy converter should have a large, displaced volume
but low mass; TRIZ separation principles consider problems in time, in
space, between parts and whole, and upon conditions. The physical
contradictions are resolved by separating the contradictory
requirements.

In time, the schedule of operations may be arranged so that requirements
are met at each time or phase of operation (one example would be the
traffic lights). In space, the contradictory requirements are defined in
phases, where a particular phase or subsystem does not require a
specific implementation of parameters (e.g., a seesaw or bi-focal
reading glasses). The separation between part and a whole considers
using the characteristics of a system to be represented as parts of the
system, meaning that “at the same critical moment in time and in the
same space, a grouping of objects can have a collective property, and
its parts can have the opposing property” [Ball13]_. This enables minimal
critical interaction with some parts of the system.

QFD/TRIZ integration in the SI tool
===================================

In obtaining and assessing innovative solutions, the user might be
tempted only to assess those solutions that they might come across
quickly, or perhaps pre-meditating the solutions by only considering the
advantages of their competitors’ products. This might lead to functional
fixedness and a lack of differentiation between products. Therefore, the
SI tool within the DTOceanPlus does not reinvent the TRIZ processes;
instead, the state-of-the-art comes from integrating the TRIZ into the
QFD process.

The integration of TRIZ into QFD in the SI tool allows the user to
quickly create innovative solutions using the TRIZ methods and inventive
solutions within the QFD process. For example, if there is a lack of
impact in the user’s solutions, TRIZ might allow the user to think of
the alternative, impactful solutions by reference to the TRIZ processes
– thinking of past, present and future inventions that meet a similar
problem or perhaps with a difference of scale – micro or macro.

The TRIZ examples implemented in the SI tool are presented in :numref:`triz-inventive-principles`
as a list of typical marine-related examplary solutions. TRIZ can be triggered
to support the initial requirement exploration during conflict
assessment where a solution is seen as a block or a lack of quality in
the solution provision and impact analysis.
