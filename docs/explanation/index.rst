.. _si-explanation:

*********************
Background and theory
*********************

.. toctree::
   :hidden:
   :maxdepth: 1

   si-qfd
   si-triz
   si-fmea
   si-libraries

Overview
=========

The Structured Innovation (SI) design tool is intended to provide the
designer with a process, information, validated data, analysis, and
comparative assessments, to generate new or improved concepts of a
subsystem, device, or array. Using the SI tool provokes the designer to
consider the interactions between technical solutions to a problem, the
necessary compromises required to meet the design intent or
requirements, and the interactions with the competent person. In this
context, the process needs to be carefully considered to avoid
constraining opportunistic innovation and innovation created by
systematic thinking. In addition, the tool aims to encourage the
break-down of functional fixedness – a cognitive bias that is commonly
employed to understand the operation of an object quickly. This
fixedness, which impedes creativity or innovation, is countered by many
features within the tool, including the TRIZ component.

The Structured Innovation design tool is built upon three methodologies:

- :ref:`si-qfd`
- :ref:`si-triz`
- :ref:`si-fmea`
   

The Quality Function Deployment process defines the innovation problem
and identifies trade-offs in the system. TRIZ, a systematic inventive
problem-solving process, generates potential solutions to the
contradictions raised from the QFD requirements. The QFD and TRIZ
processes (combined into a single module) generate several design
requirements and target engineering metrics. The FMEA process (which is
the second module) is used to assess the technical risks associated with
the proposed design concepts. 

This background section has been adapted
from Deliverable D3.1 [ESC19]_ and previous work [Tunga19a]_.

Concepts
========

The ability of a company to put forward an idea of a higher value can be
a source of competitive advantage and represents the reason for which a
customer may opt for one company to the disadvantage of another.
Nowadays, most companies developing new products or services use
innovation to identify, create, and develop innovative solutions,
measure ‘success’ against their competitors and manage the uncertainties
and risks associated with the implementation processes.

In Deliverable D3.1 [ESC19]_, an analysis of innovative best practices was
described across a wide variety of sectors. Despite the fact that the
positive impacts of the structured innovation approach across some of
the mature sectors (e.g. automotive, aerospace), the application is less
evident in the ocean energy sector. Organisations such as Wave
Energy Scotland, and the US-based National Renewable Energy Laboratory
(NREL) and Sandia National Laboratories use structured innovation
approaches to identify and develop new wave energy converter concepts
with high techno-economic performance potentials. From the horizon
scanning of the various sectors in [ESC19]_, it is seen that most sectors are
benefiting from using one, or a hybrid of two, of the three
QFD/TRIZ/FMEA methods to implement a structured innovation approach to
their designs. Each methodology can be applied standalone. However, when
applied together, the limitations of one methodology are overcome by the
strength of the others (e.g. the QFD-TRIZ hybrid combination with
customer-driven and innovation-driven design) [Caligiana17]_ [RahnejatZairi98]_.






References
==========

.. [ESC19] Energy Systems Catapult, “Technical Requirement for the
   implementation of Structured Innovation in Ocean Energy Systems,”
   DTOceanPlus, 2019.

.. [Tunga19a] I. Tunga, Thesis: Market strategies for the next
   generation of Offshore Wind in the UK, Edinburgh: University of
   Edinburgh, 2019.

.. [Caligiana17] G. Caligiana, “Integrating QFD and TRIZ for
   innovative design,” *Journal of Advanced Mechanical Design, Systems, and
   Manufacturing,* vol. 11, no. 2, p. 15, 2017.

.. [RahnejatZairi98] H. Rahnejat and M.
   Zairi, “The “QFD/FMEA interface”,” *European Journal of Innovation
   Management,* p. 18, 1998.

.. [Mazur97] G. Mazur, “Voice of Customer Analysis: A
   modern System of front-end QFD tools, with Case studies,” *AGC,* 1997.

.. [Tunga19b] I. Tunga, EngD Thesis- The Next generation UK offshore wind
   strategies, Edinburgh: Unpublished , 2019.

.. [ODI09] ODI, “Management
   Techniques: Structured Innovation,” 01 2009. [Online]. Available:
   https://www.odi.org/publications/5220-management-techniques-structured-innovation.
   [Accessed 01 08 2019].

.. [Roberts11] H. Roberts, “TRIZ Overview Design: Theory of
   Inventive Problem Solving (powerpoint slides),” GE, 2011.

.. [Gadd11] K. Gadd,
   TRIZ for Engineers: Enabling Inventive Problem Solving: Enabling
   Inventive Problem Solving, Chichester: John Wiley & Sons, Ltd, 2011.

.. [TTJ19] The TRIZ journal, “What is TRIZ? The origins of TRIZ and how it
   support innovation today,” 2019 [Online]. Available:
   https://triz-journal.com/what-is-triz/. [Accessed 05 04 2020].

.. [CarayannisElias13] D.
   Carayannis and G. Elias, The Encyclopedia of Creativity, Invention,
   Innovation, and Entrepreneurship (CI2E), Springer-Verlag New York, 2013.

.. [Haines-Gadd16] L. Haines-Gadd, TRIZ for Dummies Cheat Sheet, Chichester: John Wiley
   & Sons, Ltd, 2016.

.. [Ball13] Ball, Larry et al, “TRIZ Power tools Job #5
   Resolving Problems,” 05 2013. [Online]. Available:
   http://www.opensourcetriz.com/images/1_OpenSourceTRIZ_Pictures/15_Resolving_Problems/Resolving_Problems.pdf.
   [Accessed 05 04 2020].

.. [Arabian-HoseynabadiTavner10] H. Arabian-Hoseynabadi and P. Tavner,
   “Failure Modes and Effect Analysis (FMEA) for Wind Turbines,”
   *International Journal of Electrical Power and Energy Systems-32,* pp.
   817-824, 2010.

.. [Denning11] R. Denning, “Chapter 33 R&M Failure Modes, Effects (and
   Criticality) Analyses (FMEA/FMECA),” in *Applied R&M Manual for Defence
   Systems Part C- R&M Related Techniques: BS EN 60812*, -, 2011, pp. 1-12.

.. [ISO17] ISO/TC 123/SC 5 Quality analysis and assurance, “ISO
   12132:2017 Plain bearings — Quality assurance of thin-walled half
   bearings — Design FMEA,” ISO, 2017.
