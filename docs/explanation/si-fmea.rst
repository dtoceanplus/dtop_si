.. _si-fmea:

*****************************************
Failure Mode and Effects Analysis (FMEA)
*****************************************

Widely used in engineering design, Failure Mode and Effect Analysis
(FMEA) is used to identify and eliminate potential system failures. It
provides a means to compare various system configurations by identifying
possible root causes of failure(s), failure modes and estimation of
relative risks, to drive higher reliability, quality and enhanced safety
[Arabian-HoseynabadiTavner10]_.
The tool aids in developing robust design and control measures to
prevent potential failures from occurring.

There are two main approaches to performing an FMEA: one is based on the
physical structure of the system (e.g. details of variation in design
data), and one based on its functional structure [Denning11]_. The latter
(Concept/Design FMEA) is used as part of the DTOceanPlus Structured
Innovation tool for design or concept assessments. The methodology
assesses the system to highlight potential risks associated with the
system’s failure and its functions and identify ways to resolve them
before actual designs are implemented.

The FMEA is conducted as follows:

-  The key functional requirements of a given conceptual design are
   first determined.

-  For each functional requirement, potential failure modes are
   determined.

-  The Occurrence probability of each failure is established.

-  Additionally, the severity of each failure is determined, indicating
   the consequential effects of that failure mode.

-  Finally, the likelihood that controls will detect that a failure has
   occurred (Detection) is determined for each failure based on the
   existing detection and control system in place.

-  The criticalities of failures are then determined using the Risk
   Priority Number (RPN), which is calculated by multiplying the
   Severity (SEV), Occurrence (OCC), and Detection (DET) rankings
   associated with each failure: RPN = SEV*OCC\* DET.

-  This RPN is then used to prioritise risks, and suitable follow-up
   corrective actions are proposed to reduce the criticality of
   potential failures by implementing the corrective actions. These
   corrective actions can be obtained from the QFD alternative
   solutions, specific actions for the system (e.g. proposed design
   review, enhanced material properties), and background literature
   (e.g. measures implemented in other sectors).

-  The RPN is then re-calculated to establish the impact of the
   corrective actions on the system and the level of criticality of the
   system with the proposed measures.

-  These mitigation actions should then be implemented in the design of
   the systems.

A scoring matrix with a scale of 1-10 for Severity, Occurrence and
Detection is defined either by using the standard predefined FMEA
scaling matrix or adopting a user-defined one agreed at the project’s
outset\ *.*

An example of Severity categories and corresponding severity rankings
implemented in the SI tool are given in :numref:`severity_rating`.

.. _severity_rating:
.. csv-table:: Example of severity rating [Denning11]_ :download:`Download CSV <severity_rating.csv>`
   :header-rows: 1
   :file: severity_rating.csv

The complete scaling matrix implemented in the SI tool for Severity,
Occurrence and Detection is presented in :numref:`severity_ranking`, :numref:`occurrence_ranking`
and :numref:`detection_ranking`. These definitions are taken from ISO 12132 standard [ISO17]_.
A template of an FMEA worksheet is shown in :numref:`image11`. The generic FMEA
library implemented in the SI tool is presented in the Annex in :numref:`generic_failure_modes` to :numref:`generic_controls`.

.. _severity_ranking:
.. csv-table:: Severity ranking :download:`Download CSV <severity_ranking.csv>`
   :header-rows: 1
   :file: severity_ranking.csv

.. _occurrence_ranking:
.. csv-table:: Occurrence ranking :download:`Download CSV <occurrence_ranking.csv>`
   :header-rows: 1
   :file: occurrence_ranking.csv

.. _detection_ranking:
.. csv-table:: Detection ranking :download:`Download CSV <detection_ranking.csv>`
   :header-rows: 1
   :file: detection_ranking.csv

.. _image11:
.. figure:: ../media/image11.png

   Template of an FMEA worksheet
