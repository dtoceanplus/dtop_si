dtop\_structinn.suggestions package
===================================

.. automodule:: dtop_structinn.suggestions
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   dtop_structinn.suggestions.main_module
   dtop_structinn.suggestions.schemas
   dtop_structinn.suggestions.solution_hierarchy
   dtop_structinn.suggestions.stagegate
   dtop_structinn.suggestions.structured_innovation
