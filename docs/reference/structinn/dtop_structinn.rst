.. _si-dtopstructinn:

dtop\_structinn package
=======================

.. automodule:: dtop_structinn
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   dtop_structinn.app
   dtop_structinn.migrations
   dtop_structinn.notes
   dtop_structinn.pyfmea
   dtop_structinn.pyintegration
   dtop_structinn.pyqfd
   dtop_structinn.pysi
   dtop_structinn.solution_hierarchy
   dtop_structinn.suggestions
   dtop_structinn.triz

Submodules
----------

.. toctree::
   :maxdepth: 4

   dtop_structinn.database
   dtop_structinn.declarations
   dtop_structinn.errors
   dtop_structinn.schemas
   dtop_structinn.types
