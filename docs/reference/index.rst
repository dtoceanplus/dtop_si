.. _si-reference:

*************
API Reference
*************

This section of the documentation describes the technical reference manual for the Structured Innovation module.
It is formed of two components. 

1. The OpenAPI specification for the Structured Innovation tool. 
   This documentation, powered by `ReDoc <https://redoc.ly/redoc/>`_, describes the *implementation layer* of the Structured Innovation module, including the endpoints and API routes provided by the module. 
   This API description is available `here <https://dtoceanplus.gitlab.io/api-cooperative/dtop-structinn.html>`_. 

2. The documentation describing the source code of the module itself, extracted from the *docstrings* of the code and converted into a technical reference manual using `Sphinx <https://pypi.org/project/Sphinx/>`_. 
   This can be found :ref:`here <si-dtopstructinn>`.

.. toctree::
   :maxdepth: 1

   OpenAPI description <https://dtoceanplus.gitlab.io/api-cooperative/dtop-sg.html>
   structinn\dtop_structinn
