.. _si-data-requirements:

*********************************************
How to Understand SI Data Input Requirements
*********************************************

Overview of data requirements
=============================

This section describes the types of input data required to run the
Structured Innovation module. Full details and data specifications are
given in the how-to guide on preparing data. The required and optional
inputs to run the module are summarised in the tables below. Note that
in integrated mode, the required inputs – except for the proposed
functional requirements – will all come from other modules by selecting
from the solution hierarchy within the tool.

Input data provided by the user
===============================

:numref:`qfd_triz_input` lists all data types in page order that need to be entered by
the user for a QFD/TRIZ study.

.. _qfd_triz_input:
.. csv-table:: QFD/TRIZ Input Data :download:`Download CSV <qfd_triz_input.csv>`
   :header-rows: 1
   :stub-columns: 1
   :file: qfd_triz_input.csv

:numref:`fmea_input` lists all data types in page order that need to be entered by
the user for an FMEA study.

.. _fmea_input:
.. csv-table:: FMEA Input Data :download:`Download CSV <fmea_input.csv>`
   :header-rows: 1
   :stub-columns: 1
   :file: fmea_input.csv

Input data from other modules (or user in standalone mode)
==========================================================

To be defined

-  For QFD/TRIZ

-  FOR FMEA

Output data
===========

:numref:`qfd_triz_output` lists all data types in page order that will be output from a
QFD/TRIZ study.

.. _qfd_triz_output:
.. csv-table:: QFD/TRIZ Output Data :download:`Download CSV <qfd_triz_output.csv>`
   :header-rows: 1
   :stub-columns: 1
   :file: qfd_triz_output.csv

:numref:`fmea_output` lists all data types in page order that will be output from an
FMEA study.

.. _fmea_output:
.. csv-table:: FMEA Output Data :download:`Download CSV <fmea_output.csv>`
   :header-rows: 1
   :stub-columns: 1
   :file: fmea_output.csv