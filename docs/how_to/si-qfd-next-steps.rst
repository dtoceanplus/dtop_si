.. _si-qfd-next-steps:

*************************************************************
How to Review QFD/TRIZ Achievements and Suggested Next Steps
*************************************************************

**Definition and analysis of the innovation problem using QFD.**

QFD is a structured process used to identify the voice of the customer
(needs, expectations, etc.), prioritise them, and translate them into
applicable technical requirements for each stage of product development
and production. The process enables structured thinking, facilitating
the development of innovative concept designs.

When fully completed, the process captures :

-  customer requirements (and importance) to define the innovation
   design space;

-  functional requirements (and importance), with measurable target
   values, to meet or exceed the customer requirements;

-  interactions between requirements;

-  the organisational effort to engineer and deliver such requirements
   at or beyond the target set; and

-  benchmarking of competing state-of-the-art designs (leading-edge
   technology or design data, including the newest ideas or concepts
   across the sector) to understand the extent to which each of the
   proposed functional requirement targets has been met elsewhere.

Concept designs can therefore be created with confidence that all key
requirements have been fully considered. Innovators can also understand
whether it is worth investing in developing novel solutions to meeting
particular requirements and can prioritise important innovation areas
based on:

a) the importance of the customer requirements;

b) the functional requirements that would be most likely to disrupt the
   market; and

c) the organisational effort to engineer and deliver these requirements
   (i.e. to secure the skills, resources, supply, finance, etc.,
   required to deliver).

**Resolving conflicting requirements and innovating using TRIZ**

TRIZ is a systematic inventive problem-solving methodology that enables
users to generate potential innovative solutions to any conflicting
requirements raised during the QFD process.

When fully completed, each conflict between requirements should be
eliminated by applying generic inventive principles to determine the
most appropriate alternative solutions.

**Potential Next Steps**

The critical functional requirements are those with the highest
**overall impact** and the **least organisational effort** to implement.
Reviewing these rankings allows the user to understand the
interrelationship between the functional requirements, so one
lower-ranked requirement might be key to more impactful requirements,
and this should be considered in those cases. The user should also
consider value-added **areas beyond the state-of-the-art** that
contribute to the intended targets and customer needs.

Once the critical functional requirements are defined, the user can
choose to:

-  Repeat the TRIZ process to consider and compare two or more potential
   innovative approaches to meeting the defined requirements;

-  Assess the technical risks associated with the selected functional
   requirements using FMEA;

-  Refine the functional requirements into more detailed and specific
   requirements by “Diving Deeper” into the QFD/TRIZ analysis, either by
   selecting a subsystem, assembly or component or defining requirements
   for the following system design and production phases. More
   information can be found in the documentation; and/or

-  Use the detailed Design and Assessment tools in other modules of the
   DTOcean+ suite to develop detailed designs for the concepts created
   in this Structured Innovation tool, and assess their potential
   deployment to specific sites.

Once the critical functional requirements are defined, the user can
choose to:

-  Assess the technical risks associated with the selected functional
   requirements using FMEA (detailed in the how-guide :ref:`How to mitigate
   risks associated with innovative
   solutions <si-how-to-mitigate-technical-risks>`)
   and/or,

-  Refine the functional requirements into more detailed and specific
   requirements by “Diving Deeper” into the QFD/TRIZ analysis, either by
   selecting a subsystem, assembly or component or defining requirements
   for the following system design and production phases (detailed in
   the how-guide :ref:`How to Dive deeper into sub-studies using
   QFD <si-dive-deeper>`).

.. _si-how-to-mitigate-technical-risks:

How to mitigate technical risks associated with innovative solutions
=====================================================================

QFD is a process that translates the Voice of the Customer (needs,
expectations, etc.) into functional requirements. Upon completing a
QFD/TRIZ analysis, the user will have selected critical functional
requirements that meet the customer needs to assess further. These
critical requirements are used as input into the Concept or Design FMEA.
The FMEA, a risk mitigation process, is used to determine the technical
risks associated with how the design could potentially fail and develop
mitigation measures.

The steps are as follows:

1. Launch the FMEA module, create a new study, and define the study
   name, the action level and occurrence limit. (see :ref:`Tutorial
   6 <si-tutorial-6>`).

2. On the *Design requirements* Page, add the list of specific
   requirements to assess by clicking on an empty description field and
   select the Data Sources icon from the right-hand pane Information
   bar. Then select the required entry from the drop-down list(s) under
   a specific QFD/TRIZ study.

3. On the *Failure Modes* page, enter all possible failure mode
   descriptions for each design requirement.

4. Once all the potential failure modes are listed, follow the steps in
   :ref:`Tutorial 6 <si-tutorial-6>` to define the associated effects,
   causes, and design controls.

5. From the *Mitigations* page, Review the RPN generated for each design
   requirement. Status is provided to indicate if the action level (RPN
   threshold) or occurrence limit has been exceeded.

   .. note::
   
      Red (x) indicator - indicates that the final RPN value is
      higher than the action level
   
      Orange (!) indicator- indicates that the RPN is below the action level,
      but the occurrence is higher than the limit. Green (ü) indicator-
      indicates that the RPN value & Occurrence limit is lower than the
      threshold set.

1. Where required to reduce the RPN to below the action level, enter the
   name of the mitigation and any revisions to the severity, probability
   of occurrence, and detection likelihood.

2. On the *Report* page, review the updated FMEA to identify if any
   remaining mitigations are required or if proposed mitigations are
   sufficient. When corrective actions cannot be implemented to
   eliminate or reduce the RPN below the defined threshold/action level,
   follow steps in the How-to-guide :ref:`How to innovation to reduce the
   risks of
   failure <si-review-fmea-and-next-steps>` to
   obtain specific actions for the system (e.g. proposed design review,
   enhanced material properties, measures implemented in other sectors).

.. _si-dive-deeper:

How to “Dive Deeper” into sub-studies of QFD/TRIZ
=================================================

The deployment of the QFD is a 4-phase process used to translate the
customer requirements into functional requirements, Part or Product
requirements, Process or Manufacturing phase, to Production/Quality
Control phase. The requirements of the system of interest are then
refined into specific requirements for the subsystems and components.
(More details about each phase can be obtained in `Background and
Theory- Phases of the House of Quality <si-house-of-quality>`).

In the Guide :ref:`What are the next steps for a QFD/TRIZ
analysis <si-qfd-next-steps>`, steps to select the functional
requirements for further assessment are explained once the user has
assessed the outputs. Once decided on the optimum functional
requirements, the following steps should be followed:

1) Select the specific functional requirements for further assessment
   from the drop-down list.

2) Start a new analysis with those requirements by clicking on “Dive
   Deeper”.

3) Define the name and objective of the analysis.

4) Specify the level of the Solution hierarchy for the analysis.

.. warning::

      By default, the solution hierarchy will enable suggestions
      at the next level (e.g. Level 2). The user can amend the level;
      however, this is discouraged as it may lead to the user not
      considering all the potential solutions for ideality, innovation and
      thoroughness. This might result in assessing only those solutions
      that they might come across quickly, leading to a lack of
      differentiation between products.

5) Follow :ref:`Tutorial 3 <si-tutorial-3>` to :ref:`Tutorial
   5 <si-tutorial-5>` to scan the design space, identify
   innovation areas, and obtain alternative solutions to
   contradictive requirements.

6) Assess the outputs as described in :ref:`QFD/TRIZ Next
   steps <si-qfd-next-steps>`

7) Repeat Steps 1-to-6 for further detailed assessment and/or mitigate
   the technical risks associated with the requirements as described
   in :ref:`Tutorial 6 <si-tutorial-6>`.