.. _si-identify-assess-innovation:

**********************************************************************
How to identify and assess innovation areas using the Stage Gate tool 
**********************************************************************

:ref:`Tutorial4 <si-tutorial-4>` and :ref:`Tutorial5 <si-tutorial-5>` showed
that the Structured Innovation tool could be used to identify areas of
innovation, encouraging the user to adopt principles that might offer
idealised solutions from other industries, time or scale. This guide
describes how the improvement areas identified in the Stage Gate tool
can be used as a basis of an improvement cycle.

In this case, the Structured Innovation tool will be deployed in
integrated mode with the Stage Gate design tool to allow the user to
assess the areas of the design that require improvements and then launch
the Structured Innovation tool for (one or more) improvement cycles.

.. _use-case-sg:
.. figure:: ../media/use-case-sg.svg

   Use Case of Stage Gate as input to Structured Innovation

To obtain improvement areas from the Stage Gate tool, you must do the
following:

1. Run the framework of the Stage Gate tool. The Deployment and
   Assessment tools may be used to provide design information and
   calculate key metrics, which are fed back into the Stage Gate design
   tool.

2. The Structured Innovation tool is triggered when the results of the
   stage gate assessment highlight specific Evaluation Areas that need
   to be improved.

3. Launch the Structured Innovation, and define the QFD/TRIZ study name
   and objective. Once defined, click next to define and prioritise the
   Customer requirements (see :ref:`Tutorial 3 <si-tutorial-3>`).

4. From the Functional Requirements page, add the requirements using
   data from the Stage Gate tool, click on an empty description field
   and select the *Data Sources icon* from the right-hand pane
   Information bar. Then select the required entry from the drop-down
   list(s) under the Stage Gate heading.

   .. note::
      On selection, applicable data will be added to a new functional
      requirement. This will reflect content already obtained in the Stage
      Gate, including the direction of improvement and the target values and
      units (referred to as threshold in the Stage Gate tool), Missing data
      will need to be added.
   
   .. note::
      To edit any functional requirement, change the relevant field. To
      delete, move the cursor over the relevant functional requirement and
      select the ‘bin’ icon.

5. The Structured Innovation tool will check that the needed information
   has been inputted; if not, it will request the user to input the
   information.

6. Select *Next* to proceed to the next step as described in :ref:`Tutorial
   4 <si-tutorial-4>` & :ref:`Tutorial 5 <si-tutorial-5>` to complement
   the information and then generate (one or more) innovative solutions
   using the SI tool.
