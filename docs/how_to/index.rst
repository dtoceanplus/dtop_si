.. _si-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on
achieving specific outcomes using the Structured Innovation tool. These
guides are intended for users who have previously completed all the
Structured Innovation tutorials and have a good knowledge of the
features and workings of the Structured Innovation tool. While the
tutorials introduce the basic usage of the tool, these *how-to guides*
tackle slightly more advanced topics.

1. :ref:`si-identify-assess-innovation`

2. :ref:`si-review-fmea-and-next-steps`

   -  How to innovate to reduce the risks of failure (from FMEA to
      QFD/TRIZ and vice versa)

3. :ref:`si-qfd-next-steps`

   -  How to mitigate technical risks associated with innovative
      solutions.

   -  How to “Dive deeper” into sub-studies of QFD/TRIZ
 
4. :ref:`si-data-requirements`

.. toctree::
   :maxdepth: 1
   :hidden:

   si-identify-assess-innovation
   si-review-fmea-and-next-steps
   si-qfd-next-steps
   si-data-requirements

Workflow
=========

The workflow for the SI module can be summarised as:

1. Provide of design objective 
2. Identify and assess areas of innovation 
3. Assess mitigations for failures 
4. Return potential areas of improvement/innovation.
    
   
This flow is shown in more detail in :numref:`si-data-flow`.
   
.. _si-data-flow:
.. figure:: ../media/data-flow.svg

   SI tool diagram showing functionalities/inputs/outputs

.. _software-routes:
.. figure:: ../media/software-routes.svg

   High-level software route for SI tool
   
