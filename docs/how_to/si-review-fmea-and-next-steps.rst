.. _si-review-fmea-and-next-steps:

*********************************************************
How to Review FMEA Achievements and Suggested Next Steps
*********************************************************

**Risk Mitigation using FMEA**

A concept or design failure mode and effect analysis ( CFMEA or DFMEA)
is a process designed to identify and evaluate potential design risks
introduced in a new or improved design. This process identifies the
effects and outcomes of these failures, eliminates or mitigates the
failures and provides a design mitigation plan when a solution exists.

As described in :ref:`Tutorial 6 <si-tutorial-6>` - Assessing technical risks, the criticalities
of failures are determined using the Risk Priority Number (RPN), which
is a product of the Severity, Occurrence, and Detection rankings
associated with each potential failure. A threshold RPN and occurrence
limit are set beyond which an intervention/mitigation is needed. This
enables prioritising risks and proposing suitable follow-up corrective
actions to reduce the criticality of potential failures. These
corrective actions can be in the form of implementing further design
controls for early risk identification (e.g. sensing) and protection
(e.g. device settings).

**Potential Next Steps**

When corrective actions cannot be implemented to eliminate or reduce the
RPN below the defined threshold/action level, alternative innovative
solutions are sought using the QFD/TRIZ process to obtain specific
actions for the system (e.g. proposed design review, enhanced material
properties, measures implemented in other sectors). A new QFD/TRIZ
analysis (or reviewing an existing one) can be launched from the SI Home
page to address these specific requirements and risks.

Finally, the detailed Design and Assessment tools in other modules of
the DTOcean+ suite can be used to develop detailed designs for the
concepts created in this Structured Innovation tool and to assess their
potential deployment to specific sites.

How to innovate to reduce the risks of failure
===============================================

A Concept or Design failure mode and effect analysis ( cFMEA or DFMEA)
is a process designed to identify and evaluate potential design
failures. This process identifies the effects and outcomes of these
failures, eliminates or mitigates the failures and provides a design
mitigation plan when a solution exists.

As described in :ref:`Tutorial 6 <si-tutorial-6>`, the criticalities of
failures are determined using the Risk Priority Number (RPN), which is a
product of the Severity, Occurrence, and Detection rankings associated
with each potential failure. A threshold RPN and occurrence limit are
set beyond which an intervention/mitigation is needed. This enables
prioritising risks and proposing suitable follow-up corrective actions
to reduce the criticality of potential failures.

When corrective actions cannot be obtained to eliminate or reduce the
RPN below the defined threshold/action level, alternative innovative
solutions are sought using the QFD/TRIZ process to obtain specific
actions for the system (e.g. proposed design review, enhanced material
properties, measures implemented in other sectors). A new QFD/TRIZ
analysis (or reviewing an existing one) can be launched from the SI Home
page to address these specific requirements and risks.

.. image:: ../media/howto2.svg

The steps are as follows:

1.  Review the mitigated RPNs (mRPN) and Occurrences (mOCC) to establish
    if all the functions are within the acceptable limits (i.e. below the
    specified RPN and OCC threshold). Note that the FMEA’s Report page
    presents the proposed mitigative actions for all actionable failure
    modes. (Steps are detailed in :ref:`Tutorial
    6 <si-tutorial-6>`).

2.  Further, mitigate all failure modes with mRPN and/or mOCC exceeding
    the threshold (as described in :ref:`Tutorial 6 <si-tutorial-6>`).

3.  If unable to reduce the failure modes within the acceptable limit,
    launch the QFD/TRIZ module to identify alternative functional
    requirements to meet the customer needs.

4.  On the QFD/TRIZ Home page, create a new study and specify the study
    objective. Details about selecting the solution hierarchy level can
    be found in the :ref:`Background section-Solution
    hierarchy <si-solhi>`.

5.  On the Customer requirement page, specify the design requirements
    requiring further mitigation by selecting the specific design
    requirements.
 
    .. note::

      The user must click within the description field to view the
      list of available data for the specific step. This list is shown in
      the Data Source tab in the right-hand pane information bar. In this
      case, the description suggested by the specific FMEA study relates to
      all the design requirements defined in the FMEA study. The user will
      be required only to select those requirements requiring further
      mitigation.

6.  Determine the importance of each identified designed requirement
    relative to others.

7.  Follow :ref:`Tutorial 4 <si-tutorial-4>` to define Functional
    Requirements that would meet the design requirements, including
    corresponding target values, Direction of Improvement (DoI),
    engineering difficulty and delivery difficulty.

8.  Define the Level of Impact between Requirements and Functional
    Requirements, and correlations between the functional requirements
    (See :ref:`Tutorial 4 <si-tutorial-4>`)

9.  Follow the steps in :ref:`Tutorial 5 <si-tutorial-5>` to identifying
    the attractive areas of innovation and assessing contradictive
    requirements using the TRIZ library.

10. The optimum functional requirements are those with the highest
    impact on meeting the requirements (in this case, the design
    requirements). Review the report to understand the impacts
    between functional requirements and reassess the technical risks
    (See :ref:`Tutorial 6 <si-tutorial-6>`) to ensure RPN and
    Occurrence are within acceptable limits.
