.. _si-dive-deeper:

How to “Dive Deeper” into sub-studies of QFD/TRIZ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The deployment of the QFD is a 4-phase process used to translate the
customer requirements into functional requirements, Part or Product
requirements, Process or Manufacturing phase, to Production/Quality
Control phase. The requirements of the system of interest are then
refined into specific requirements for the subsystems and components.
(More details about each phase can be obtained in `Background and
Theory- Phases of the House of Quality <si-house-of-quality>`).

In the Guide :ref:`What are the next steps for a QFD/TRIZ
analysis <si-qfd-next-steps>`, steps to select the functional
requirements for further assessment are explained once the user has
assessed the outputs. Once decided on the optimum functional
requirements, the following steps should be followed:

1) Select the specific functional requirements for further assessment
   from the drop-down list.

2) Start a new analysis with those requirements by clicking on “Dive
   Deeper”.

3) Define the name and objective of the analysis.

4) Specify the level of the Solution hierarchy for the analysis.

.. warning::

      By default, the solution hierarchy will enable suggestions
      at the next level (e.g. Level 2). The user can amend the level;
      however, this is discouraged as it may lead to the user not
      considering all the potential solutions for ideality, innovation and
      thoroughness. This might result in assessing only those solutions
      that they might come across quickly, leading to a lack of
      differentiation between products.

5) Follow :ref:`Tutorial 3 <si-tutorial-3>` to :ref:`Tutorial
   5 <si-tutorial-5>` to scan the design space, identify
   innovation areas, and obtain alternative solutions to
   contradictive requirements.

6) Assess the outputs as described in :ref:`QFD/TRIZ Next
   steps <si-qfd-next-steps>`

7) Repeat Steps 1-to-6 for further detailed assessment and/or mitigate
   the technical risks associated with the requirements as described
   in :ref:`Tutorial 6 <si-tutorial-6>`.