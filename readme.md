# DTOcean+ Structured Innovation Module

## Developers

* Install pre-commit (on your computer, and in the repository).
* `pip install requirements-dev.txt` will install dependencies for tests.
* Write and run unit tests: `pytest` and `npm run test:unit`
* Please use virtual environments for Python code.
* Lint and fix your code. For python we mostly use [Black's](black.readthedocs.io) style.
* Lines shouldn't be longer than 80 characters (72 for documentation/comments)

Linting
Fixing
Running tests (backend, frontend, integration, dredd...)

## Making changes

Branching
Rebasing / merging
Committing and pushing

### Merge requests
* We review each others code
* Even if you have the access rights, never merge your own commits without discussing it first.


### CI/CD

```mermaid
graph LR
    Trigger((Trigger)) --> prep
    prep[Prepare Images] -.-> pytest
    prep -.-> jest
    pytest --> pybuild[Build backend image]
    jest --> pybuild
    pybuild --> E2E[E2E tests]
    pybuild --> API[API tests]
    E2E --> Merge((Merge))
    API --> Merge((Merge))
```


The continuous integration pipeline will run on merge requests to master and commits to master (including merge commits).
This means the pipeline will run twice for a successful merge request, but it should be fairly quick.

Although users should be linting locally using pre-commit, there is a lint job in the pipeline as a failsafe. Please don't rely on it. The same applies to testing.

The pipeline users Docker images defined in the `docker` directory. The intention is that we are testing the same environment as will be used for deployment. The images will only be rebuilt when something changes that requires them to be, and even then a cache ensures the build is as efficient as possible.

When a commit is made directly to master, or a merge to master is successful, an additional job will be run to update the documentation website if necessary.

If the Dredd or Documentation jobs fail, and you haven't done anything to affect them, it could be that you don't have the right access to the repositories they depend on.

## Documenting

Sphinx is used for project documentation.

* Project documentation should follow the rules outlined [here](https://gitlab.com/wave-energy-scotland/dtoceanplus/dtop_documentation/-/blob/master/README.md).
* For Python code, we use [Google style docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html).
* For javascript and Vue we **should** be using jsdoc.
* The current documentation for master can be previewed at https://energysystemscatapult.gitlab.io/dtoceanplus/documentation/
