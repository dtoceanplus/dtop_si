# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from os.path import join
from pandas import read_excel
from re import sub


# Load contradiction matrix from spreadsheet
spreadsheet = (
    r"S:\Projects\ESC00213 DTOceanPlus\4. Deliver"
    r"\4.13 WP3 Structured Innovation\Tool Development_Excel"
    r"\Copy of Copy of QFD TRIZ development Heat Exchangers "
    r"Example of Component Development Version 1_4.xlsm"
)
df = read_excel(spreadsheet, "Contradiction Matrix", index_col=None, na_values="-")

# Remove unwanted rows and columns from worksheet to leave just the matrix
df.reset_index(1, True, True)
df = df.iloc[df.index.notna(), : df.isna().values.all(0).argmax()]
df.fillna("", inplace=True)

contradiction = df.applymap(
    lambda x: x if x == "+" else sub("[^\d]+", ", ", str(x)).strip(", ")
)
contradiction.index.name = u"\u2b10ImprovingFeature \ WorseningFeature\u2192"
contradiction.columns = contradiction.columns.str.replace("  ", " ")
contradiction.index = (
    contradiction.index.str.replace("  ", " ")
    .str.replace(
        "Duration of action by stationary object",
        "Duration of action of stationary object",
    )
    .str.replace("Loss of substance", "Loss of Substance")
    .str.replace("Quantity of substance/the matter", "Quantity of substance")
)
contradiction.to_csv(
    join("..", "src", "dtop_structinn", "pyqfd", "ContradictionMatrix.tsv"), "\t"
)

principles = read_excel(
    spreadsheet, "Inventive Principles", skiprows=2, index_col="Number"
)
principles.dropna(1, "all", inplace=True)
principles.columns = (
    principles.columns.str.lower()
    .str.split()
    .str[0]
    .str.replace("define", "definition")
)
principles.to_csv(
    join("..", "src", "dtop_structinn", "pyqfd", "InventivePrinciples.tsv"), "\t"
)
