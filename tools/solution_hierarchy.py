# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from os.path import join
from pandas import DataFrame, read_excel
from re import sub


# Load solutions hierarchy from spreadsheet
df = read_excel(
    "SI tool- What is implemented.xlsm",
    "Solution Hierachy-Full",
    header=None,
    usecols="C:G",
    skiprows=6,
)

sh = DataFrame(
    (
        [None] * i + [j.strip()]
        for _, r in df.dropna(0, "all").iterrows()
        for i, j in r.dropna().iteritems()
    )
)

with open(
    join("..", "src", "dtop_structinn", "pyqfd", "SolutionHierarchy.tsv"), "w"
) as f:
    f.write(sub("\t+\n", "\n", sh.to_csv(None, "\t", header=False, index=False)))
