# DTOcean+ Structured Innovation Module
# Copyright (C) 2021 Energy Systems Catapult
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import json
with open("tests/backend/megasplosher.json") as fd:
    ms = json.load(fd)

analysis = ms
requirements = ms.pop("requirements")
functional_requirements = ms.pop("functional_requirements")
solutions = ms.pop("solutions")

analysis.keys()
correlations = analysis.pop("correlations")

if 0 in (r["importance"] for r in requirements):
    # importance may no longer be 0, and is no longer a simple series (magnitude matters as well as order)
    # there was a version with this change and still using the old schema, so we check for a 0 in the
    # importances to decide what to do. Although naive, we assume that each item is twice as important as
    # the last when entered in the old style. Keep in mind also that in the original version, 0 was the
    # most important.
    for r in requirements:
        r["importance"] = 2 ** (-1 * (r["importance"] - len(requirements) + 1))


for fr in functional_requirements:
    # new style only stores the triz id
    fr["triz"] = [t["id"] for t in fr["triz"]]

impacts = []
for r in requirements:
    ips = r.pop("impacts")
    for i in ips:
        impacts.append({"functional_requirement_id": i["functional_requirement"]["id"], "requirement_id": r["id"], "impact": i["impact"]})

for c in correlations:
    fr1 = c.pop("functional_requirement")
    fr2 = c.pop("functional")
    c["functional_requirement_1_id"] = fr1["id"]
    c["functional_requirement_2_id"] = fr2["id"]

achievements = []
for s in solutions:
    achs = s.pop("achievements")
    for a in achs:
        achievements.append({
            "solution_id": s["id"],
            "functional_requirement_id": a["functional_requirement"]["id"],
            "value": a["value"]
            })

analysis["requirements"] = requirements
analysis["functional_requirements"] = functional_requirements
analysis["solutions"] = solutions
analysis["achievements"] = achievements
analysis["correlations"] = correlations
analysis["impacts"] = impacts

# analysis is now in "new" style, give or take.

r_ids = {r["id"]: None for r in analysis["requirements"]}
fr_ids = {fr["id"]: None for fr in analysis["functional_requirements"]}
sol_ids = {sol["id"]: None for sol in analysis["solutions"]}

# the following uses the REST API but in reality import should be done in one foul/fell swoop.

from requests import post

base_url = "http://127.0.0.1:5000"  # needs adjusting when the blueprint is set up properly

resp = post(f"{base_url}", json=analysis)
assert resp.status_code == 201
qfd = resp.json()["data"]["id"]

for r in requirements:
    id = r.pop("id")
    resp = post(f"{base_url}/{qfd}/requirements/", json=r)
    assert resp.status_code == 201  # not for production use! Asserts get stripped when optimised.
    r_ids[id] = resp.json()["data"]["id"]

for fr in functional_requirements:
    id = fr.pop("id")
    resp = post(f"{base_url}/{qfd}/functional_requirements/", json=fr)
    assert resp.status_code == 201  # not for production use! Asserts get stripped when optimised.
    fr_ids[id] = resp.json()["data"]["id"]

for s in solutions:
    id = s.pop("id")
    resp = post(f"{base_url}/{qfd}/solutions/", json=s)
    assert resp.status_code == 201  # not for production use! Asserts get stripped when optimised.
    sol_ids[id] = resp.json()["data"]["id"]

for c in correlations:
    c["functional_requirement_1_id"] = fr_ids[c["functional_requirement_1_id"]]
    c["functional_requirement_2_id"] = fr_ids[c["functional_requirement_2_id"]]
    resp = post(f"{base_url}/{qfd}/correlations/", json=c)
    assert resp.status_code == 201

for i in impacts:
    i["functional_requirement_id"] = fr_ids[i["functional_requirement_id"]]
    i["requirement_id"] = r_ids[i["requirement_id"]]
    resp = post(f"{base_url}/{qfd}/impacts/", json=i)
    assert resp.status_code == 201

for a in achievements:
    a["solution_id"] = sol_ids[a["solution_id"]]
    a["functional_requirement_id"] = fr_ids[a["functional_requirement_id"]]
    resp = post(f"{base_url}/{qfd}/achievements/", json=a)
    assert resp.status_code == 201
